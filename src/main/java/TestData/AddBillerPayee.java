package TestData;

public class AddBillerPayee {

    String billername = "sfggggeg";
    String billeraccnumber = "1475214521";

    String billeraccountname = "adadaddf";
    String billeraccaccnumber = "1236523698";


    public String getBillername() {
        return billername;
    }

    public void setBillername(String billername) {
        this.billername = billername;
    }

    public String getBilleraccnumber() {
        return billeraccnumber;
    }

    public void setBilleraccnumber(String billeraccnumber) {
        this.billeraccnumber = billeraccnumber;
    }

    public String getBilleraccountname() {
        return billeraccountname;
    }

    public void setBilleraccountname(String billeraccountname) {
        this.billeraccountname = billeraccountname;
    }

    public String getBilleraccaccnumber() {
        return billeraccaccnumber;
    }

    public void setBilleraccaccnumber(String billeraccaccnumber) {
        this.billeraccaccnumber = billeraccaccnumber;
    }


    String slippayeename = "sdsadasd";
    String slippayeenickname = "SFFSvvdvFSA";
    String slippayeeaccountnmber = "14774654654";

    public String getSlippayeename() {
        return slippayeename;
    }

    public void setSlippayeename(String slippayeename) {
        this.slippayeename = slippayeename;
    }

    public String getSlippayeenickname() {
        return slippayeenickname;
    }

    public void setSlippayeenickname(String slippayeenickname) {
        this.slippayeenickname = slippayeenickname;
    }

    public String getSlippayeeaccountnmber() {
        return slippayeeaccountnmber;
    }

    public void setSlippayeeaccountnmber(String slippayeeaccountnmber) {
        this.slippayeeaccountnmber = slippayeeaccountnmber;
    }

    String ceftpayeename = "adsad";
    String ceftpayeenickname = "dsdada";
    String ceftpayeeaccountnmber = "465464646";

    public String getCeftpayeename() {
        return ceftpayeename;
    }

    public void setCeftpayeename(String ceftpayeename) {
        this.ceftpayeename = ceftpayeename;
    }

    public String getCeftpayeenickname() {
        return ceftpayeenickname;
    }

    public void setCeftpayeenickname(String ceftpayeenickname) {
        this.ceftpayeenickname = ceftpayeenickname;
    }

    public String getCeftpayeeaccountnmber() {
        return ceftpayeeaccountnmber;
    }

    public void setCeftpayeeaccountnmber(String ceftpayeeaccountnmber) {
        this.ceftpayeeaccountnmber = ceftpayeeaccountnmber;
    }


    String ntbpayeename = "daddwre";
    String ntbpayeenickname = "ythgsefew";
    String ntbpayeeaccountnmber = "200790040228";

    public String getNtbpayeename() {
        return ntbpayeename;
    }

    public void setNtbpayeename(String ntbpayeename) {
        this.ntbpayeename = ntbpayeename;
    }

    public String getNtbpayeenickname() {
        return ntbpayeenickname;
    }

    public void setNtbpayeenickname(String ntbpayeenickname) {
        this.ntbpayeenickname = ntbpayeenickname;
    }

    public String getNtbpayeeaccountnmber() {
        return ntbpayeeaccountnmber;
    }

    public void setNtbpayeeaccountnmber(String ntbpayeeaccountnmber) {
        this.ntbpayeeaccountnmber = ntbpayeeaccountnmber;
    }

    String onetimebillername = "daadadsada";
    String onetimebillernameaccnumber = "1485123695";//10 numbers

    public String getOnetimebillername() {
        return onetimebillername;
    }

    public void setOnetimebillername(String onetimebillername) {
        this.onetimebillername = onetimebillername;
    }

    public String getOnetimebillernameaccnumber() {
        return onetimebillernameaccnumber;
    }

    public void setOnetimebillernameaccnumber(String onetimebillernameaccnumber) {
        this.onetimebillernameaccnumber = onetimebillernameaccnumber;
    }




    String onetimefundtransferpayeenamebelowotp = "dwfdgfer";
    String onetimefundtransferaccnumberbelowotp = "54498754";

    public String getOnetimefundtransferpayeenamebelowotp() {
        return onetimefundtransferpayeenamebelowotp;
    }

    public String getOnetimefundtransferaccnumberbelowotp() {
        return onetimefundtransferaccnumberbelowotp;
    }

    String newonetimefundtransferpayeename = "sumaann";
    String newonetimefundtransferaccnumber = "98755654";

    public String getNewonetimefundtransferpayeename() {
        return newonetimefundtransferpayeename;
    }

    public String getNewonetimefundtransferaccnumber() {
        return newonetimefundtransferaccnumber;
    }

    String onetimefundtransferpayeenameaboveotp = "fsfegg";
    String onetimefundtransferaccnumberaboveotp = "68645646546";

    public String getOnetimefundtransferpayeenameaboveotp() {
        return onetimefundtransferpayeenameaboveotp;
    }

    public String getOnetimefundtransferaccnumberaboveotp() {
        return onetimefundtransferaccnumberaboveotp;
    }

    String onetimefundtransfernicknameaboveotp = "fafvdgrre";

    public String getOnetimefundtransfernicknameaboveotp() {
        return onetimefundtransfernicknameaboveotp;
    }

    String nicknamefundtransferbyaddingpayee = "gomasha";
    String payeenamefundtransferbyaddingpayee = "mashani";
    String accnumberfundtransferbyaddingpayee = "35245464";

    public String getNicknamefundtransferbyaddingpayee() {
        return nicknamefundtransferbyaddingpayee;
    }

    public String getPayeenamefundtransferbyaddingpayee() {
        return payeenamefundtransferbyaddingpayee;
    }

    public String getAccnumberfundtransferbyaddingpayee() {
        return accnumberfundtransferbyaddingpayee;
    }
}
