package TestData;

public class InvalidLoginDetails {

    String validusername="BOTHTEN";
    String validpassword="Pasword@2";
    String invalidusername = "ACC99";
    String invalidpassword = "Paswor2";

    public String getValidusername() {
        return validusername;
    }

    public void setValidusername(String validusername) {
        this.validusername = validusername;
    }

    public String getValidpassword() {
        return validpassword;
    }

    public void setValidpassword(String validpassword) {
        this.validpassword = validpassword;
    }



    public String getInvalidusername() {
        return invalidusername;
    }

    public void setInvalidusername(String changeusername) {
        this.invalidusername = changeusername;
    }

    public String getInvalidpassword() {
        return invalidpassword;
    }

    public void setInvalidpassword(String changepassword) {
        this.invalidpassword = changepassword;
    }
}
