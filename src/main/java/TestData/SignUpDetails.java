package TestData;

public class SignUpDetails {

    String accountnumber = "029215035020";
    String nic = "950431016V";
    String dateofbirth = "12 February 1995";


    String username = "sdfee";
    String password = "Pasword@4";
    String confirmpassword = "Pasword@4";

    String existingusername = "SSDSD";

    String creditcardnumber = "1478523698741256";
    String mm = "02";
    String yy = "24";
    String cvv = "123";

    String debitcardnumber = "1478523698741256";
    String atmpin = "2555";



    public String getAccountnumber() {
        return accountnumber;
    }

    public String getNic() {
        return nic;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }
    String invalidaccountnumber = "999215035020";
    String invalidnic = "948512564V";
    String invaliddateofbirth = "12 October 1995";

    public String getInvalidaccountnumber() {
        return invalidaccountnumber;
    }

    public String getInvalidnic() {
        return invalidnic;
    }

    public String getInvaliddateofbirth() {
        return invaliddateofbirth;
    }



    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmpassword() {
        return confirmpassword;
    }


    public String getExistingusername() {
        return existingusername;
    }


    public String getCreditcardnumber() {
        return creditcardnumber;
    }

    public String getMm() {
        return mm;
    }

    public String getYy() {
        return yy;
    }

    public String getCvv() {
        return cvv;
    }

    public String getDebitcardnumber() {
        return debitcardnumber;
    }

    public String getAtmpin() {
        return atmpin;
    }
}
