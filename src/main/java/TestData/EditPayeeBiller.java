package TestData;

public class EditPayeeBiller {

    String editbillername = "Ackley";
    String editbilleraccnumber = "1478451236";//10 numbers

    String editbilleraccountname = "Madison";
    String editbilleraccaccnumber = "9856321478";//10 numbers

    public String getEditbillername() {
        return editbillername;
    }

    public void setEditbillername(String editbillername) {
        this.editbillername = editbillername;
    }

    public String getEditbilleraccnumber() {
        return editbilleraccnumber;
    }

    public void setEditbilleraccnumber(String editbilleraccnumber) {
        this.editbilleraccnumber = editbilleraccnumber;
    }

    public String getEditbilleraccountname() {
        return editbilleraccountname;
    }

    public void setEditbilleraccountname(String editbilleraccountname) {
        this.editbilleraccountname = editbilleraccountname;
    }

    public String getEditbilleraccaccnumber() {
        return editbilleraccaccnumber;
    }

    public void setEditbilleraccaccnumber(String editbilleraccaccnumber) {
        this.editbilleraccaccnumber = editbilleraccaccnumber;
    }

    String slipeditpayeename = "Addis";
    String slipeditpayeenickname = "Addison";
    String slipeditpayeeaccountnmber = "147889997";

    public String getSlipeditpayeename() {
        return slipeditpayeename;
    }

    public void setSlipeditpayeename(String slipeditpayeename) {
        this.slipeditpayeename = slipeditpayeename;
    }

    public String getSlipeditpayeenickname() {
        return slipeditpayeenickname;
    }

    public void setSlipeditpayeenickname(String slipeditpayeenickname) {
        this.slipeditpayeenickname = slipeditpayeenickname;
    }

    public String getSlipeditpayeeaccountnmber() {
        return slipeditpayeeaccountnmber;
    }

    public void setSlipeditpayeeaccountnmber(String slipeditpayeeaccountnmber) {
        this.slipeditpayeeaccountnmber = slipeditpayeeaccountnmber;
    }


    String cefteditpayeename = "Carter";
    String cefteditpayeenickname = "Hazel";
    String cefteditpayeeaccountnmber = "1889753232";

    public String getCefteditpayeename() {
        return cefteditpayeename;
    }

    public void setCefteditpayeename(String cefteditpayeename) {
        this.cefteditpayeename = cefteditpayeename;
    }

    public String getCefteditpayeenickname() {
        return cefteditpayeenickname;
    }

    public void setCefteditpayeenickname(String cefteditpayeenickname) {
        this.cefteditpayeenickname = cefteditpayeenickname;
    }

    public String getCefteditpayeeaccountnmber() {
        return cefteditpayeeaccountnmber;
    }

    public void setCefteditpayeeaccountnmber(String cefteditpayeeaccountnmber) {
        this.cefteditpayeeaccountnmber = cefteditpayeeaccountnmber;
    }

    String ntbeditpayeename = "Lily";
    String ntbeditpayeenickname = "Ellie";
    String ntbeditpayeeaccountnmber = "503212045153";

    public String getNtbeditpayeename() {
        return ntbeditpayeename;
    }

    public void setNtbeditpayeename(String ntbeditpayeename) {
        this.ntbeditpayeename = ntbeditpayeename;
    }

    public String getNtbeditpayeenickname() {
        return ntbeditpayeenickname;
    }

    public void setNtbeditpayeenickname(String ntbeditpayeenickname) {
        this.ntbeditpayeenickname = ntbeditpayeenickname;
    }

    public String getNtbeditpayeeaccountnmber() {
        return ntbeditpayeeaccountnmber;
    }

    public void setNtbeditpayeeaccountnmber(String ntbeditpayeeaccountnmber) {
        this.ntbeditpayeeaccountnmber = ntbeditpayeeaccountnmber;
    }
}
