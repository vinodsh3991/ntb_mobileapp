package TestData;

public class Dates {

    String year = "2018";
    String fromdate = "06 August 2018";
    String todate ="01 November 2018";

    public String getYear() {
        return year;
    }

    public String getFromdate() {
        return fromdate;
    }

    public String getTodate() {
        return todate;
    }
}
