package Activities.Notification;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class TransactionDetailsScreen extends Configuration {

    public static void  TransactionDetailsScreen_title() throws Exception{
        String screenheading = "Notifications";
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void TransactionDetailsScreen_labels() throws Exception{
        String LKR = "LKR";
        String PaidFrom = "Paid From";
        String PaidTo = "Paid To";
        String 	Date = "Date";
        String 	Remarks = "Remarks";


        try {

            String LKR1 =driver.findElementById("com.nationstrust.mobilebanking:id/currency").getText();
            Assert.assertEquals(LKR1, LKR);
            WebElement amountRupees =driver.findElementById("com.nationstrust.mobilebanking:id/amountRupees");
            amountRupees.isDisplayed();
            WebElement amountCents =driver.findElementById("com.nationstrust.mobilebanking:id/amountCents");
            amountCents.isDisplayed();
            String PaidFrom1 =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]").getText();
            Assert.assertEquals(PaidFrom1, PaidFrom);
            String PaidTo1 =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView[1]").getText();
            Assert.assertEquals(PaidTo1, PaidTo);
            String Date1 =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Date1, Date);
            String Remarks1 =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Remarks1, Remarks);
            WebElement PayFromnumber =driver.findElementById("com.nationstrust.mobilebanking:id/tvPayFrom");
            PayFromnumber.isDisplayed();
            WebElement  PayToNumber =driver.findElementById("com.nationstrust.mobilebanking:id/tvPayTo");
            PayToNumber.isDisplayed();
            WebElement tvDate =driver.findElementById("com.nationstrust.mobilebanking:id/tvDate");
            tvDate.isDisplayed();
            WebElement tvRemarks =driver.findElementById("com.nationstrust.mobilebanking:id/tvRemarks");
            tvRemarks.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void back(){
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    
}
