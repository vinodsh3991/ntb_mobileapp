package Activities.Notification;

import Activities.LongPress;
import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class TransactionScreen extends Configuration {


    public static void 	tapontransationnotification(){
        WebElement transactiontitle =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]");
        transactiontitle.isDisplayed();
        WebElement transactiondateandtime=driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]");
        transactiondateandtime.isDisplayed();
        WebElement selecttransaction = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout");
        selecttransaction.click();


    }


    public static void 	selectonenotification(){
        WebElement selectnotification =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout");
        LongPress.longpressselect(selectnotification);
        WebElement deleteicon =driver.findElementById("com.nationstrust.mobilebanking:id/delete");
        deleteicon.isDisplayed();
        WebElement checkbox =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.CheckBox");
        checkbox.isDisplayed();
        WebElement selectall =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView");
        selectall.isDisplayed();

        String selectalllabel = "Select All";
        String selectall1 =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
        Assert.assertEquals(selectall1,selectalllabel);

    }

    public static void 	selectallnotification(){
        WebElement selectnotification =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout");
        LongPress.longpressselect(selectnotification);
        WebElement checkbox =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.CheckBox");
        checkbox.click();

    }

    public static void 	deselectallnotification(){
        WebElement checkbox =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.CheckBox");
        checkbox.click();

    }

    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    public static void 	tapondelete(){
        WebElement tapondelete =driver.findElementById("com.nationstrust.mobilebanking:id/delete");
        tapondelete.click();

    }

    public static void 	offertab(){
        WebElement offertab =driver.findElementById("com.nationstrust.mobilebanking:id/llOffers");
        offertab.click();

    }

    public static void 	emptytransactionsscreen(){
        String noofferslabel= "You have read all";
        WebElement emptyoffersscreen =driver.findElementById("com.nationstrust.mobilebanking:id/empty_list");
        emptyoffersscreen.isDisplayed();
        WebElement bellicon =driver.findElementById("com.nationstrust.mobilebanking:id/imageView14");
        bellicon.isDisplayed();
        String noofferslabel1 =driver.findElementById("com.nationstrust.mobilebanking:id/tvEmptyDesc").getText();
        Assert.assertEquals(noofferslabel1,noofferslabel);

    }

}
