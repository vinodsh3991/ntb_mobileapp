package Activities.Notification;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class OfferDetailsScreen extends Configuration {

    public static void OfferDetailsScreen_labels() throws Exception{

        try {

            WebElement imagetitle =driver.findElementById("com.nationstrust.mobilebanking:id/tvImageTitle");
            imagetitle.isDisplayed();
            WebElement validdate =driver.findElementById("com.nationstrust.mobilebanking:id/tvDate");
            validdate.isDisplayed();
            WebElement bodytitle =driver.findElementById("com.nationstrust.mobilebanking:id/tvTitleBody");
            bodytitle.isDisplayed();
            WebElement bodydescription =driver.findElementById("com.nationstrust.mobilebanking:id/tvDesc");
            bodydescription.isDisplayed();
            WebElement callbutton =driver.findElementById("com.nationstrust.mobilebanking:id/button");
            callbutton.isDisplayed();
            WebElement moreinfor =driver.findElementById("com.nationstrust.mobilebanking:id/button2");
            moreinfor.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	getcall(){
        WebElement callbutton =driver.findElementById("com.nationstrust.mobilebanking:id/button");
        callbutton.click();

    }
    public static void 	moreinfor(){
        WebElement moreinfor =driver.findElementById("com.nationstrust.mobilebanking:id/button2");
        moreinfor.click();

    }


    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    
}
