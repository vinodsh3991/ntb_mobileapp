package Activities.Notification;

import Activities.LongPress;
import Activities.PopUpMessages.PopUpMessages;
import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class NotificationMainScreen extends Configuration {
    public static void  NotificationMainScreen_title() throws Exception{
        String screenheading = "Notifications";
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void NotificationMainScreen_labels() throws Exception{
        String Offers = "Offers";
        String 	Transactions ="Transactions";
        try {
            String Offers1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvOffers").getText();
            Assert.assertEquals(Offers1, Offers);
            String 	Transactions1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTransactions").getText();
            Assert.assertEquals(Transactions1,Transactions);
            WebElement offerimage =driver.findElementById("com.nationstrust.mobilebanking:id/ivOffers");
            offerimage.isDisplayed();
            WebElement transactionimage =driver.findElementById("com.nationstrust.mobilebanking:id/ivTransactions");
            transactionimage.isDisplayed();
            WebElement quicaccessmenu =driver.findElementById("com.nationstrust.mobilebanking:id/iv_menu");
            quicaccessmenu.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	taponoffer(){
        WebElement offertitle =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]");
        offertitle.isDisplayed();
        WebElement offerdateandtime=driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]");
        offerdateandtime.isDisplayed();
        WebElement selectoffer = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout");
        selectoffer.click();


    }


    public static void 	selectonenotification(){
        WebElement selectnotification =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout");

        LongPress.longpressselect(selectnotification);
        WebElement deleteicon =driver.findElementById("com.nationstrust.mobilebanking:id/delete");
        deleteicon.isDisplayed();
        WebElement checkbox =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.CheckBox");
        checkbox.isDisplayed();
        WebElement selectall =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView");
        selectall.isDisplayed();

        String selectalllabel = "Select All";
        String selectall1 =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
        Assert.assertEquals(selectall1,selectalllabel);

    }

    public static void 	selectallnotification(){
        WebElement selectnotification = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout");
        LongPress.longpressselect(selectnotification);

//        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
//                until(ExpectedConditions.elementToBeClickable(selectnotification));
//        new Actions(driver).clickAndHold(longpress).perform();
        WebElement checkbox =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.CheckBox");
        checkbox.click();

    }

    public static void 	deselectallnotification(){
        WebElement checkbox =driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.CheckBox");
        checkbox.click();

    }

    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    public static void 	tapondelete(){
        WebElement tapondelete =driver.findElementById("com.nationstrust.mobilebanking:id/delete");
        tapondelete.click();

    }

    public static void 	transactiontab(){
        WebElement transactiontab =driver.findElementById("com.nationstrust.mobilebanking:id/llTransactions");
        transactiontab.click();

    }

    public static void 	emptyoffersscreen(){
        String noofferslabel= "You have read all";
        WebElement emptyoffersscreen =driver.findElementById("com.nationstrust.mobilebanking:id/empty_list");
        emptyoffersscreen.isDisplayed();
        WebElement bellicon =driver.findElementById("com.nationstrust.mobilebanking:id/imageView14");
        bellicon.isDisplayed();
        String noofferslabel1 =driver.findElementById("com.nationstrust.mobilebanking:id/tvEmptyDesc").getText();
        Assert.assertEquals(noofferslabel1,noofferslabel);

    }

}
