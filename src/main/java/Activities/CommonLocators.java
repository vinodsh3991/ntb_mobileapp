package Activities;

import org.openqa.selenium.By;

import java.io.IOException;
import java.text.ParseException;

import static TestBase.Configuration.driver;

public class CommonLocators extends datepicker {

    public static void setAndroidDatePicker(String date) throws IOException, ParseException {
        int thisYear = Integer.valueOf(driver.findElement(By.id("android:id/date_picker_header_year")).getAttribute("name"));
        String today = driver.findElement(By.id("android:id/date_picker_header_date")).getAttribute("name");
        int thisMonth = getMonthNumber(getMonthNameInThreeChars(today));

        // Split the given date into date, month and year
        String[] splitdate = date.split("/");

        int givenDay = Integer.valueOf(splitdate[0]);
        int givenMonth = getMonthNumber(splitdate[1]);
        int givenYear = Integer.valueOf(splitdate[2]);

        int forwardTaps = 0;
        int backwardTaps = 0;
        int yearFactor = 0;

        if (givenYear == thisYear)
        {
            if (givenMonth >= thisMonth)
            {
                forwardTaps = givenMonth - thisMonth;
            } else {
                backwardTaps = thisMonth - givenMonth;
            }
        }
        else if (givenYear > thisYear)
        {
            yearFactor = (givenYear - thisYear) * 12;
            if (givenMonth >= thisMonth)
            {
                forwardTaps = yearFactor + (givenMonth - thisMonth);
            } else {
                forwardTaps = yearFactor - (thisMonth - givenMonth);
            }
        }
        else {
            yearFactor = (thisYear - givenYear) * 12;
            if (givenMonth >= thisMonth)
            {
                backwardTaps = yearFactor - (givenMonth - thisMonth);
            } else {
                backwardTaps = yearFactor + (thisMonth - givenMonth);
            }
        }


        for (int i=1; i<=forwardTaps; i++) {
            driver.findElementByXPath("// android.widget.ImageButton[@index='android:id/next']").click();
        }

        for (int i=1; i<=backwardTaps; i++) {
            driver.findElementByXPath("// android.widget.ImageButton[@index='android:id/prev']").click();
        }

        String xpath = "//android.view.View[@text='day']";
        driver.findElement(By.xpath(xpath.replace("day", String.valueOf(givenDay)))).click();
        //Tap on OK button of the date picker
        driver.findElement(By.id("android.widget.Button[@text='OK']")).click();

    }
    }

