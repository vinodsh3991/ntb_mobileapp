package Activities;

import TestBase.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class OpenNotification extends Configuration {

    public static void 	opennotifications(String pushtitle,String pushcontent) {
        driver.openNotifications();
        List<WebElement> allnotificationstitle = driver.findElements(By.id("android:id/title"));
        List<WebElement> allnotificationscontent = driver.findElements(By.id("android:id/text"));

        for (WebElement webElement : allnotificationstitle) {

            if (webElement.getText().contains(pushtitle)) {
                System.out.println(webElement.getText());
                WebElement content1 = driver.findElement(By.id("android:id/text"));
                content1.isDisplayed();
                break;
            }
        }

        for (WebElement webElement : allnotificationscontent) {
            if (webElement.getText().contains(pushcontent)) {
                System.out.println(webElement.getText());
                WebElement clear = driver.findElementById("com.android.systemui:id/clear_notification");
                clear.click();
                break;
            }
        }



    }
}
