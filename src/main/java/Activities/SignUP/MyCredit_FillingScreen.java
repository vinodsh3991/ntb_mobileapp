package Activities.SignUP;

import Activities.MonthCompare;
import TestBase.Configuration;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class MyCredit_FillingScreen extends Configuration {

    public static void MyCredit_FillingScreen_title() throws Exception{
        String screenheading = "Register";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void MyCredit_FillingScreen_labels() throws Exception{
        String CreditCardNumber = "Credit Card Number";
        String onlyprimary = "* Only Primary Credit cards and Traveler cards";
        String MM = "MM";
        String CVV = "CVV";
        String YY = "YY";
        String ValidThru = "Valid Thru";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String CreditCardNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/creditCardNumberET").getText();
            String CVV1 = driver.findElementById("com.nationstrust.mobilebanking:id/cvvTextET").getText();
            String onlyprimary1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            String MM1 = driver.findElementById("com.nationstrust.mobilebanking:id/validThruMM").getText();
            String YY1 = driver.findElementById("com.nationstrust.mobilebanking:id/validThruYY").getText();
            String ValidThru1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView7").getText();

            Assert.assertEquals(CreditCardNumber1, CreditCardNumber);
            Assert.assertEquals(CVV1, CVV);
            Assert.assertEquals(onlyprimary1, onlyprimary);
            Assert.assertEquals(MM1, MM);
            Assert.assertEquals(YY1, YY);
            Assert.assertEquals(ValidThru1, ValidThru);

            WebElement linebar = driver.findElementById("com.nationstrust.mobilebanking:id/imageView2");
            linebar.isDisplayed();
            WebElement frontimage = driver.findElementById("com.nationstrust.mobilebanking:id/cardFrontImage");
            frontimage.isDisplayed();
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void entercreditcardNumber(String cardnumber){
        WebElement entercreditcardNumber = driver.findElementById("com.nationstrust.mobilebanking:id/creditCardNumberET");
        entercreditcardNumber.sendKeys(cardnumber);
        driver.hideKeyboard();

    }
    public static void entermonth(String month){
        WebElement entermonth = driver.findElementById("com.nationstrust.mobilebanking:id/validThruMM");
        entermonth.sendKeys(month);
        driver.hideKeyboard();

    }

    public static void enteryear(String year){
        WebElement enteryear = driver.findElementById("com.nationstrust.mobilebanking:id/validThruYY");
        enteryear.sendKeys(year);
        driver.hideKeyboard();

    }
    public static void entercvv(String cvv){
        WebElement entercvv = driver.findElementById("com.nationstrust.mobilebanking:id/cvvTextET");
        entercvv.click();
        entercvv.sendKeys(cvv);
        driver.hideKeyboard();
    }
    public static void nextbutton(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement nextbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/nextButton");
        nextbutton1.click();

    }

    public static void backbutton(){
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']");
        backbutton.click();
    }





}
