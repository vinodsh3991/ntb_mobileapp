package Activities.SignUP;

import Activities.MonthCompare;
import TestBase.Configuration;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class MyAccount_FillingScreen extends Configuration {

    public static void MyAccount_FillingScreen_title() throws Exception{
        String screenheading = "Register";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void MyAccount_FillingScreen_labels() throws Exception{
        String nicplaceholder = "NIC/ Passport Number";
        String accountnumberplaceholder = "Account Number";
        String birtdayplaceholder = "Birthday";
        String nextbuttonname = "Next";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String nicplaceholder1 = driver.findElementById("com.nationstrust.mobilebanking:id/nic").getText();
            String accountnumberplaceholder1 = driver.findElementById("com.nationstrust.mobilebanking:id/accountNo").getText();
            String birtdayplaceholder1 = driver.findElementById("com.nationstrust.mobilebanking:id/birthday").getText();
            String nextbuttonname1 = driver.findElementById("com.nationstrust.mobilebanking:id/accNextButton").getText();

            Assert.assertEquals(nicplaceholder, nicplaceholder1);
            Assert.assertEquals(accountnumberplaceholder, accountnumberplaceholder1);
            Assert.assertEquals(birtdayplaceholder, birtdayplaceholder1);
            Assert.assertEquals(nextbuttonname, nextbuttonname1);

            WebElement linebar = driver.findElementById("com.nationstrust.mobilebanking:id/imageView2");
            linebar.isDisplayed();
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void enterMyAccountNumber(String accountnumber){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement myaccounttextfiled = driver.findElementById("com.nationstrust.mobilebanking:id/accountNo");
        myaccounttextfiled.sendKeys(accountnumber);
        driver.hideKeyboard();

    }
    public static void enterNIC(String nic){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement mycreditcardstextfiled = driver.findElementById("com.nationstrust.mobilebanking:id/nic");
        mycreditcardstextfiled.sendKeys(nic);
        driver.hideKeyboard();

    }
    public static void enterBirthday(String dateofbirth){
        WebElement mydebitcardstextfileds = driver.findElementById("com.nationstrust.mobilebanking:id/birthday");
        mydebitcardstextfileds.click();
        WebElement taponyear = driver.findElementById("android:id/date_picker_header_year");
        taponyear.click();

        ((FindsByAndroidUIAutomator<MobileElement>) driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+1995+"\").instance(0))").click();

        MonthCompare monthCompare =new MonthCompare();
        monthCompare.comparemonth(dateofbirth);



    }
    public static void nextbutton(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement nextbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/accNextButton");
        nextbutton1.click();

    }

    public static void backbutton(){
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']");
        backbutton.click();
    }





}
