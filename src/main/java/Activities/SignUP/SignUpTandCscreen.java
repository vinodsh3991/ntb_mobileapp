package Activities.SignUP;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SignUpTandCscreen extends Configuration {

    public static void SignUpTandCscreen_title() throws Exception{
        String screentitle = "Terms and Conditions";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void SignUpTandCscreen_labels() throws Exception{
        String screensubtitle = "Terms and Conditions";
        String tandccontent = "1. Nations Mobile Banking Service is for my/our personal use only. \n" +
                "\n" +
                " 2. I/We shall register with the Bank my/our mobile device/s by disclosing my/our mobile phone number/s and/or any other information the Bank may require for the provision of the Nations Mobile Banking Service to me/us. \n" +
                "\n" +
                " 3. I/We shall follow all security features advised by the Bank and terms and conditions in relation to all savings/current accounts and/or credit cards and/or debit cards and mobile device/s which I/ we may use to register for the Nations Mobile Banking Service including the PIN or passwords relating thereto. \n" +
                "\n" +
                " All mobile devices which I/we may use to access the Nations Mobile Banking Service (“Device/s”) shall be kept in my/our sole possession and custody. I/We shall not provide any third party with access to such Device/s and/or the Nations Mobile Banking Service. . If such access is given, it shall be at my/our sole risk and responsibility. I / We hereby further agree and undertake to inform the Bank immediately in the event such cards, Device/s and/or PIN / passwords are stolen, lost or comes into the possession of third parties. \n" +
                "\n" +
                " 4. I/we shall keep the mobile Personal Identity Number/s (MPIN) for Mobile Banking Services (MPIN) strictly confidential and undertake not to reveal such MPIN to any person at anytime or under any circumstance. MPIN shall mean the original PIN confidentially generated for me/us by the Bank and any substitution effected by me/us thereon or the PIN generated solely by me/us at the time of registering for the Nations Mobile Banking Service. \n" +
                "\n" +
                " 5. In the event I/we agree to login to Nations Mobile Banking Service using biometrics i.e. using facial measurements, finger print or voice, I/we authorize the Bank to save the facial measurements and the voice. However, I/we hereby acknowledge that the fingerprint will not be saved by the Bank but shall be saved on the mobile phone unit/s registered for Nations Mobile Banking Service. Therefore, I/we agree and acknowledge that I/we understand the importance of keeping the mobile phone unit/s with me/us at all times and not allow third party to access my mobile phone unit/s for what so ever reason. \n" +
                "\n" +
                " 6. In the event I/we change my/our mobile phone number/s and/or any Device registered with the Bank and/or lose or damage such registered Device and/ or SIM/s and/ or if my/our MPIN falls into the hands of any unauthorized person and/or if my/our mobile phone number stops functioning for any reason, I/we shall inform the Bank immediately by calling the Call Center on - (011) 4711411 and the Bank shall thereupon have the right to disable the Nations Mobile Banking Service. \n" +
                "\n" +
                " 7. The Bank shall be entitled to assume that any instructions received by the Bank through my/our registered Device/s and/or mobile phone number/s and/or through the use of my/our MPIN have been originated by me/us and shall be entitled to act thereon without any further inquiry. \n" +
                "\n" +
                " 8. The Bank shall not be liable or responsible for any losses, damages, expenses or detriment suffered or incurred by me/us as a result of the Bank acting upon instructions received through my/our registered Device/s and/or mobile phone number/s and/or through the use of my/our MPIN, even if such instructions may not have originated from me/us. \n" +
                "\n" +
                " 9. I/we hereby authorize the Bank to debit my/our account and/or my/our relevant card account where applicable with the amount of any transaction (including all charges relating thereto (as per the Bank’s published tariff and statutory (if any)) effected through Nations Mobile Banking Service (the card account to be debited only in respect of transactions effected on the card) with the use of registered Device/s and/or mobile phone number/s and/or MPIN whether with or without my/our knowledge or authority. \n" +
                "\n" +
                " 10. At no time shall I/we use or attempt to use the Nations Mobile Banking Service for effecting transactions unless sufficient funds are available in my/our account for carrying out such transactions. I/we agree that the Bank is under no obligation to honour my/our payment instructions unless there are sufficient funds in my/our designated account at the time of receiving my/our payment instructions. \n" +
                "\n" +
                " 11. I/We understand and accept that transmission of data via the Nations Mobile Banking Service cannot be guaranteed to be error free due to the inherent nature of electronic transmissions. I/We accept and agree that this service is provided on an “As is” and “As Available” basis and the Bank will not be liable or responsible for any losses, damages or inconvenience which I/we may incur or suffer by using this service including due to interruptions or non availability of the service at any time for any reason whatsoever or due to loss of data, data transmission errors or corruption that may occur in the use of the service or for any other reason whatsoever. \n" +
                "\n" +
                " 12. I/We accept and agree that the Bank will not be responsible or liable for non availability of the Nations Mobile Banking Service due to any technical or other defect in the registered Devices/s and or disconnection of the registered mobile number/s s for whatsoever reason which will automatically disable me/us from using this service. \n" +
                "\n" +
                " 13. I/We understand and agree that third party fund transfers maybe effected under Nations Mobile Banking Service only subject to conditions as maybe imposed by the Bank from time to time. \n" +
                "\n" +
                " 14. The Nations Mobile Banking Service will be available on all bank accounts maintained at the Bank, whether now opened or opened in the future. However I/we understand and agree that the Nations Mobile Banking Service may be restricted to certain types of accounts and account operations at the sole discretion of the Bank of which the Bank may inform me/us from time to time. \n" +
                "\n" +
                " 15. The use of Nations Mobile Banking Service shall be subject to the Bank’s prevailing General Business Conditions. \n" +
                "\n" +
                " 16. I/We confirm and assure the Bank that all data transmitted to the Bank including those for or in connection to Nations Mobile Banking Service is accurate complete conclusive and binding on me/us and I/we shall let the Bank know immediately of any errors, discrepancies or omissions which may have occurred inadvertently or otherwise. \n" +
                "\n" +
                " 17. I/We accept the Bank’s records and statements of all transactions processed under the Nations Mobile Banking Service as correct conclusive and binding on me/us. \n" +
                "\n" +
                " 18. The Bank shall have the right to determine the privileges features services and conditions attached to the use of Nations Mobile Banking Service and shall have the absolute discretion to change withdraw vary or amend these privileges, features, services and conditions at any time or from time to time or to withdraw this service altogether as the Bank deems fit. \n" +
                "\n" +
                " 19. If my/our bank account is a joint bank account I am/we are inter-alia jointly and severally bound by these terms and conditions and are jointly and severally liable for all transactions processed by the use of the Nations Mobile Banking Service irrespective of the instructions having been initiated by one party to the account. . Furthermore, until and unless all the relevant signatories to the joint bank account authorize the transaction on Nations Mobile Banking Service, such transaction shall not be processed. \n" +
                "\n" +
                " 20. The Bank reserves the right to amend, vary, add or delete any of these terms and conditions from time to time and the same shall be binding on me/us. \n" +
                "\n" +
                " 21. The Bank shall have the right at any time to refuse to carry out any instruction/s given by me/us if reasons exist, which in the opinion of the Bank justifies such action. The Bank shall also be entitled at its absolute discretion to discontinue or prevent me/us from using the Nations Mobile Banking Service, service if reasons exist which in the opinion of the Bank justifies such action. \n" +
                "\n" +
                " 22. I/We hereby acknowledge that any applications made by me/us for banking facilities do not guarantee that I/we shall be granted such facilities. Banking facilities shall be provided to me/us subject to credit verifications and solely at the discretion of the Bank. \n" +
                "\n" +
                " 23. In consideration of the Bank providing me/us the Nations Mobile Banking Service, I/we undertake to indemnify and keep indemnified the Bank at all times against and saved harmless from all actions, proceedings, claims, losses, damages, costs and expenses which may be brought or made against the Bank or suffered or incurred by the Bank directly or indirectly out of or in connection with the Bank providing me/us with the Nations Mobile Banking Service upon my/our instructions and/or the Bank acting thereon and/or communicating with me/us in accordance therewith. \n" +
                "\n" +
                " 24. All charges, fees, fines relating to the provision of the Nations Mobile Banking Service are subject to change at the Bank’s sole and absolute discretion. Details of all fees, fines and charges applicable to this service, if any, are stated in the Tariff booklet published by the Bank, copies of which will be made available on request at any of the Bank’s branches in Sri Lanka. \n" +
                "\n" +
                " 25. Any notice hereunder maybe given to me/us by the Bank by way of a narrative in or enclosed with any periodic statement or by publication of such notice in one or more newspapers published in Sri Lanka in all three languages or by publication on the notice boards at the Bank’s branches or in any other manner deemed suitable by the Bank. \n" +
                "\n" +
                " 26. The Bank has the right to report suspicious transactions to the Financial Intelligence Unit established under the Financial Transactions Reporting Act No.6 of 2006 and any other law enforcement authorities and other regulators as the case may be. \n" +
                "\n" +
                " 27. In the event I/we wish to terminate Nations Mobile Banking Service I/we shall give 7 days prior notice in writing to the Bank requesting termination of the Nations Mobile Banking Service. \n" +
                "\n" +
                " 28. I/we understand that any complaints and or disputes with regard to any transaction or related matter with regard to Nations Mobile Banking Service maybe communicated to the Bank in the following manner: \n" +
                "\n" +
                " Phone/Fax: (+94)11 4711411 \n" +
                " Email: customerservice@nationstrust.com \n" +
                "\n" +
                " 29. The acceptance of these terms and conditions overrides any decision taken by me/us to opt out of the Nations Mobile Banking Service on a previous occasion. \n" +
                "\n" +
                " 30. These terms and conditions shall be governed by and construed in accordance with the laws of Sri Lanka. \n";


        String Accept = "Accept";
        String Decline = "Decline";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screensubtitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTNCHeader").getText();
            Assert.assertEquals(screensubtitle1, screensubtitle);
            String tandccontent1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTermsNConditions").getText();
            Assert.assertEquals(tandccontent1, tandccontent);
            String Accept1 = driver.findElementById("com.nationstrust.mobilebanking:id/termsAcceptButton").getText();
            Assert.assertEquals(Accept1, Accept);
            String Decline1 = driver.findElementById("com.nationstrust.mobilebanking:id/termsDeclineButton").getText();
            Assert.assertEquals(Decline1, Decline);
            WebElement linebar = driver.findElementById("com.nationstrust.mobilebanking:id/imageView2");
            linebar.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void acceptbutton(){
        WebElement acceptbutton = driver.findElementById("com.nationstrust.mobilebanking:id/termsAcceptButton");
        acceptbutton.click();
    }
    public static void declinebutton(){
        WebElement declinebutton = driver.findElementById("com.nationstrust.mobilebanking:id/termsDeclineButton");
        declinebutton.click();
    }
    public static void backbutton(){
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']");
        backbutton.click();
    }

    public static void tickbox(){
        String tickboxlabel ="I have read and understood terms and conditions";
        MobileElement scrolltoendpage = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"I have read and understood terms and conditions\"));");
        String tickboxlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/acceptDescription").getText();
        Assert.assertEquals(tickboxlabel1, tickboxlabel);
        WebElement tickbox = driver.findElementById("com.nationstrust.mobilebanking:id/acceptCheckBox");
        tickbox.click();
    }


}
