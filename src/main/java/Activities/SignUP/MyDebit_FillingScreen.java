package Activities.SignUP;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class MyDebit_FillingScreen extends Configuration {

    public static void MyDebit_FillingScreen_title() throws Exception{
        String screenheading = "Register";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void MyDebit_FillingScreen_labels() throws Exception{
        String DebitCardNumber = "Debit Card Number";
        String ATMPIN = "ATM PIN";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String DebitCardNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/creditCardNumberET").getText();
            String ATMPIN1 = driver.findElementById("com.nationstrust.mobilebanking:id/atmPinTextET").getText();

            Assert.assertEquals(DebitCardNumber1, DebitCardNumber);
            Assert.assertEquals(ATMPIN1, ATMPIN);

            WebElement linebar = driver.findElementById("com.nationstrust.mobilebanking:id/imageView2");
            linebar.isDisplayed();
            WebElement frontimage = driver.findElementById("com.nationstrust.mobilebanking:id/cardLayout");
            frontimage.isDisplayed();
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void enterdebitcardNumber(String cardnumber){
        WebElement enterdebitcardNumber = driver.findElementById("com.nationstrust.mobilebanking:id/creditCardNumberET");
        enterdebitcardNumber.sendKeys(cardnumber);
        driver.hideKeyboard();

    }
    public static void enteratmpin(String atmpin){
        WebElement enteratmpin = driver.findElementById("com.nationstrust.mobilebanking:id/atmPinTextET");
        enteratmpin.sendKeys(atmpin);
        driver.hideKeyboard();

    }

    public static void nextbutton(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement nextbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/nextButton");
        nextbutton1.click();

    }

    public static void backbutton(){
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']");
        backbutton.click();
    }





}
