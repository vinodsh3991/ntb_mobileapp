package Activities.SignUP;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SignUpConfigureBiometricsScreen extends Configuration {

    public static void signUpConfigureBiometricsScreenn_title() throws Exception{
        String screentitle = "Configure Biometrics";

        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);

    }

    public static void signUpConfigureBiometricsScreenn_labels() throws Exception{
        String skipbutton = "Skip";
        String description = "It's recommended to setup additional security measures using biometric options. You can change these later in settings.";
        String fingerprint = "Fingerprint";
        try {
            String skipbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/skipBioMetric").getText();
            String description1 = driver.findElementById("com.nationstrust.mobilebanking:id/eStatementDescp").getText();
            String fingerprint1 = driver.findElementById("com.nationstrust.mobilebanking:id/touchIDText").getText();
            Assert.assertEquals(skipbutton1, skipbutton);
            Assert.assertEquals(description1, description);
            Assert.assertEquals(fingerprint1, fingerprint);

            WebElement toggle = driver.findElementById("com.nationstrust.mobilebanking:id/switchFingerPrint");
            toggle.isDisplayed();
            WebElement inforicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ImageView");
            inforicon.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);

    }
    public static void togglebutton(){
        WebElement togglebutton = driver.findElementById("com.nationstrust.mobilebanking:id/switchFingerPrint");
        togglebutton.click();
    }
    public static void skipbutton(){
        WebElement skipbutton = driver.findElementById("com.nationstrust.mobilebanking:id/skipBioMetric");
        skipbutton.click();
    }
    public static void quickaccessbutton(){
        WebElement quickaccessbutton = driver.findElementById("com.nationstrust.mobilebanking:id/faq_menu");
        quickaccessbutton.click();
    }

}
