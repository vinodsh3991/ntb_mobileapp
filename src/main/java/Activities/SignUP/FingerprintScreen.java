package Activities.SignUP;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class FingerprintScreen extends Configuration {

    public static void FingerprintScreen_title() throws Exception{
        String screentitle = "Fingerprint";

        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);

    }

    public static void FingerprintScreen_labels() throws Exception{
        String Configure = "Configure Your Fingerprint";
        String Place = "Place your finger on the fingerprint scanner";
        try {
            String Configure1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricDescription").getText();
            String Place1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricInstruction").getText();
            Assert.assertEquals(Configure1, Configure);
            Assert.assertEquals(Place1, Place);


            WebElement fingericon = driver.findElementById("com.nationstrust.mobilebanking:id/biometricIImage");
            fingericon.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);

    }

    public static void back(){
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
}
