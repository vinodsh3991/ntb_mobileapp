package Activities.SignUP;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class RegisterScreen extends Configuration {
    public static void registerScreen_title() throws Exception{
        String screentitle = "Register";

        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);

    }

    public static void registerScreen_labels() throws Exception{
            String Username = "Username";
            String Password = "Password";
            String ConfirmPassword = "Confirm Password";
            String Inorder = "In order to keep your valuable information safe, we encourage you to use a strong password.\n" +
                    "Your password must meet the following minimum requirements,";
            String registerbutton ="Register";
            String cancelbutton ="Cancel";
            String use ="Use at least 1 each from the following";
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            try {
                String Username1 = driver.findElementById("com.nationstrust.mobilebanking:id/username").getText();
                String Password1 = driver.findElementById("com.nationstrust.mobilebanking:id/pin").getText();
                String ConfirmPassword1 = driver.findElementById("com.nationstrust.mobilebanking:id/confirmPin").getText();
                String Inorder1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPwPolicyInfo1").getText();
                String registerbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/registerButton").getText();
                String cancelbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/cancelButton").getText();
                Assert.assertEquals(Username1, Username);
                Assert.assertEquals(Password1, Password);
                Assert.assertEquals(ConfirmPassword1, ConfirmPassword);
                Assert.assertEquals(Inorder1, Inorder);
                Assert.assertEquals(registerbutton1, registerbutton);
                Assert.assertEquals(cancelbutton1, cancelbutton);

                WebElement linebar = driver.findElementById("com.nationstrust.mobilebanking:id/imageView2");
                linebar.isDisplayed();
                MobileElement scrolltoendpage = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"cannot be used.\"));");

                String use1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPwPolicyUseFollow").getText();
                Assert.assertEquals(use1, use);
                MobileElement scrolltotoppage = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Username\"));");

            }
            catch(Exception ex){
                System.out.println("Cause: "+ex.getCause());
                System.out.println("Message: "+ex.getMessage());
                ex.printStackTrace();
            }
            Thread.sleep(3000);

    }
    public static void backbutton(){
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']");
        backbutton.click();
    }

    public static void registerbutton(){
        WebElement registerbutton = driver.findElementById("com.nationstrust.mobilebanking:id/registerButton");
        registerbutton.click();
    }
    public static void cancelbutton(){
        WebElement cancelbutton = driver.findElementById("com.nationstrust.mobilebanking:id/cancelButton");
        cancelbutton.click();
    }
    public static void eneterusername(String username ) {
        WebElement eneterusername = driver.findElementById("com.nationstrust.mobilebanking:id/username");
        eneterusername.sendKeys(username);
        driver.hideKeyboard();
    }
    public static void eneterpassword(String password ) {
        WebElement eneterpassword = driver.findElementById("com.nationstrust.mobilebanking:id/pin");
        eneterpassword.sendKeys(password);
        driver.hideKeyboard();
    }
    public static void eneterconfirmpassword(String confirmpassword ) {
        WebElement eneterconfirmpassword = driver.findElementById("com.nationstrust.mobilebanking:id/confirmPin");
        eneterconfirmpassword.sendKeys(confirmpassword);
    }

}
