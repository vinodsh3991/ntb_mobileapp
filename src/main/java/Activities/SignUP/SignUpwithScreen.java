package Activities.SignUP;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SignUpwithScreen extends Configuration {

    public static void SignupwithScreen_labels() throws Exception{
        String screenheading = "Sign Up With";
        String accountbutton = "My Accounts";
        String creditbutton = "My Credit Cards";
        String debitbutton = "My Debit Cards";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/welcomeText").getText();
            String accountbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/myAccountsButton").getText();
            String creditbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/myCardsButton").getText();
            String debitbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/myDebitCardsButton").getText();


            Assert.assertEquals(screenheading, screenheading1);
            Assert.assertEquals(accountbutton, accountbutton1);
            Assert.assertEquals(creditbutton, creditbutton1);
            Assert.assertEquals(debitbutton, debitbutton1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void MyAccounts(){
        WebElement myaccountbutton = driver.findElementById("com.nationstrust.mobilebanking:id/myAccountsButton");
        myaccountbutton.click();

    }
    public static void MyCreditCards(){
        WebElement mycreditcards = driver.findElementById("com.nationstrust.mobilebanking:id/myCardsButton");
        mycreditcards.click();

    }
    public static void MyDebitCards(){
        WebElement mydebitcards = driver.findElementById("com.nationstrust.mobilebanking:id/myDebitCardsButton");
        mydebitcards.click();

    }

    public static void back(){
        WebElement back = driver.findElementById("com.nationstrust.mobilebanking:id/backNavigation");
        back.click();

    }


}
