package Activities.Setting;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ProfileSettingScreen extends Configuration {
    public static void ProfileSettingScreen_title() throws Exception{
        String screenheading = "Profile Settings";
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void ProfileSettingScreen_labels() throws Exception{
        String Edit = "Edit";
        String ChangeContactDetails = "Change Contact Details";
        String SocialMedia = "Social Media";
        String fb = "Connect with Facebook";
        String insta = "Connect with Instagram";
        String twt = "Connect with Twitter";
        String link = "Connect with LinkedIn";

        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Edit1 = driver.findElementById("com.nationstrust.mobilebanking:id/editLable").getText();
            Assert.assertEquals(Edit1, Edit);
            String ChangeContactDetails1 = driver.findElementById("com.nationstrust.mobilebanking:id/changeContactDetails").getText();
            Assert.assertEquals(ChangeContactDetails1, ChangeContactDetails);
            String SocialMedia1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();
            Assert.assertEquals(SocialMedia1, SocialMedia);
            WebElement toggle = driver.findElementById("com.nationstrust.mobilebanking:id/imageView4");
            toggle.isDisplayed();
            WebElement arrow = driver.findElementById("com.nationstrust.mobilebanking:id/imageView7");
            arrow.isDisplayed();
            WebElement image = driver.findElementById("com.nationstrust.mobilebanking:id/imageView");
            image.isDisplayed();
            String fb1 = driver.findElementById("com.nationstrust.mobilebanking:id/fbProfileText").getText();
            Assert.assertEquals(fb1, fb);
            String insta1 = driver.findElementById("com.nationstrust.mobilebanking:id/instaProfileText").getText();
            Assert.assertEquals(insta1, insta);
            String twt1 = driver.findElementById("com.nationstrust.mobilebanking:id/twitterProfileText").getText();
            Assert.assertEquals(twt1, twt);
            String link1 = driver.findElementById("com.nationstrust.mobilebanking:id/liProfileText").getText();
            Assert.assertEquals(link1, link);
            WebElement fbImageView = driver.findElementById("com.nationstrust.mobilebanking:id/fbImageView");
            fbImageView.isDisplayed();
            WebElement instaImageView = driver.findElementById("com.nationstrust.mobilebanking:id/instaImageView");
            instaImageView.isDisplayed();
            WebElement twitterImageView = driver.findElementById("com.nationstrust.mobilebanking:id/twitterImageView");
            twitterImageView.isDisplayed();
            WebElement liImageView = driver.findElementById("com.nationstrust.mobilebanking:id/liImageView");
            liImageView.isDisplayed();



        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void edit(){
        WebElement edit = driver.findElementById("com.nationstrust.mobilebanking:id/editLable");
        edit.click();
        WebElement allow = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
        allow.click();

    }

    public static void takepicture(){
        WebElement takepicture = driver.findElementById("com.huawei.camera:id/shutter_button");
        takepicture.click();
        WebElement addpicture = driver.findElementById("com.huawei.camera:id/btn_review_confirm");
        addpicture.click();
    }

    public static void 	changecontactdetails(){
        WebElement changecontactdetails = driver.findElementById("com.nationstrust.mobilebanking:id/imageView7");
        changecontactdetails.click();

    }
  

    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}

