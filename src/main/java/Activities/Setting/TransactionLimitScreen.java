package Activities.Setting;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TransactionLimitScreen extends Configuration {
    public static void TransactionLimit_title() throws Exception{
        String screenheading = "Transaction Limits";
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void TransactionLimit_labels() throws Exception{
        String label = "Transfers to Own Nations accounts";
        String label2 = "Transfers to Third Party Nations Accounts";
        String label3 = "Transfers to Other Banks";
        String label4 = "Card Payments to Own NTB Accounts";
        String label5 = "Card Payments to Third Party within NTB";
        String label6 = "Card Payments to Other Banks";
        String label7 = "Bill Payments";

        String label8 = "Change limit";
        String label9 = "LKR";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String label1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label, label1);
            String label66 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label6, label66);
            String label33 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label3, label33);
            String label55 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label5, label55);

            //scrol the page to end
            MobileElement pageend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Bill Payments\"));");
            String label22 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label2, label22);
            String label44 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label4, label44);

            String label77 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label7, label77);

            String label88 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(label8, label88);
            String label99 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[2]").getText();
            Assert.assertEquals(label9, label99);

            MobileElement pageup = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Transfers to Own Nations accounts\"));");



        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void save(){
        MobileElement pageend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Bill Payments\"));");

        WebElement save = driver.findElementById("com.nationstrust.mobilebanking:id/btnSave");
        save.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void 	cancel(){
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/tvCancel");
        cancel.click();

    }
    public static void 	amount(String enteredamount){
        WebElement amount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.EditText");
        amount.click();
        amount.clear();
        amount.sendKeys(enteredamount);
        driver.hideKeyboard();

    }

    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
//    public static void 	opennotifications() {
//        driver.openNotifications();
//        List<WebElement> allnotificationstitle = driver.findElements(By.id("android:id/title"));
//        List<WebElement> allnotificationscontent = driver.findElements(By.id("android:id/text"));
//
//        for (WebElement webElement : allnotificationstitle) {
//            System.out.println(webElement.getText());
//            if (webElement.getText().contains("NTB Mobile banking")) {
//                WebElement content1 = driver.findElement(By.id("android:id/text"));
//                content1.isDisplayed();
//                break;
//            }
//        }
//
//        for (WebElement webElement : allnotificationscontent) {
//            System.out.println(webElement.getText());
//            if (webElement.getText().contains("You have successfully changed your Nations Mobile Banking Transfers to other banks Limit from")) {
//                WebElement clear = driver.findElementById("com.android.systemui:id/clear_notification");
//                clear.click();
//                break;
//            }
//        }
//
//
//
//    }
}

