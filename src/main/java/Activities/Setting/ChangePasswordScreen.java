package Activities.Setting;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ChangePasswordScreen extends Configuration {
    public static void  ChangePasswordScreen_title() throws Exception{
        String screenheading = "Change Password";
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  ChangePasswordScreen_labels() throws Exception{
        String 	policydescription = "In order to keep your valuable information safe, we encourage you to use a strong password.\n" +
                "Your password must meet the following minimum requirements,";


       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            driver.hideKeyboard();
            String policydescription1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPwPolicyInfo1").getText();
            Assert.assertEquals(policydescription, policydescription1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void scrolltoend(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement scrolltoend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Use at least 1 each from the following\"));");
    }
    public static void scrolltostart(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement scrolltostart = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Current Password\"));");
    }

    public static void 	changebutton(){
        WebElement changebutton = driver.findElementById("com.nationstrust.mobilebanking:id/btnDone");
        changebutton.click();
    }
    public static void 	enternewpassword(String newpassword){
        String NewPINlabel="New Password";
        String NewPINlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/newPIN").getText();
        Assert.assertEquals(NewPINlabel, NewPINlabel1);
        WebElement NewPIN = driver.findElementById("com.nationstrust.mobilebanking:id/etNewPIN");
        NewPIN.click();
        NewPIN.sendKeys(newpassword);
        driver.hideKeyboard();
    }
    public static void 	entercurrentpassword(String currentpassword){
        String currentPINlabel="Current Password";
        String currentPINlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/currentPIN").getText();
        Assert.assertEquals(currentPINlabel, currentPINlabel1);
        WebElement currentPIN = driver.findElementById("com.nationstrust.mobilebanking:id/etCurrentPIN");
        currentPIN.sendKeys(currentpassword);
        driver.hideKeyboard();
    }
    public static void 	enterconfirmpassword(String confirmnewpassword){
        String ConfirmcurrentPINlabel="Confirm New Password";
        String ConfirmcurrentPINlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/confirmNewPIN").getText();
        Assert.assertEquals(ConfirmcurrentPINlabel, ConfirmcurrentPINlabel1);
        WebElement currentPIN = driver.findElementById("com.nationstrust.mobilebanking:id/etConfirmNewPIN");
        currentPIN.click();
        currentPIN.sendKeys(confirmnewpassword);


//        driver.findElement(MobileBy.AndroidUIAutomator(
//                "new UiScrollable(new UiSelector()..description(\"\")).getChildByText("
//                        + "new UiSelector().className(\"android.widget.ScrollView\"), \"1 Special character (!@#$%^&*)\")"));

        MobileElement slippayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"1 Special character (!@#$%^&*)\"));");
        slippayee.click();
    }

    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}
