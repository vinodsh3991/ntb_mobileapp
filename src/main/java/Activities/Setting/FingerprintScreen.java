package Activities.Setting;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FingerprintScreen extends Configuration {
    public static void FingerprintScreen_title() throws Exception{
        String screenheading = "Fingerprint";
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void FingerprintScreen_labels() throws Exception{
        String EnableFingerprint = "Enable Fingerprint";
        String unlock = "Unlock Nations Mobile Banking with your fingerprint";
        String ConfigureFingerprint = "Configure Fingerprint";
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String EnableFingerprint1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricType").getText();
            Assert.assertEquals(EnableFingerprint1, EnableFingerprint);
            String unlock1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricTypeDesc").getText();
            Assert.assertEquals(unlock1, unlock);
            String ConfigureFingerprint1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricTypeText").getText();
            Assert.assertEquals(ConfigureFingerprint1, ConfigureFingerprint);
            WebElement toggle = driver.findElementById("com.nationstrust.mobilebanking:id/switchBiometric");
            toggle.isDisplayed();
            WebElement arrow = driver.findElementById("com.nationstrust.mobilebanking:id/biometricArrow");
            arrow.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void arrow(){
        WebElement arrow = driver.findElementById("com.nationstrust.mobilebanking:id/biometricArrow");
        arrow.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void 	ontoggle(){
        WebElement toggle = driver.findElementById("com.nationstrust.mobilebanking:id/switchBiometric");
        toggle.click();

    }
  

    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}

