package Activities.Setting;


import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import java.util.concurrent.TimeUnit;

public class EditQuickAccessMenuScreen extends Configuration {
    public static void EditQuickAccessMenuScreen_labels() throws Exception{
        String screenheading = "Edit Quick Access Menu";
        String 	titledescription = "Menu items which are switched on will be displayed in the quick access menu. At a given time you can switch on only 6 items. Reset to default.";
        String SpecialOffers = "Special Offers";
        String 	ServiceRequests = "Service Requests";
        String BillerMaintenance = "Biller Maintenance";
        String PayeeMaintenance = "Payee Maintenance";
        String 	BankProducts = "Bank Products";
        String Rates = "Rates";
        String Calculators = "Calculators";
        String 	LocationView = "Location View";
        String ContactUs = "Contact Us";
        String FAQ = "FAQ";
        String 	Settings = "Settings";
        String TermsandConditions = "Terms and Conditions";
        String Logout = "Logout";

        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            String titledescription1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvDescription").getText();
            String SpecialOffers1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            WebElement specialoffericon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView");
            specialoffericon.isDisplayed();
            String ServiceRequests1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();
            WebElement ServiceRequestsicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ImageView");
            ServiceRequestsicon.isDisplayed();
            String BillerMaintenance1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView").getText();
            WebElement BillerMaintenanceicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.ImageView");
            BillerMaintenanceicon.isDisplayed();
            String PayeeMaintenance1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView").getText();
            WebElement PayeeMaintenanceicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.ImageView");
            PayeeMaintenanceicon.isDisplayed();
            String BankProducts1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView").getText();
            WebElement BankProductsicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.ImageView");
            BankProductsicon.isDisplayed();
            scrolltoFAQ();
            String Rates1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            WebElement Ratesicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView");
            Ratesicon.isDisplayed();
            String Calculators1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();
            WebElement Calculatorsicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ImageView");
            Calculatorsicon.isDisplayed();
            String LocationView1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView").getText();
            WebElement LocationViewicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.ImageView");
            LocationViewicon.isDisplayed();
            String ContactUs1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView").getText();
            WebElement ContactUsicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.ImageView");
            ContactUsicon.isDisplayed();
            String FAQ1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView").getText();
            WebElement FAQicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.ImageView");
            FAQicon.isDisplayed();
            scrolltologout();
            String Settings1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView").getText();
            WebElement Settingsicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.ImageView");
            Settingsicon.isDisplayed();
            String TermsandConditions1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView").getText();
            WebElement TermsandConditionsicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.ImageView");
            TermsandConditionsicon.isDisplayed();
            String Logout1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.TextView").getText();
            WebElement  Logouticon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.ImageView");
            Logouticon.isDisplayed();
            scrolltospecialoffer();


            Assert.assertEquals(screenheading, screenheading1);
            Assert.assertEquals(titledescription, titledescription1);
            Assert.assertEquals(SpecialOffers, SpecialOffers1);
            Assert.assertEquals(ServiceRequests, ServiceRequests1);
            Assert.assertEquals(BillerMaintenance, BillerMaintenance1);
            Assert.assertEquals(PayeeMaintenance, PayeeMaintenance1);
            Assert.assertEquals(BankProducts, BankProducts1);


            Assert.assertEquals(Rates, Rates1);
            Assert.assertEquals(Calculators, Calculators1);
            Assert.assertEquals(LocationView, LocationView1);
            Assert.assertEquals(ContactUs, ContactUs1);
            Assert.assertEquals(FAQ, FAQ1);
            Assert.assertEquals(Settings, Settings1);
            Assert.assertEquals(TermsandConditions, TermsandConditions1);
            Assert.assertEquals(Logout, Logout1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void scrolltoFAQ(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement scrolltoFAQ = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"FAQ\"));");
        scrolltoFAQ.click();
    }
    public static void scrolltologout(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement scrolltologout = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Logout\"));");
        scrolltologout.click();
    }
    public static void scrolltospecialoffer(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement scrolltospecialoffer = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Special Offers\"));");
        scrolltospecialoffer.click();
    }



    public static void save(){
        WebElement save = driver.findElementById("com.nationstrust.mobilebanking:id/btnSave");
        save.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }

    public static void checkdefault(){
        WebElement element = driver.findElementById("com.nationstrust.mobilebanking:id/tvDescription");
        TouchAction touchAction = new TouchAction(driver);
        touchAction.tap(PointOption.point(279, 383)).perform();

    }

    public static void selectlessthan6items(){
        WebElement specialoffer = driver.findElementById("com.nationstrust.mobilebanking:id/switchSpecialOffers");
        specialoffer.click();
        WebElement servicerequest = driver.findElementById("com.nationstrust.mobilebanking:id/switchServiceRequests");
        servicerequest.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }

    public static void selectgreaterthan6items(){
        WebElement billermaintenance = driver.findElementById("com.nationstrust.mobilebanking:id/switchBillerMaintenance");
        billermaintenance.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }



    public static void specialoffers(){
        WebElement specialoffers = driver.findElementById("com.nationstrust.mobilebanking:id/switchSpecialOffers");
        specialoffers.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }
    public static void ServiceRequests(){
        WebElement ServiceRequests = driver.findElementById("com.nationstrust.mobilebanking:id/switchServiceRequests");
        ServiceRequests.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }
    public static void BillerMaintenance(){
        WebElement BillerMaintenance = driver.findElementById("com.nationstrust.mobilebanking:id/switchBillerMaintenance");
        BillerMaintenance.click();
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void PayeeMaintenance(){
        WebElement PayeeMaintenance = driver.findElementById("com.nationstrust.mobilebanking:id/switchPayeeMaintenance");
        PayeeMaintenance.click();
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void BankProducts(){
        WebElement BankProducts = driver.findElementById("com.nationstrust.mobilebanking:id/switchBankProducts");
        BankProducts.click();
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void Rates(){
        WebElement Rates = driver.findElementById("com.nationstrust.mobilebanking:id/switchRates");
        Rates.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void Calculators(){
        WebElement Calculators = driver.findElementById("com.nationstrust.mobilebanking:id/switchCalculators");
        Calculators.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void Locations(){
        WebElement Locations = driver.findElementById("com.nationstrust.mobilebanking:id/switchLocations");
        Locations.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void ContactUs(){
        WebElement ContactUs = driver.findElementById("com.nationstrust.mobilebanking:id/switchContactUs");
        ContactUs.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void Faq(){
        WebElement Faq = driver.findElementById("com.nationstrust.mobilebanking:id/switchFaqs");
        Faq.click();
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void Settings(){
        WebElement Settings = driver.findElementById("com.nationstrust.mobilebanking:id/switchSettings");
        Settings.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void TermsConditions(){
        WebElement TermsConditions = driver.findElementById("com.nationstrust.mobilebanking:id/switchTermsConditions");
        TermsConditions.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void Logout(){
        WebElement Logout = driver.findElementById("com.nationstrust.mobilebanking:id/switchLogout");
        Logout.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void back(){
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
}

