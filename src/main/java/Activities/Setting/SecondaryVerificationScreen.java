package Activities.Setting;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SecondaryVerificationScreen extends Configuration {
    public static void SecondaryVerification_title() throws Exception{
        String screenheading = "Secondary Verification";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void SecondaryVerification_labels() throws Exception{
        String label = "Secondary Verification Limit";
        String label2 = "Change limit";
        String label3 = "LKR";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String label1 = driver.findElementById("com.nationstrust.mobilebanking:id/txnTypeDescription").getText();
            Assert.assertEquals(label, label1);
            String label22 = driver.findElementById("com.nationstrust.mobilebanking:id/tvChangeLimit").getText();
            Assert.assertEquals(label2, label22);
            String label33 = driver.findElementById("com.nationstrust.mobilebanking:id/currency").getText();
            Assert.assertEquals(label3, label33);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void save(){
        WebElement save = driver.findElementById("com.nationstrust.mobilebanking:id/btnSave");
        save.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void 	cancel(){
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/tvCancel");
        cancel.click();

    }
    public static void 	amount(String enteredamount){
        WebElement amount = driver.findElementById("com.nationstrust.mobilebanking:id/limitAmount");
        amount.click();
        amount.clear();
        amount.sendKeys(enteredamount);
        driver.hideKeyboard();

    }

    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}

