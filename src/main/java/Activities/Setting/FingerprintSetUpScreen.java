package Activities.Setting;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FingerprintSetUpScreen extends Configuration {
    public static void FingerprintSetUpScreen_title() throws Exception{
        String screenheading = "Fingerprint";
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void FingerprintSetUpScreen_labels() throws Exception{
        String Configure = "Configure Your Fingerprint";
        String Place = "Place your finger on the fingerprint scanner";
        String ConfigureFingerprint = "Configure Fingerprint";
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Configure1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricDescription").getText();
            Assert.assertEquals(Configure1, Configure);
            String Place1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricInstruction").getText();
            Assert.assertEquals(Place1, Place);
            WebElement icon = driver.findElementById("com.nationstrust.mobilebanking:id/biometricIImage");
            icon.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void arrow(){
        WebElement arrow = driver.findElementById("com.nationstrust.mobilebanking:id/biometricArrow");
        arrow.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void 	ontoggle(){
        WebElement toggle = driver.findElementById("com.nationstrust.mobilebanking:id/switchBiometric");
        toggle.click();

    }
  

    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}

