package Activities.Setting;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SettingMainScreen extends Configuration {
    public static void SettingMainScreen_title() throws Exception{
        String screenheading = "Settings";
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void SettingMainScreen_labels() throws Exception{
    String screenheading = "Settings";
    String 	ProfileSettings = "Profile Settings";
    String LanguageSettings = "Language Settings";
    String 	ChangePassword = "Change Password";
    String TransactionLimits = "Transaction Limits";
    String SecondaryVerification = "Secondary Verification";
    String 	BiometricOptions = "Biometric Options";
    String EditQuickAccessMenu = "Edit Quick Access Menu";

   // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    try {
        String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
        String ProfileSettings1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
        String LanguageSettings1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();
        String ChangePassword1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView").getText();
        String TransactionLimits1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView").getText();
        String SecondaryVerification1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView").getText();
        String BiometricOptions1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.TextView").getText();
        String EditQuickAccessMenu1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.TextView").getText();

        Assert.assertEquals(screenheading1, screenheading);
        Assert.assertEquals(ProfileSettings, ProfileSettings1);
        Assert.assertEquals(LanguageSettings, LanguageSettings1);
        Assert.assertEquals(ChangePassword, ChangePassword1);
        Assert.assertEquals(TransactionLimits, TransactionLimits1);
        Assert.assertEquals(SecondaryVerification, SecondaryVerification1);
        Assert.assertEquals(BiometricOptions, BiometricOptions1);
        Assert.assertEquals(EditQuickAccessMenu, EditQuickAccessMenu1);

    }
    catch(Exception ex){
        System.out.println("Cause: "+ex.getCause());
        System.out.println("Message: "+ex.getMessage());
        ex.printStackTrace();
    }
    Thread.sleep(3000);
}
    public static void Profilesetting(){
        WebElement Profilesetting = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView");
        Profilesetting.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }
    public static void Languagesetting(){
        WebElement Languagesetting = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView");
        Languagesetting.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }
    public static void ChangePassword(){
        WebElement ChangePassword = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView");
        ChangePassword.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void TransactionLimits(){
        WebElement TransactionLimits = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView");
        TransactionLimits.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void SecondaryVerifiation(){
        WebElement SecondaryVerifiation = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView");
        SecondaryVerifiation.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void BiometricOptions(){
        WebElement BiometricOptions = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.TextView");
        BiometricOptions.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void Editquickaccessmenu(){
        WebElement Editquickaccessmenu = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.TextView");
        Editquickaccessmenu.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void quickaccessmenuicon(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenuicon.click();

    }
    public static void back(){
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
}
