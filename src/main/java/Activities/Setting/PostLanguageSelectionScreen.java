package Activities.Setting;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PostLanguageSelectionScreen extends Configuration {
    public static void  LanguageSettings_title() throws Exception{
        String screenheading = "Language Settings";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  LanguageSettings_labels() throws Exception{
        String 	English = "English";
        String 	sinhala = "සිංහල";
        String Tamil = "தமிழ்";

       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String English1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvEnglish").getText();
            Assert.assertEquals(English, English1);
            String sinhala1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvSinhala").getText();
            Assert.assertEquals(sinhala, sinhala1);
            String Tamil1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTamil").getText();
            Assert.assertEquals(Tamil, Tamil1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }



    public static void 	quickaccessmenu(){
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/languageFab");
        quickaccessmenu.click();
    }
    public static void 	englishbutton(){
        WebElement englishbutton = driver.findElementById("com.nationstrust.mobilebanking:id/tvEnglish");
        englishbutton.click();

    }
    public static void 	sinhalabutton(){
        WebElement sinhalabutton = driver.findElementById("com.nationstrust.mobilebanking:id/tvSinhala");
        sinhalabutton.click();

    }
    public static void 	tamilbutton(){
        WebElement tamilbutton = driver.findElementById("com.nationstrust.mobilebanking:id/tvTamil");
        tamilbutton.click();

    }

    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }

    public static void 	sinhalatitle(){
        String sinhalatitle ="භාෂා සැකසුම්";
        String sinhalatitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
        Assert.assertEquals(sinhalatitle, sinhalatitle1);

    }

    public static void 	tamiltitle(){
        String tamiltitle ="மொழி அமைவு";
        String tamiltitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
        Assert.assertEquals(tamiltitle, tamiltitle1);

    }
    public static void englishtitle(){
        String englishtitle ="Language Settings";
        String englishtitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
        Assert.assertEquals(englishtitle, englishtitle1);

    }

}
