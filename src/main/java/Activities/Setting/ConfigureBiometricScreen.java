package Activities.Setting;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ConfigureBiometricScreen extends Configuration {
    public static void ConfigureBiometricScreen_title() throws Exception{
        String screenheading = "Configure Biometrics";
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void ConfigureBiometricScreen_labels() throws Exception{
        String Fingerprint = "Fingerprint";
        String EnableSecondaryVerification = "Enable Secondary Verification";
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Fingerprint1 = driver.findElementById("com.nationstrust.mobilebanking:id/touchIDText").getText();
            Assert.assertEquals(Fingerprint1, Fingerprint);
            String EnableSecondaryVerification1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricType").getText();
            Assert.assertEquals(EnableSecondaryVerification1, EnableSecondaryVerification);
            WebElement icon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.ImageView");
            icon.isDisplayed();
            WebElement toggle = driver.findElementById("com.nationstrust.mobilebanking:id/switchSecondaryVerification");
            toggle.isDisplayed();
            WebElement arrow = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView");
            arrow.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void arrow(){
        WebElement arrow = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView");
        arrow.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void 	ontoggle(){
        WebElement toggle = driver.findElementById("com.nationstrust.mobilebanking:id/switchSecondaryVerification");
        toggle.click();

    }


    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}

