package Activities.ForgotUsername;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ForgotUsernameListScreen extends Configuration {
    public static void ForgotUsernameListScreen_title() throws Exception{
        String screenheading = "Forgot Username";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/welcomeText").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void myAccounts(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement myAccounts = driver.findElementById("com.nationstrust.mobilebanking:id/myAccountsButton");
        myAccounts.click();
    }

    public static void myCredit(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement myCredit = driver.findElementById("com.nationstrust.mobilebanking:id/myCardsButton");
        myCredit.click();
    }

    public static void myDebit(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement myDebit = driver.findElementById("com.nationstrust.mobilebanking:id/myDebitCardsButton");
        myDebit.click();
    }
    public static void quickaccessmenu(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/optionsBtn");
        quickaccessmenu.click();
    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementById("com.nationstrust.mobilebanking:id/backNavigation");
        back.click();
    }





}


