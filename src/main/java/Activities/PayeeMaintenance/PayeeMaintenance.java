package Activities.PayeeMaintenance;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PayeeMaintenance extends Configuration {
    public static void PayeeMaintenance_title() throws Exception{
    String screenheading = "Payee List";
   // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    try {
        String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
        Assert.assertEquals(screenheading, screenheading1);

    }
    catch(Exception ex){
        System.out.println("Cause: "+ex.getCause());
        System.out.println("Message: "+ex.getMessage());
        ex.printStackTrace();
    }
    Thread.sleep(3000);
}

    public static void emptyscreen(){
        String emptyscreenlabel1= "You do not have any current accounts to make this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/emptyChequeBook").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView");
        imageicon.isDisplayed();
    }

    public static void plusicon(){
        WebElement plusicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_add");
        plusicon.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }



    public static void 	quickaccessmenuicon(){
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenuicon.click();

    }

    public static void selectbranch(){
        WebElement selectbranch = driver.findElementById("com.nationstrust.mobilebanking:id/branchET");
        selectbranch.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement searchbrach = driver.findElementById("com.nationstrust.mobilebanking:id/etSearch");
        searchbrach.click();
        searchbrach.sendKeys("Matara");
        driver.hideKeyboard();
        WebElement setbrach = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout");
        setbrach.click();
    }



    public static void back(){
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }


    public static void Sliptoscroll(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement slippayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"ALLIANCE FINANCE COMPANY PLC\"));");
        slippayee.click();
    }

    public static void Cefttoscroll(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement slippayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Bank Of Ceylon\"));");
        slippayee.click();
    }
    public static void ntbtoscroll(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement slippayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"NTB 1\"));");
        slippayee.click();
    }



}

