package Activities.PayeeMaintenance;

import Activities.Scrollingpage;
import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class DeletePayeeScreen extends Configuration {

    public static void 	quickaccessmenuicon(){
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenuicon.click();

    }

    public static void deleteicon(){
        WebElement deleteicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_delete");
        deleteicon.click();

    }

    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }


    public static void Sliptoscroll(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement slippayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"ALLIANCE FINANCE COMPANY PLC\"));");
        WebElement slippayee1 = (WebElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"ALLIANCE FINANCE COMPANY PLC\"));");
        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
                until(ExpectedConditions.elementToBeClickable(slippayee1));
        LongPressOptions longPressOptions = new LongPressOptions();
        longPressOptions.withDuration(Duration.ofSeconds(3)).withElement(ElementOption.element(longpress));

        new TouchAction(driver).longPress(longPressOptions).perform();

    }

    public static void Cefttoscroll(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement ceftpayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Amana Bank Ltd\"));");
        WebElement ceftpayee1 = (WebElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Amana Bank Ltd\"));");
        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
                until(ExpectedConditions.elementToBeClickable(ceftpayee1));

        LongPressOptions longPressOptions = new LongPressOptions();
        longPressOptions.withDuration(Duration.ofSeconds(3)).withElement(ElementOption.element(longpress));

        new TouchAction(driver).longPress(longPressOptions).perform();
    }

    public static void ntbtoscroll(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement ntbpayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"NTB 1\"));");
        WebElement ntbpayee1 = (WebElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"NTB 1\"));");

        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
                until(ExpectedConditions.elementToBeClickable(ntbpayee1));
        LongPressOptions longPressOptions = new LongPressOptions();
        longPressOptions.withDuration(Duration.ofSeconds(3)).withElement(ElementOption.element(longpress));

        new TouchAction(driver).longPress(longPressOptions).perform();


    }



}


