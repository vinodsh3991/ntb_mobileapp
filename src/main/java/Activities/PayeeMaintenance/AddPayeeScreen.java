package Activities.PayeeMaintenance;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import static io.appium.java_client.touch.offset.PointOption.point;
import static org.testng.Assert.assertNotNull;

public class AddPayeeScreen extends Configuration {
    public static void AddPayeeScreen_title() throws Exception{
    String screenheading = "Add Payee";
    //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    try {
        String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
        Assert.assertEquals(screenheading, screenheading1);

    }
    catch(Exception ex){
        System.out.println("Cause: "+ex.getCause());
        System.out.println("Message: "+ex.getMessage());
        ex.printStackTrace();
    }
    Thread.sleep(3000);
}
    public static void AddPayeeScreen_labels() throws Exception{
        String 	Nickname = "Nickname";
        String 	PayeeName = "Payee Name";
        String Bank = "Bank";
        String SelectaBank = "Select a Bank";
        String Branch = "Branch";
        String Branch2 = "Select Bank Branch";
        String 	AccountNumber = "Account Number";

        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNick").getText();
            Assert.assertEquals(Nickname, Nickname1);
            String PayeeName1 = driver.findElementById("com.nationstrust.mobilebanking:id/payeeName").getText();
            Assert.assertEquals(PayeeName, PayeeName1);
            String 	Bank1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(Bank, Bank1);
            String 	SelectaBank1 = driver.findElementById("com.nationstrust.mobilebanking:id/bankName").getText();
            Assert.assertEquals(SelectaBank, SelectaBank1);
            String 	Branch1 = driver.findElementById("com.nationstrust.mobilebanking:id/branchNameLable").getText();
            Assert.assertEquals(Branch, Branch1);
            String 	Branch22 = driver.findElementById("com.nationstrust.mobilebanking:id/branchName").getText();
            Assert.assertEquals(Branch2, Branch22);
            String 	AccountNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/accNo").getText();
            Assert.assertEquals(AccountNumber, AccountNumber1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void FundtransferAddPayeeScreen_labels() throws Exception{
        MobileElement scrolltobiller = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"option\"));");
        scrolltobiller.click();
        String 	label = "This option is valid only to add an \"Account\" as a Payee. To add a Credit Card as a Biller please use the \"Payments\" option.";

        try {
            String label1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView").getText();
            Assert.assertEquals(label1, label);
            WebElement inforicon = driver.findElementById("com.nationstrust.mobilebanking:id/btnNominalInfo");
            inforicon.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void 	addbutton(){
        WebElement addbutton = driver.findElementById("com.nationstrust.mobilebanking:id/addPayeeButton");
        addbutton.click();

    }

    public static void 	cancelbutton(){
        WebElement cancelbutton = driver.findElementById("com.nationstrust.mobilebanking:id/cancelAddPayeeButton");
        cancelbutton.click();

    }
    public static void 	addnickname(String nickname){
        WebElement addnickname = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNickName");
        addnickname.click();
        addnickname.sendKeys(nickname);
        driver.hideKeyboard();

    }
    public static void 	addpayeename(String payeename){
        WebElement addpayeename = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNameText");
        addpayeename.click();
        addpayeename.sendKeys(payeename);
        driver.hideKeyboard();

    }

    public static void selectslipbank(){
        String bankdropdowntitle ="Select a Bank";
        String branchdropdowntitle ="Select a Branch";
        WebElement selectbank = driver.findElementById("com.nationstrust.mobilebanking:id/bankName");
        selectbank.click();
        String 	bankdropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(bankdropdowntitle, bankdropdowntitle1);
        MobileElement slipbank = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"ALLIANCE FINANCE COMPANY PLC\"));");
        slipbank.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement searchbranch = driver.findElementById("com.nationstrust.mobilebanking:id/getBranchBtn");
        searchbranch.click();
        String 	branchdropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(branchdropdowntitle, branchdropdowntitle1);
        WebElement selectbranch = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectbranch.click();

    }



    public static void selectceftbrank(){

         String bankdropdowntitle ="Select a Bank";
         WebElement selectbank = driver.findElementById("com.nationstrust.mobilebanking:id/getBankBtn");
         selectbank.click();
         String bankdropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
         Assert.assertEquals(bankdropdowntitle, bankdropdowntitle1);
         MobileElement ceftbank = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"AMANA BANK LTD\"));");
         ceftbank.click();



    }

    public static void selectntbbank(){

        String bankdropdowntitle ="Select a Bank";
        WebElement selectbank = driver.findElementById("com.nationstrust.mobilebanking:id/bankName");
        selectbank.click();
        String 	bankdropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(bankdropdowntitle, bankdropdowntitle1);
        MobileElement ntbbank = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"NATIONS TRUST BANK\"));");
        ntbbank.click();

    }

    public static void addaccountnumber(String accountnumber){
        WebElement addaccountnumber = driver.findElementById("com.nationstrust.mobilebanking:id/payeeAccNo");
        addaccountnumber.click();
        addaccountnumber.sendKeys(accountnumber);
    }


    public static void back(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }





}


