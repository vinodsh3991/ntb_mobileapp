package Activities.PayeeMaintenance;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class EditPayeeScreen  extends Configuration {
    public static void EditPayeeScreen_title() throws Exception{
        String screenheading = "Edit Payee";
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void ceftEditPayeeScreen_labels() throws Exception{
        String 	Nickname = "Nickname";
        String 	PayeeName = "Payee Name";
        String Bank = "Bank";
        String 	AccountNumber = "Account Number";

        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNick").getText();
            Assert.assertEquals(Nickname, Nickname1);
            String PayeeName1 = driver.findElementById("com.nationstrust.mobilebanking:id/payeeName").getText();
            Assert.assertEquals(PayeeName, PayeeName1);
            String 	Bank1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(Bank, Bank1);
            String 	AccountNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/accNo").getText();
            Assert.assertEquals(AccountNumber, AccountNumber1);
            WebElement 	banklisticon = driver.findElementById("com.nationstrust.mobilebanking:id/getBankBtn");
            banklisticon.isDisplayed();



        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void slipEditPayeeScreen_labels() throws Exception{
        String 	Nickname = "Nickname";
        String 	PayeeName = "Payee Name";
        String Bank = "Bank";
        String Branch = "Branch";
        String 	AccountNumber = "Account Number";

       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNick").getText();
            Assert.assertEquals(Nickname, Nickname1);
            String PayeeName1 = driver.findElementById("com.nationstrust.mobilebanking:id/payeeName").getText();
            Assert.assertEquals(PayeeName, PayeeName1);
            String 	Bank1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            Assert.assertEquals(Bank, Bank1);
            String 	Branch1 = driver.findElementById("com.nationstrust.mobilebanking:id/branchNameLable").getText();
            Assert.assertEquals(Branch, Branch1);
            String 	AccountNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/accNo").getText();
            Assert.assertEquals(AccountNumber, AccountNumber1);
            WebElement 	banklisticon = driver.findElementById("com.nationstrust.mobilebanking:id/getBankBtn");
            banklisticon.isDisplayed();
            WebElement 	branchlisticon = driver.findElementById("com.nationstrust.mobilebanking:id/getBranchBtn");
            banklisticon.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void save(){
        WebElement save = driver.findElementById("com.nationstrust.mobilebanking:id/addPayeeButton");
        save.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void cancel(){
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/cancelAddPayeeButton");
        cancel.click();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }






    public static void selectslipbank(){
        String bankdropdowntitle ="Select a Bank";
        String branchdropdowntitle ="Select a Branch";
        WebElement selectbank = driver.findElementById("com.nationstrust.mobilebanking:id/getBankBtn");
        selectbank.click();
        String 	bankdropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(bankdropdowntitle, bankdropdowntitle1);
        MobileElement slipbank = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"ALLIANCE FINANCE COMPANY PLC\"));");
        slipbank.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement searchbranch = driver.findElementById("com.nationstrust.mobilebanking:id/getBranchBtn");
        searchbranch.click();
        String 	branchdropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(branchdropdowntitle, branchdropdowntitle1);
        WebElement selectbranch = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectbranch.click();

    }



    public static void selectceftbank(){

        String bankdropdowntitle ="Select a Bank";
        WebElement selectbank = driver.findElementById("com.nationstrust.mobilebanking:id/getBankBtn");
        selectbank.click();
        String bankdropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(bankdropdowntitle, bankdropdowntitle1);
        MobileElement ceftbank = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"AMANA BANK LTD\"));");
        ceftbank.click();



    }

    public static void editpayeenickname(String newnickname){
        WebElement oldnickname = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNickName");
        oldnickname.clear();
        oldnickname.sendKeys(newnickname);
        driver.hideKeyboard();


    }
    public static void editpayeename(String newpayeename){
        WebElement oldpayeename = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNameText");
        oldpayeename.clear();
        oldpayeename.sendKeys(newpayeename);
        driver.hideKeyboard();


    }
    public static void editaccountnumber(String newaccnumber){
        WebElement oldaccountnumber = driver.findElementById("com.nationstrust.mobilebanking:id/payeeAccNo");
        oldaccountnumber.clear();
        oldaccountnumber.sendKeys(newaccnumber);

    }

    public static void selectntbbank(){

        String bankdropdowntitle ="Select a Bank";
        WebElement selectbank = driver.findElementById("com.nationstrust.mobilebanking:id/bankName");
        selectbank.click();
        String 	bankdropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(bankdropdowntitle, bankdropdowntitle1);
        MobileElement ntbbank = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"NATIONS TRUST BANK\"));");
        ntbbank.click();

    }



    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }





}


