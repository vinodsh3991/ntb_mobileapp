package Activities.PayeeMaintenance;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ConfirmPayeeDetailsScreen extends Configuration {

    public static void  ConfirmPayeeDetails_title() throws Exception{
        String screenheading = "Confirm Payee Details";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").getText();
            Assert.assertEquals(screenheading, screenheading1);


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void  ConfirmPayeeDetailsceft_labels() throws Exception{
        String 	Nickname = "Nickname";
        String 	PayeeName = "Payee Name";
        String accountnumber = "Account Number";
        String bankname = "Bank Name";

       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView17").getText();
            Assert.assertEquals(Nickname, Nickname1);
            String PayeeName1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView26").getText();
            Assert.assertEquals(PayeeName, PayeeName1);
            String 	bankname1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView30").getText();
            Assert.assertEquals(bankname, bankname1);
            String 	accountnumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView32").getText();
            Assert.assertEquals(accountnumber, accountnumber1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void  ConfirmPayeeDetailsslip_labels() throws Exception{
        String 	Nickname = "Nickname";
        String 	PayeeName = "Payee Name";
        String accountnumber = "Account Number";
        String bankname = "Bank Name";
        String branchname = "Branch Name";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView17").getText();
            Assert.assertEquals(Nickname, Nickname1);
            String PayeeName1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView26").getText();
            Assert.assertEquals(PayeeName, PayeeName1);
            String 	bankname1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView30").getText();
            Assert.assertEquals(bankname, bankname1);
            String 	accountnumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView32").getText();
            Assert.assertEquals(accountnumber, accountnumber1);
            String 	branchname1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView34").getText();
            Assert.assertEquals(branchname, branchname1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void 	confirmbutton(){
        WebElement confirmbutton = driver.findElementById("com.nationstrust.mobilebanking:id/btnConfirm");
        confirmbutton.click();

    }

    public static void 	cancelbutton(){
        WebElement cancelbutton = driver.findElementById("com.nationstrust.mobilebanking:id/contactCancelButton");
        cancelbutton.click();

    }

    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }


}
