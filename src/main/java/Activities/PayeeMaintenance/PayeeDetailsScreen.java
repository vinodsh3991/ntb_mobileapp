package Activities.PayeeMaintenance;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PayeeDetailsScreen  extends Configuration {
    public static void PayeeDetailsScreen_title() throws Exception{
        String screenheading = "Payee Details";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void OwnPayeeSetailsScreen_labels() throws Exception{
        String 	Bank = "Bank";
        String 	AccountNo = "Account No";

        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Bank1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Bank, Bank1);
            String AccountNo1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.TextView[1]").getText();
            Assert.assertEquals(AccountNo, AccountNo1);
            WebElement 	bileerlogo = driver.findElementById("com.nationstrust.mobilebanking:id/billerLogo");
            bileerlogo.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void thirdpartyntbPayeeSetailsScreen_labels() throws Exception{
        String 	Bank = "Bank";
        String 	Branch = "Branch";
        String 	AccountNo = "Account No";

       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Bank1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Bank, Bank1);
            String AccountNo1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.TextView[1]").getText();
            Assert.assertEquals(AccountNo, AccountNo1);
            String Branch1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Branch, Branch1);
            WebElement 	bileerlogo = driver.findElementById("com.nationstrust.mobilebanking:id/billerLogo");
            bileerlogo.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void SlipceftPayeeSetailsScreen_labels() throws Exception{
        String 	Bank = "Bank";
        String 	AccountNo = "Account No";
        String 	Branch = "Branch";

      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Bank1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Bank, Bank1);
            String AccountNo1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.TextView[1]").getText();
            Assert.assertEquals(AccountNo, AccountNo1);
            String 	Branch1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Branch, Branch1);
            WebElement 	bileerlogo = driver.findElementById("com.nationstrust.mobilebanking:id/billerLogo");
            bileerlogo.isDisplayed();
            WebElement 	editicon = driver.findElementById("com.nationstrust.mobilebanking:id/editPayeeButton");
            editicon.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void editicon(){
        WebElement 	editicon = driver.findElementById("com.nationstrust.mobilebanking:id/editPayeeButton");
        editicon.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void back(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }

}