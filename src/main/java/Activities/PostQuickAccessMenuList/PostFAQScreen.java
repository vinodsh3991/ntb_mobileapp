package Activities.PostQuickAccessMenuList;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PostFAQScreen extends Configuration {
    public static void PostFAQScreen_labels() throws Exception{
        String screenheading = "FAQ";
        String subtitle = "Frequently Asked Questions";
        String more = "Have more questions?";
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);
            String subtitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFAQTitle").getText();
            Assert.assertEquals(subtitle, subtitle1);
            String more1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMoreOrHide").getText();
            Assert.assertEquals(more, more1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void more(){
        WebElement more = driver.findElementById("com.nationstrust.mobilebanking:id/tvMoreOrHide");
        more.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }


    public static void quickaccessmenu(){
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/faq_menu");
        quickaccessmenu.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    public static void scrolltohide(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement scrolltohide = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Hide more questions?\"));");
        String hide1="Hide more questions?";
        String hide2 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMoreOrHide").getText();
        Assert.assertEquals(hide2, hide1);
        WebElement scrolltohide1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMoreOrHide");
        scrolltohide1.click();
    }


}

