package Activities.PostQuickAccessMenuList;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PostQuickAccessMenuList extends Configuration {
    public static void PostQuickAccessMenuList_labels() throws Exception{
        String homelabel = "Home";
        String paymentlabel = "Payments";
        String transferlabel = "Transfers";
        String inboxlabel = "Inbox";
        String offerlabel = "Special Offers";
        String servicerequestlabel = "Service Requests";
        String bankproductslabel = "Bank Products";
        String rateslabel = "Rates";
        String calculatorslabel = "Calculators";
        String faqlabel = "FAQ";


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String homelabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView2").getText();
            String paymentlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView").getText();
            String transferlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView3").getText();
            String inboxlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView4").getText();
            String offerlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvOne").getText();
            String servicerequestlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTwo").getText();
            String bankproductslabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvThree").getText();
            String rateslabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFour").getText();
            String calculatorslabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFive").getText();
            String faqlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvSix").getText();
            Assert.assertEquals(homelabel1, homelabel);
            Assert.assertEquals(paymentlabel1, paymentlabel);
            Assert.assertEquals(transferlabel1, transferlabel);
            Assert.assertEquals(inboxlabel1, inboxlabel);
            Assert.assertEquals(offerlabel1, offerlabel);
            Assert.assertEquals(servicerequestlabel1, servicerequestlabel);
            Assert.assertEquals(bankproductslabel1, bankproductslabel);
            Assert.assertEquals(rateslabel1, rateslabel);
            Assert.assertEquals(calculatorslabel1, calculatorslabel);
            Assert.assertEquals(faqlabel1, faqlabel);


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void fundtransfer(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement fundtransfer = driver.findElementById("com.nationstrust.mobilebanking:id/ivTransfers");
        fundtransfer.click();
    }
    public static void home(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement home = driver.findElementById("com.nationstrust.mobilebanking:id/homeView");
        home.click();
    }
    public static void billpayment(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement billpayment = driver.findElementById("com.nationstrust.mobilebanking:id/ivPayments");
        billpayment.click();
    }
    public static void inbox(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement inbox = driver.findElementById("com.nationstrust.mobilebanking:id/ivInbox");
        inbox.click();
    }
    public static void offers(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement offers = driver.findElementById("com.nationstrust.mobilebanking:id/tvOne");
        offers.click();
    }
    public static void servicerequest(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement servicerequest = driver.findElementById("com.nationstrust.mobilebanking:id/tvTwo");
        servicerequest.click();
    }
    public static void bankproducts(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement bankproducts = driver.findElementById("com.nationstrust.mobilebanking:id/tvThree");
        bankproducts.click();
    }
    public static void rates(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement rates = driver.findElementById("com.nationstrust.mobilebanking:id/tvFour");
        rates.click();
    }
    public static void calculator(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement calculator = driver.findElementById("com.nationstrust.mobilebanking:id/ivFive");
        calculator.click();
    }
    public static void faq(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement faq = driver.findElementById("com.nationstrust.mobilebanking:id/tvSix");
        faq.click();
    }
    public static void cross(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement cross = driver.findElementById("com.nationstrust.mobilebanking:id/menuCloseIcon");
        cross.click();
    }
    public static void payeeMaintenance(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement faq = driver.findElementById("com.nationstrust.mobilebanking:id/tvTwo");
        faq.isDisplayed();
        WebElement payeeMaintenanceicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivTwo");
        payeeMaintenanceicon.isDisplayed();
    }
    public static void billerMaintenance(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement billerMaintenance = driver.findElementById("com.nationstrust.mobilebanking:id/tvOne");
        billerMaintenance.isDisplayed();
        WebElement billerMaintenanceicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivOne");
        billerMaintenanceicon.isDisplayed();
    }


}
