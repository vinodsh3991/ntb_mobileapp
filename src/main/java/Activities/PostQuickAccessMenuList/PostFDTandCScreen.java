package Activities.PostQuickAccessMenuList;

import TestBase.Configuration;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PostFDTandCScreen extends Configuration {
    public static void  FDTandCScreen_title() throws Exception{
        String screenheading = "Terms and Conditions";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  FDTandCScreen_labels() throws Exception{
        String 	content1 = "I hereby confirm that copies of the General Business Conditions and other Terms and Conditions of Nations Trust Bank PLC applicable to the product(s)/ service(s) which I have applied for hereunder together with details relevant to such product(s)/service(s) were given and explained to me in the language of my choice before signing hereof and I have read and understood the detailed terms and conditions therein contained and agree and consent to be bound thereby.";


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String content11 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTermsText").getText();
            Assert.assertEquals(content1, content11);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }



    public static void 	generallink(){

        TouchAction touchAction = new TouchAction(driver);
        touchAction.tap(new PointOption().withCoordinates(240, 399)).perform();

    }

    public static void 	agree(){
        WebElement agree = driver.findElementById("com.nationstrust.mobilebanking:id/btnSubmit");
        agree.click();
    }
    public static void 	decline(){
        WebElement agree = driver.findElementById("com.nationstrust.mobilebanking:id/decline");
        agree.click();

    }

}
