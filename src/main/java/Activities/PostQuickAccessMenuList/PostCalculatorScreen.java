package Activities.PostQuickAccessMenuList;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PostCalculatorScreen extends Configuration {
    public static void PostCalculatorScreen_title() throws Exception{
        String screenheading = "Calculators";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void  PostCalculatorScreen_labels() throws Exception{
                String 	FDcalculator = "Fixed Deposit Calculator";
                String 	Leasing = "Leasing";


                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                try {
                    String FDcalculator1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.TextView").getText();
                    Assert.assertEquals(FDcalculator, FDcalculator1);
                    String 	Leasing1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.TextView").getText();
                    Assert.assertEquals(Leasing, Leasing1);
                    WebElement Fdicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.ImageView[1]");
                    Fdicon.isDisplayed();
                    WebElement leasingicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.ImageView[1]");
                    leasingicon.isDisplayed();
                    WebElement quickaccessicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
                    quickaccessicon.isDisplayed();

                }
                catch(Exception ex){
                    System.out.println("Cause: "+ex.getCause());
                    System.out.println("Message: "+ex.getMessage());
                    ex.printStackTrace();
                }
                Thread.sleep(3000);
            }



    public static void fdcalculator(){
        WebElement fdcalculator = driver.findElementById("com.nationstrust.mobilebanking:id/llFixedDepositCal");
        fdcalculator.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void leasingcalculator(){
        WebElement leasingcalculator = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.TextView");
        leasingcalculator.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }



    public static void 	quickaccessmenuicon(){
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenuicon.click();

    }


}

