package Activities.PostQuickAccessMenuList;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PostFDOpenSuccessScreen extends Configuration {
    public static void  FDOpenSuccessScreen_title() throws Exception{
        String screenheading = "Fixed Deposit Opening Successful";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvStatus").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  FDOpenSuccessScreen_labels() throws Exception{
        String 	FixedDepositNumber = "Fixed Deposit Number";
        String 	FundingAccount = "Funding Account";
        String 	Amount = "Amount";
        String 	Date = "Date";
        String 	content1 = "";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            WebElement successvicon= driver.findElementById("com.nationstrust.mobilebanking:id/statusImage");
            successvicon.isDisplayed();
            WebElement emailheader= driver.findElementById("com.nationstrust.mobilebanking:id/tvEmailHeader");
            emailheader.isDisplayed();
            String FixedDepositNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFdNo").getText();
            Assert.assertEquals(FixedDepositNumber, FixedDepositNumber1);
            String FundingAccount1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2").getText();
            Assert.assertEquals(FundingAccount, FundingAccount1);
            String Amount1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView").getText();
            Assert.assertEquals(Amount, Amount1);
            String Date1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvDate").getText();
            Assert.assertEquals(Date, Date1);

            WebElement fdtype= driver.findElementById("com.nationstrust.mobilebanking:id/tvFdNoTitle");
            fdtype.isDisplayed();
            WebElement fdnumber= driver.findElementById("com.nationstrust.mobilebanking:id/tvFdNoVal");
            fdnumber.isDisplayed();
            WebElement fundingaccounttype= driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2Name");
            fundingaccounttype.isDisplayed();
            WebElement fundingaccnumber= driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2Ref");
            fundingaccnumber.isDisplayed();
            WebElement amount= driver.findElementById("com.nationstrust.mobilebanking:id/tvAmountRupees");
            amount.isDisplayed();
            WebElement date= driver.findElementById("com.nationstrust.mobilebanking:id/tvDateText");
            date.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void 	home(){
        WebElement home = driver.findElementById("com.nationstrust.mobilebanking:id/homeButton");
        home.click();
    }


}
