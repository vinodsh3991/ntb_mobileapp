package Activities.PostQuickAccessMenuList;

import TestBase.Configuration;
import Activities.PopUpMessages.PopUpMessages;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PostFDFundingScreen extends Configuration {
    public static void PostFDFundingScreen_title() throws Exception {
        String screenheading = "Fixed Deposit Opening";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void PostFDFundingScreen_labels() throws Exception {
        String 	MaturityValue = "Maturity Value";
        String 	LKR = "LKR";
        String description = "This amount is subject to the prevailing withholding tax structure";
        String 	NominalInterestRate = "Nominal Interest Rate";
        String 	AnnualEquivalentRate = "Annual Equivalent Rate";
        String question = "Would you like to open fixed deposit for above details?";


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String MaturityValue1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(MaturityValue, MaturityValue1);
            String description1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[2]").getText();
            Assert.assertEquals(description, description1);
            String NominalInterestRate1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(NominalInterestRate, NominalInterestRate1);
            String LKR1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(LKR, LKR1);
            String 	AnnualEquivalentRate1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(AnnualEquivalentRate, AnnualEquivalentRate1);
            WebElement NIRMore = driver.findElementById("com.nationstrust.mobilebanking:id/ivNIRMore");
            NIRMore.isDisplayed();
            WebElement AERMore = driver.findElementById("com.nationstrust.mobilebanking:id/ivAERMore");
            AERMore.isDisplayed();
            WebElement NominalIR = driver.findElementById("com.nationstrust.mobilebanking:id/tvNominalIR");
            NominalIR.isDisplayed();
            WebElement AnnualER = driver.findElementById("com.nationstrust.mobilebanking:id/tvAnnualER");
            AnnualER.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	NIR(){
        WebElement NIR = driver.findElementById("com.nationstrust.mobilebanking:id/ivNIRMore");
        NIR.click();
        PopUpMessages.popupmessage_header("Nominal Interest Rate");
        PopUpMessages.popupmessage_content("The general rate of interest applicable for the investment");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
    }
    public static void 	ATR(){
        WebElement ATR = driver.findElementById("com.nationstrust.mobilebanking:id/ivAERMore");
        ATR.click();
        PopUpMessages.popupmessage_header("Annual Equivalent Rate");
        PopUpMessages.popupmessage_content("Effective annual interest rate applicable for the interest");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        MobileElement scrolltoendofthepage = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Confirm\"));");

    }

    public static void confirm() {
        String confirmbtn = "Confirm";
        String confirmbtn1 = driver.findElementById("com.nationstrust.mobilebanking:id/btnConfirm").getText();
        Assert.assertEquals(confirmbtn, confirmbtn1);
        WebElement confirm = driver.findElementById("com.nationstrust.mobilebanking:id/btnConfirm");
        confirm.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void cancel() {
        String cancelbtn = "Cancel";
        String cancelbtn1 = driver.findElementById("com.nationstrust.mobilebanking:id/cancel").getText();
        Assert.assertEquals(cancelbtn, cancelbtn1);
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/cancel");
        cancel.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void selectfundingaccount() {
        String dropdwontitle="Select Funding Account";
        String floatinglabel ="Funding Account";

        WebElement fundingaccount = driver.findElementById("com.nationstrust.mobilebanking:id/etFundingAccount");
        fundingaccount.click();
        String dropdwontitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(dropdwontitle, dropdwontitle1);

        WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout");
        selectaccount.click();

        String floatinglabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tiFundingAcc").getText();
        Assert.assertEquals(floatinglabel, floatinglabel1);

        WebElement accountbalance = driver.findElementById("com.nationstrust.mobilebanking:id/tvFundingAccBalance");
        accountbalance.isDisplayed();

    }

    public static void back() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    public static void 	enterbranch(){
        String dropdwontitle="Select Branch";
        String label="If you wish to uplift your Fixed Deposits, Please send us instructions including the Fixed deposit number and the account number that you wish to credit funds, via the Mobile Banking Mail-box.";
        WebElement enterbranch = driver.findElementById("com.nationstrust.mobilebanking:id/etBranch");
        enterbranch.click();
        String dropdwontitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(dropdwontitle, dropdwontitle1);

        WebElement selectbranch = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectbranch.click();

     //   MobileElement scrolltoageend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"If you wish to uplift your Fixed Deposits, Please send us instructions including the Fixed deposit number and the account number that you wish to credit funds, via the Mobile Banking Mail-box.\"));");
        String label1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFdInterestStatus2").getText();
        Assert.assertEquals(label, label1);

    }


}

