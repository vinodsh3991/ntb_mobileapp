package Activities.PostQuickAccessMenuList;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ContactUsScreen  extends Configuration {

    public static void ContatUs_title() throws Exception{
        String screenheading = "Contact Us";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void ContactUsScreen_labels() throws Exception{
        String 	subtitle = "Want to reach us?";
        String 	NationsCustomer = "Nations Customer";
        String 	AmericanExpress = "American Express";
        String 	MasterCardslabel = "MasterCards";
        String 	Visitusat = "Visit us at";
        String 	nationemail = "customerservice@nationstrust.com";
        String 	americanemail = "americanexpress@nationstrust.com";
        String 	masteremail = "creditcards@nationstrust.com";
        String 	nationsurl = "www.nationstrust.com";
        String 	americanurl = "www.americanexpress.lk";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String subtitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView15").getText();
            Assert.assertEquals(subtitle, subtitle1);
            String NationsCustomer1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView16").getText();
            Assert.assertEquals(NationsCustomer, NationsCustomer1);
            String AmericanExpress1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView20").getText();
            Assert.assertEquals(AmericanExpress, AmericanExpress1);
            String 	MasterCardslabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView21").getText();
            Assert.assertEquals(MasterCardslabel, MasterCardslabel1);
            String 	Visitusat1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView24").getText();
            Assert.assertEquals(Visitusat, Visitusat1);
            WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/contactFab");
            quickaccessmenuicon.isDisplayed();
            WebElement arrow1 = driver.findElementById("com.nationstrust.mobilebanking:id/imageView3");
            arrow1.isDisplayed();
            WebElement arrow2 = driver.findElementById("com.nationstrust.mobilebanking:id/imageView5");
            arrow2.isDisplayed();
            WebElement arrow3 = driver.findElementById("com.nationstrust.mobilebanking:id/imageView14");
            arrow3.isDisplayed();
            String nationemail1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMailNtb").getText();
            Assert.assertEquals(nationemail, nationemail1);
            String americanemail1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMailAmex").getText();
            Assert.assertEquals(americanemail, americanemail1);
            String masteremail1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMailMasterCard").getText();
            Assert.assertEquals(masteremail, masteremail1);
            String nationsurl1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvWebNtb").getText();
            Assert.assertEquals(nationsurl, nationsurl1);
            String americanurl1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvWebAmex").getText();
            Assert.assertEquals(americanurl, americanurl1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void nationemail(){
        String nationemail = "<customerservice@nationstrust.com>, ";
        WebElement email = driver.findElementById("com.nationstrust.mobilebanking:id/tvMailNtb");
        email.click();
        String nationemail1 = driver.findElementById("com.google.android.gm:id/to").getText();
        Assert.assertEquals(nationemail, nationemail1);
        driver.hideKeyboard();
    }


    public static void americanemail(){
        String americanemail = "<americanexpress@nationstrust.com>, ";
        WebElement email = driver.findElementById("com.nationstrust.mobilebanking:id/tvMailAmex");
        email.click();
        String americanemail1 = driver.findElementById("com.google.android.gm:id/to").getText();
        Assert.assertEquals(americanemail, americanemail1);
        driver.hideKeyboard();
    }

    public static void masteremail(){
        String masteremail = "<creditcards@nationstrust.com>, ";
        WebElement email = driver.findElementById("com.nationstrust.mobilebanking:id/tvMailMasterCard");
        email.click();
        String masteremail1 = driver.findElementById("com.google.android.gm:id/to").getText();
        Assert.assertEquals(masteremail, masteremail1);
        driver.hideKeyboard();
    }
    public static void nationurl(){
        String browserurl ="nationstrust.com";
        WebElement url = driver.findElementById("com.nationstrust.mobilebanking:id/tvWebNtb");
        url.click();
        WebElement selectbrowser = driver.findElementById("com.huawei.android.internal.app:id/hw_button_always");
        selectbrowser.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        String browserurl1 = driver.findElementById("com.android.chrome:id/url_bar").getText();
        Assert.assertEquals(browserurl, browserurl1);


    }

    public static void americanurl(){
        String browserurl = "americanexpress.lk";
        WebElement url = driver.findElementById("com.nationstrust.mobilebanking:id/tvWebAmex");
        url.click();
        WebElement selectbrowser = driver.findElementById("com.huawei.android.internal.app:id/hw_button_always");
        selectbrowser.click();
        String browserurl1 = driver.findElementById("com.android.chrome:id/url_bar").getText();
        Assert.assertEquals(browserurl, browserurl1);
        driver.hideKeyboard();
    }





    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }


}
