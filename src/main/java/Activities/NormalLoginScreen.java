package Activities;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class NormalLoginScreen extends Configuration {

    public static void NormalLoginScreen_labels() throws Exception{
        String screenheading = "Welcome";
        String screensubheading = "New World of Mobile Banking";
        String placeholder2 = "Password";
        String placeholder1 = "Username";
        String forgotpassword = "Forgot Password?";
        String loginbuttoname  = "Login";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/welcomeText").getText();
            String screensubheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/homeDescription").getText();
            String usernameplaceholder = driver.findElementById("com.nationstrust.mobilebanking:id/usernameTextInput").getText();
            String passwordplaceholder = driver.findElementById("com.nationstrust.mobilebanking:id/passwordTextInput").getText();
            String forgotPinlabel = driver.findElementById("com.nationstrust.mobilebanking:id/forgotPin").getText();
            String loginbuttoname1 = driver.findElementById("com.nationstrust.mobilebanking:id/loginButton").getText();

            Assert.assertEquals(screenheading1, screenheading);
            Assert.assertEquals(screensubheading1,screensubheading );
            Assert.assertEquals(placeholder2, passwordplaceholder);
            Assert.assertEquals(placeholder1, usernameplaceholder);
            Assert.assertEquals(forgotpassword, forgotPinlabel);
            Assert.assertEquals(loginbuttoname, loginbuttoname1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void forgotpassword(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement forgotpassword= driver.findElementById("com.nationstrust.mobilebanking:id/forgotPin");
        forgotpassword.click();


    }



    public static void Login(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement login = driver.findElementById("com.nationstrust.mobilebanking:id/loginButton");
        login.click();
    }

    public static void Sign_Up(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement signup = driver.findElementById("com.nationstrust.mobilebanking:id/signUpBtn");
        String signuplabel = driver.findElementById("com.nationstrust.mobilebanking:id/signUpBtn").getText();
        Assert.assertEquals(signuplabel, "Sign Up");
        signup.click();

    }

    public static void quickaccessmenu(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/optionsBtn");
        quickaccessmenu.click();
    }
    public static void locationicon(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement locationicon = driver.findElementById("\tcom.nationstrust.mobilebanking:id/ivLocation");
        locationicon.click();
    }
    public static void loginbutton(){


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement loginbutton = driver.findElementById("com.nationstrust.mobilebanking:id/loginButton");
        loginbutton.click();

    }



    public static void notification(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement notification = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ImageView");
        notification.click();
    }


}

