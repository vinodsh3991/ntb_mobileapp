package Activities.LoginScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class NormalLoginScreen extends Configuration {

    public static void NormalLoginScreen_labels() throws Exception{
        String screenheading = "Welcome";
        String screensubheading = "New World of Mobile Banking";
        String placeholder2 = "Password";
        String forgotpasswordlabel = "Forgot Password?";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/welcomeText").getText();
            String screensubheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/homeDescription").getText();
            String placeholder22 = driver.findElementById("com.nationstrust.mobilebanking:id/pin").getText();
            String forgotpasswordlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/forgotPin").getText();
            WebElement locationicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivLocation");
            locationicon.isDisplayed();
            WebElement notificationicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ImageView");
            notificationicon.isDisplayed();
            WebElement quickaccessicon = driver.findElementById("com.nationstrust.mobilebanking:id/optionsBtn");
            quickaccessicon.isDisplayed();
            Assert.assertEquals(screenheading1, screenheading);
            Assert.assertEquals(screensubheading1, screensubheading);
            Assert.assertEquals(placeholder22, placeholder2);
            Assert.assertEquals(forgotpasswordlabel1, forgotpasswordlabel);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void forgotpassword(){
        String ForgotPassword = "Forgot Password?";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement forgotpassword = driver.findElementById("com.nationstrust.mobilebanking:id/forgotPin");
        String forgotpassword1 = driver.findElementById("com.nationstrust.mobilebanking:id/forgotPin").getText();
        Assert.assertEquals(forgotpassword1, ForgotPassword);
        forgotpassword.click();


    }


    public static void location(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement location = driver.findElementById("com.nationstrust.mobilebanking:id/ivLocation");
        location.click();
    }

    public static void notification(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement notification = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ImageView");
        notification.click();
    }

    public static void quickaccessmenu(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/optionsBtn");
        quickaccessmenu.click();
    }


    public static void loginbutton(){
        String Login = "Login";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement loginbutton = driver.findElementById("com.nationstrust.mobilebanking:id/pin");
        loginbutton.click();
        String Login1 = driver.findElementById("com.nationstrust.mobilebanking:id/pin").getText();
        Assert.assertEquals(Login1, Login);

        }



    public static void devicelocationpopup(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement allowbutton = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
        allowbutton.click();
    }


}
