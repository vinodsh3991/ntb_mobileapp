package Activities.LoginScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FristTimeLoginScreen extends Configuration {

    public static void FristTimeLoginScreen_labels() throws Exception{
        String screenheading = "Welcome";
        String screensubheading = "New World of Mobile Banking";
        String placeholder2 = "Password";
        String placeholder1 = "Username";
        String forgotusernamelabel = "Forgot Username?";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView[1]").getText();
            String screensubheading1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView[2]").getText();
            //String placeholder11 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/TextInputLayout[1]").getText();
           // String placeholder11 = driver.findElementById("com.nationstrust.mobilebanking:id/usernameTextInput").getText();
            //String placeholder22 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/TextInputLayout[2]").getText();
            String placeholder22 = driver.findElementById("com.nationstrust.mobilebanking:id/pin").getText();

            String forgotusernamelabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/forgotUsername").getText();

            System.out.println(screenheading1);
            Assert.assertEquals(screenheading1, screenheading);
            Assert.assertEquals(screensubheading1, screensubheading);
            //Assert.assertEquals(placeholder11, placeholder1);
            Assert.assertEquals(placeholder22, placeholder2);
            Assert.assertEquals(forgotusernamelabel1, forgotusernamelabel);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void forgotusername(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement forgotusername = driver.findElementById("com.nationstrust.mobilebanking:id/forgotUsername");
        String forgotusernamelabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/forgotUsername").getText();
        Assert.assertEquals(forgotusernamelabel1, "Forgot Username?");
        forgotusername.click();


    }


    public static void Sign_IN(){
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement signin = driver.findElementById("com.nationstrust.mobilebanking:id/signInBtn");
            String signinlabel = driver.findElementById("com.nationstrust.mobilebanking:id/signInBtn").getText();
            Assert.assertEquals(signinlabel, "Sign In");
            signin.click();
        }

    public static void Sign_Up(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement signup = driver.findElementById("com.nationstrust.mobilebanking:id/signUpBtn");
        String signuplabel = driver.findElementById("com.nationstrust.mobilebanking:id/signUpBtn").getText();
        Assert.assertEquals(signuplabel, "Sign Up");
        signup.click();

    }

    public static void backbutton(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementById("com.nationstrust.mobilebanking:id/backNavigation");
        backbutton.click();
    }
    public static void locationicon(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement locationicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivLocation");
        locationicon.click();
    }

    public static void quickaccessmenu(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/optionsBtn");
        quickaccessmenu.click();
    }


    public static void loginbutton(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement loginbutton = driver.findElementById("com.nationstrust.mobilebanking:id/loginButton");
        loginbutton.click();

        }



    public static void devicelocationpopup(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement allowbutton = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
        allowbutton.click();
    }


}
