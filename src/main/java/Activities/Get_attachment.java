package Activities;

import TestBase.Configuration;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class Get_attachment extends Configuration {

    public static void 	attachimage(){
        WebElement attachimage = driver.findElementById("com.nationstrust.mobilebanking:id/attachment");
        attachimage.click();

        try {
            if(driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]").isDisplayed()){
                WebElement chooseimage = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]");
                chooseimage.click();

                try{
                    if(driver.findElementById("com.android.packageinstaller:id/permission_allow_button").isDisplayed()){

                        WebElement allow = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
                        allow.click();
                        WebElement selectphotos = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.view.ViewPager/android.widget.GridView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.ImageView");
                        selectphotos.click();
                        WebElement selectfolder = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.view.View");
                        selectfolder.click();

                        TouchAction touchAction = new TouchAction(driver);
                        touchAction.tap(PointOption.point(161, 523)).perform();

                        WebElement attachedimage= driver.findElementById("com.nationstrust.mobilebanking:id/attachmentImage");
                        attachedimage.isDisplayed();
                    }

                }catch(NoSuchElementException e){

                    WebElement selectimagesfolder = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView");
                    selectimagesfolder.click();
                    WebElement selectcameraimages = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.GridView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.ImageView");
                    selectcameraimages.click();
//                    TouchAction touchAction = new TouchAction(driver);
//                    touchAction.tap(PointOption.point(246, 374)).perform();
                    WebElement selectimage= driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.GridView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView");
                    selectimage.click();
                }

            }

        }catch (NoSuchElementException error) {


        }






        }



    }


