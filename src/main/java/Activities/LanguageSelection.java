package Activities;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class LanguageSelection extends Configuration {

    public static void language_button_name() throws Exception{
        String englishbutton = "English";
        String sinhalabutton = "සිංහල";
        String tamilbutton = "தமிழ்";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String button_name1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button[1]").getText();
            String button_name2 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button[2]").getText();
            String button_name3 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button[3]").getText();
            System.out.println(button_name1);
            Assert.assertEquals(button_name1, englishbutton);
            System.out.println(button_name2);
            Assert.assertEquals(button_name2, sinhalabutton);
            System.out.println(button_name3);
            Assert.assertEquals(button_name3, tamilbutton);
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void english_button() throws Exception {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement english_button = driver.findElementById("com.nationstrust.mobilebanking:id/englishButton");
        english_button.click();
    }
    public static void sinhala_button() throws Exception {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement sinhala_button = driver.findElementById("com.nationstrust.mobilebanking:id/sinhalaButton");
        sinhala_button.click();
    }

    public static void tamil_button() throws Exception {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement tamil_button = driver.findElementById("com.nationstrust.mobilebanking:id/tamilButton");
        tamil_button.click();
    }



}
