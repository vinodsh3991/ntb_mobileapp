package Activities.SavingAccountOpening;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SavingAccountOpeningScreen extends Configuration {
    public static void  SavingAccountOpeningScreen_title() throws Exception{
        String screenheading = "Savings Account Opening";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	enteracctype(){
        String dropdowntitle="Select Account Type";
        String NationsSaver="Nations Saver";
        String NationsMaxBonus="Nations MaxBonus";
        String NationsMegaSaver="Nations Mega Saver";
        String NationsPrabbuddha="Nations Prabbuddha";
        String NationsTaxPlanner ="Nations Tax Planner";

        WebElement enteracctype = driver.findElementById("com.nationstrust.mobilebanking:id/etAccType");
        enteracctype.click();

        String accountypedropdowntitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(dropdowntitle, accountypedropdowntitle1);

        String NationsSaver1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView").getText();
        Assert.assertEquals(NationsSaver, NationsSaver1);
        String NationsMaxBonus1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView").getText();
        Assert.assertEquals(NationsMaxBonus, NationsMaxBonus1);
        String NationsMegaSaver1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView").getText();
        Assert.assertEquals(NationsMegaSaver, NationsMegaSaver1);
        String NationsTaxPlanner1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView").getText();
        Assert.assertEquals(NationsTaxPlanner, NationsTaxPlanner1);
    }

    public static void 	selectNationSaver(){
        //WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView");
        WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        enteracctype.click();
    }
    public static void 	selectNationsMaxBonus(){
       // WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView");
        WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout");
        enteracctype.click();
    }
    public static void 	selectNationsMegaSaver(){
        //WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView");
        WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.LinearLayout");
        enteracctype.click();
    }
    public static void 	selectNationsPrabbuddha(){
        WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView");
        enteracctype.click();
    }
    public static void 	selectNationsTaxPlanner(){
        //WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView");
        WebElement enteracctype = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout");
        enteracctype.click();
    }





    public static void 	enterfundingaccount(){
        String dropdowntitle="Select Funding Account";
        String floatinglabel="Funding Account";

        String floatinglabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/etFundingAccount").getText();
        Assert.assertEquals(floatinglabel,floatinglabel1);

        WebElement enterfundingaccount = driver.findElementById("com.nationstrust.mobilebanking:id/etFundingAccount");
        enterfundingaccount.click();

        String dropdwontitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(dropdowntitle, dropdwontitle1);

       // WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView");
        WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]/android.widget.LinearLayout");
        selectaccount.click();

        WebElement accountbalcelabel = driver.findElementById("com.nationstrust.mobilebanking:id/tvAccountBalance");
        accountbalcelabel.isDisplayed();



    }

    public static void 	enterbranch(){
        String dropdowntitle="Select Branch";
        String floatinglabel="Branch";

        String floatinglabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/etBranch").getText();
        Assert.assertEquals(floatinglabel,floatinglabel1);

        WebElement enterbranch = driver.findElementById("com.nationstrust.mobilebanking:id/etBranch");
        enterbranch.click();

        String dropdwontitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(dropdowntitle, dropdwontitle1);

        WebElement selectbranch = driver.findElementById("com.nationstrust.mobilebanking:id/tvBankName");
        selectbranch.click();

//        String floatinglabel1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[3]").getText();
//        Assert.assertEquals(floatinglabel,floatinglabel1);

    }


    public static void 	enteramount(String amount){
        String dropdowntitle="Minimum of LKR 2,000.00";
        String floatinglabel="Amount (LKR)";

//        String floatinglabel1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[4]").getText();
//        Assert.assertEquals(floatinglabel,floatinglabel1);

        WebElement enteramount = driver.findElementById("com.nationstrust.mobilebanking:id/etAmount");
        enteramount.click();
        enteramount.sendKeys(amount);
        driver.hideKeyboard();


    }

    public static void 	enterpurpose(){
        String dropdowntitle="Select Purpose";
        String floatinglabel="Purpose";

        String floatinglabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/etPurpose").getText();
        Assert.assertEquals(floatinglabel,floatinglabel1);

        WebElement enterpurpose = driver.findElementById("com.nationstrust.mobilebanking:id/etPurpose");
        enterpurpose.click();

        String dropdwontitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(dropdowntitle, dropdwontitle1);

        WebElement selectpurpose= driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectpurpose.click();



    }




    public static void 	confirm(){
        WebElement confirm = driver.findElementById("com.nationstrust.mobilebanking:id/btnConfirm");
        confirm.click();

    }
    public static void 	cancel(){
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/buttonCancel");
        cancel.click();

    }


    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}
