package Activities.SavingAccountOpening;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class OpenAccountsScreen extends Configuration {
    public static void  OpenAccountsScreen_title() throws Exception{
        String screenheading = "Open Accounts";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  OpenAccountsScreen_labels() throws Exception{
        String 	FixedDeposit = "Fixed Deposit";
        String 	SavingsAccount = "Savings Account";


       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String FixedDeposit1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            Assert.assertEquals(FixedDeposit, FixedDeposit1);
            String SavingsAccount1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.TextView").getText();
            Assert.assertEquals(SavingsAccount, SavingsAccount1);
            WebElement FDicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.ImageView[1]");
            FDicon.isDisplayed();
            WebElement savingicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.ImageView[1]");
            savingicon.isDisplayed();
            WebElement quickaccessicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
            quickaccessicon.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }



    public static void 	quickaccessmenu(){
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenu.click();
    }
    public static void 	fdopen(){
        WebElement fdopen = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.TextView");
        fdopen.click();

    }
    public static void 	savingaccopen(){
        WebElement savingaccopen = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.TextView");
        savingaccopen.click();

    }


    public static void back(){
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}
