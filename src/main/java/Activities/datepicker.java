package Activities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class datepicker {
    // Pass monthName param as "August"
    public static int getMonthNumber(String monthName) throws ParseException {
        Date date = new SimpleDateFormat("MMMM").parse(monthName);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        System.out.println(calendar.get(Calendar.MONTH) + 1);
        return calendar.get(Calendar.MONTH) + 1;
    }

    // Pass date param as "Sun, Jul 1"
    public static String getMonthNameInThreeChars(String date) {
        return date.substring(5, 8);
    }
}


