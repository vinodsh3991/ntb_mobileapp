package Activities;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class Selectbrowser extends Configuration {

    public static void Selectbrowser(){

        WebElement selectbrowser = driver.findElementById("com.huawei.android.internal.app:id/hw_button_always");
        selectbrowser.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }
}
