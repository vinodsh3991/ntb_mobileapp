package Activities.BillPayment;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class OneTimePaymentScreen extends Configuration {
    public static void OneTimePaymentScreen_lables() throws Exception{
        String screenheading = "One Time Payment";
        String category = "Category";
        String Biller = "Biller";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading1, screenheading);
            String category1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvCategoryTitle").getText();
            Assert.assertEquals(category1, category);
            String Biller1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvProviderTitle").getText();
            Assert.assertEquals(Biller1, Biller);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void selectcategory(){
        WebElement selectcategory = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.TextView[2]");
        selectcategory.click();

    }

    public static void selectbiller(){
        WebElement selectbiller = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.widget.TextView[1]");
        selectbiller.click();

         WebElement textfiled = driver.findElementById("com.nationstrust.mobilebanking:id/dynamicEditText1");
        textfiled.isDisplayed();


    }

    public static void eneteraccnumber(String accountnumber){
        String placeholder = "10 Digit Account Number";
        WebElement textfiled = driver.findElementById("com.nationstrust.mobilebanking:id/dynamicEditText1");
        textfiled.click();

        String placeholder1 = driver.findElementById("com.nationstrust.mobilebanking:id/dynamicTextInput1").getText();
        Assert.assertEquals(placeholder1, placeholder);

        textfiled.sendKeys(accountnumber);
        driver.hideKeyboard();

    }

    public static void backbutton(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }
    public static void confirm(){
        WebElement confirm = driver.findElementById("com.nationstrust.mobilebanking:id/addBillerButton");
        confirm.click();
    }
    public static void cancel(){
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/addBillerCancelButton");
        cancel.click();
    }


}

