package Activities.BillPayment;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class OneTimeBillPaymentSuccessScreen extends Configuration {
    public static void OneTimeBillPaymentSuccessScreen_title() throws Exception {
        String screenheading = "Bill Payment Successful";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvStatus").getText();
            Assert.assertEquals(screenheading1, screenheading);


        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void OneTimeBillPaymentSuccessScreen_labels() throws Exception {
        String emaillabel = "A copy of receipt sent to";
        String payfrom = "Paid From";
        String payto = "Paid To";
        String date = "Date";
        String remarks = "Remarks";
        String servicecharge = "Service Charge";
        String RefNumber = "Ref. Number";
        String amount = "Amount";
        String savereceipt2 = "Save Receipt";

        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String emaillabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvEmailHeader").getText();
            Assert.assertEquals(emaillabel, emaillabel1);
            String payfrom1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2").getText();
            Assert.assertEquals(payfrom1, payfrom);
            String payto1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay1").getText();
            Assert.assertEquals(payto, payto1);
            String date1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvDate").getText();
            Assert.assertEquals(date, date1);
            String remarks1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvRemark").getText();
            Assert.assertEquals(remarks, remarks1);
            String servicecharge1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView").getText();
            Assert.assertEquals(servicecharge, servicecharge1);
            String 	amount1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView").getText();
            Assert.assertEquals(amount1, amount);
            String savereceipt1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();
            Assert.assertEquals(savereceipt1, savereceipt2);
            WebElement tickicon = driver.findElementById("com.nationstrust.mobilebanking:id/statusImage");
            tickicon.isDisplayed();

            WebElement billername = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay1Name");
            billername.isDisplayed();
            WebElement billeraccnumber = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay1Ref");
            billeraccnumber.isDisplayed();
            WebElement accountname = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2Name");
            accountname.isDisplayed();
            WebElement accountnumber = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2Ref");
            accountnumber.isDisplayed();
            WebElement AmountRupees = driver.findElementById("com.nationstrust.mobilebanking:id/tvAmountRupees");
            AmountRupees.isDisplayed();
            WebElement AmountCents = driver.findElementById("com.nationstrust.mobilebanking:id/tvAmountCents");
            AmountCents.isDisplayed();
            WebElement ServiceCharge = driver.findElementById("com.nationstrust.mobilebanking:id/tvServiceCharge");
            ServiceCharge.isDisplayed();
            WebElement ServiceChargeCents = driver.findElementById("com.nationstrust.mobilebanking:id/tvServiceChargeCents");
            ServiceChargeCents.isDisplayed();
            WebElement dateandtime = driver.findElementById("com.nationstrust.mobilebanking:id/tvDateText");
            dateandtime.isDisplayed();
            WebElement RemarkText = driver.findElementById("com.nationstrust.mobilebanking:id/tvRemarkText");
            RemarkText.isDisplayed();
            MobileElement scrolltoend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Ref. Number\"));");
            String 	RefNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvRef").getText();
            Assert.assertEquals(RefNumber1, RefNumber);
            WebElement RefText = driver.findElementById("com.nationstrust.mobilebanking:id/tvRefText");
            RefText.isDisplayed();


        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void homebutton() {
        String homebutton2 ="Home";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement homebutton = driver.findElementById("com.nationstrust.mobilebanking:id/anotherPaymentButton");
        String homebutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/anotherPaymentButton").getText();
        Assert.assertEquals(homebutton1, homebutton2);
        homebutton.click();


    }


    public static void 	savebiller() {
        String savebiller2 = "Save Biller";
        WebElement savebiller = driver.findElementById("com.nationstrust.mobilebanking:id/homeButton");
        String savebiller1 = driver.findElementById("com.nationstrust.mobilebanking:id/homeButton").getText();
        Assert.assertEquals(savebiller1, savebiller2);
        savebiller.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void savereceipt() {

      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement savereceipt = driver.findElementById("com.nationstrust.mobilebanking:id/llSaveReceipt");
        savereceipt.click();
    }
}
