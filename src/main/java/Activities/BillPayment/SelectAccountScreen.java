package Activities.BillPayment;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SelectAccountScreen extends Configuration {
    public static void SelectAccountScreen_lables() throws Exception{
        String screenheading = "Select Account";
        String screensubheading = "Accounts";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading1, screenheading);
            String screensubheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvAccounts").getText();
            Assert.assertEquals(screensubheading1, screensubheading);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void quickaccessmenuicon(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenuicon.click();


    }
    public static void backbutton(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }
    public static void selectaccount(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.FrameLayout");
        selectaccount.click();
    }


}

