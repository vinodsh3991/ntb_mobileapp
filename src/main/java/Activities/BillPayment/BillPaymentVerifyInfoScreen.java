package Activities.BillPayment;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class BillPaymentVerifyInfoScreen extends Configuration {
    public static void BillPaymentVerifyInfoScreen_title() throws Exception {
        String screenheading = "Verify Information";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading1, screenheading);


        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void BillPaymentVerifyInfoScreen_labels() throws Exception {
        String payfrom = "Pay From";
        String payto = "Pay To";
        String date = "Date";
        String remarks = "Remarks";
        String servicecharge = "Service Charge";
        String LKR = "LKR";

       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String payfrom1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPayFrom").getText();
            Assert.assertEquals(payfrom, payfrom1);
            String LKR1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvAmount").getText();
            Assert.assertEquals(LKR1, LKR);
            String payto1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPayTo").getText();
            Assert.assertEquals(payto, payto1);
            String date1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvDate").getText();
            Assert.assertEquals(date, date1);
            String remarks1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvRemark").getText();
            Assert.assertEquals(remarks, remarks1);
            String servicecharge1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView22").getText();
            Assert.assertEquals(servicecharge, servicecharge1);


        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void backbutton() {
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void confirmbutton() {
        String confirmbutton2 = "Confirm";
        WebElement confirmbutton = driver.findElementById("com.nationstrust.mobilebanking:id/payConfirmButton");
        String  confirmbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/payConfirmButton").getText();
        Assert.assertEquals(confirmbutton1, confirmbutton2);
        confirmbutton.click();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    public static void cancel() {
        String cancel2 = "Cancel";
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/payCancelButton");
        String  cance1 = driver.findElementById("com.nationstrust.mobilebanking:id/payCancelButton").getText();
        Assert.assertEquals(cance1, cancel2);
        cancel.click();
    }
}
