package Activities.BillPayment;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class BillPaymentFillingScreen extends Configuration {

    public static void BillPaymentFillingScreen_title() throws Exception{
        String screenheading = "Bill Payment";

      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading1, screenheading);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void BillPaymentFillingScreen_labels() throws Exception{
        String PayTo = "Pay To";
        String 	SelectBiller = "Select Biller";
        String PayFrom = "Pay From";
        String SelectAccount = "Select Account";
        String 	Amount = "Amount";
        String LKR = "LKR";
        String ooo = "0.00";
        String Remarks = "Remarks";
        String maximum = "(Maximum 30 alphanumeric)";

      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String PayTo1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(PayTo1,PayTo );
            String SelectBiller1 = driver.findElementById("com.nationstrust.mobilebanking:id/selectPayee").getText();
            Assert.assertEquals(SelectBiller1, SelectBiller);
            String PayFrom1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(PayFrom1,PayFrom );
            String SelectAccount1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvSelectAccount").getText();
            Assert.assertEquals(SelectAccount1, SelectAccount);
            String 	Amount1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(Amount1,Amount);
            String LKR1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvCurrency").getText();
            Assert.assertEquals(LKR1,LKR);
            String ooo1 = driver.findElementById("com.nationstrust.mobilebanking:id/paymentAmount").getText();
            Assert.assertEquals(ooo1,ooo );
            String Remarks1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(Remarks1, Remarks);
            String maximum1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]").getText();
            Assert.assertEquals(maximum1,maximum );
            String Remarks2 = driver.findElementById("com.nationstrust.mobilebanking:id/etRemarks").getText();
            Assert.assertEquals(Remarks2,Remarks );
            WebElement billerarrow = driver.findElementById("com.nationstrust.mobilebanking:id/billerArrow");
            billerarrow.isDisplayed();
            WebElement accountarrow = driver.findElementById("com.nationstrust.mobilebanking:id/ivSelectAccountNext");
            accountarrow.isDisplayed();
            WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
            quickaccessmenu.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void quickaccessmenuicon(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenuicon.click();


    }
    public static void backbutton(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }
    public static void paybutton(){
        String pay ="Pay";
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        String pay1 = driver.findElementById("com.nationstrust.mobilebanking:id/btnPay").getText();
        Assert.assertEquals(pay1,pay );
        WebElement paybutton = driver.findElementById("com.nationstrust.mobilebanking:id/btnPay");
        paybutton.click();
    }

    public static void payfrom(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement payfrom = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.ImageView");
        payfrom.click();
    }
    public static void payto(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement paybutton = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout");
        paybutton.click();
    }
    public static void amount(String amount){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement amount1 = driver.findElementById("com.nationstrust.mobilebanking:id/paymentAmount");
        amount1.click();
        amount1.sendKeys(amount);
        driver.hideKeyboard();
    }
    public static void remarks(String remarks){
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement remarks1 = driver.findElementById("com.nationstrust.mobilebanking:id/etRemarks");
        remarks1.click();
        remarks1.sendKeys(remarks);
        driver.hideKeyboard();

    }
}
