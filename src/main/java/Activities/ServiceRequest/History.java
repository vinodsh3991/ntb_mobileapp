package Activities.ServiceRequest;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class History extends Configuration {
    public static void History_title() throws Exception{
        String screenheading = "History";
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void History_labels() throws Exception{
        String ChequeBookFor = "Cheque Book For";


       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String ChequeBookFor1 = driver.findElementById("com.nationstrust.mobilebanking:id/chequeBookDescp").getText();
            Assert.assertEquals(ChequeBookFor, ChequeBookFor1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void emptyscreen(){
        String emptyscreenlabel1= "You do not have any current accounts to make this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/emptyChequeBook").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView");
        imageicon.isDisplayed();
    }






    public static void back(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }

}


