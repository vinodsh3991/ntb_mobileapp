package Activities.ServiceRequest;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class RequestAccountStatement extends Configuration {

    public static void RequestAccountStatement_title() throws Exception{
        String screenheading = "Request Account Statement";

        //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void RequestAccountStatement_labels() throws Exception{
        String eStatementDescp = "Statement For";
        String StatementDuration = "Statement Duration";
        String CollectionType = "Collection Type";
        String Email = "Email";
        String CorrespondenceAddress = "Correspondence Address";
        String 	Branch = "Branch";
        String 	CollectFromBranch = "Collect From Branch";
        String SelectBranch = "Select Branch";
        String Account = "Account";
        String From = "From";
        String To = "To";

       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String eStatementDescp1 = driver.findElementById("com.nationstrust.mobilebanking:id/eStatementDescp").getText();
            String StatementDuration1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[2]").getText();
            String 	CollectionType1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[3]").getText();
            String Email1 = driver.findElementById("com.nationstrust.mobilebanking:id/emailText").getText();
            String CorrespondenceAddress1 = driver.findElementById("com.nationstrust.mobilebanking:id/correspondenceAddressText").getText();
            String 	Branch1 = driver.findElementById("com.nationstrust.mobilebanking:id/branchText").getText();
            String CollectFromBranch1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
            String SelectBranch1 = driver.findElementById("com.nationstrust.mobilebanking:id/branchET").getText();
            String Account1 = driver.findElementById("com.nationstrust.mobilebanking:id/accountNoET").getText();
            String From1 = driver.findElementById("com.nationstrust.mobilebanking:id/fromDateET").getText();
            String To1 = driver.findElementById("com.nationstrust.mobilebanking:id/toDateET").getText();

            Assert.assertEquals(eStatementDescp, eStatementDescp1);
            Assert.assertEquals(StatementDuration, StatementDuration1);
            Assert.assertEquals(CollectionType, CollectionType1);
            Assert.assertEquals(Email, Email1);
            Assert.assertEquals(CorrespondenceAddress, CorrespondenceAddress1);
            Assert.assertEquals(Branch, Branch1);
            Assert.assertEquals(CollectFromBranch, CollectFromBranch1);
            Assert.assertEquals(SelectBranch, SelectBranch1);
            Assert.assertEquals(Account, Account1);
            Assert.assertEquals(From, From1);
            Assert.assertEquals(To, To1);


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void submit(){

        WebElement submit = driver.findElementById("com.nationstrust.mobilebanking:id/submitButton");
        submit.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    public static void Accountlist(){
        WebElement Accountlist = driver.findElementById("com.nationstrust.mobilebanking:id/getAcocuntBtn");
        Accountlist.click();
        WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectaccount.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    public static void email(){
        WebElement email = driver.findElementById("com.nationstrust.mobilebanking:id/emailCheckBox");
        email.click();

    }
    public static void 	correspondenceaddress(){
        WebElement correspondenceaddress = driver.findElementById("com.nationstrust.mobilebanking:id/addressCheckBox");
        correspondenceaddress.click();

    }

    public static void selectbranch(){
        WebElement selectbranch = driver.findElementById("com.nationstrust.mobilebanking:id/branchET");
        selectbranch.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement searchbrach = driver.findElementById("com.nationstrust.mobilebanking:id/etSearch");
        searchbrach.click();
        searchbrach.sendKeys("Matara");
        driver.hideKeyboard();
        WebElement setbrach = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout");
        setbrach.click();
    }

    public static void fromdate(){
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement fromdate = driver.findElementById("com.nationstrust.mobilebanking:id/fromDateET");
        fromdate.click();

//        WebElement selectyear = driver.findElementById("android:id/date_picker_header_year");
//        selectyear.click();
//        WebElement taponyear = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.DatePicker/android.widget.LinearLayout/android.widget.ViewAnimator/android.widget.ListView/android.widget.TextView[2]");
//        taponyear.click();
//        WebElement selectdate = driver.findElementByXPath("//android.view.View[@content-desc=\"01 April 2018\"]");
//        selectdate.click();
//        WebElement okbutton = driver.findElementById("android:id/button1");
//        okbutton.click();

    }
    public static void todate(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement todate = driver.findElementById("com.nationstrust.mobilebanking:id/toDateET");
        todate.click();
//        WebElement selectdate = driver.findElementByXPath("//android.view.View[@content-desc=\"16 July 2018\"]");
//        selectdate.click();
//        WebElement okbutton = driver.findElementById("android:id/button1");
//        okbutton.click();

    }


    public static void emptyscreen(){
        String emptyscreenlabel1= "You do not have any accounts to facilitate this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/tvEmptyDesc").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementById("com.nationstrust.mobilebanking:id/imageView14");
        imageicon.isDisplayed();
    }

    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}



