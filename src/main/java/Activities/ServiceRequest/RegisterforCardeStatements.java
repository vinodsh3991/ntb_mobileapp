package Activities.ServiceRequest;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class RegisterforCardeStatements extends Configuration {

    public static void RegisterforCardeStatements_title() throws Exception{
        String screenheading = "Register for Account e-Statements";

        //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void RegisterforCardeStatements_labels() throws Exception{
        String emailaddress = "Preferred Email Address";
        String SelectAccount = "Select Account";


       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String emailaddress1 = driver.findElementById("com.nationstrust.mobilebanking:id/preferredEmail").getText();
            Assert.assertEquals(emailaddress1, emailaddress);
            WebElement email = driver.findElementById("com.nationstrust.mobilebanking:id/emailAddressET");
            email.isDisplayed();
            String SelectAccount1 = driver.findElementById("com.nationstrust.mobilebanking:id/selectCardText").getText();
            Assert.assertEquals(SelectAccount1, SelectAccount);

            WebElement accountname = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]");
            accountname.isDisplayed();
            WebElement accountnumber = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]");
            accountnumber.isDisplayed();
            WebElement checkboxicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.CheckBox");
            checkboxicon.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void submit(){

        WebElement submit = driver.findElementById("com.nationstrust.mobilebanking:id/submtButton");
        submit.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public static void emptyscreen(){
        String emptyscreenlabel1= "You do not have any cards to facilitate this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/tvEmptyDesc").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementById("com.nationstrust.mobilebanking:id/imageView14");
        imageicon.isDisplayed();
    }

    public static void selectaccount(){
        String emptyscreenlabel1= "You do not have any accounts to facilitate this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/tvEmptyDesc").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementById("com.nationstrust.mobilebanking:id/imageView14");
        imageicon.isDisplayed();
    }

    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}



