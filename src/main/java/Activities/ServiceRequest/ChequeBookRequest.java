package Activities.ServiceRequest;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ChequeBookRequest extends Configuration {
    public static void ChequeBookRequest_title() throws Exception{
        String screenheading = "Cheque Book Request";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void ChequeBookRequest_labels() throws Exception{
        String ChequeBookFor = "Cheque Book For";
        String CurrentAccount = "Current Account";
        String ChequeLeaves = "Cheque Leaves";
        String ChequeLeaves2 = "Cheque Leaves";
        String CollectionPoint = "Collection Point";
        String CorrespondenceAddress = "Correspondence Address";
        String 	Branch = "Branch";
        String CollectFromBranch = "Collect From Branch";
        String SelectBranch = "Select Branch";




       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String ChequeBookFor1 = driver.findElementById("com.nationstrust.mobilebanking:id/chequeBookDescp").getText();
            Assert.assertEquals(ChequeBookFor, ChequeBookFor1);
            String CurrentAccount1 = driver.findElementById("com.nationstrust.mobilebanking:id/currentAccountNoET").getText();
            Assert.assertEquals(CurrentAccount, CurrentAccount1);
            String 	ChequeLeaves1 = driver.findElementById("com.nationstrust.mobilebanking:id/chequeLeavesDescp").getText();
            Assert.assertEquals(ChequeLeaves, ChequeLeaves1);
            String 	ChequeLeaves22 = driver.findElementById("com.nationstrust.mobilebanking:id/chequeLeavesNoET").getText();
            Assert.assertEquals(ChequeLeaves2, ChequeLeaves22);
            String 	CollectionPoint1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[3]").getText();
            Assert.assertEquals(CollectionPoint, CollectionPoint1);
            String 	CorrespondenceAddress1 = driver.findElementById("com.nationstrust.mobilebanking:id/correspondenceText").getText();
            Assert.assertEquals(CorrespondenceAddress, CorrespondenceAddress1);
            String 	Branch1 = driver.findElementById("com.nationstrust.mobilebanking:id/branchText").getText();
            Assert.assertEquals(Branch, Branch1);
            String 	CollectFromBranch1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(CollectFromBranch, CollectFromBranch1);
            String 	SelectBranch1 = driver.findElementById("com.nationstrust.mobilebanking:id/branchET").getText();
            Assert.assertEquals(SelectBranch, SelectBranch1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void emptyscreen(){
        String emptyscreenlabel1= "You do not have any current accounts to make this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/emptyChequeBook").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView");
        imageicon.isDisplayed();
    }
    public static void submit(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement submit = driver.findElementById("com.nationstrust.mobilebanking:id/submitButton");
        submit.click();
    }
    public static void Accountlist(){
        WebElement Accountlist = driver.findElementById("com.nationstrust.mobilebanking:id/getAcocuntBtn");
        Accountlist.click();
        WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectaccount.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void Chequenumberist(){
        WebElement Chequenumberist = driver.findElementById("com.nationstrust.mobilebanking:id/getChequeLeavesBtn");
        Chequenumberist.click();
        WebElement selectChequenumber = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectChequenumber.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void 	correspondenceaddress(){
        WebElement correspondenceaddress = driver.findElementById("com.nationstrust.mobilebanking:id/addressCheckBox");
        correspondenceaddress.click();

    }

    public static void selectbranch(){
        WebElement selectbranch = driver.findElementById("com.nationstrust.mobilebanking:id/branchET");
        selectbranch.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement searchbrach = driver.findElementById("com.nationstrust.mobilebanking:id/etSearch");
        searchbrach.click();
        searchbrach.sendKeys("Matara");
        driver.hideKeyboard();
        WebElement setbrach = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout");
        setbrach.click();
    }



    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }

}
