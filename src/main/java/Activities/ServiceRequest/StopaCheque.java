package Activities.ServiceRequest;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class StopaCheque extends Configuration {
    public static void StopaCheque_title() throws Exception{
        String screenheading = "Stop a Cheque";

      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();

            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void StopaCheque_labels() throws Exception{
        String SelectCurrentAccount = "Select Current Account";
        String 	CurrentAccount = "Current Account";
        String ChequeNumber = "Cheque Number";
        String Reason = "Reason";
        String SelectReason = "Select Reason";
        String note = "Already presented cheque cannot be stopped. Amount of LKR 2,000 will be charged to your selected account. If funds not available for cheque value LKR 4,000 will be charged";


        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String SelectCurrentAccount1 = driver.findElementById("com.nationstrust.mobilebanking:id/chequeBookDescp").getText();
            Assert.assertEquals(SelectCurrentAccount, SelectCurrentAccount1);
            String CurrentAccount1 = driver.findElementById("com.nationstrust.mobilebanking:id/currentAccountNoET").getText();
            Assert.assertEquals(CurrentAccount, CurrentAccount1);
            String ChequeNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/chequeNoText").getText();
            Assert.assertEquals(ChequeNumber, ChequeNumber1);
            String Reason1 = driver.findElementById("com.nationstrust.mobilebanking:id/reasonDescp").getText();
            Assert.assertEquals(Reason, Reason1);
            String SelectReason1 = driver.findElementById("com.nationstrust.mobilebanking:id/reasonET").getText();
            Assert.assertEquals(SelectReason, SelectReason1);
            String note1 = driver.findElementById("com.nationstrust.mobilebanking:id/stopChequeDisclaimer").getText();
            Assert.assertEquals(note, note1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void emptyscreen(){
        String emptyscreenlabel1= "You do not have any current accounts to make this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/emptyChequeBook").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView");
        imageicon.isDisplayed();
    }
    public static void StopaCheque(){
        String StopaCheque2 = "Stop a Cheque";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        String 	StopaCheque1 = driver.findElementById("com.nationstrust.mobilebanking:id/stopChequeButton").getText();
        Assert.assertEquals(StopaCheque2, StopaCheque1);
        WebElement 	StopaCheque = driver.findElementById("com.nationstrust.mobilebanking:id/stopChequeButton");
        StopaCheque.click();
    }
    public static void Accountlist(){
        WebElement Accountlist = driver.findElementById("com.nationstrust.mobilebanking:id/getAcocuntBtn");
        Accountlist.click();
        WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectaccount.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void ReasonList(){
        WebElement ReasonList = driver.findElementById("com.nationstrust.mobilebanking:id/getReasonLayout");
        ReasonList.click();
        WebElement selectreason = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout");
        selectreason.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void enterchequenbr(String chequenumber){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement 	enterchequenbr = driver.findElementById("com.nationstrust.mobilebanking:id/chequeNoET");
        enterchequenbr.sendKeys(chequenumber);
    }
    public static void back(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}



