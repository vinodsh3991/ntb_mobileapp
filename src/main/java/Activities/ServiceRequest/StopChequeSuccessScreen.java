package Activities.ServiceRequest;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class StopChequeSuccessScreen extends Configuration {

    public static void StopChequeSuccessScreen_labels() throws Exception{
        String StopChequeReceipt = "Stop Cheque Receipt";
        String 	message = "Your cheque has been stopped successfully.";
        String 	AccountNumber = "Account Number";
        String 	ChequeNumber = "Cheque Number";
        String Reason = "Reason";
        String Date = "Date";
        String ServiceCharge = "Service Charge";


      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String StopChequeReceipt1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView8").getText();
            Assert.assertEquals(StopChequeReceipt, StopChequeReceipt1);
            String message1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView13").getText();
            Assert.assertEquals(message, message1);
            String AccountNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView14").getText();
            Assert.assertEquals(AccountNumber, AccountNumber1);
            String ChequeNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView19").getText();
            Assert.assertEquals(ChequeNumber, ChequeNumber1);
            String Reason1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView25").getText();
            Assert.assertEquals(Reason, Reason1);
            String Date1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView27").getText();
            Assert.assertEquals(Date, Date1);
            String ServiceCharge1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView28").getText();
            Assert.assertEquals(ServiceCharge, ServiceCharge1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void home(){
        WebElement home = driver.findElementById("com.nationstrust.mobilebanking:id/btnGoToHome");
        home.click();
    }

    public static void imageicon(){
        WebElement home = driver.findElementById("com.nationstrust.mobilebanking:id/imageView16");
        home.isDisplayed();
    }

    public static void anothercheque(){
        WebElement anothercheque = driver.findElementById("com.nationstrust.mobilebanking:id/btnStopAnotherCheque");
        anothercheque.click();
    }

}
