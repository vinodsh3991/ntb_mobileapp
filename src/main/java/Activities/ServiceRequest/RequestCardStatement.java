package Activities.ServiceRequest;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class RequestCardStatement extends Configuration {

    public static void RequesCardStatement_title() throws Exception{
        String screenheading = "Request Card Statement";

      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void RequesCardStatement_labels() throws Exception{
        String eStatementDescp = "Statement For";
        String StatementDuration = "Statement Duration";
        String CollectionType = "Collection Type";
        String Email = "Email";
        String CorrespondenceAddress = "Correspondence Address";
        String 	Branch = "Branch";
        String 	CollectFromBranch = "Collect From Branch";
        String SelectBranch = "Select Branch";
        String Card = "Card";
        String From = "From";
        String To = "To";

       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String eStatementDescp1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            String StatementDuration1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[2]").getText();
            String 	CollectionType1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[3]").getText();
            String Email1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView").getText();
            String CorrespondenceAddress1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView").getText();
            String 	Branch1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.TextView").getText();
            String CollectFromBranch1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
            String SelectBranch1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText").getText();
            String Card1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.EditText").getText();
            String From1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.EditText[1]").getText();
            String To1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.EditText[2]").getText();


            Assert.assertEquals(eStatementDescp, eStatementDescp1);
            Assert.assertEquals(StatementDuration, StatementDuration1);
            Assert.assertEquals(CollectionType, CollectionType1);
            Assert.assertEquals(Email, Email1);
            Assert.assertEquals(CorrespondenceAddress, CorrespondenceAddress1);
            Assert.assertEquals(Branch, Branch1);
            Assert.assertEquals(CollectFromBranch, CollectFromBranch1);
            Assert.assertEquals(SelectBranch, SelectBranch1);
            Assert.assertEquals(Card, Card1);
            Assert.assertEquals(From, From1);
            Assert.assertEquals(To, To1);


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void emptyscreen(){
        String emptyscreenlabel1= "You do not have any cards to facilitate this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/tvEmptyDesc").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementById("com.nationstrust.mobilebanking:id/imageView14");
        imageicon.isDisplayed();
    }

    public static void submit(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement submit = driver.findElementById("com.nationstrust.mobilebanking:id/submitButton");
        submit.click();
    }
    public static void Cardlist(){
        WebElement Cardlist = driver.findElementById("com.nationstrust.mobilebanking:id/getAcocuntBtn");
        Cardlist.click();
        WebElement selectcard = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectcard.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void email(){
        WebElement email = driver.findElementById("com.nationstrust.mobilebanking:id/emailCheckBox");
        email.click();

    }
    public static void 	correspondenceaddress(){
        WebElement correspondenceaddress = driver.findElementById("com.nationstrust.mobilebanking:id/addressCheckBox");
        correspondenceaddress.click();

    }

    public static void selectbranch(){
        WebElement selectbranch = driver.findElementById("com.nationstrust.mobilebanking:id/branchET");
        selectbranch.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement searchbrach = driver.findElementById("com.nationstrust.mobilebanking:id/etSearch");
        searchbrach.click();
        searchbrach.sendKeys("Matara");
        driver.hideKeyboard();
        WebElement setbrach = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout");
        setbrach.click();
    }

    public static void fromdate(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement fromdate = driver.findElementById("com.nationstrust.mobilebanking:id/fromDateET");
        fromdate.click();

//        WebElement selectyear = driver.findElementById("android:id/date_picker_header_year");
//        selectyear.click();
//        WebElement taponyear = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.DatePicker/android.widget.LinearLayout/android.widget.ViewAnimator/android.widget.ListView/android.widget.TextView[2]\n");
//        taponyear.click();
//        WebElement selectdate = driver.findElementByXPath("//android.view.View[@content-desc=\"01 April 2018\"]");
//        selectdate.click();
//        WebElement okbutton = driver.findElementById("android:id/button1");
//        okbutton.click();

    }
    public static void todate(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement todate = driver.findElementById("com.nationstrust.mobilebanking:id/toDateET");
        todate.click();
//        WebElement selectdate = driver.findElementByXPath("//android.view.View[@content-desc=\"16 July 2018\"]");
//        selectdate.click();
//        WebElement okbutton = driver.findElementById("android:id/button1");
//        okbutton.click();


    }

    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}
