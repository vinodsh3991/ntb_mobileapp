package Activities.Calender;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.appium.java_client.touch.offset.PointOption.point;

public class ServiceRequestCalender extends Configuration {


    public static void selectYear(String useryear) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement selectYear = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.DatePicker/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]");
        selectYear.click();
        MobileElement scrolltoyear = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\""+ useryear +"\"));");
        scrolltoyear.click();

    }
        public static void selectmonth(String monthdate){
           for (int i =0 ; i<= 1000 ; i++) {
              // Boolean year1 = driver.findElementsByAccessibilityId(monthdate).size() != 0;


//            if(year1== true){
//                WebElement year =  driver.findElementByXPath("//android.view.View[@content-desc=\""+ monthdate+ "\"]");
//                year.click();
//            }
                try{
               if(driver.findElementByXPath("//android.view.View[@content-desc=\""+ monthdate+"\"]").isDisplayed()){
                   WebElement year =  driver.findElementByXPath("//android.view.View[@content-desc=\""+ monthdate+ "\"]");
                   year.click();
                   WebElement okbutton = driver.findElementById("android:id/button1");
                   okbutton.click();
                   break;
               }} catch (NoSuchElementException e){

                }

               WebElement arrowicon =  driver.findElementById("android:id/prev");
               arrowicon.click();


           }

    }



    private static void swipeInListTillExpectedTextAndTap(List<MobileElement> yearheadr, String expectedText) {
        int i = 1;
        while (!driver.getPageSource().contains(expectedText)) {
            swipeInListFromLastToFirst(yearheadr);
            i++;

        }
        driver.findElementByAndroidUIAutomator("new UiSelector().textContains(\"" + expectedText + "\")");
    }


    public static void swipeInListFromLastToFirst(List<MobileElement> listID) {
        int items = listID.size();
        org.openqa.selenium.Point centerOfFirstElement = listID.get(0).getLocation();
        org.openqa.selenium.Point centerOfLastElement = listID.get(items - 1).getLocation();
        new TouchAction<>(driver).longPress(point(centerOfLastElement.x, centerOfLastElement.y))
                .moveTo(point(centerOfFirstElement.x, centerOfFirstElement.y)).release().perform();
    }




}
