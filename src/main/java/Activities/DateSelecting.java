package Activities;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.appium.java_client.touch.offset.PointOption.point;

public class DateSelecting extends Configuration {


    public static void dateselect(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement yearheader = driver.findElementById("android:id/date_picker_header_year");
        yearheader.click();

        WebElement years =  driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.DatePicker/android.widget.LinearLayout/android.widget.ViewAnimator/android.widget.ListView");
        swipeInListTillExpectedTextAndTap((List<io.appium.java_client.MobileElement>) yearheader,"1965");
    }



    private static void swipeInListTillExpectedTextAndTap(List<MobileElement> yearheadr, String expectedText) {
        int i = 1;
        while (!driver.getPageSource().contains(expectedText)) {
            swipeInListFromLastToFirst(yearheadr);
            i++;

        }
        driver.findElementByAndroidUIAutomator("new UiSelector().textContains(\"" + expectedText + "\")");
    }


    public static void swipeInListFromLastToFirst(List<MobileElement> listID) {
        int items = listID.size();
        org.openqa.selenium.Point centerOfFirstElement = listID.get(0).getLocation();
        org.openqa.selenium.Point centerOfLastElement = listID.get(items - 1).getLocation();
        new TouchAction<>(driver).longPress(point(centerOfLastElement.x, centerOfLastElement.y))
                .moveTo(point(centerOfFirstElement.x, centerOfFirstElement.y)).release().perform();
    }



}