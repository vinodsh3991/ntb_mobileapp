package Activities.PopUpMessages;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PopUpMessages extends Configuration {

    public static void popupmessage_header(String popupmessage_header ){
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        WebElement errormessage_header1 = driver.findElementById("com.nationstrust.mobilebanking:id/dialogTitle");
        String errormessage_header2 = driver.findElementById("com.nationstrust.mobilebanking:id/dialogTitle").getText();
        Assert.assertEquals(errormessage_header2, popupmessage_header);


    }


    public static void popupmessage_content(String popupmessage_content){
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        WebElement errormessage_content1 = driver.findElementById("com.nationstrust.mobilebanking:id/dialogMessage");
        String errormessage_content2 = driver.findElementById("com.nationstrust.mobilebanking:id/dialogMessage").getText();
        Assert.assertEquals(errormessage_content2, popupmessage_content);


    }

    public static void popupsuccessmessage_buttons(String popsuccessmessage_buttons1){
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        String popupsuccessmessage_buttons = driver.findElementById("com.nationstrust.mobilebanking:id/positiveButton").getText();
        Assert.assertEquals(popupsuccessmessage_buttons, popsuccessmessage_buttons1);
        WebElement popupsuccessmessage_buttons2 = driver.findElementById("com.nationstrust.mobilebanking:id/positiveButton");
        popupsuccessmessage_buttons2.click();

    }

    public static void popupsuccessmessage_okbuttons(String popsuccessmessage_okbuttons1){
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        String popupsuccessmessage_okbuttons = driver.findElementById("com.nationstrust.mobilebanking:id/okButton").getText();
        Assert.assertEquals(popupsuccessmessage_okbuttons, popsuccessmessage_okbuttons1);
        WebElement popupsuccessmessage_okbuttons2 = driver.findElementById("com.nationstrust.mobilebanking:id/okButton");
        popupsuccessmessage_okbuttons2.click();

    }
    public static void popupsuccessmessage_negativebutton(String popsuccessmessage_negativebutton1){
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        String popupsuccessmessage_negativebutton = driver.findElementById("com.nationstrust.mobilebanking:id/negativeButton").getText();
        Assert.assertEquals(popupsuccessmessage_negativebutton, popsuccessmessage_negativebutton1);
        WebElement popupsuccessmessage_negativebutton3 = driver.findElementById("com.nationstrust.mobilebanking:id/negativeButton");
        popupsuccessmessage_negativebutton3.click();

    }
    public static void popupsuccessmessage_positivebutton(String popsuccessmessage_positivebutton1){
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        String popupsuccessmessage_positivebutton = driver.findElementById("com.nationstrust.mobilebanking:id/positiveButton").getText();
        Assert.assertEquals(popupsuccessmessage_positivebutton, popsuccessmessage_positivebutton1);
        WebElement popupsuccessmessage_positivebutton3 = driver.findElementById("com.nationstrust.mobilebanking:id/positiveButton");
        popupsuccessmessage_positivebutton3.click();                           

    }
    public static void devicepopup(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement allowbutton = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
        allowbutton.click();
    }

    public static void popupmessage_email(String popupmessage_email ){
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        WebElement errormessage_email1= driver.findElementById("com.nationstrust.mobilebanking:id/dialogEmail");
        String errormessage_email2 = driver.findElementById("com.nationstrust.mobilebanking:id/dialogEmail").getText();
        Assert.assertEquals(errormessage_email2, popupmessage_email);


    }

}
