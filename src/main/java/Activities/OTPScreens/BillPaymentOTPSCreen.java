package Activities.OTPScreens;

import TestBase.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class BillPaymentOTPSCreen extends Configuration {

    public static void BillPaymentOTPSCreen_labels() throws Exception{
        String screentitle = "Secondary Verification";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void backbutton(){
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void OTPbutton(){
        driver.findElement(By.xpath("// android.widget.EditText[@index='0"));
        WebElement otpbutton = driver.findElementById("com.nationstrust.mobilebanking:id/resendOTP");
        otpbutton.click();
    }

    public static void enetrOTP(String otp){
        driver.hideKeyboard();
        WebElement enterotp = driver.findElementById("com.nationstrust.mobilebanking:id/otpText");
        enterotp.sendKeys(otp);


    }





}
