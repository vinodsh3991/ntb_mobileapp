package Activities.OTPScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class LoginOTPScreen extends Configuration {

    public static void LoginOTPScreen_labels() throws Exception{
        String screentitle = "Secondary Verification";

        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void backbutton(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void OTPbutton(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement otpbutton = driver.findElementById("com.nationstrust.mobilebanking:id/resendOTP");
        otpbutton.click();

    }

    public static void enetrOTP(String otp){
        driver.hideKeyboard();
        WebElement enterotp = driver.findElementById("com.nationstrust.mobilebanking:id/otpText");
        enterotp.sendKeys(otp);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }


}





