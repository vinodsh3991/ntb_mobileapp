package Activities.OTPScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class CommonOTPScreen extends Configuration {

    public static void CommonOTPScreen_title() throws Exception{
        String screentitle = "Secondary Verification";
        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);
            driver.hideKeyboard();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void CommonOTPScreen_labels() throws Exception{
        String Wehave = "We have sent a verification code to";
        String Pleaseenter = "Please enter the verification code here.";
        String Notyou = "Not you? Call";
        String telenumber = "+94 114 711411";
        try {
            String Wehave1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Wehave1, Wehave);
            String Pleaseenter1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[4]").getText();
            Assert.assertEquals(Pleaseenter1, Pleaseenter);
            String Notyou1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Notyou1, Notyou);
            String telenumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/notYouCallText").getText();
            Assert.assertEquals(telenumber1, telenumber);
            WebElement 	mobilenumber = driver.findElementById("com.nationstrust.mobilebanking:id/mobileNoText");
            mobilenumber.isDisplayed();
            WebElement 	email = driver.findElementById("com.nationstrust.mobilebanking:id/emailText");
            email.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void backbutton(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void OTPbutton(){
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement otpbutton = driver.findElementById("com.nationstrust.mobilebanking:id/resendOTP");
        otpbutton.click();

    }

    public static void enetrOTP(String otp){
        WebElement enterotp = driver.findElementById("com.nationstrust.mobilebanking:id/otpText");
        enterotp.click();
        enterotp.sendKeys(otp);

    }

    public static void signupotp(){
        WebElement linebar = driver.findElementById("com.nationstrust.mobilebanking:id/imageView2");
        linebar.isDisplayed();


    }
}
