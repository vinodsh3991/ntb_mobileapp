package Activities.OTPScreens;

import TestBase.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class FundTrnasferOTPSCreen extends Configuration {

    public static void OTPScreen_labels() throws Exception{
        String screentitle = "Secondary Verification";
        // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void backbutton(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void OTPbutton(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.xpath("// android.widget.EditText[@index='0"));
        WebElement otpbutton = driver.findElementById("com.nationstrust.mobilebanking:id/resendOTP");
        otpbutton.click();

    }

    public static void enetrOTP(String otp){
        WebElement enterotp = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.EditText");
        enterotp.sendKeys(otp);
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

    }


    private static Function<WebDriver,WebElement> presenceOfElementLocated(final By locator) {
        return new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                return driver.findElement(locator);
            }
        };
    }





}
