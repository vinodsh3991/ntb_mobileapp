package Activities.OTPScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FDOTPScreen extends Configuration {

    public static void FDOTPScreen_labels() throws Exception{
        String screentitle = "Secondary Verification";
        String label1 = "We have sent a verification code to";
        String label2 = "Please enter the verification code here.";
        String label3 = "Not you? Call";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screentitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screentitle1, screentitle);
            String label11 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label11, label1);
            String label21 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[4]").getText();
            Assert.assertEquals(label21, label2);
            String label31 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView[1]").getText();
            Assert.assertEquals(label31, label3);
            WebElement number = driver.findElementById("com.nationstrust.mobilebanking:id/notYouCallText");
            number.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void backbutton(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void OTPbutton(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement otpbutton = driver.findElementById("com.nationstrust.mobilebanking:id/resendOTP");
        otpbutton.click();

    }

    public static void enetrOTP(String otp){
        driver.hideKeyboard();
        WebElement enterotp = driver.findElementById("com.nationstrust.mobilebanking:id/otpText");
        enterotp.sendKeys(otp);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }


}






