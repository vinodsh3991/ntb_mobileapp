package Activities.FDAccountOpening;

import TestBase.Configuration;
import Activities.PopUpMessages.PopUpMessages;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FixedDepositAccountOpeningScreen extends Configuration {
    public static void  FixedDepositAccountOpeningScreen_title() throws Exception{
        String screenheading = "Fixed Deposit Opening";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  FixedDepositAccountOpeningScreen_labels() throws Exception{
        String depositamount = "Deposit Amount (Minimum LKR 50,000.00)";
        String LKR ="LKR";
        String 	FDType ="FD Type";
        String Maturity ="Maturity";
        String MaturityDes ="Interest is paid at the end of the fixed deposit tenure.";
        String FlexiFixedDeposit ="Flexi Fixed Deposit";
        String FlexiFixedDepositDes ="Fixed deposit based on number of days.";
        String InvestmentPeriod ="Investment Period";
        String interestrate ="Check current interest rates";


        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String LKR1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView36").getText();
            Assert.assertEquals(LKR, LKR1);
            String depositamount1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(depositamount, depositamount1);
          //  String FDType1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            String FDType1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();

            Assert.assertEquals(FDType, FDType1);

            String Maturity1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbMaturity").getText();
            Assert.assertEquals(Maturity, Maturity1);
            String MaturityDes1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMaturityDesc").getText();
            Assert.assertEquals(MaturityDes, MaturityDes1);
            String FlexiFixedDeposit1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbFlexiFD").getText();
            Assert.assertEquals(FlexiFixedDeposit, FlexiFixedDeposit1);
            String FlexiFixedDepositDes1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFlexiDesc").getText();
            Assert.assertEquals(FlexiFixedDepositDes, FlexiFixedDepositDes1);
//            String InvestmentPeriod1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();
//            Assert.assertEquals(InvestmentPeriod, InvestmentPeriod1);
            String interestrate1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvInterestRates").getText();
            Assert.assertEquals(interestrate, interestrate1);

            WebElement inforicon =driver.findElementById("com.nationstrust.mobilebanking:id/imageView20");
            inforicon.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	interatestrateslink(){
        String browserurl = "nationstrust.com/deposit-rates";
        WebElement interatestrateslink = driver.findElementById("com.nationstrust.mobilebanking:id/tvInterestRates");
        interatestrateslink.click();
        WebElement selectbrowser = driver.findElementById("com.huawei.android.internal.app:id/hw_button_always");
        selectbrowser.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        String browserurl1 = driver.findElementById("com.android.chrome:id/url_bar").getText();
        Assert.assertEquals(browserurl, browserurl1);

    }

    public static void 	selectmaturity(){
        String minmonth ="1";
        String maxmonth ="60";
        String minlabel ="month(s)";
        String maxlabel ="month(s)";

        String minmonth1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMinMonths").getText();
        Assert.assertEquals(minmonth, minmonth1);
        String maxmonth1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMaxMonths").getText();
        Assert.assertEquals(maxmonth, maxmonth1);
        String minlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvInterestPeriodMin").getText();
        Assert.assertEquals(minlabel, minlabel1);
        String maxlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvInterestPeriodMax").getText();
        Assert.assertEquals(maxlabel, maxlabel1);
    }

    public static void 	selectflexi(){
        String mindays ="30";
        String maxdays ="365";
        String minlabel ="Days";
        String maxlabel ="Days";

        WebElement selectflexi = driver.findElementById("com.nationstrust.mobilebanking:id/rbFlexiFD");
        selectflexi.click();

        String mindays1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMinMonths").getText();
        Assert.assertEquals(mindays, mindays1);
        String maxdays1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMaxMonths").getText();
        Assert.assertEquals(maxdays, maxdays1);
        String minlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvInterestPeriodMin").getText();
        Assert.assertEquals(minlabel, minlabel1);
        String maxlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvInterestPeriodMax").getText();
        Assert.assertEquals(maxlabel, maxlabel1);
    }

    public static void 	continuebtn(){
        String buttonname ="Continue";
        String buttonname1 = driver.findElementById("com.nationstrust.mobilebanking:id/btnCalculate").getText();
        Assert.assertEquals(buttonname, buttonname1);
        WebElement continuebtn = driver.findElementById("com.nationstrust.mobilebanking:id/btnCalculate");
        continuebtn.click();
    }
    public static void 	selectmonthordays(){
        WebElement selectmonthordays = driver.findElementById("com.nationstrust.mobilebanking:id/ivPlus");
        selectmonthordays.click();
        WebElement selectmonthordays2 = driver.findElementById("com.nationstrust.mobilebanking:id/ivPlus");
        selectmonthordays2.click();
    }

    public static void 	enterdepositeamount(String amount){
        WebElement enterdepositeamount = driver.findElementById("com.nationstrust.mobilebanking:id/etDepositAmount");
        enterdepositeamount.click();
        enterdepositeamount.sendKeys(amount);
        driver.hideKeyboard();

    }

    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }


    public static void  aftercalculate_labels() throws Exception{
        String 	MaturityValue = "Maturity Value";
        String 	LKR = "LKR";
        String description = "This amount is subject to the prevailing withholding tax structure";
        String 	NominalInterestRate = "Nominal Interest Rate";
        String 	AnnualEquivalentRate = "Annual Equivalent Rate";
        String 	biometricType = "Re-invest After Maturity";
        String 	label1 = "Capital and interest will be re-invested at maturity.";
        String 	label2 = "If you wish to uplift your Fixed Deposits, Please send us instructions including the Fixed deposit number and the account number that you wish to credit funds, via the Mobile Banking Mail-box.";


       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String MaturityValue1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(MaturityValue, MaturityValue1);
            String description1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[2]").getText();
            Assert.assertEquals(description, description1);
            String NominalInterestRate1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(NominalInterestRate, NominalInterestRate1);
            String LKR1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(LKR, LKR1);
            String 	AnnualEquivalentRate1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(AnnualEquivalentRate, AnnualEquivalentRate1);
            String 	biometricType1 = driver.findElementById("com.nationstrust.mobilebanking:id/biometricType").getText();
            Assert.assertEquals(biometricType, biometricType1);
            WebElement toggle = driver.findElementById("com.nationstrust.mobilebanking:id/switchReInvest");
            toggle.isDisplayed();
            String 	label11 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFdInterestStatus").getText();
            Assert.assertEquals(label1, label11);
            String 	label22 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFdInterestStatus2").getText();
            Assert.assertEquals(label2, label22);


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void 	NIR(){
        WebElement NIR = driver.findElementById("com.nationstrust.mobilebanking:id/ivNIRMore");
        NIR.click();
        PopUpMessages.popupmessage_header("Nominal Interest Rate");
        PopUpMessages.popupmessage_content("The general rate of interest applicable for the investment");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
    }
    public static void 	ATR(){
        WebElement ATR = driver.findElementById("com.nationstrust.mobilebanking:id/ivAERMore");
        ATR.click();
        PopUpMessages.popupmessage_header("Annual Equivalent Rate");
        PopUpMessages.popupmessage_content("Effective annual interest rate applicable for the interest");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        //MobileElement scrolltoendofthepage = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Calculate\"));");

    }

    public static void 	enterbranch(){
        String dropdwontitle="Select Branch";
        WebElement enterbranch = driver.findElementById("com.nationstrust.mobilebanking:id/etBranch");
        enterbranch.click();
        String dropdwontitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(dropdwontitle, dropdwontitle1);

        //WebElement selectbranch = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        WebElement selectbranch = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        selectbranch.click();

        MobileElement scrolltoageend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Capital and interest will be re-invested at maturity.\"));");


    }

    public static void 	confirmbtn2(){
        String buttonname ="Confirm";
        String buttonname1 = driver.findElementById("com.nationstrust.mobilebanking:id/btnConfirm").getText();
        Assert.assertEquals(buttonname, buttonname1);
        WebElement confirmbtn2 = driver.findElementById("com.nationstrust.mobilebanking:id/btnConfirm");
        confirmbtn2.click();
    }

    public static void 	cancel(){
        String buttonname ="Cancel";
        String buttonname1 = driver.findElementById("com.nationstrust.mobilebanking:id/cancel").getText();
        Assert.assertEquals(buttonname, buttonname1);
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/cancel");
        cancel.click();
    }

    public static void 	selectfundingaccount(){
        String dropdwontitle="Select Funding Account";
        String floatinglabel ="Funding Account";

        String floatinglabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/etFundingAccount").getText();
        Assert.assertEquals(floatinglabel, floatinglabel1);

        WebElement enterfundingaccount = driver.findElementById("com.nationstrust.mobilebanking:id/etFundingAccount");
        enterfundingaccount.click();
        String dropdwontitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/title").getText();
        Assert.assertEquals(dropdwontitle, dropdwontitle1);

       // WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.TextView");
        WebElement selectaccount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]/android.widget.LinearLayout");
        selectaccount.click();

//        String floatinglabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tiFundingAcc").getText();
//        Assert.assertEquals(floatinglabel, floatinglabel1);

        WebElement accountbalance = driver.findElementById("com.nationstrust.mobilebanking:id/tvFundingAccBalance");
        accountbalance.isDisplayed();


        MobileElement scrolltoageend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Check current interest rates\"));");


    }

}
