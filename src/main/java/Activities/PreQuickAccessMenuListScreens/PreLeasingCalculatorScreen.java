package Activities.PreQuickAccessMenuListScreens;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PreLeasingCalculatorScreen extends Configuration {
    public static void PreLeasingCalculatorScreen_title() throws Exception {
        String screenheading = "Leasing Calculator";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void PreLeasingCalculatorScreen_labels() throws Exception {
        String VehicleCondition = "Vehicle Condition";
        String Brand_New = "Brand New";
        String Registered = "Registered";
        String Un_Registered = "Un-Registered";
        String Vehicle_Type = "Vehicle Type";
        String Cars_SUVs = "Cars / SUVs";
        String Dual_Purpose_Vehicles = "Dual Purpose Vehicles";
        String Motor_Bikes = "Motor Bikes";
        String Lorries_Buses = "Lorries / Buses";
        String advanced = "Advance to be paid to seller";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String VehicleCondition1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(VehicleCondition, VehicleCondition1);
            String Vehicle_Type1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[2]").getText();
            Assert.assertEquals(Vehicle_Type, Vehicle_Type1);
            String Brand_New1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbBrandNew").getText();
            Assert.assertEquals(Brand_New, Brand_New1);
            String Registered1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbRegistered").getText();
            Assert.assertEquals(Registered, Registered1);
            String Un_Registered1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbUnRegistered").getText();
            Assert.assertEquals(Un_Registered, Un_Registered1);
            String Cars_SUVs1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbCarSUV").getText();
            Assert.assertEquals(Cars_SUVs, Cars_SUVs1);
            String Dual_Purpose_Vehicles1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbDualPurpose").getText();
            Assert.assertEquals(Dual_Purpose_Vehicles, Dual_Purpose_Vehicles1);
            String Motor_Bikes1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbMotorBikes").getText();
            Assert.assertEquals(Motor_Bikes, Motor_Bikes1);
            String Lorries_Buses1 = driver.findElementById("com.nationstrust.mobilebanking:id/rbLorriesBusses").getText();
            Assert.assertEquals(Lorries_Buses, Lorries_Buses1);
            String advanced1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[2]").getText();
            Assert.assertEquals(advanced, advanced1);

        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void calculate() {
        String calculatebtn = "Calculate";
        String calculatebtn1 = driver.findElementById("com.nationstrust.mobilebanking:id/btnCalculate").getText();
        Assert.assertEquals(calculatebtn, calculatebtn1);
        WebElement calculate = driver.findElementById("com.nationstrust.mobilebanking:id/btnCalculate");
        calculate.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void clear() {
        String clearbtn = "Clear";
        String clearbtn1 = driver.findElementById("com.nationstrust.mobilebanking:id/btnClear").getText();
        Assert.assertEquals(clearbtn, clearbtn1);
        WebElement clear = driver.findElementById("com.nationstrust.mobilebanking:id/btnClear");
        clear.click();
    }

    public static void back() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    public static void BrandNew() {
        WebElement BrandNew = driver.findElementById("com.nationstrust.mobilebanking:id/rbBrandNew");
        BrandNew.click();
    }
    public static void Registered() {
        WebElement Registered = driver.findElementById("com.nationstrust.mobilebanking:id/rbRegistered");
        Registered.click();
    }
    public static void UnRegistered() {
        WebElement UnRegistered = driver.findElementById("com.nationstrust.mobilebanking:id/rbUnRegistered");
        UnRegistered.click();
    }


    public static void CarSUV() {
        WebElement CarSUV = driver.findElementById("com.nationstrust.mobilebanking:id/rbCarSUV");
        CarSUV.click();
    }
    public static void DualPurpose() {
        WebElement DualPurpose = driver.findElementById("com.nationstrust.mobilebanking:id/rbDualPurpose");
        DualPurpose.click();
    }
    public static void MotorBikes() {
        WebElement MotorBikes = driver.findElementById("com.nationstrust.mobilebanking:id/rbMotorBikes");
        MotorBikes.click();
    }
    public static void LorriesBusses() {
        WebElement LorriesBusses = driver.findElementById("com.nationstrust.mobilebanking:id/rbLorriesBusses");
        LorriesBusses.click();
    }

    public static void enteradvance(String amount) {
        WebElement enteradvance = driver.findElementById("com.nationstrust.mobilebanking:id/etAdvanceToPay");
        enteradvance.click();
        enteradvance.sendKeys(amount);
        //scroll to page end
        MobileElement pageend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Clear\"));");

    }

    public static void enterleaseamount(String amount) {
        String leaseamount = "Required Lease Amount";
        String leaseamount1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[3]").getText();
        Assert.assertEquals(leaseamount, leaseamount1);

        WebElement enterleaseamount = driver.findElementById("com.nationstrust.mobilebanking:id/etRequiredLease");
        enterleaseamount.click();
        enterleaseamount.sendKeys(amount);
        driver.hideKeyboard();
    }
    public static void selectduration() {
        WebElement enterleaseamount = driver.findElementById("com.nationstrust.mobilebanking:id/etDuration");
        enterleaseamount.click();
        String dropdowntitle ="Duration (In Years)";
        String dropdowntitle1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
        Assert.assertEquals(dropdowntitle, dropdowntitle1);
        WebElement years = driver.findElementById("com.nationstrust.mobilebanking:id/tvTwo");
        years.click();
    }

    public static void MonthlyInstallment_label() throws Exception {
        String MonthlyInstallment = "Monthly Installment";
        String tandc = "Terms and conditions apply";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String MonthlyInstallment1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(MonthlyInstallment, MonthlyInstallment1);
            String tandc1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            Assert.assertEquals(tandc, tandc1);

        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void arrow() {
        String lengthylabel = "Would you like to be contacted by the bank for more information?";
        WebElement arrow = driver.findElementById("com.nationstrust.mobilebanking:id/moreResult");
        arrow.click();
        String lengthylabel1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();
        Assert.assertEquals(lengthylabel, lengthylabel1);

    }

    public static void yes() {
        WebElement yes = driver.findElementById("com.nationstrust.mobilebanking:id/leasingCalYesBtn");
        yes.click();
    }
    public static void no() {
        WebElement no = driver.findElementById("com.nationstrust.mobilebanking:id/leasingCalNoBtn");
        no.click();
    }

}

