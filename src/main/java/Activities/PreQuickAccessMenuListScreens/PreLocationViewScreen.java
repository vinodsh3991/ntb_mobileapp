package Activities.PreQuickAccessMenuListScreens;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PreLocationViewScreen extends Configuration {
    public static void  PreLocationViewScreen_title() throws Exception{
        String screenheading = "Location View";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  PreLocationViewScreen_labels() throws Exception{
        String 	Branch = "Branch";
        String 	SelfService = "Self Service";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Branch1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvBranch").getText();
            Assert.assertEquals(Branch, Branch1);
            String SelfService1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvSelfService").getText();
            Assert.assertEquals(SelfService, SelfService1);
            WebElement menuicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_toggle");
            menuicon.isDisplayed();
            WebElement searchbar = driver.findElementById("com.nationstrust.mobilebanking:id/actvSearch");
            searchbar.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }



    public static void 	quickaccessmenu(){
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenu.click();
    }
    public static void 	searchbranch(String brancname){
        WebElement searchbranch = driver.findElementById("com.nationstrust.mobilebanking:id/actvSearch");
        searchbranch.click();
        searchbranch.sendKeys(brancname);
//        WebElement taponbranch = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout[2]");
//        taponbranch.click();
    }

    public static void 	searchATM(String brancname){
        WebElement searchbranch = driver.findElementById("com.nationstrust.mobilebanking:id/actvSearch");
        searchbranch.click();
        searchbranch.sendKeys(brancname);
        WebElement taponATM = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout[2]");
        taponATM.click();
    }


    public static void 	taponpicker(){
        WebElement taponpicker = driver.findElementById("//android.view.View[@content-desc=\"Matara. 56, Esplanade Road, Matara.\"]");
        taponpicker.click();
    }

    public static void 	mylocation(){
        WebElement mylocation = driver.findElementById("//android.widget.ImageView[@content-desc=\"My Location\"]");
        mylocation.click();
    }
    public static void 	selfservice(){
        WebElement selfservice = driver.findElementById("com.nationstrust.mobilebanking:id/tvSelfService");
        selfservice.click();
    }

    public static void 	menuicon(){
        WebElement menuicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_toggle");
        menuicon.click();
    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    public static void 	branchlist() {
        MobileElement scrolltobranch = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Badulla\"));");
        scrolltobranch.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }



}
