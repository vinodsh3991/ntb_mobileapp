package Activities.PreQuickAccessMenuListScreens;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PreFAQScreen extends Configuration {
    public static void  FAQscreen_title() throws Exception{
        String screenheading = "FAQ";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  FAQscreen_title_labels() throws Exception{
        String 	subtitle = "Frequently Asked Questions";
        String 	morequestions = "Have more questions?";


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String subtitle1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvFAQTitle").getText();
            Assert.assertEquals(subtitle, subtitle1);
            String morequestions1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMoreOrHide").getText();
            Assert.assertEquals(morequestions, morequestions1);
            WebElement quickaccessicon = driver.findElementById("com.nationstrust.mobilebanking:id/faq_menu");
            quickaccessicon.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	quickaccessmenu(){
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/faq_menu");
        quickaccessmenu.click();
    }
    public static void 	morequestions(){
        WebElement morequestions = driver.findElementById("com.nationstrust.mobilebanking:id/tvMoreOrHide");
        morequestions.click();
    }

    public static void 	taponaquestion(){
        WebElement taponaquestion = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.support.v7.widget.RecyclerView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView");
        taponaquestion.click();

    }
    public static void 	closeanswer(){
        WebElement closeanswer = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.support.v7.widget.RecyclerView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView");
        closeanswer.click();

    }

    public static void 	hidequestions(){
        //scroll to page end
        MobileElement pageend = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Hide more questions?\"));");

        String 	hidequestions2 = "Hide more questions?";
        String hidequestions22 = driver.findElementById("com.nationstrust.mobilebanking:id/tvMoreOrHide").getText();
        Assert.assertEquals(hidequestions2, hidequestions22);
        WebElement hidequestions = driver.findElementById("com.nationstrust.mobilebanking:id/tvMoreOrHide");
        hidequestions.click();

    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }

}
