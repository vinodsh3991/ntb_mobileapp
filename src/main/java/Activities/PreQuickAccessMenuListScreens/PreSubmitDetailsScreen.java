package Activities.PreQuickAccessMenuListScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PreSubmitDetailsScreen extends Configuration {
    public static void  PreSubmitDetailsScreen_title() throws Exception{
        String screenheading = "Submit Details";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  PreSubmitDetailsScreen_labels() throws Exception{
        String 	label = "Enter details for the bank to contact you";
        String 	YourName = "Your Name";

        try {
            String label1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            Assert.assertEquals(label, label1);
            String YourName1 = driver.findElementById("com.nationstrust.mobilebanking:id/nameText").getText();
            Assert.assertEquals(YourName, YourName1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }



    public static void 	submit(){
        WebElement submit = driver.findElementById("com.nationstrust.mobilebanking:id/submitButton");
        submit.click();
    }
    public static void 	entername(String  name){
        WebElement entername = driver.findElementById("com.nationstrust.mobilebanking:id/nameTextET");
        entername.click();
        entername.sendKeys(name);
        driver.hideKeyboard();
    }
    public static void 	enteremail(String  email){
        String emaillabel = "Email Address";
        WebElement enteremail = driver.findElementById("com.nationstrust.mobilebanking:id/emailTextET");
        enteremail.click();
        String emaillabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/emailText").getText();
        Assert.assertEquals(emaillabel, emaillabel1);
        enteremail.sendKeys(email);
        driver.hideKeyboard();
    }

    public static void 	enterphonenumber(String  number){
        String PhoneNumber = "Phone Number";
        WebElement enterphonenumber = driver.findElementById("com.nationstrust.mobilebanking:id/phoneTextET");
        enterphonenumber.click();
        String PhoneNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/phoneText").getText();
        Assert.assertEquals(PhoneNumber, PhoneNumber1);
        enterphonenumber.sendKeys(number);
        driver.hideKeyboard();
    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }
    public static void 	clearname(){
        WebElement entername = driver.findElementById("com.nationstrust.mobilebanking:id/nameTextET");
        entername.clear();

    }
    public static void 	clearemail(){
        WebElement enteremail = driver.findElementById("com.nationstrust.mobilebanking:id/emailTextET");
        enteremail.clear();

    }

    public static void 	clearphonenumber(){
        WebElement enterphonenumber = driver.findElementById("com.nationstrust.mobilebanking:id/phoneTextET");
        enterphonenumber.clear();

    }


}
