package Activities.PreQuickAccessMenuListScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PreQuickAccessMenuList extends Configuration {
    public static void PreQuickAccessMenuList_labels() throws Exception{
        String homelabel = "Home";
        String languagelabel = "Language";
        String 	FAQlabel = "FAQ";
        String 	ContactUslabel = "Contact Us";
        String offerlabel = "Special Offers";
        String bankproductslabel = "Bank Products";
        String ratelabel = "Rates";
        String calculatorslabel = "Calculators";
        String locationlabel = "Location View";
        //String tclabel = "Terms and Conditions";


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String homelabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView2").getText();
            String languagelabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView").getText();
            String FAQlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView3").getText();
            String ContactUslabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView4").getText();
            String offerlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView5").getText();
            String locationlabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView11").getText();
            String bankproductslabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView6").getText();
            String rateslabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvRates").getText();
            String calculatorslabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView9").getText();
           // String tclabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView12").getText();
            Assert.assertEquals(homelabel1, homelabel);
            Assert.assertEquals(languagelabel1, languagelabel);
            Assert.assertEquals(FAQlabel1, FAQlabel);
            Assert.assertEquals(ContactUslabel1, ContactUslabel);
            Assert.assertEquals(offerlabel1, offerlabel);
            Assert.assertEquals(locationlabel1, locationlabel);
            Assert.assertEquals(bankproductslabel1, bankproductslabel);
            Assert.assertEquals(rateslabel1, ratelabel);
            Assert.assertEquals(calculatorslabel1, calculatorslabel);
           // Assert.assertEquals(tclabel1, tclabel);


            WebElement homeicon = driver.findElementById("com.nationstrust.mobilebanking:id/homeView");
            homeicon.isDisplayed();
            WebElement languageicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivLanguage");
            languageicon.isDisplayed();
            WebElement faqicon = driver.findElementById("com.nationstrust.mobilebanking:id/menuFAQ");
            faqicon.isDisplayed();
            WebElement contacticon = driver.findElementById("com.nationstrust.mobilebanking:id/ivContactUs");
            contacticon.isDisplayed();
            WebElement offericon = driver.findElementById("com.nationstrust.mobilebanking:id/bankProductMenu");
            offericon.isDisplayed();
            WebElement producticon = driver.findElementById("com.nationstrust.mobilebanking:id/ivSpecialOffers");
            producticon.isDisplayed();
            WebElement rateicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivRates");
            rateicon.isDisplayed();
            WebElement calicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivCalculators");
            calicon.isDisplayed();
            WebElement locationicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivLocationView");
            locationicon.isDisplayed();
            WebElement tandcicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivTermsConditions");
            tandcicon.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void language(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement language = driver.findElementById("com.nationstrust.mobilebanking:id/ivLanguage");
        language.click();
    }

    public static void FAQ(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement FAQ = driver.findElementById("com.nationstrust.mobilebanking:id/menuFAQ");
        FAQ.click();
    }
    public static void home(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement home = driver.findElementById("com.nationstrust.mobilebanking:id/homeView");
        home.click();
    }
    public static void contactus(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement contactus = driver.findElementById("com.nationstrust.mobilebanking:id/ivContactUs");
        contactus.click();
    }

    public static void offers(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement offers = driver.findElementById("com.nationstrust.mobilebanking:id/bankProductMenu");
        offers.click();
    }
    public static void servicerequest(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement servicerequest = driver.findElementById("com.nationstrust.mobilebanking:id/tvTwo");
        servicerequest.click();
    }
    public static void bankproducts(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement bankproducts = driver.findElementById("com.nationstrust.mobilebanking:id/ivSpecialOffers");
        bankproducts.click();
    }
    public static void rates(){
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        WebElement rates = driver.findElementById("com.nationstrust.mobilebanking:id/ivRates");
        rates.click();
    }
    public static void calculator(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement calculator = driver.findElementById("com.nationstrust.mobilebanking:id/ivCalculators");
        calculator.click();
    }

    public static void cross(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement cross = driver.findElementById("com.nationstrust.mobilebanking:id/menuCloseIcon");
        cross.click();
    }
    public static void locationview(){
        WebElement locationview = driver.findElementById("com.nationstrust.mobilebanking:id/ivLocationView");
        locationview.click();

    }
    public static void tandc(){
        WebElement tandc = driver.findElementById("com.nationstrust.mobilebanking:id/ivTermsConditions");
        tandc.click();

    }


}
