package Activities.PreQuickAccessMenuListScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PreAtmLocationViewDetailsScreen extends Configuration {
    public static void  PreAtmLocationViewDetailsScreen_title() throws Exception{
        String screenheading = "Location View";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  PreAtmLocationViewDetailsScreen_labels() throws Exception{
        String 	SelfService = "Self Service";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            WebElement Badulla = driver.findElementById("com.nationstrust.mobilebanking:id/tvCity");
            Badulla.isDisplayed();
            WebElement address = driver.findElementById("com.nationstrust.mobilebanking:id/tvAddress");
            address.isDisplayed();
            WebElement addressicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView");
            addressicon.isDisplayed();
            WebElement opentime = driver.findElementById("com.nationstrust.mobilebanking:id/tvOpenTime");
            opentime.isDisplayed();
            WebElement ServicesTopic = driver.findElementById("com.nationstrust.mobilebanking:id/tvServicesTopic");
            ServicesTopic.isDisplayed();
            WebElement Services = driver.findElementById("com.nationstrust.mobilebanking:id/tvServices");
            Services.isDisplayed();
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }



    public static void 	btnGo(){
        WebElement btnGo = driver.findElementById("com.nationstrust.mobilebanking:id/btnGo");
        btnGo.click();
    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}
