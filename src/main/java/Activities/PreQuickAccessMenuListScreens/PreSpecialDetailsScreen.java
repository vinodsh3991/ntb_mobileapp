package Activities.PreQuickAccessMenuListScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class PreSpecialDetailsScreen extends Configuration {

    public static void  PreSpecialDetails_labels() throws Exception{

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            WebElement validdate = driver.findElementById("com.nationstrust.mobilebanking:id/tvDate");
            validdate.isDisplayed();
            WebElement imagetitle = driver.findElementById("com.nationstrust.mobilebanking:id/tvImageTitle");
            imagetitle.isDisplayed();
            WebElement bodytitle = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitleBody");
            bodytitle.isDisplayed();
            WebElement body = driver.findElementById("com.nationstrust.mobilebanking:id/tvDesc");
            body.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	call(){
        WebElement call = driver.findElementById("com.nationstrust.mobilebanking:id/button");
        call.click();
        Boolean permission = driver.findElementsById("com.android.packageinstaller:id/permission_allow_button").size() != 0;
        if (permission == true) {
            WebElement allow = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
            allow.click();
        }
    }
    public static void 	moreinfo(){
        WebElement moreinfo = driver.findElementById("com.nationstrust.mobilebanking:id/button2");
        moreinfo.click();
        WebElement selectbrowser = driver.findElementById("com.huawei.android.internal.app:id/hw_button_always");
        selectbrowser.click();
    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }

}
