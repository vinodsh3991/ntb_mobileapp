package Activities.PreQuickAccessMenuListScreens;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PreMainCalculatorScreen extends Configuration {
    public static void  PreMainCalculatorScreen_title() throws Exception{
        String screenheading = "Calculators";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  PreMainCalculatorScreen_labels() throws Exception{
        String 	FDcalculator = "Fixed Deposit Calculator";
        String 	Leasing = "Leasing";
        String Tamil = "தமிழ்";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String FDcalculator1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            Assert.assertEquals(FDcalculator, FDcalculator1);
            String Leasing1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.TextView").getText();
            Assert.assertEquals(Leasing, Leasing1);
            WebElement FDicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.ImageView[1]");
            FDicon.isDisplayed();
            WebElement leasingicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.ImageView[1]");
            leasingicon.isDisplayed();
            WebElement quickaccessicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
            quickaccessicon.isDisplayed();
            WebElement arrow1icon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.ImageView[2]");
            arrow1icon.isDisplayed();
            WebElement arrow2icon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.ImageView[2]");
            arrow2icon.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }



    public static void 	quickaccessmenu(){
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenu.click();
    }
    public static void 	fdcalculator(){
        WebElement fdcalculator = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.TextView");
        fdcalculator.click();

    }
    public static void 	leasingcalculator(){
        WebElement leasingcalculator = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.TextView");
        leasingcalculator.click();

    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}
