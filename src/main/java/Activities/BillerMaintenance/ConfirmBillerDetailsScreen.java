package Activities.BillerMaintenance;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ConfirmBillerDetailsScreen extends Configuration {

    public static void  ConfirmBillerDetails_title() throws Exception{
        String screenheading = "Confirm Biller Details";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  ConfirmBillerDetails_labels() throws Exception{
        String 	Category = "Category";
        String 	Biller = "Biller";
        String Nickname = "Nickname";
        String bankname = "Bank Name";

       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String Category1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(Category, Category1);
            String Biller1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[3]").getText();
            Assert.assertEquals(Biller, Biller1);
            String 	Nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/nickNameTitle").getText();
            Assert.assertEquals(Nickname, Nickname1);
            WebElement 	referencelabel = driver.findElementById("com.nationstrust.mobilebanking:id/tvD1");
            referencelabel.isDisplayed();

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }



    public static void 	confirmbutton(){
        WebElement confirmbutton = driver.findElementById("com.nationstrust.mobilebanking:id/confirmBillerButton");
        confirmbutton.click();

    }

    public static void 	cancelbutton(){
        WebElement cancelbutton = driver.findElementById("com.nationstrust.mobilebanking:id/cancelButton");
        cancelbutton.click();

    }

    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}
