package Activities.BillerMaintenance;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class BillerMaintenance extends Configuration {
    public static void BillerMaintenance_title() throws Exception{
        String screenheading = "Biller List";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void emptyscreen(){
        String emptyscreenlabel1= "You do not have any current accounts to make this request";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/emptyChequeBook").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView");
        imageicon.isDisplayed();
    }

    public static void plusicon(){
        WebElement plusicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_add");
        plusicon.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void deleteicon(){
        WebElement plusicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_add");
        plusicon.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }



    public static void 	quickaccessmenuicon(){
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/billerMenu");
        quickaccessmenuicon.click();

    }

    public static void selectbranch(){
        WebElement selectbranch = driver.findElementById("com.nationstrust.mobilebanking:id/branchET");
        selectbranch.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement searchbrach = driver.findElementById("com.nationstrust.mobilebanking:id/etSearch");
        searchbrach.click();
        searchbrach.sendKeys("Matara");
        driver.hideKeyboard();
        WebElement setbrach = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout");
        setbrach.click();
    }



    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }


    public static void scrolltobiller(){
        //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement scrolltobiller = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"CEB\"));");
        scrolltobiller.click();
    }





}

