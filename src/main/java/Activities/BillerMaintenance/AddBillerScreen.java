package Activities.BillerMaintenance;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class AddBillerScreen extends Configuration {
    public static void AddBillerScreen_title() throws Exception{
        String screenheading = "Add Biller";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void AddPayeeScreen_labels() throws Exception{
        String 	Category = "Category";
        String 	Biller = "Biller";

        try {
            String 	Category1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvCategoryTitle").getText();
            Assert.assertEquals(Category, Category1);
            String 	Biller1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvProviderTitle").getText();
            Assert.assertEquals(Biller, Biller1);


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void Add(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement Add = driver.findElementById("com.nationstrust.mobilebanking:id/addBillerButton");
        Add.click();
    }


    public static void selectcategory(){
        WebElement selectcategory = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.TextView[2]");
        selectcategory.click();

    }
    public static void selectbiller(){
        WebElement selectbiller = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.support.v7.widget.RecyclerView/android.widget.TextView[1]");
        selectbiller.click();
//        WebElement accountfiled = driver.findElementById("com.nationstrust.mobilebanking:id/dynamicField1");
//        accountfiled.isDisplayed();
        WebElement accountfiled = driver.findElementById("com.nationstrust.mobilebanking:id/dynamicEditText1");
        accountfiled.isDisplayed();
    }


    public static void 	cancelbutton(){
        WebElement cancelbutton = driver.findElementById("com.nationstrust.mobilebanking:id/addBillerCancelButton");
        cancelbutton.click();

    }
    public static void 	addnickname(String nickname){
        String nickname2 ="Nickname";
        WebElement addnickname = driver.findElementById("com.nationstrust.mobilebanking:id/billerNickName");
        addnickname.click();
        addnickname.sendKeys(nickname);
        String 	nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/billerNick").getText();
        Assert.assertEquals(nickname1, nickname2);
        driver.hideKeyboard();

    }
    public static void 	addaccountnumber(String number){
//        WebElement addaccountnumber = driver.findElementById("com.nationstrust.mobilebanking:id/dynamicField1");
//        addaccountnumber.click();
        WebElement addaccountnumber = driver.findElementById("com.nationstrust.mobilebanking:id/dynamicEditText1");
        addaccountnumber.click();
        addaccountnumber.sendKeys(number);

    }



    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }





}


