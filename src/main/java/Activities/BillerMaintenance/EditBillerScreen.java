package Activities.BillerMaintenance;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class EditBillerScreen extends Configuration {
    public static void EditBillerScreen_title() throws Exception{
        String screenheading = "Edit Biller";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void EditBilerScreen_labels() throws Exception{
        String 	Category = "Category";
        String 	Biller = "Biller";
        String 	Nickname = "Nickname";

        try {
            String Category1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvCategoryTitle").getText();
            Assert.assertEquals(Category, Category1);
            String Biller1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvProviderTitle").getText();
            Assert.assertEquals(Biller, Biller1);
            String 	Nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/billerNick").getText();
            Assert.assertEquals(Nickname, Nickname1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void save(){
        WebElement save = driver.findElementById("com.nationstrust.mobilebanking:id/addBillerButton");
        save.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    public static void cancel(){
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/addBillerCancelButton");
        cancel.click();
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }


    public static void editbillernickname(String newnickname){
        WebElement oldnickname = driver.findElementById("com.nationstrust.mobilebanking:id/billerNickName");
        oldnickname.clear();
        oldnickname.sendKeys(newnickname);
        driver.hideKeyboard();


    }
    public static void editaccountnumber(String newaccnumber){
        WebElement oldaccountnumber = driver.findElementById("com.nationstrust.mobilebanking:id/dynamicField1");
        oldaccountnumber.clear();
        oldaccountnumber.sendKeys(newaccnumber);
        driver.hideKeyboard();

    }





    public static void back(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }





}


