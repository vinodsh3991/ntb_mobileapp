package Activities.BillerMaintenance;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class DeleteBillerScreen extends Configuration {
    public static void 	quickaccessmenuicon(){
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/billerMenu");
        quickaccessmenuicon.click();

    }

    public static void deleteicon(){
        WebElement deleteicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_delete");
        deleteicon.click();

    }



    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }


    public static void scrolltobiller(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement selectedbiller = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"CEB\"));");
        //WebElement selectedbiller1 = (WebElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"CEB\"));");
        selectedbiller.click();
//        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
//                until(ExpectedConditions.elementToBeClickable(selectedbiller1));
//        new Actions(driver).clickAndHold(longpress).perform();

    }
    public static void scrolltobilleraccount(){
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement selectedbiller = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"mn bm\"));");
        WebElement selectedbiller1 = (WebElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"mn bm\"));");
        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
                until(ExpectedConditions.elementToBeClickable(selectedbiller1));
        new Actions(driver).clickAndHold(longpress).perform();

    }
    public static void selectbilleraccount(){

        WebElement selectedbiller = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout");

        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
                until(ExpectedConditions.elementToBeClickable(selectedbiller));

        LongPressOptions longPressOptions = new LongPressOptions();
        longPressOptions.withDuration(Duration.ofSeconds(3)).withElement(ElementOption.element(longpress));

        new TouchAction(driver).longPress(longPressOptions).perform();

    }


    public static void selectbiller(){

        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        MobileElement selectedbiller = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"CEB\"));");
        WebElement selectedbiller1 = (WebElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"CEB\"));");
        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
                until(ExpectedConditions.elementToBeClickable(selectedbiller1));

        LongPressOptions longPressOptions = new LongPressOptions();
        longPressOptions.withDuration(Duration.ofSeconds(3)).withElement(ElementOption.element(longpress));

        new TouchAction(driver).longPress(longPressOptions).perform();

    }




}


