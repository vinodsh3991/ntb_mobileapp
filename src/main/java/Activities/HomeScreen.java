package Activities;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class HomeScreen extends Configuration {

    public static void HomeScreen_labels() throws Exception{
        String screenheading = "Home";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void quickaccessmenuicon(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

     WebElement quickaccessmenuicon = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.ExpandableListView/android.widget.LinearLayout[6]/android.widget.GridLayout/android.view.ViewGroup[5]/android.widget.ImageView");
//      //  WebElement quickaccessmenuicon  = driver.findElementByXPath("//android.widget.ImageView[@bounds='[472,1678][607,1813]']");
//
        quickaccessmenuicon.click();





    }


    public static void location(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement location = driver.findElementById("com.nationstrust.mobilebanking:id/location");
        location.click();

    }
    public static void navigationdrawericon(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement navigationdrawericon = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Open\"]");
        navigationdrawericon.click();

    }


    public static void notification(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement notification = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.support.v7.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.ImageView");
        notification.click();

    }

}
