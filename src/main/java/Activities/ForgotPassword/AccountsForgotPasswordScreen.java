package Activities.ForgotPassword;

import Activities.MonthCompare;
import TestBase.Configuration;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AccountsForgotPasswordScreen extends Configuration {
    public static void AccountsForgotPasswordScreen_title() throws Exception{
        String screenheading = "Forgot Password";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void AAccountsForgotPasswordScreen_labels() throws Exception{
        String 	AccountNumber = "Account Number";
        String 	Next = "Next";
        String 	NIC = "NIC/ Passport Number";
        String 	Birthday = "Birthday";
        try {
            String 	AccountNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/accNumberTextET").getText();
            Assert.assertEquals(AccountNumber1, AccountNumber);
            String 	Next1 = driver.findElementById("com.nationstrust.mobilebanking:id/accNextButton").getText();
            Assert.assertEquals(Next1, Next);
            String 	NIC1 = driver.findElementById("com.nationstrust.mobilebanking:id/idTextET").getText();
            Assert.assertEquals(NIC1, NIC);
            String 	Birthday1 = driver.findElementById("com.nationstrust.mobilebanking:id/birthdayTextET").getText();
            Assert.assertEquals(Birthday1, Birthday);



        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void enteraccountnumber(String accnumber){
        WebElement enteraccountnumber = driver.findElementById("com.nationstrust.mobilebanking:id/accNumberTextET");
        enteraccountnumber.click();
        enteraccountnumber.sendKeys(accnumber);
        driver.hideKeyboard();
    }
    public static void enternicnumber(String nicnumber){
        WebElement enternicnumber = driver.findElementById("com.nationstrust.mobilebanking:id/idTextET");
        enternicnumber.click();
        enternicnumber.sendKeys(nicnumber);
        driver.hideKeyboard();
    }

    public static void enterDOB(String dateofbirth){
        WebElement enterDOB = driver.findElementById("com.nationstrust.mobilebanking:id/birthdayTextET");
        enterDOB.click();
        WebElement taponyear = driver.findElementById("android:id/date_picker_header_year");
        taponyear.click();

        ((FindsByAndroidUIAutomator<MobileElement>) driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+1995+"\").instance(0))").click();

        MonthCompare monthCompare =new MonthCompare();
        monthCompare.comparemonth(dateofbirth);

    }


    public List   yearlist(){
        List yearlist1 = (List) driver.findElementById("android:id/date_picker_year_picker");

        return yearlist1;
    }

    public static void 	next(){
        WebElement next = driver.findElementById("com.nationstrust.mobilebanking:id/accNextButton");
        next.click();

    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]\n");
        back.click();
    }





}


