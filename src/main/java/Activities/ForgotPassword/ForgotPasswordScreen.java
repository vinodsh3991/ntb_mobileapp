package Activities.ForgotPassword;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class ForgotPasswordScreen extends Configuration {
    public static void  ForgotPasswordScreen_title() throws Exception{
        String screenheading = "Forgot Password";
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void  ForgotPasswordScreen_labels() throws Exception{
        String newpassword = "New Password";
        String confirmnewpassword = "Confirm New Password";
        String 	policydescription = "In order to keep your valuable information safe, we encourage you to use a strong password.\n" +
                "Your password must meet the following minimum requirements,";
        String use = "Use at least 1 each from the following";

        try {
            driver.hideKeyboard();
            String policydescription1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPwPolicyInfo1").getText();
            Assert.assertEquals(policydescription, policydescription1);
            String newpassword1 = driver.findElementById("com.nationstrust.mobilebanking:id/etNewPIN").getText();
            Assert.assertEquals(newpassword1, newpassword1);
            String confirmnewpassword1 = driver.findElementById("com.nationstrust.mobilebanking:id/etConfirmNewPIN").getText();
            Assert.assertEquals(confirmnewpassword1, confirmnewpassword);
            String use1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPwPolicyUseFollow").getText();
            Assert.assertEquals(use1, use);


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void 	donebutton(){
        WebElement donebutton = driver.findElementById("com.nationstrust.mobilebanking:id/btnDone");
        donebutton.click();
    }
    public static void 	enternewpassword(String newpassword){
        WebElement NewPIN = driver.findElementById("com.nationstrust.mobilebanking:id/etNewPIN");
        NewPIN.click();
        NewPIN.sendKeys(newpassword);
        driver.hideKeyboard();
    }

    public static void 	enterconfirmpassword(String confirmnewpassword){
        WebElement currentPIN = driver.findElementById("com.nationstrust.mobilebanking:id/etConfirmNewPIN");
        currentPIN.click();
        currentPIN.sendKeys(confirmnewpassword);
        driver.hideKeyboard();

    }

    public static void back(){
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}
