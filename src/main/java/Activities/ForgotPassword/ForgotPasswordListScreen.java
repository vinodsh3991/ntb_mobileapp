package Activities.ForgotPassword;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ForgotPasswordListScreen extends Configuration {
    public static void ForgotPasswordListScreen_title() throws Exception{
        String screenheading = "Forgot Password";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/welcomeText").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void myAccounts(){
        String 	MyAccounts = "My Accounts";
        String MyAccounts1 = driver.findElementById("com.nationstrust.mobilebanking:id/myAccountsButton").getText();
        Assert.assertEquals(MyAccounts1, MyAccounts);
        WebElement myAccounts = driver.findElementById("com.nationstrust.mobilebanking:id/myAccountsButton");
        myAccounts.click();
    }

    public static void myCredit(){
        String 	MyCreditCards = "My Credit Cards";
        String MyCreditCards1 = driver.findElementById("com.nationstrust.mobilebanking:id/myCardsButton").getText();
        Assert.assertEquals(MyCreditCards1, MyCreditCards);
        WebElement myCredit = driver.findElementById("com.nationstrust.mobilebanking:id/myCardsButton");
        myCredit.click();
    }

    public static void myDebit(){
        String screenheading = "My Debit Cards";
        String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/myDebitCardsButton").getText();
        Assert.assertEquals(screenheading, screenheading1);
        WebElement myDebit = driver.findElementById("com.nationstrust.mobilebanking:id/myDebitCardsButton");
        myDebit.click();
    }
    public static void quickaccessmenu(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/optionsBtn");
        quickaccessmenu.click();
    }


    public static void back(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementById("com.nationstrust.mobilebanking:id/backNavigation");
        back.click();
    }





}


