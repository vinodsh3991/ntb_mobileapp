package Activities;

import TestBase.Configuration;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.Date;

public class MonthCompare extends Configuration {

        public static void comparemonth(String date)
        {
            //12 February 1995
            String str = date;
            String[] arrOfStr = str.split(" ");
            String usermonth = arrOfStr[1];
            int monthumber = Month.valueOf(usermonth.toUpperCase()).getValue();

            YearMonth thisMonth    = YearMonth.now();

            if(thisMonth.isAfter(YearMonth.of(Integer.parseInt(arrOfStr[2]),monthumber ))){

                for (int i =0 ; i<= 1000 ; i++) {

                    try{
                        if(driver.findElementByXPath("//android.view.View[@content-desc=\""+ date+"\"]").isDisplayed()){
                            WebElement year =  driver.findElementByXPath("//android.view.View[@content-desc=\""+ date+ "\"]");
                            year.click();
                            WebElement okbutton = driver.findElementById("android:id/button1");
                            okbutton.click();
                            break;
                        }} catch (NoSuchElementException e){

                    }
                    WebElement arrowicon =  driver.findElementById("android:id/prev");
                    arrowicon.click();
                }


            }

            if(thisMonth.isBefore(YearMonth.of(Integer.parseInt(arrOfStr[2]),monthumber ))){

                for (int i =0 ; i<= 1000 ; i++) {

                    try{
                        if(driver.findElementByXPath("//android.view.View[@content-desc=\""+ date+"\"]").isDisplayed()){
                            WebElement year =  driver.findElementByXPath("//android.view.View[@content-desc=\""+ date+ "\"]");
                            year.click();
                            WebElement okbutton = driver.findElementById("android:id/button1");
                            okbutton.click();
                            break;
                        }} catch (NoSuchElementException e){

                    }
                    WebElement arrowicon =  driver.findElementById("android:id/next");
                    arrowicon.click();



                }

            }

            if(thisMonth.equals(YearMonth.of(Integer.parseInt(arrOfStr[2]),monthumber ))){
                WebElement year =  driver.findElementByXPath("//android.view.View[@content-desc=\""+ date+ "\"]");
                year.click();
                WebElement okbutton = driver.findElementById("android:id/button1");
                okbutton.click();
            }

            System.out.println();
        }


    }

