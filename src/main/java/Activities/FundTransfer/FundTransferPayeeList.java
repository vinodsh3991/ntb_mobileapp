package Activities.FundTransfer;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FundTransferPayeeList extends Configuration {
    public static void FundTransferPayeeList_labels() throws Exception {
        String screenheading = "Payee List";
        String screensubheading = "This transaction is allowed for following accounts";

        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading1, screenheading);
            String screensubheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/message").getText();
            Assert.assertEquals(screensubheading, screensubheading);
            WebElement onetimefundtransfer = driver.findElementById("com.nationstrust.mobilebanking:id/oneTimePaymentButton");
            onetimefundtransfer.isDisplayed();
            WebElement plusicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_add");
            plusicon.isDisplayed();

        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void onetimefundtransfer() {
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement onetimefundtransfer = driver.findElementById("com.nationstrust.mobilebanking:id/oneTimePaymentButton");
        onetimefundtransfer.click();


    }

    public static void backbutton() {
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void plusicon() {
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement plusicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_add");
        plusicon.click();
    }

    public static void selectownaccountpayee() {
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement selectpayee = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]");
        selectpayee.click();
    }
    public static void selectthirdpartyntbpayee() {
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement selectpayee = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView[2]");
        selectpayee.click();
    }

    public static void selectslipbankpayee() {
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        WebElement selectpayee = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView[2]");
//        selectpayee.click();
        MobileElement slippayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"ALLIANCE FINANCE COMPANY PLC\"));");
        slippayee.click();

    }
    public static void selectceftbankpayee() {
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        WebElement selectpayee = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView[2]");
//        selectpayee.click();
        MobileElement ceftpayee = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Bank Of Ceylon\"));");
        ceftpayee.click();
    }
}


