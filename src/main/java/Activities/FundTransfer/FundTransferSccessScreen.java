package Activities.FundTransfer;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FundTransferSccessScreen extends Configuration {
    public static void FundTransferSccessScreen_title() throws Exception {
        String screenheading = "Fund Transfer Successful";

        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvStatus").getText();
            Assert.assertEquals(screenheading1, screenheading);


        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void FundTransferSccessScreen_label() throws Exception {
        String emaillabel = "A copy of receipt sent to";
        String payfrom = "Paid From";
        String payto = "Paid To";
        String date = "Date";
        String remarks = "Remarks";
        String servicecharge = "Service Charge";
        String RefNumber = "Ref. Number";
        String amount = "Amount";
        String 	SaveReceipt = "Save Receipt";

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String emaillabel1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvEmailHeader").getText();
            Assert.assertEquals(emaillabel, emaillabel1);
            String payfrom1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay1").getText();
            Assert.assertEquals(payfrom1, payfrom);
            String payto1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2").getText();
            Assert.assertEquals(payto, payto1);
            String date1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvDate").getText();
            Assert.assertEquals(date, date1);
            String remarks1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvRemark").getText();
            Assert.assertEquals(remarks, remarks1);
            String servicecharge1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView").getText();
            Assert.assertEquals(servicecharge, servicecharge1);
            String 	amount1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView").getText();
            Assert.assertEquals(amount1, amount);
            WebElement tickimage = driver.findElementById("com.nationstrust.mobilebanking:id/statusImage");
            tickimage.isDisplayed();
            String 	SaveReceipt1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView").getText();
            Assert.assertEquals(SaveReceipt1, SaveReceipt);
            WebElement imageview = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ImageView");
            imageview.isDisplayed();
            WebElement savebutton = driver.findElementById("com.nationstrust.mobilebanking:id/llSaveReceipt");
            savebutton.isDisplayed();

            WebElement Pay1Name = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay1Name");
            Pay1Name.isDisplayed();
            WebElement Pay1Ref = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay1Ref");
            Pay1Ref.isDisplayed();
            WebElement Pay2Name = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2Name");
            Pay2Name.isDisplayed();
            WebElement Pay2Ref = driver.findElementById("com.nationstrust.mobilebanking:id/tvPay2Ref");
            Pay2Ref.isDisplayed();
            WebElement AmountRupees = driver.findElementById("com.nationstrust.mobilebanking:id/tvAmountRupees");
            AmountRupees.isDisplayed();
            WebElement AmountCents = driver.findElementById("com.nationstrust.mobilebanking:id/tvAmountCents");
            AmountCents.isDisplayed();
            WebElement ServiceCharge = driver.findElementById("com.nationstrust.mobilebanking:id/tvServiceCharge");
            ServiceCharge.isDisplayed();
            WebElement ServiceChargeCents = driver.findElementById("com.nationstrust.mobilebanking:id/tvServiceChargeCents");
            ServiceChargeCents.isDisplayed();
            WebElement DateText = driver.findElementById("com.nationstrust.mobilebanking:id/tvDateText");
            DateText.isDisplayed();



            MobileElement scrolltobiller = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Ref. Number\"));");
            scrolltobiller.click();

            String 	RefNumber1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvRef").getText();
            Assert.assertEquals(RefNumber1, RefNumber);
            WebElement RefText = driver.findElementById("com.nationstrust.mobilebanking:id/tvRefText");
            RefText.isDisplayed();



        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void homebutton() {
        String homebutton2 ="Home";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement homebutton = driver.findElementById("com.nationstrust.mobilebanking:id/homeButton");
        String homebutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/homeButton").getText();
        Assert.assertEquals(homebutton1, homebutton2);
        homebutton.click();


    }

    public static void backbutton() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void 	ReturntoTransfers() {
        String ReturntoTransfers2 = "Return to Transfers Screen";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement ReturntoTransfers = driver.findElementById("com.nationstrust.mobilebanking:id/anotherPaymentButton");
        String ReturntoTransfers1 = driver.findElementById("com.nationstrust.mobilebanking:id/anotherPaymentButton").getText();
        Assert.assertEquals(ReturntoTransfers1, ReturntoTransfers2);
        ReturntoTransfers.click();
    }

    public static void savereceipt() {
        String savereceipt2 = "Save Receipt";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement savereceipt = driver.findElementByXPath("com.nationstrust.mobilebanking:id/llSaveReceipt");
        String savereceipt1 = driver.findElementByXPath("com.nationstrust.mobilebanking:id/llSaveReceipt").getText();
        Assert.assertEquals(savereceipt1, savereceipt2);
        savereceipt.click();
    }

    public static void 	savebiller() {
        String savepayee2 = "Save Payee";
        WebElement savepayee = driver.findElementById("com.nationstrust.mobilebanking:id/homeButton");
        String savepayee1 = driver.findElementById("com.nationstrust.mobilebanking:id/homeButton").getText();
        Assert.assertEquals(savepayee1, savepayee2);
        savepayee.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

}
