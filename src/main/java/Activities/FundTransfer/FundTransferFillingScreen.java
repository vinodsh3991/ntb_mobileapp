package Activities.FundTransfer;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FundTransferFillingScreen extends Configuration {

    public static void FundTransferFillingScreen_title() throws Exception{
        String screenheading = "Fund Transfer";

        //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading1, screenheading);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void FundTransferFillingScreen_labels() throws Exception{
        String PayTo = "Pay To";
        String 	SelectBiller = "Select Payee";
        String PayFrom = "Pay From";
        String SelectAccount = "Select Account";
        String 	Amount = "Amount";
        String LKR = "LKR";
        String ooo = "0.00";
        String Remarks = "Remarks";
        String maximum = "(Maximum 30 alphanumeric)";
        String description="To make payments to Credit Cards please use the \"Payments\" Option on the Main Menu.";

        //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String PayTo1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(PayTo1,PayTo );
            String SelectBiller1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvSelectPayee").getText();
            Assert.assertEquals(SelectBiller1, SelectBiller);
            String PayFrom1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(PayFrom1,PayFrom );
            String SelectAccount1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvSelectAccount").getText();
            Assert.assertEquals(SelectAccount1, SelectAccount);
            String 	Amount1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(Amount1,Amount);
            String LKR1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvCurrency").getText();
            Assert.assertEquals(LKR1,LKR);
            String ooo1 = driver.findElementById("com.nationstrust.mobilebanking:id/paymentAmount").getText();
            Assert.assertEquals(ooo1,ooo );
            String Remarks1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]").getText();
            Assert.assertEquals(Remarks1, Remarks);
            String maximum1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]").getText();
            Assert.assertEquals(maximum1,maximum );
            String Remarks2 = driver.findElementById("com.nationstrust.mobilebanking:id/etRemarks").getText();
            Assert.assertEquals(Remarks2,Remarks );
            WebElement payeearrow = driver.findElementById("com.nationstrust.mobilebanking:id/ivSelectPayeeNext");
            payeearrow.isDisplayed();
            WebElement accountarrow = driver.findElementById("com.nationstrust.mobilebanking:id/ivSelectAccountNext");
            accountarrow.isDisplayed();
            WebElement quickaccessmenu = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
            quickaccessmenu.isDisplayed();
            String description1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            Assert.assertEquals(description1,description );
            WebElement inforicon = driver.findElementById("com.nationstrust.mobilebanking:id/btnNominalInfo");
            inforicon.isDisplayed();


        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void quickaccessmenuicon(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement quickaccessmenuicon = driver.findElementById("com.nationstrust.mobilebanking:id/ivMenu");
        quickaccessmenuicon.click();


    }
    public static void backbutton(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }
    public static void paybutton(){

        WebElement paybutton = driver.findElementById("com.nationstrust.mobilebanking:id/btnPay");
        paybutton.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static void payfrom(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement payfrom = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout");
        payfrom.click();
    }
    public static void payto(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement paybutton = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout");
        paybutton.click();
    }
    public static void amount(String amount){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement amount1 = driver.findElementById("com.nationstrust.mobilebanking:id/paymentAmount");
        amount1.click();
        amount1.sendKeys(amount);
        driver.hideKeyboard();
    }
    public static void remarks(String remarks){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement remarks1 = driver.findElementById("com.nationstrust.mobilebanking:id/etRemarks");
        remarks1.click();
        remarks1.sendKeys(remarks);
        driver.hideKeyboard();

    }
}
