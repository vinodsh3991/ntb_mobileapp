package Activities.FundTransfer;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class FundTransferVerifyInfoScreen  extends Configuration {
    public static void FundTransferVerifyInfoScreen_title() throws Exception {
        String screenheading = "Verify Information";
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading1, screenheading);


        } catch (Exception ex) {
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }
    public static void FundTransferVerifyInfoScreen_labels() throws Exception {
        String payfrom = "Pay From";
        String payto = "Pay To";
        String date = "Date";
        String remarks = "Remarks";
        String servicecharge = "Service Charge";
        String 	Balancebefore = "Balance before";
        String Balanceafter = "Balance after";
        String lkr="LKR";
        try {
        String payfrom1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView").getText();
        Assert.assertEquals(payfrom, payfrom1);
        String payto1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvPayTo").getText();
        Assert.assertEquals(payto, payto1);
        String date1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvDate").getText();
        Assert.assertEquals(date, date1);
        String remarks1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvRemark").getText();
        Assert.assertEquals(remarks, remarks1);
        WebElement lkr3 = driver.findElementById("com.nationstrust.mobilebanking:id/tvAmount");
        lkr3.isDisplayed();
        WebElement amount = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout");
        amount.isDisplayed();
        WebElement cent = driver.findElementById("com.nationstrust.mobilebanking:id/tvAmountCents");
        cent.isDisplayed();
        String Balancebefore1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
        Assert.assertEquals(Balancebefore1, Balancebefore);
        String Balanceafter1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView").getText();
        Assert.assertEquals(Balanceafter1, Balanceafter);
        String lkr1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvBalBefore").getText();
        Assert.assertEquals(lkr1, lkr);
        WebElement BalanceCents = driver.findElementById("com.nationstrust.mobilebanking:id/tvBalanceCents");
        BalanceCents.isDisplayed();
        WebElement BalanceRupees = driver.findElementById("com.nationstrust.mobilebanking:id/tvBalanceRupees");
        BalanceRupees.isDisplayed();
        String lkr2 = driver.findElementById("com.nationstrust.mobilebanking:id/tvBalAfter").getText();
        Assert.assertEquals(lkr2, lkr);
        WebElement BalanceAfterRupees = driver.findElementById("com.nationstrust.mobilebanking:id/tvBalanceAfterRupees");
        BalanceAfterRupees.isDisplayed();
        WebElement BalanceAfterCents = driver.findElementById("com.nationstrust.mobilebanking:id/tvBalanceAfterCents");
        BalanceAfterCents.isDisplayed();

        WebElement PayFromName = driver.findElementById("com.nationstrust.mobilebanking:id/tvPayFromName");
        PayFromName.isDisplayed();
        WebElement PayFromAccount = driver.findElementById("com.nationstrust.mobilebanking:id/tvPayFromAccount");
        PayFromAccount.isDisplayed();
        WebElement PayToName = driver.findElementById("com.nationstrust.mobilebanking:id/tvPayToName");
        PayToName.isDisplayed();
        WebElement PayToAccount = driver.findElementById("com.nationstrust.mobilebanking:id/tvPayToAccount");
        PayToAccount.isDisplayed();
        WebElement DateText = driver.findElementById("com.nationstrust.mobilebanking:id/tvDateText");
        DateText.isDisplayed();

        MobileElement scrolltobiller = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"Service Charge\"));");
        scrolltobiller.click();

        String servicecharge1 = driver.findElementById("com.nationstrust.mobilebanking:id/textView22").getText();
        Assert.assertEquals(servicecharge, servicecharge1);

        WebElement ServiceChargeCurrency = driver.findElementById("com.nationstrust.mobilebanking:id/tvServiceChargeCurrency");
        ServiceChargeCurrency.isDisplayed();
        WebElement ServiceCharge = driver.findElementById("com.nationstrust.mobilebanking:id/tvServiceCharge");
        ServiceCharge.isDisplayed();
        WebElement ServiceChargeCents = driver.findElementById("com.nationstrust.mobilebanking:id/tvServiceChargeCents");
        ServiceChargeCents.isDisplayed();


        } catch (Exception ex) {
        System.out.println("Cause: " + ex.getCause());
        System.out.println("Message: " + ex.getMessage());
        ex.printStackTrace();
    }
        Thread.sleep(3000);
}


    public static void onetimefundtransfer() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement onetimefundtransfer = driver.findElementById("com.nationstrust.mobilebanking:id/oneTimePaymentButton");
        onetimefundtransfer.click();


    }

    public static void backbutton() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }

    public static void confirmbutton() {
        String confirmbutton2 = "Confirm";
        WebElement confirmbutton = driver.findElementById("com.nationstrust.mobilebanking:id/payConfirmButton");
        String  confirmbutton1 = driver.findElementById("com.nationstrust.mobilebanking:id/payConfirmButton").getText();
        Assert.assertEquals(confirmbutton1, confirmbutton2);
        confirmbutton.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public static void cancel() {
        String cancel2 = "Cancel";
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/payCancelButton");
        String  cance1 = driver.findElementById("com.nationstrust.mobilebanking:id/payCancelButton").getText();
        Assert.assertEquals(cance1, cancel2);
        cancel.click();
    }
}
