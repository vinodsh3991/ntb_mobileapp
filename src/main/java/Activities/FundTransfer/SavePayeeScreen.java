package Activities.FundTransfer;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class SavePayeeScreen extends Configuration {
    public static void SavePayeeScreen_lables() throws Exception{
        String screenheading = "Add Payee";
        String Nickname = "Nickname";
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading1, screenheading);
            String Nickname1 = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNick").getText();
            Assert.assertEquals(Nickname1, Nickname);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void save(){
        WebElement save = driver.findElementById("com.nationstrust.mobilebanking:id/addPayeeButton");
        save.click();

    }

    public static void cancel(){
        WebElement cancel = driver.findElementById("com.nationstrust.mobilebanking:id/cancelAddPayeeButton");
        cancel.click();

    }


    public static void enternickname(String newnickname){
        WebElement enternickname = driver.findElementById("com.nationstrust.mobilebanking:id/payeeNickName");
        enternickname.click();
        enternickname.clear();
        enternickname.sendKeys(newnickname);
        driver.hideKeyboard();
    }


    public static void backbutton(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement backbutton = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        backbutton.click();
    }



}

