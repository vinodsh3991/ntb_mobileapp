package Activities.Inbox;

import Activities.LongPress;
import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class SentMainScreen extends Configuration {
    public static void  SentMainScreen_title() throws Exception{
        String screenheading = "Sent";
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void  SentMainScreen_labels() throws Exception{

        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            driver.hideKeyboard();
            WebElement composeicon = driver.findElementById("com.nationstrust.mobilebanking:id/inboxFab");
            composeicon.isDisplayed();
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void sentemptyscreen(){
        String emptyscreenlabel1= "You have no mails";
        String emptyscreenlabel = driver.findElementById("com.nationstrust.mobilebanking:id/inboxEmptyDes").getText();
        Assert.assertEquals(emptyscreenlabel, emptyscreenlabel1);

        WebElement imageicon = driver.findElementById("com.nationstrust.mobilebanking:id/inboxEmptyImage");
        imageicon.isDisplayed();
    }


    public static void 	searchbar(){
        WebElement searchbar = driver.findElementById("com.nationstrust.mobilebanking:id/inboxSearch");
        searchbar.click();
    }

    public static void 	taponmail(){
        WebElement taponmail = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout");
        taponmail.click();
    }
    public static void 	composebutton(){
        WebElement composebutton = driver.findElementById("com.nationstrust.mobilebanking:id/inboxFab");
        composebutton.click();
    }

    public static void 	deleteicon(){
        WebElement deletebutton = driver.findElementById("com.nationstrust.mobilebanking:id/action_delete");
        deletebutton.click();
    }
    public static void 	selectallicon(){
        WebElement selectallicon = driver.findElementById("com.nationstrust.mobilebanking:id/cbSelectAll");
        selectallicon.click();
    }
    public static void 	selectmail(){
        WebElement selectmail = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout");
        LongPress.longpressselect(selectmail);
//        MobileElement longpress = (MobileElement) new WebDriverWait(driver, 30).
//                until(ExpectedConditions.elementToBeClickable(selectmail));
//
//        LongPressOptions longPressOptions = new LongPressOptions();
//        longPressOptions.withDuration(Duration.ofSeconds(3)).withElement(ElementOption.element(longpress));
//
//        new TouchAction(driver).longPress(longPressOptions).perform();
    }



    public static void back(){
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }


    public static void 	inboxtab(){
        WebElement inboxtab = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.a.c[1]/android.widget.TextView");
        inboxtab.click();
    }

}


