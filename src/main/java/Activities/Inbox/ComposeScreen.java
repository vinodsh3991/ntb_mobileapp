package Activities.Inbox;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ComposeScreen  extends Configuration {
    public static void  ComposeScreen_title() throws Exception{
        String screenheading = "Compose";
      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void  ComposeScreen_labels() throws Exception{
        String 	Subject = "Subject";
        String 	Message = "Message";
        String 	attachmentlabel = "(Tap icon to attach an image)";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            driver.hideKeyboard();
            String Subject1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").getText();
            Assert.assertEquals(Subject, Subject1);
            String Message1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(Message, Message1);
            WebElement attachmenticon = driver.findElementById("com.nationstrust.mobilebanking:id/attachment");
            attachmenticon.isDisplayed();
            String attachmentlabel1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]").getText();
            Assert.assertEquals(attachmentlabel, attachmentlabel1);
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }


    public static void 	sendbutton(){
        WebElement sendbutton = driver.findElementById("com.nationstrust.mobilebanking:id/sendInboxButton");
        sendbutton.click();
    }
    public static void 	messagebox(String message){
        WebElement messagebox = driver.findElementById("com.nationstrust.mobilebanking:id/message");
        messagebox.click();
        messagebox.sendKeys(message);
        driver.hideKeyboard();
    }
    public static void 	selectsubject(){
        WebElement selectsubject = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.TextView[4]");
        selectsubject.click();
    }
    public static void 	attachimage(){
        WebElement attachimage = driver.findElementById("com.nationstrust.mobilebanking:id/attachment");
        attachimage.click();
        WebElement chooseimage = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]");
        chooseimage.click();
        WebElement allow = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
        allow.click();
        WebElement selectphotos = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.view.ViewPager/android.widget.GridView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.ImageView");
        selectphotos.click();
        WebElement selectfolder = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[4]/android.view.View");
        selectfolder.click();
//        TouchAction touchAction = new TouchAction(driver);
//        touchAction.tap(PointOption.point(137, 329)).perform();
//        TouchAction touchAction = new TouchAction(driver);
//        touchAction.tap(new PointOption().withCoordinates(137, 329)).perform();

        WebElement selectimage = driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Photo taken on 9 Sep 2020 12:34:51 p.m.\"]");
        selectimage.click();
//        WebElement tick = driver.findElementById("com.android.gallery3d:id/head_select_right");
//        tick.click();
        WebElement attachedimage= driver.findElementById("com.nationstrust.mobilebanking:id/attachmentImage");
        attachedimage.isDisplayed();

    }

    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}


