package Activities.Inbox;

import TestBase.Configuration;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class InboxMessageDetailsScreen extends Configuration {

    public static void  MessageDetailsScreen_title() throws Exception{
        String screenheading = "Inbox";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void  MessageDetailsScreen_labels() throws Exception{

        try {
            WebElement deleteicon = driver.findElementById("com.nationstrust.mobilebanking:id/action_delete");
            deleteicon.isDisplayed();
            WebElement messagethreadicon = driver.findElementById("com.nationstrust.mobilebanking:id/inboxDetailsDrawable");
            messagethreadicon.isDisplayed();
            WebElement timeanddate = driver.findElementById("com.nationstrust.mobilebanking:id/inboxDetailsDate");
            timeanddate.isDisplayed();
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	replybutton(){
        WebElement replybutton = driver.findElementById("com.nationstrust.mobilebanking:id/replyButton");
        replybutton.click();
    }
    public static void 	deleteicon(){
        WebElement deletebutton = driver.findElementById("com.nationstrust.mobilebanking:id/action_delete");
        deletebutton.click();
    }


    public static void back(){
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}


