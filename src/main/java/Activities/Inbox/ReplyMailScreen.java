package Activities.Inbox;

import TestBase.Configuration;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ReplyMailScreen extends Configuration {
    public static void  ReplyMailScreen_title() throws Exception{
        String screenheading = "Reply";
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        try {
            String screenheading1 = driver.findElementById("com.nationstrust.mobilebanking:id/tvTitle").getText();
            Assert.assertEquals(screenheading, screenheading1);

        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void  ReplyMailScreen_labels() throws Exception{
        String Subject = "Subject";
        String Message = "Message";
        String 	attachmentlabel = "(Tap icon to attach an image)";
        try {
            String Subject1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText();
            Assert.assertEquals(Subject, Subject1);
            String Message1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView").getText();
            Assert.assertEquals(Message, Message1);
            WebElement subjecticon = driver.findElementById("com.nationstrust.mobilebanking:id/subject");
            subjecticon.isDisplayed();
            String attachmentlabel1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]").getText();
            Assert.assertEquals(attachmentlabel, attachmentlabel1);
            WebElement morebutton = driver.findElementById("com.nationstrust.mobilebanking:id/inboxMoreButton");
            morebutton.isDisplayed();
        }
        catch(Exception ex){
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }
        Thread.sleep(3000);
    }

    public static void 	morebutton(){
        WebElement morebutton = driver.findElementById("com.nationstrust.mobilebanking:id/inboxMoreButton");
        morebutton.click();
        MobileElement scroll = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().textContains(\"wrote\"));");
        WebElement dateandsubject = driver.findElementById("com.nationstrust.mobilebanking:id/inboxReplyDate");
        dateandsubject.isDisplayed();
//        WebElement wrote = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView[2]");
//        wrote.isDisplayed();
        WebElement subject = driver.findElementById("com.nationstrust.mobilebanking:id/inboxReplyType");
        subject.isDisplayed();
        WebElement date = driver.findElementById("com.nationstrust.mobilebanking:id/inboxReplyTime");
        date.isDisplayed();
        WebElement subjecticon = driver.findElementById("com.nationstrust.mobilebanking:id/inboxReplyDrawable");
        subjecticon.isDisplayed();
    }

    public static void 	sendbutton(){
        WebElement sendbutton = driver.findElementById("com.nationstrust.mobilebanking:id/sendInboxButton");
        sendbutton.click();
    }

    public static void 	messagebox(String message){
        WebElement messagebox = driver.findElementById("com.nationstrust.mobilebanking:id/message");
        messagebox.click();
        messagebox.sendKeys(message);
        driver.hideKeyboard();
    }

    public static void back(){
     //   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement back = driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");
        back.click();
    }



}


