package TestBase;



import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.remote.DesiredCapabilities;




import java.net.URL;

public class Configuration {

    public static AndroidDriver driver;



    @BeforeMethod
    public void setup() {

        try {


            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "HUAWEI BLL-L22");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0");
            caps.setCapability(MobileCapabilityType.UDID, "84RDU18108000595");
            caps.setCapability("appPackage", "com.nationstrust.mobilebanking");
            caps.setCapability("appActivity", "com.nationstrust.mobilebanking.fFwEfWdObofzXcD.SDDdsBDiJbTFKmS");
            //caps.setCapability(MobileCapabilityType.NO_RESET, true);





            URL url = new URL("http://localhost:4723/wd/hub");
            driver = new AndroidDriver(url, caps);



        }
        catch(Exception ex) {
            System.out.println("Cause: "+ex.getCause());
            System.out.println("Message: "+ex.getMessage());
            ex.printStackTrace();
        }





}
    public static void main(String[] args){
        Configuration configuration=new Configuration();
        configuration.setup();

    }



}


