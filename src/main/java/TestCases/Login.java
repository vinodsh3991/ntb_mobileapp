package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.InvalidLoginDetails;
import Listeners.AllureReportListener;
import Activities.*;

import java.util.concurrent.TimeUnit;

import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({AllureReportListener.class})

public class Login extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();

    }



    @Test(priority = 1, description = "Verify user can login using valid username and password")
    public static void login_validUsername_ValidPassword() throws Exception {
        InvalidLoginDetails invalidLoginDetails = new InvalidLoginDetails();
        LoginUsername.valid_username(invalidLoginDetails.getValidusername());
        LoginPassword.valid_password(invalidLoginDetails.getValidpassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

       driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test(priority = 2, description = "Verify user can login using invalid username and valid password")
    public static void login_invalidUsername_validPassword() throws Exception {
        InvalidLoginDetails  invalidLoginDetails = new InvalidLoginDetails();
        LoginUsername.invalid_username(invalidLoginDetails.getInvalidusername());
        LoginPassword.valid_password(invalidLoginDetails.getValidpassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Inactive User");
        PopUpMessages.popupmessage_content("User is not active");


    }

    @Test(priority = 3, description = "Verify user can login using valid username and invalid password")
    public static void login_validUsername_invalidPassword() throws Exception {
        InvalidLoginDetails invalidLoginDetails  = new InvalidLoginDetails();
        LoginUsername.valid_username(invalidLoginDetails.getValidusername());
        LoginPassword.invalid_password(invalidLoginDetails.getInvalidpassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Incorrect information");
        PopUpMessages.popupmessage_content("Incorrect information. Please check and try again");
    }

    @Test(priority = 4, description = "Verify user can login using invalid username and invalid password")
    public static void login_invalidUsername_invalidPassword() throws Exception {
        InvalidLoginDetails invalidLoginDetails  = new InvalidLoginDetails();
        LoginUsername.invalid_username(invalidLoginDetails.getInvalidusername());
        LoginPassword.invalid_password(invalidLoginDetails.getInvalidpassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Inactive User");
        PopUpMessages.popupmessage_content("User is not active");

    }
// @DataProvider
//    public Object[][] invalidusernamevalidpassword() {
//        return new Object[][]{
//                new Object[]{"BOTHTEN1", "Password@2"},
//
//        };
//    }
//
//    @DataProvider
//    public Object[][] validusernamepassword() {
//        return new Object[][]{
//                new Object[]{"BOTHTEN", "Password@2"},
//
//        };
//    }
//
//    @DataProvider
//    public Object[][] validusernameinvalidpassword() {
//        return new Object[][]{
//                new Object[]{"BOTHTEN", "Passwordd@2"},
//
//        };
//    }
//
//    @DataProvider
//    public Object[][]  invalidusernameinvalidpassword() {
//        return new Object[][]{
//                new Object[]{"BOTHTwEN", "Passwordw@2"},
//
//        };
    }


