package TestCases;

import Activities.Common;
import Activities.ForgotPassword.AccountsForgotPasswordScreen;
import Activities.ForgotPassword.ForgotPasswordListScreen;
import Activities.ForgotPassword.ForgotPasswordScreen;
import Activities.ForgotUsername.AccountsForgotUsernameScreen;
import Activities.ForgotUsername.ForgotUsernameListScreen;
import Activities.HomeScreen;
import Activities.LanguageSelection;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.LoginScreens.FristTimeLoginScreen;
import Activities.LoginScreens.NormalLoginScreen;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.OTPScreens.CommonOTPScreen;
import Activities.OTPScreens.ForgotPasswordOTPScreen;
import Activities.OTPScreens.ForgotUsernameOTPScreen;
import Activities.OTPScreens.LoginOTPScreen;
import Activities.PopUpMessages.PopUpMessages;
import TestBase.Configuration;
import TestData.ChangePassworDetails;
import TestData.ForgotPasswordLoginDetails;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ForgotPassword extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        ForgotPasswordLoginDetails forgotPasswordLoginDetails = new ForgotPasswordLoginDetails();
        LoginUsername.valid_username(forgotPasswordLoginDetails.getUsername());
        LoginPassword.valid_password(forgotPasswordLoginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();


        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

        HomeScreen.navigationdrawericon();
        Navigationdrawer.logout();

    }

    @Test(priority = 1, description = "My accounts - forgot password")
    public static void myaccountforgotpassword() throws Exception {
        NormalLoginScreen.forgotpassword();
        ForgotPasswordListScreen.ForgotPasswordListScreen_title();
        ForgotPasswordListScreen.myAccounts();
        AccountsForgotPasswordScreen.AccountsForgotPasswordScreen_title();
        AccountsForgotPasswordScreen.AAccountsForgotPasswordScreen_labels();
        AccountsForgotPasswordScreen.enteraccountnumber("029215035020");
        AccountsForgotPasswordScreen.enternicnumber("950431016V");
        AccountsForgotPasswordScreen.enterDOB("12 February 1995");
        AccountsForgotPasswordScreen.next();
        CommonOTPScreen.CommonOTPScreen_title();
        CommonOTPScreen.CommonOTPScreen_labels();
        CommonOTPScreen.enetrOTP("1236589563");
        ForgotPasswordScreen.ForgotPasswordScreen_title();
        ForgotPasswordScreen.ForgotPasswordScreen_labels();
        ForgotPasswordScreen.enternewpassword("Pasword@1");
        ForgotPasswordScreen.enterconfirmpassword("Pasword@1");
        ForgotPasswordScreen.donebutton();
        NormalLoginScreen.NormalLoginScreen_labels();

    }

    @Test(priority = 2, description = "My accounts - forgot password - Negative Flow 01")
    public static void negatibeflow01() throws Exception {
        NormalLoginScreen.forgotpassword();
        ForgotPasswordListScreen.ForgotPasswordListScreen_title();
        ForgotPasswordListScreen.myAccounts();
        AccountsForgotPasswordScreen.AccountsForgotPasswordScreen_title();
        AccountsForgotPasswordScreen.AAccountsForgotPasswordScreen_labels();
        AccountsForgotPasswordScreen.enteraccountnumber("029215035020");
        AccountsForgotPasswordScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AccountsForgotPasswordScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        NormalLoginScreen.NormalLoginScreen_labels();

    }
    @Test(priority = 3, description = "My accounts - forgot password - Negative Flow 02")
    public static void negatibeflow02() throws Exception {
        NormalLoginScreen.forgotpassword();
        ForgotPasswordListScreen.ForgotPasswordListScreen_title();
        ForgotPasswordListScreen.myAccounts();
        AccountsForgotPasswordScreen.AccountsForgotPasswordScreen_title();
        AccountsForgotPasswordScreen.AAccountsForgotPasswordScreen_labels();
        AccountsForgotPasswordScreen.enteraccountnumber("029215035020");
        AccountsForgotPasswordScreen.enternicnumber("950431016V");
        AccountsForgotPasswordScreen.enterDOB("12 February 1995");
        AccountsForgotPasswordScreen.next();
        CommonOTPScreen.CommonOTPScreen_title();
        CommonOTPScreen.CommonOTPScreen_labels();
        CommonOTPScreen.backbutton();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        CommonOTPScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        NormalLoginScreen.NormalLoginScreen_labels();

    }


    @Test(priority = 3, description = "My accounts - forgot password - Negative Flow 03")
    public static void negatibeflow03() throws Exception {
        NormalLoginScreen.forgotpassword();
        ForgotPasswordListScreen.ForgotPasswordListScreen_title();
        ForgotPasswordListScreen.myAccounts();
        AccountsForgotPasswordScreen.AccountsForgotPasswordScreen_title();
        AccountsForgotPasswordScreen.AAccountsForgotPasswordScreen_labels();
        AccountsForgotPasswordScreen.enteraccountnumber("029215035020");
        AccountsForgotPasswordScreen.enternicnumber("950431016V");
        AccountsForgotPasswordScreen.enterDOB("12 February 1995");
        AccountsForgotPasswordScreen.next();
        CommonOTPScreen.CommonOTPScreen_title();
        CommonOTPScreen.CommonOTPScreen_labels();
        CommonOTPScreen.enetrOTP("1236589563");
        ForgotPasswordScreen.ForgotPasswordScreen_title();
        ForgotPasswordScreen.ForgotPasswordScreen_labels();
        ForgotPasswordScreen.enternewpassword("Pasword@1");
        ForgotPasswordScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        ForgotPasswordScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        NormalLoginScreen.NormalLoginScreen_labels();


    }


}