package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import Activities.PayeeMaintenance.AddPayeeScreen;
import Activities.PayeeMaintenance.ConfirmPayeeDetailsScreen;
import Activities.PayeeMaintenance.PayeeDetailsScreen;
import Activities.PayeeMaintenance.PayeeMaintenance;
import Activities.PopUpMessages.PopUpMessages;
import TestData.AddBillerPayee;
import TestData.LoginDetails;
import Activities.*;
import TestBase.Configuration;
import Activities.FundTransfer.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.PostQuickAccessMenuList.PostQuickAccessMenuList;
import Activities.OTPScreens.FundTrnasferOTPSCreen;
import Activities.OTPScreens.LoginOTPScreen;
import io.qameta.allure.Step;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;


public class FundTransfer extends Configuration {

    @BeforeMethod( description = "Login to the app")
    @Step("Login with username::{0} and password: {1}")
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }

    @Test(priority = 1, description = "Fund Transfer to Own Account under Below OTP")
    public static void fundtransfertoownaccountbelowOTP() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectownaccountpayee();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        FundTransferSccessScreen.ReturntoTransfers();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to Current - Staff account no 0011******** for LKR 0.25 on "+currentDateTime.gettimeanddate()+" SLST.");

    }

    @Test(priority = 2, description = "Fund Transfer to Own Account under Above OTP")
    public static void fundtransfertoownaccountaboveotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectownaccountpayee();
        FundTransferFillingScreen.amount("5.65");
        FundTransferFillingScreen.remarks("AboveOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTrnasferOTPSCreen.enetrOTP("12345678");
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        FundTransferSccessScreen.ReturntoTransfers();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to Current - Staff account no 0011******** for LKR 5.65 on "+currentDateTime.gettimeanddate()+" SLST.");

    }

    @Test(priority = 3, description = "Fund Transfer to ThirdParty NTB  under Below OTP")
    public static void fundtransfertothirdpartyntbbelowotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectthirdpartyntbpayee();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        FundTransferSccessScreen.ReturntoTransfers();
//        CurrentDateTime currentDateTime =new CurrentDateTime();
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to Current - Staff account no 0011******** for LKR 5.65 on "+currentDateTime.gettimeanddate()+" SLST.");

    }
    @Test(priority = 4, description = "Fund Transfer to ThirdParty NTB  under Above OTP")
    public static void fundtransfertothirdpartyntbaboveotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectthirdpartyntbpayee();
        FundTransferFillingScreen.amount("6.25");
        FundTransferFillingScreen.remarks("AboveOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTrnasferOTPSCreen.enetrOTP("12345678");
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        FundTransferSccessScreen.ReturntoTransfers();
//        CurrentDateTime currentDateTime =new CurrentDateTime();
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to Current - Staff account no 0011******** for LKR 5.65 on "+currentDateTime.gettimeanddate()+" SLST.");

    }

    @Test(priority = 5, description = "Fund Transfer to Slip Bank under Below OTP")
    public static void fundtransfertoslipbankbelowotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectslipbankpayee();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        FundTransferSccessScreen.homebutton();
        HomeScreen.HomeScreen_labels();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to");

    }
    @Test(priority = 6, description = "Fund Transfer to Slip Bank under Above OTP")
    public static void fundtransfertoslipbankaboveotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectslipbankpayee();
        FundTransferFillingScreen.amount("6.25");
        FundTransferFillingScreen.remarks("AboveOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTrnasferOTPSCreen.enetrOTP("12345678");
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        FundTransferSccessScreen.ReturntoTransfers();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to");

    }
    @Test(priority = 7, description = "Fund Transfer to Ceft Bank under Below OTP")
    public static void fundtransfertoceftbankbelowotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectceftbankpayee();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        FundTransferSccessScreen.ReturntoTransfers();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to");

    }
    @Test(priority = 8, description = "Fund Transfer to Ceft Bank under Above OTP")
    public static void fundtransfertoceftbankaboveotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectceftbankpayee();
        FundTransferFillingScreen.amount("6.25");
        FundTransferFillingScreen.remarks("AboveOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTrnasferOTPSCreen.enetrOTP("11111111");
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        FundTransferSccessScreen.ReturntoTransfers();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to");

    }

    @Test(priority = 9, description = "Fund Transfer filling screen - Negative Flow 01")
    public static void negativeflow01() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.backbutton();
        PopUpMessages.popupmessage_header("Cancel Fund Transfer");
        PopUpMessages.popupmessage_content("Do you want to cancel the transaction?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FundTransferFillingScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();

    }
    @Test(priority = 10, description = "Fund Transfer filling screen - Negative Flow 02")
    public static void negativeflow02() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.backbutton();
        HomeScreen.HomeScreen_labels();
    }

    @Test(priority = 11, description = "Fund Transfer -Verify Information screen - Negative Flow 03")
    public static void negativeflow03() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectownaccountpayee();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.backbutton();
        PopUpMessages.popupmessage_header("Cancel Fund Transfer");
        PopUpMessages.popupmessage_content("Do you want to cancel the transaction?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FundTransferVerifyInfoScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FundTransferFillingScreen.FundTransferFillingScreen_title();


    }


    @Test(priority = 11, description = "Fund Transfer -Verify Information screen - Negative Flow 04")
    public static void negativeflow04() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectownaccountpayee();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.cancel();
        PopUpMessages.popupmessage_header("Cancel Fund Transfer");
        PopUpMessages.popupmessage_content("Do you want to cancel the transaction?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FundTransferVerifyInfoScreen.cancel();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FundTransferFillingScreen.FundTransferFillingScreen_title();
    }

    @Test(priority = 11, description = "Fund Transfer - OTP screen - Negative Flow 05")
    public static void negativeflow05() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.selectslipbankpayee();
        FundTransferFillingScreen.amount("6.25");
        FundTransferFillingScreen.remarks("AboveOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.confirmbutton();
        driver.hideKeyboard();
        FundTrnasferOTPSCreen.backbutton();
        PopUpMessages.popupmessage_header("Cancel Fund Transfer");
        PopUpMessages.popupmessage_content("Do you want to cancel the transaction?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FundTrnasferOTPSCreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FundTransferFillingScreen.FundTransferFillingScreen_title();
    }

    @Test(priority = 12, description = "One Time Bill Payment - Below OTP")
    public static void onetimebillpaymenttoslibbankbelowotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.onetimefundtransfer();
        OneTimeFundTransferScreen.OneTimeFundTransferScreen_title();
        OneTimeFundTransferScreen.OneTimeFundTransferScreen_labels();
        AddBillerPayee addBillerPayee= new AddBillerPayee();
        OneTimeFundTransferScreen.addpayeename(addBillerPayee.getOnetimefundtransferpayeenamebelowotp());
        OneTimeFundTransferScreen.selectslipbank();
        OneTimeFundTransferScreen.addaccountnumber(addBillerPayee.getOnetimefundtransferaccnumberbelowotp());
        OneTimeFundTransferScreen.confirmbutton();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to");
        FundTransferSccessScreen.savebiller();
        SavePayeeScreen.SavePayeeScreen_lables();
        SavePayeeScreen.enternickname(addBillerPayee.getOnetimefundtransfernicknameaboveotp());
        SavePayeeScreen.save();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeDetailsScreen.PayeeDetailsScreen_title();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("New Payee Added","You have successfully added a new payee "+addBillerPayee.getOnetimefundtransfernicknameaboveotp()+" ALLIANCE FINANCE COMPANY PLC "+currentDateTime.gettimeanddate()+" SLST.");


    }

    @Test(priority = 13, description = "One Time Bill Payment - Above OTP")
    public static void onetimebillpaymenttoslibbankaboveotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.onetimefundtransfer();
        OneTimeFundTransferScreen.OneTimeFundTransferScreen_title();
        OneTimeFundTransferScreen.OneTimeFundTransferScreen_labels();
        AddBillerPayee addBillerPayee= new AddBillerPayee();
        OneTimeFundTransferScreen.addpayeename(addBillerPayee.getOnetimefundtransferpayeenameaboveotp());
        OneTimeFundTransferScreen.selectslipbank();
        OneTimeFundTransferScreen.addaccountnumber(addBillerPayee.getOnetimefundtransferaccnumberaboveotp());
        OneTimeFundTransferScreen.confirmbutton();
        FundTransferFillingScreen.amount("6.25");
        FundTransferFillingScreen.remarks("Above OTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_title();
        FundTransferVerifyInfoScreen.FundTransferVerifyInfoScreen_labels();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTrnasferOTPSCreen.enetrOTP("65464444");
        FundTransferSccessScreen.FundTransferSccessScreen_title();
        FundTransferSccessScreen.FundTransferSccessScreen_label();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You have made a fund transfer from Savings - Mega Saver account no 2009******** to");
        FundTransferSccessScreen.savebiller();
        SavePayeeScreen.SavePayeeScreen_lables();
        SavePayeeScreen.save();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        PayeeDetailsScreen.PayeeDetailsScreen_title();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("New Payee Added","You have successfully added a new payee");


    }


    @Test(priority = 15, description = "One Time Bill Payment - Negative Flow 06")
    public static void negativeflow06() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.onetimefundtransfer();
        OneTimeFundTransferScreen.addpayeename("kjlkjoiuouo");
        OneTimeFundTransferScreen.selectslipbank();
        OneTimeFundTransferScreen.addaccountnumber("4556789797");
        OneTimeFundTransferScreen.confirmbutton();
        FundTransferFillingScreen.amount("6.25");
        FundTransferFillingScreen.remarks("Above OTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTrnasferOTPSCreen.enetrOTP("65464444");
        FundTransferSccessScreen.savebiller();
        SavePayeeScreen.SavePayeeScreen_lables();
        SavePayeeScreen.backbutton();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SavePayeeScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 16, description = "One Time Bill Payment - Negative Flow 07")
    public static void negativeflow07() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.onetimefundtransfer();
        OneTimeFundTransferScreen.addpayeename("kjlkjoiuouo");
        OneTimeFundTransferScreen.selectslipbank();
        OneTimeFundTransferScreen.addaccountnumber("4556789797");
        OneTimeFundTransferScreen.confirmbutton();
        FundTransferFillingScreen.amount("6.25");
        FundTransferFillingScreen.remarks("Above OTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTrnasferOTPSCreen.enetrOTP("65464444");
        FundTransferSccessScreen.savebiller();
        SavePayeeScreen.SavePayeeScreen_lables();
        SavePayeeScreen.cancel();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SavePayeeScreen.cancel();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 16, description = "One Time Bill Payment - Negative Flow 08")
    public static void negativeflow08() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.onetimefundtransfer();
        OneTimeFundTransferScreen.addpayeename("kjlkjoiuouo");
        OneTimeFundTransferScreen.back();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        OneTimeFundTransferScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FundTransferPayeeList.FundTransferPayeeList_labels();
        FundTransferPayeeList.onetimefundtransfer();
        OneTimeFundTransferScreen.back();
        FundTransferPayeeList.FundTransferPayeeList_labels();

    }

    @Test(priority = 17, description = "One Time Bill Payment - Negative Flow 09")
    public static void negativeflow09() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.onetimefundtransfer();
        OneTimeFundTransferScreen.addpayeename("kjlkjoiuouo");
        OneTimeFundTransferScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        OneTimeFundTransferScreen.cancelbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FundTransferPayeeList.FundTransferPayeeList_labels();
        FundTransferPayeeList.onetimefundtransfer();
        OneTimeFundTransferScreen.cancelbutton();
        FundTransferPayeeList.FundTransferPayeeList_labels();

    }


    @Test(priority = 19, description = "One Time Bill Payment - Negative Flow 10")
    public static void negativeflow10() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.onetimefundtransfer();
        AddBillerPayee addBillerPayee= new AddBillerPayee();
        OneTimeFundTransferScreen.addpayeename(addBillerPayee.getOnetimefundtransferpayeenameaboveotp());
        OneTimeFundTransferScreen.selectslipbank();
        OneTimeFundTransferScreen.addaccountnumber(addBillerPayee.getNewonetimefundtransferaccnumber());
        OneTimeFundTransferScreen.confirmbutton();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTransferSccessScreen.savebiller();
        SavePayeeScreen.SavePayeeScreen_lables();
        SavePayeeScreen.enternickname(addBillerPayee.getOnetimefundtransfernicknameaboveotp());
        SavePayeeScreen.save();
        PopUpMessages.popupmessage_header("Invalid Data");
        PopUpMessages.popupmessage_content("Payee nickname already exists. Please try a different name");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        SavePayeeScreen.SavePayeeScreen_lables();

    }


    @Test(priority = 20, description = "One Time Bill Payment - Negative Flow 11")
    public static void negativeflow11() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.onetimefundtransfer();
        AddBillerPayee addBillerPayee= new AddBillerPayee();
        OneTimeFundTransferScreen.addpayeename(addBillerPayee.getNewonetimefundtransferpayeename());
        OneTimeFundTransferScreen.selectslipbank();
        OneTimeFundTransferScreen.addaccountnumber(addBillerPayee.getOnetimefundtransferaccnumberaboveotp());
        OneTimeFundTransferScreen.confirmbutton();
        FundTransferFillingScreen.amount("0.25");
        FundTransferFillingScreen.remarks("BelowOTP");
        FundTransferFillingScreen.paybutton();
        FundTransferVerifyInfoScreen.confirmbutton();
        FundTransferSccessScreen.savebiller();
        SavePayeeScreen.SavePayeeScreen_lables();
        SavePayeeScreen.save();
        PopUpMessages.popupmessage_header("Account Already Exists");
        PopUpMessages.popupmessage_content("Given account number already exists");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        SavePayeeScreen.SavePayeeScreen_lables();

    }
    @Test(priority = 21, description = "Fund Transfer - Add Payee - Negative 12")
    public static void negativeflow12() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.plusicon();
        AddBillerPayee addBillerPayee=new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
        AddPayeeScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AddPayeeScreen.cancelbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FundTransferPayeeList.FundTransferPayeeList_labels();
    }


    @Test(priority = 22, description = "Fund Transfer - Add Payee - Negative 13")
    public static void negativeflow13() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.plusicon();
        AddPayeeScreen.FundtransferAddPayeeScreen_labels();
        AddBillerPayee addBillerPayee=new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
        AddPayeeScreen.back();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AddPayeeScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FundTransferPayeeList.FundTransferPayeeList_labels();
    }

    @Test(priority = 23, description = "Fund Transfer - Add Payee - Negative 14")
    public static void negativeflow14() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.plusicon();
        AddPayeeScreen.FundtransferAddPayeeScreen_labels();
        AddBillerPayee addBillerPayee=new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getCeftpayeename());
        AddPayeeScreen.selectceftbrank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getCeftpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        FundTransferPayeeList.FundTransferPayeeList_labels();
    }

    @Test(priority = 24, description = "Fund Transfer - Add Payee - Negative 15")
    public static void negativeflow15() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.plusicon();
        AddBillerPayee addBillerPayee=new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getCeftpayeename());
        AddPayeeScreen.selectceftbrank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getCeftpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.cancelbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        FundTransferPayeeList.FundTransferPayeeList_labels();
    }

    @Test(priority = 25, description = "Fund Transfer - Add Payee ")
    public static void fundtransferbyaddingpayee() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.fundtransfer();
        FundTransferFillingScreen.FundTransferFillingScreen_title();
        FundTransferFillingScreen.FundTransferFillingScreen_labels();
        FundTransferFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        FundTransferFillingScreen.payto();
        FundTransferPayeeList.plusicon();
        AddBillerPayee addBillerPayee=new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getNicknamefundtransferbyaddingpayee());
        AddPayeeScreen.addpayeename(addBillerPayee.getPayeenamefundtransferbyaddingpayee());
        AddPayeeScreen.selectceftbrank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getAccnumberfundtransferbyaddingpayee());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        FundTransferPayeeList.FundTransferPayeeList_labels();

    }

}





