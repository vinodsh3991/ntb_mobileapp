package TestCases;

import Activities.*;
import Activities.ForgotPassword.ForgotPasswordScreen;
import Activities.ForgotUsername.AccountsForgotUsernameScreen;
import Activities.ForgotUsername.ForgotUsernameListScreen;
import Activities.LoginScreens.FristTimeLoginScreen;
import Activities.LoginScreens.NormalLoginScreen;
import Activities.OTPScreens.CommonOTPScreen;
import Activities.OTPScreens.ForgotUsernameOTPScreen;
import Activities.OTPScreens.SignUPOTPSCreen;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PreQuickAccessMenuListScreens.PreQuickAccessMenuList;
import Activities.SignUP.*;
import Activities.OTPScreens.SecondaryVerificationLimitOTPScreen;
import TestBase.Configuration;
import TestData.SignUpDetails;
import io.qameta.allure.Step;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import Listeners.AllureReportListener;

import java.util.concurrent.TimeUnit;
@Listeners({AllureReportListener.class})

public class SignUp extends Configuration {
    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        FristTimeLoginScreen.Sign_Up();



    }
    @Test(priority = 1, description = "My accounts - Sign Up - Negative Flow 01")
    public static void negativeflow01() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getInvalidaccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getInvalidnic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getInvaliddateofbirth());
        MyAccount_FillingScreen.nextbutton();
        PopUpMessages.popupmessage_header("Invalid Data");
        PopUpMessages.popupmessage_content("Incorrect information. Please check and try again");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();


    }
    @Test(priority = 2, description = "My accounts - Sign Up - Negative Flow 02")
    public static void negativeflow02() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.backbutton();
        PopUpMessages.popupmessage_header("Exit Sign Up");
        PopUpMessages.popupmessage_content("Do you wish to exit the Sign Up process?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        MyAccount_FillingScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SignUpwithScreen.SignupwithScreen_labels();

    }
    @Test(priority = 3, description = "My accounts - Sign Up - Negative Flow 03")
    public static void negativeflow03() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getNic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getDateofbirth());
        MyAccount_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.backbutton();
        PopUpMessages.popupmessage_header("Exit Sign Up");
        PopUpMessages.popupmessage_content("Do you wish to exit the Sign Up process?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SignUPOTPSCreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SignUpwithScreen.SignupwithScreen_labels();

    }

    @Test(priority = 4, description = "My accounts - Sign Up - Negative Flow 04")
    public static void negativeflow04() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getNic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getDateofbirth());
        MyAccount_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.enetrOTP("1232556984");
        SignUpTandCscreen.SignUpTandCscreen_title();
        SignUpTandCscreen.SignUpTandCscreen_labels();
        SignUpTandCscreen.backbutton();
        PopUpMessages.popupmessage_header("Exit Sign Up");
        PopUpMessages.popupmessage_content("Do you wish to exit the Sign Up process?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SignUPOTPSCreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SignUpwithScreen.SignupwithScreen_labels();


    }

    @Test(priority = 5, description = "My accounts - Sign Up - Negative Flow 05")
    public static void negativeflow05() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getNic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getDateofbirth());
        MyAccount_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.enetrOTP("1232556984");
        SignUpTandCscreen.SignUpTandCscreen_title();
        SignUpTandCscreen.SignUpTandCscreen_labels();
        SignUpTandCscreen.tickbox();
        SignUpTandCscreen.declinebutton();
        PopUpMessages.popupmessage_header("Terms and Conditions Declined");
        PopUpMessages.popupmessage_content("You chose to decline the Terms and Conditions. For any questions or clarifications please send us an email on");
        PopUpMessages.popupmessage_email("customerservice@nationstrust.com");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        SignUpwithScreen.SignupwithScreen_labels();


    }

    @Test(priority = 6, description = "My accounts - Sign Up - Negative Flow 06")
    public static void negativeflow06() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getNic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getDateofbirth());
        MyAccount_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.enetrOTP("1232556984");
        SignUpTandCscreen.SignUpTandCscreen_title();
        SignUpTandCscreen.SignUpTandCscreen_labels();
        SignUpTandCscreen.tickbox();
        SignUpTandCscreen.acceptbutton();
        RegisterScreen.registerScreen_title();
        RegisterScreen.registerScreen_labels();
        RegisterScreen.eneterusername(signUpDetails.getUsername());
        RegisterScreen.backbutton();
        PopUpMessages.popupmessage_header("Exit Sign Up");
        PopUpMessages.popupmessage_content("Do you wish to exit the Sign Up process?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SignUPOTPSCreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SignUpwithScreen.SignupwithScreen_labels();



    }

    @Test(priority = 7, description = "My accounts - Sign Up - Negative Flow 07")
    public static void negativeflow07() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getNic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getDateofbirth());
        MyAccount_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.enetrOTP("1232556984");
        SignUpTandCscreen.SignUpTandCscreen_title();
        SignUpTandCscreen.SignUpTandCscreen_labels();
        SignUpTandCscreen.tickbox();
        SignUpTandCscreen.acceptbutton();
        RegisterScreen.registerScreen_title();
        RegisterScreen.registerScreen_labels();
        RegisterScreen.eneterusername(signUpDetails.getUsername());
        RegisterScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Exit Sign Up");
        PopUpMessages.popupmessage_content("Do you wish to exit the Sign Up process?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SignUPOTPSCreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SignUpwithScreen.SignupwithScreen_labels();

    }

    @Test(priority = 8, description = "My accounts - Sign Up - Negative Flow 08")
    public static void negativeflow08() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getNic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getDateofbirth());
        MyAccount_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.enetrOTP("1232556984");
        SignUpTandCscreen.SignUpTandCscreen_title();
        SignUpTandCscreen.SignUpTandCscreen_labels();
        SignUpTandCscreen.tickbox();
        SignUpTandCscreen.acceptbutton();
        RegisterScreen.registerScreen_title();
        RegisterScreen.registerScreen_labels();
        RegisterScreen.eneterusername(signUpDetails.getExistingusername());
        RegisterScreen.eneterpassword("Pasword@3");
        RegisterScreen.eneterconfirmpassword("Pasword@3");
        RegisterScreen.registerbutton();
        PopUpMessages.popupmessage_header("Existing Username");
        PopUpMessages.popupmessage_content("Username Is Already Taken. Please Try With A Different Username");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        RegisterScreen.registerScreen_title();

    }

    @Test(priority = 9, description = "My accounts - Sign Up")
    public static void myaccountsignup() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getNic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getDateofbirth());
        MyAccount_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.enetrOTP("1232556984");
        SignUpTandCscreen.SignUpTandCscreen_title();
        SignUpTandCscreen.SignUpTandCscreen_labels();
        SignUpTandCscreen.tickbox();
        SignUpTandCscreen.acceptbutton();
        RegisterScreen.registerScreen_title();
        RegisterScreen.registerScreen_labels();
        RegisterScreen.eneterusername(signUpDetails.getUsername());
        RegisterScreen.eneterpassword(signUpDetails.getPassword());
        RegisterScreen.eneterconfirmpassword(signUpDetails.getConfirmpassword());
        RegisterScreen.registerbutton();
        SignUpConfigureBiometricsScreen.signUpConfigureBiometricsScreenn_title();
        SignUpConfigureBiometricsScreen.signUpConfigureBiometricsScreenn_labels();
        SignUpConfigureBiometricsScreen.togglebutton();
        PopUpMessages.popupmessage_header("Configure Fingerprint");
        PopUpMessages.popupmessage_content("Please tap on configure to setup your fingerprint.");
        PopUpMessages.popupsuccessmessage_negativebutton("Cancel");
        SignUpConfigureBiometricsScreen.togglebutton();
        PopUpMessages.popupsuccessmessage_positivebutton("Configure");
        FingerprintScreen.FingerprintScreen_title();
        FingerprintScreen.FingerprintScreen_labels();
        FingerprintScreen.back();
        SignUpConfigureBiometricsScreen.quickaccessbutton();
        PreQuickAccessMenuList.cross();
        SignUpConfigureBiometricsScreen.skipbutton();
        NormalLoginScreen.NormalLoginScreen_labels();

    }




//    @Test(priority = 9, description = "My accounts - Sign Up - Negative Flow 08")
//    public static void negativeflow08() throws Exception {
//        SignUpwithScreen.SignupwithScreen_labels();
//        SignUpwithScreen.MyAccounts();
//        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
//        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
//        MyAccount_FillingScreen.enterMyAccountNumber("029215035020");
//        MyAccount_FillingScreen.enterNIC("950431016V");
//        MyAccount_FillingScreen.enterBirthday("12 February 1995");
//        MyAccount_FillingScreen.nextbutton();
//        SignUPOTPSCreen.SignUPOTPSCreen_title();
//        SignUPOTPSCreen.SignUPOTPSCreen_labels();
//        SignUPOTPSCreen.signupotp();
//        SignUPOTPSCreen.enetrOTP("1232556984");
//        SignUpTandCscreen.SignUpTandCscreen_title();
//        SignUpTandCscreen.SignUpTandCscreen_labels();
//        SignUpTandCscreen.tickbox();
//        SignUpTandCscreen.acceptbutton();
//        RegisterScreen.registerScreen_title();
//        RegisterScreen.registerScreen_labels();
//        RegisterScreen.eneterusername("fhjgdjfgjshgf");
//        RegisterScreen.eneterpassword("Pasword@3");
//        RegisterScreen.eneterconfirmpassword("Pasword@3");
//        RegisterScreen.registerbutton();
//        SignUpConfigureBiometricsScreen.signUpConfigureBiometricsScreenn_title();
//        SignUpConfigureBiometricsScreen.signUpConfigureBiometricsScreenn_labels();
//        SignUpConfigureBiometricsScreen.togglebutton();
//        PopUpMessages.popupmessage_header("Configure Fingerprint");
//        PopUpMessages.popupmessage_content("Please tap on configure to setup your fingerprint.");
//        PopUpMessages.popupsuccessmessage_negativebutton("Cancel");
//        SignUpConfigureBiometricsScreen.togglebutton();
//        PopUpMessages.popupsuccessmessage_positivebutton("Configure");
//        FingerprintScreen.FingerprintScreen_title();
//        FingerprintScreen.FingerprintScreen_labels();
//        FingerprintScreen.back();
//        SignUpConfigureBiometricsScreen.quickaccessbutton();
//        PreQuickAccessMenuList.cross();
//        SignUpConfigureBiometricsScreen.quickaccessbutton();
//        PreQuickAccessMenuList.home();
//        NormalLoginScreen.NormalLoginScreen_labels();
//
//    }



    @Test(priority = 10, description = "My accounts - Sign Up - Negative Flow 09")
    public static void negativeflow09() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyAccounts();
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();
        MyAccount_FillingScreen.MyAccount_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyAccount_FillingScreen.enterMyAccountNumber(signUpDetails.getAccountnumber());
        MyAccount_FillingScreen.enterNIC(signUpDetails.getNic());
        MyAccount_FillingScreen.enterBirthday(signUpDetails.getDateofbirth());
        MyAccount_FillingScreen.nextbutton();
        PopUpMessages.popupmessage_header("You are already registered for Mobile Banking");
        PopUpMessages.popupmessage_content("Please use the \"Sign In\" option or use the \"Forgot Username?\" option.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        MyAccount_FillingScreen.MyAccount_FillingScreen_title();

    }


    @Test(priority = 11, description = "My credit card - Sign Up")
    public static void mycreditsignup() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyCreditCards();
        MyCredit_FillingScreen.MyCredit_FillingScreen_title();
        MyCredit_FillingScreen.MyCredit_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyCredit_FillingScreen.entercreditcardNumber(signUpDetails.getCreditcardnumber());
        MyCredit_FillingScreen.entermonth(signUpDetails.getMm());
        MyCredit_FillingScreen.enteryear(signUpDetails.getYy());
        MyCredit_FillingScreen.entercvv(signUpDetails.getCvv());
        MyCredit_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.enetrOTP("1232556984");
        SignUpTandCscreen.SignUpTandCscreen_title();
        SignUpTandCscreen.SignUpTandCscreen_labels();
        SignUpTandCscreen.tickbox();
        SignUpTandCscreen.acceptbutton();
        RegisterScreen.registerScreen_title();
        RegisterScreen.registerScreen_labels();
        RegisterScreen.eneterusername(signUpDetails.getUsername());
        RegisterScreen.eneterpassword(signUpDetails.getPassword());
        RegisterScreen.eneterconfirmpassword(signUpDetails.getConfirmpassword());
        RegisterScreen.registerbutton();
        SignUpConfigureBiometricsScreen.signUpConfigureBiometricsScreenn_title();
        SignUpConfigureBiometricsScreen.signUpConfigureBiometricsScreenn_labels();
        SignUpConfigureBiometricsScreen.togglebutton();
        PopUpMessages.popupmessage_header("Configure Fingerprint");
        PopUpMessages.popupmessage_content("Please tap on configure to setup your fingerprint.");
        PopUpMessages.popupsuccessmessage_negativebutton("Cancel");
        SignUpConfigureBiometricsScreen.togglebutton();
        PopUpMessages.popupsuccessmessage_positivebutton("Configure");
        FingerprintScreen.FingerprintScreen_title();
        FingerprintScreen.FingerprintScreen_labels();
        FingerprintScreen.back();
        SignUpConfigureBiometricsScreen.quickaccessbutton();
        PreQuickAccessMenuList.cross();
        SignUpConfigureBiometricsScreen.skipbutton();
        NormalLoginScreen.NormalLoginScreen_labels();

    }

    @Test(priority = 12, description = "My Credit card - Sign Up - Negative Flow 01")
    public static void negativeflowcredit01() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyCreditCards();
        MyCredit_FillingScreen.MyCredit_FillingScreen_title();
        MyCredit_FillingScreen.MyCredit_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyCredit_FillingScreen.entercreditcardNumber(signUpDetails.getCreditcardnumber());
        MyCredit_FillingScreen.entermonth(signUpDetails.getMm());
        MyCredit_FillingScreen.enteryear(signUpDetails.getYy());
        MyCredit_FillingScreen.entercvv(signUpDetails.getCvv());
        MyCredit_FillingScreen.nextbutton();
        PopUpMessages.popupmessage_header("Unknown Error");
        PopUpMessages.popupmessage_content("We are unable to serve your request at the moment, please try again later");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        MyCredit_FillingScreen.MyCredit_FillingScreen_title();


    }
    @Test(priority = 13, description = "My Credit card - Sign Up - Negative Flow 02")
    public static void negativeflowcredit02() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyCreditCards();
        MyCredit_FillingScreen.MyCredit_FillingScreen_title();
        MyCredit_FillingScreen.MyCredit_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyCredit_FillingScreen.entercreditcardNumber(signUpDetails.getCreditcardnumber());
        MyCredit_FillingScreen.backbutton();
        PopUpMessages.popupmessage_header("Exit Sign Up");
        PopUpMessages.popupmessage_content("Do you wish to exit the Sign Up process?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        MyAccount_FillingScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SignUpwithScreen.SignupwithScreen_labels();

    }


    @Test(priority = 14, description = "My debit card - Sign Up")
    public static void mydebitsignup() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyDebitCards();
        MyDebit_FillingScreen.MyDebit_FillingScreen_title();
        MyDebit_FillingScreen.MyDebit_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyDebit_FillingScreen.enterdebitcardNumber(signUpDetails.getDebitcardnumber());
        MyDebit_FillingScreen.enteratmpin(signUpDetails.getAtmpin());
        MyDebit_FillingScreen.nextbutton();
        SignUPOTPSCreen.SignUPOTPSCreen_title();
        SignUPOTPSCreen.SignUPOTPSCreen_labels();
        SignUPOTPSCreen.signupotp();
        SignUPOTPSCreen.enetrOTP("1232556984");
        SignUpTandCscreen.SignUpTandCscreen_title();
        SignUpTandCscreen.SignUpTandCscreen_labels();
        SignUpTandCscreen.tickbox();
        SignUpTandCscreen.acceptbutton();
        RegisterScreen.registerScreen_title();
        RegisterScreen.registerScreen_labels();
        RegisterScreen.eneterusername(signUpDetails.getUsername());
        RegisterScreen.eneterpassword(signUpDetails.getPassword());
        RegisterScreen.eneterconfirmpassword(signUpDetails.getConfirmpassword());
        RegisterScreen.registerbutton();
        SignUpConfigureBiometricsScreen.signUpConfigureBiometricsScreenn_title();
        SignUpConfigureBiometricsScreen.signUpConfigureBiometricsScreenn_labels();
        SignUpConfigureBiometricsScreen.togglebutton();
        PopUpMessages.popupmessage_header("Configure Fingerprint");
        PopUpMessages.popupmessage_content("Please tap on configure to setup your fingerprint.");
        PopUpMessages.popupsuccessmessage_negativebutton("Cancel");
        SignUpConfigureBiometricsScreen.togglebutton();
        PopUpMessages.popupsuccessmessage_positivebutton("Configure");
        FingerprintScreen.FingerprintScreen_title();
        FingerprintScreen.FingerprintScreen_labels();
        FingerprintScreen.back();
        SignUpConfigureBiometricsScreen.quickaccessbutton();
        PreQuickAccessMenuList.cross();
        SignUpConfigureBiometricsScreen.skipbutton();
        NormalLoginScreen.NormalLoginScreen_labels();

    }

    @Test(priority = 12, description = "My Debit card - Sign Up - Negative Flow 01")
    public static void negativeflowdebit01() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyDebitCards();
        MyDebit_FillingScreen.MyDebit_FillingScreen_title();
        MyDebit_FillingScreen.MyDebit_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyDebit_FillingScreen.enterdebitcardNumber(signUpDetails.getDebitcardnumber());
        MyDebit_FillingScreen.enteratmpin(signUpDetails.getAtmpin());
        MyDebit_FillingScreen.nextbutton();
        PopUpMessages.popupmessage_header("Invalid Data");
        PopUpMessages.popupmessage_content("Incorrect information. Please check and try again");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        MyDebit_FillingScreen.MyDebit_FillingScreen_title();


    }
    @Test(priority = 13, description = "My Debit card - Sign Up - Negative Flow 02")
    public static void negativeflowdebit02() throws Exception {
        SignUpwithScreen.SignupwithScreen_labels();
        SignUpwithScreen.MyDebitCards();
        MyDebit_FillingScreen.MyDebit_FillingScreen_title();
        MyDebit_FillingScreen.MyDebit_FillingScreen_labels();
        SignUpDetails signUpDetails = new SignUpDetails();
        MyDebit_FillingScreen.enterdebitcardNumber(signUpDetails.getDebitcardnumber());
        MyDebit_FillingScreen.backbutton();
        PopUpMessages.popupmessage_header("Exit Sign Up");
        PopUpMessages.popupmessage_content("Do you wish to exit the Sign Up process?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        MyAccount_FillingScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SignUpwithScreen.SignupwithScreen_labels();

    }





}
