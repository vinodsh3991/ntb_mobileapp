package TestCases;

import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.LoginScreens.FristTimeLoginScreen;
import Activities.Notification.NotificationMainScreen;
import Activities.Notification.OfferDetailsScreen;
import Activities.Notification.TransactionDetailsScreen;
import Activities.Notification.TransactionScreen;
import Activities.OTPScreens.LoginOTPScreen;
import Activities.PopUpMessages.PopUpMessages;
import TestBase.Configuration;
import TestData.LoginDetails;
import io.qameta.allure.Step;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Notification  extends Configuration {
    @BeforeMethod( description = "Login to the app")
    @Step("Login with username::{0} and password: {1}")
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }

    @Test(priority = 1, description = "View Offers - get call")
    public static void offersgetcall() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();

        Boolean nooffers = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nooffers == true) {
            NotificationMainScreen.emptyoffersscreen();
        } else {

            NotificationMainScreen.taponoffer();
            OfferDetailsScreen.OfferDetailsScreen_labels();
            OfferDetailsScreen.getcall();
            PopUpMessages.devicepopup();
        }
    }

    @Test(priority = 2, description = "View Offers - more information")
    public static void offersmoreinfor() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        Boolean nooffers = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nooffers == true) {
            NotificationMainScreen.emptyoffersscreen();
        } else {
            NotificationMainScreen.taponoffer();
            OfferDetailsScreen.OfferDetailsScreen_labels();
            OfferDetailsScreen.moreinfor();
            Selectbrowser.Selectbrowser();
        }
    }

    @Test(priority = 3, description = "Delete Offers ")
    public static void deleteoffer() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        Boolean nooffers = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nooffers == true) {
            NotificationMainScreen.emptyoffersscreen();
        } else {
            NotificationMainScreen.selectonenotification();
            NotificationMainScreen.tapondelete();
            PopUpMessages.popupmessage_header("Delete Notifications?");
            PopUpMessages.popupmessage_content("Are you sure you want to delete?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            NotificationMainScreen.tapondelete();
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");

        }
    }

    @Test(priority = 4, description = "Delete Offers - Negative Flow 01 ")
    public static void negativeflow01() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        Boolean nooffers = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nooffers == true) {
            NotificationMainScreen.emptyoffersscreen();
        } else {
            NotificationMainScreen.selectallnotification();
            NotificationMainScreen.deselectallnotification();
        }
    }

    @Test(priority = 5, description = "Delete Offers ")
    public static void deletealloffer() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        Boolean nooffers = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nooffers == true) {
            NotificationMainScreen.emptyoffersscreen();
        } else {
            NotificationMainScreen.selectallnotification();
            NotificationMainScreen.tapondelete();
            PopUpMessages.popupmessage_header("Delete Notifications?");
            PopUpMessages.popupmessage_content("Are you sure you want to delete?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            NotificationMainScreen.tapondelete();
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            NotificationMainScreen.emptyoffersscreen();
        }

    }

    @Test(priority = 6, description = "Negative flow 02 ")
    public static void negativeflow02() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        Boolean nooffers = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nooffers == true) {
            NotificationMainScreen.emptyoffersscreen();
        } else {
            NotificationMainScreen.taponoffer();
            OfferDetailsScreen.OfferDetailsScreen_labels();
            OfferDetailsScreen.back();
            NotificationMainScreen.NotificationMainScreen_title();
            NotificationMainScreen.NotificationMainScreen_labels();
            NotificationMainScreen.back();
            HomeScreen.HomeScreen_labels();

        }

    }


    @Test(priority = 7, description = "Delete transaction notificatio")
    public static void deletetransactionnotification() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        NotificationMainScreen.transactiontab();

        Boolean notransactionnotification = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (notransactionnotification == true) {
            TransactionScreen.emptytransactionsscreen();
        } else {
            TransactionScreen.selectonenotification();
            TransactionScreen.tapondelete();
            PopUpMessages.popupmessage_header("Delete Notifications?");
            PopUpMessages.popupmessage_content("Are you sure you want to delete?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            TransactionScreen.tapondelete();
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            NotificationMainScreen.NotificationMainScreen_title();
            NotificationMainScreen.NotificationMainScreen_labels();
        }
    }


    @Test(priority = 7, description = "Transaction notification - Negative Flow 03")
    public static void negativeflow03() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        NotificationMainScreen.transactiontab();

        Boolean notransactionnotification = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (notransactionnotification == true) {
            TransactionScreen.emptytransactionsscreen();
        } else {
            TransactionScreen.selectallnotification();
            TransactionScreen.deselectallnotification();
            NotificationMainScreen.NotificationMainScreen_title();
            NotificationMainScreen.NotificationMainScreen_labels();
        }
    }

    @Test(priority = 8, description = "Transaction notification - Negative Flow 04")
    public static void negativeflow04() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        NotificationMainScreen.transactiontab();

        Boolean notransactionnotification = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (notransactionnotification == true) {
            TransactionScreen.emptytransactionsscreen();
        } else {
            TransactionScreen.back();
        }
    }


    @Test(priority = 9, description = "Transaction notification - Negative Flow 05")
    public static void negativeflow05() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        NotificationMainScreen.transactiontab();

        Boolean notransactionnotification = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (notransactionnotification == true) {
            TransactionScreen.emptytransactionsscreen();
        } else {
            TransactionScreen.tapontransationnotification();
            TransactionDetailsScreen.TransactionDetailsScreen_title();
            TransactionDetailsScreen.TransactionDetailsScreen_labels();
            TransactionDetailsScreen.back();
            NotificationMainScreen.NotificationMainScreen_title();
            NotificationMainScreen.NotificationMainScreen_labels();

        }
    }


    @Test(priority = 10, description = "Transaction notification - Delete all notification")
    public static void deletealltransactionnotification() throws Exception {
        HomeScreen.notification();
        NotificationMainScreen.NotificationMainScreen_title();
        NotificationMainScreen.NotificationMainScreen_labels();
        NotificationMainScreen.transactiontab();

        Boolean notransactionnotification = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (notransactionnotification == true) {
            TransactionScreen.emptytransactionsscreen();
        } else {
            TransactionScreen.selectallnotification();
            TransactionScreen.tapondelete();
            PopUpMessages.popupmessage_header("Delete Notifications?");
            PopUpMessages.popupmessage_content("Are you sure you want to delete?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            TransactionScreen.tapondelete();
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            NotificationMainScreen.NotificationMainScreen_title();
            TransactionScreen.emptytransactionsscreen();

        }
    }

}
