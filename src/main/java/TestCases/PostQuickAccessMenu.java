package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PostQuickAccessMenuList.*;
import Activities.OTPScreens.FDOTPScreen;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PostQuickAccessMenu extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }

    @Test(priority = 1, description = "Leasing calculator")
    public static void Leasing_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.leasingcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.PostLeasingCalculatorScreen_title();
        PostLeasingCalculatorScreen.PostLeasingCalculatorScreen_labels();
        PostLeasingCalculatorScreen.BrandNew();
        PostLeasingCalculatorScreen.CarSUV();
        PostLeasingCalculatorScreen.enteradvance("5000");
        PostLeasingCalculatorScreen.enterleaseamount("6000");
        PostLeasingCalculatorScreen.selectduration();
        PostLeasingCalculatorScreen.calculate();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.MonthlyInstallment_label();
        PostLeasingCalculatorScreen.arrow();
        PostLeasingCalculatorScreen.yes();
        PopUpMessages.popupmessage_header("Details Submitted");
        PopUpMessages.popupmessage_content("Your leasing quotation request has been sent to the bank successfully, we will contact you for further details.");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        PostCalculatorScreen.PostCalculatorScreen_title();

    }

    @Test(priority = 2, description = "Fixed Deposit calculator - Maturity")
    public static void FDMaturity_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectmaturity();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.selectfundingaccount();
        PostFDFundingScreen.enterbranch();
        PostFDFundingScreen.confirm();
        PostFDTandCScreen.FDTandCScreen_title();
        PostFDTandCScreen.FDTandCScreen_labels();
        PostFDTandCScreen.agree();
        FDOTPScreen.enetrOTP("1232556984");
        PostFDOpenSuccessScreen.FDOpenSuccessScreen_title();
        PostFDOpenSuccessScreen.FDOpenSuccessScreen_labels();
        PostFDOpenSuccessScreen.home();
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 3, description = "Fixed Deposit calculator - Flexi Fixed Deposit")
    public static void FDFlexi_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.selectfundingaccount();
        PostFDFundingScreen.enterbranch();
        PostFDFundingScreen.confirm();
        PostFDTandCScreen.FDTandCScreen_title();
        PostFDTandCScreen.FDTandCScreen_labels();
        PostFDTandCScreen.agree();
        FDOTPScreen.enetrOTP("1232556984");
        PostFDOpenSuccessScreen.FDOpenSuccessScreen_title();
        PostFDOpenSuccessScreen.FDOpenSuccessScreen_labels();
        PostFDOpenSuccessScreen.home();
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 4, description = "Fixed Deposit calculator - Negative Flow 01")
    public static void FDNegativeflow01_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        PostFDCalculatorScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();

    }

    @Test(priority = 5, description = "Fixed Deposit calculator - Negative Flow 02")
    public static void FDNegativeFlow02_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.no();
        PostCalculatorScreen.PostCalculatorScreen_title();

    }

    @Test(priority = 6, description = "Fixed Deposit calculator - Negative Flow 03")
    public static void FDNegative03_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.cancel();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        PostFDFundingScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();

    }
    @Test(priority = 7, description = "Fixed Deposit calculator - Negative Flow 04")
    public static void FDNegativeFlow4_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.selectfundingaccount();
        PostFDFundingScreen.enterbranch();
        PostFDFundingScreen.confirm();
        PostFDTandCScreen.FDTandCScreen_title();
        PostFDTandCScreen.FDTandCScreen_labels();
        PostFDTandCScreen.decline();
        HomeScreen.HomeScreen_labels();

    }
    @Test(priority = 8, description = "Fixed Deposit calculator - Negative flow 05")
    public static void FDNegativeFlow5_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.selectfundingaccount();
        PostFDFundingScreen.enterbranch();
        PostFDFundingScreen.confirm();
        PostFDTandCScreen.FDTandCScreen_title();
        PostFDTandCScreen.FDTandCScreen_labels();
        PostFDTandCScreen.agree();
        driver.hideKeyboard();
        FDOTPScreen.backbutton();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FDOTPScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PostFDTandCScreen.FDTandCScreen_title();


    }


}
