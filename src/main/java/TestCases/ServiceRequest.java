package TestCases;

import Activities.Calender.ServiceRequestCalender;
import Activities.LoginScreens.FristTimeLoginScreen;
import Activities.ServiceRequest.*;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.Dates;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ServiceRequest extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }
    @Test(priority = 1, description = "Stop a Cheque - success path(anothercheque)")
    public static void StopaCheque_success1() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if (nocurrentaccount == true) {
            StopaCheque.emptyscreen();

        } else {
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            StopChequeSuccessScreen.StopChequeSuccessScreen_labels();
            StopChequeSuccessScreen.anothercheque();
            StopaCheque.StopaCheque_title();
            StopaCheque.StopaCheque_title();

        }
    }

    @Test(priority = 2, description = "Stop a Cheque - success path(Home)")
    public static void StopaCheque_success2() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if (nocurrentaccount == true) {
            StopaCheque.emptyscreen();

        } else {
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            StopChequeSuccessScreen.StopChequeSuccessScreen_labels();
            StopChequeSuccessScreen.home();
            HomeScreen.HomeScreen_labels();

        }
    }



    @Test(priority = 3, description = "Stop a Cheque - already used cheque")
    public static void StopaCheque_already() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if(nocurrentaccount == true){
            StopaCheque.emptyscreen();

        }else{
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            PopUpMessages.popupmessage_header("Cheque already presented");
            PopUpMessages.popupmessage_content("The Cheque number you entered has already been presented. Please enter a valid cheque number");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            StopaCheque.StopaCheque_title();
        }

    }

    @Test(priority = 4, description = "Stop a Cheque - invalid cheque")
    public static void StopaCheque_invalid() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if(nocurrentaccount == true){
            StopaCheque.emptyscreen();

        }else{
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            PopUpMessages.popupmessage_header("Invalid Cheque Book");
            PopUpMessages.popupmessage_content("You have entered an incorrect cheque number. Please enter the cheque number correctly.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            StopaCheque.StopaCheque_title();
        }

    }

    @Test(priority = 5, description = "Stop a Cheque - no need")
    public static void StopaCheque_noneed() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if(nocurrentaccount == true){
            StopaCheque.emptyscreen();

        }else{
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            StopaCheque.StopaCheque_title();
        }
    }

    @Test(priority = 6, description = "Request Account Statement - Email")
    public static void RequestAccountStatementusingemail() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true){
            Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
            Activities.ServiceRequest.RequestAccountStatement.emptyscreen();

        }else {
            Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_title();
            Activities.ServiceRequest.RequestAccountStatement.Accountlist();
            Activities.ServiceRequest.RequestAccountStatement.fromdate();
            Dates dates = new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestAccountStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestAccountStatement.email();
            Activities.ServiceRequest.RequestAccountStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.ServiceRequests_labels();
            CurrentDateTime currentDateTime =new CurrentDateTime();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            OpenNotification.opennotifications("NTB Mobile banking","Dear Customer,Your request for a new Account statement on "+currentDateTime.gettimeanddate()+" SLST for account");


        }
    }

    @Test(priority = 7, description = "Request Account Statement - Address  ")
    public static void RequestAccountStatementusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nocards == true) {
            Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_title();
            Activities.ServiceRequest.RequestAccountStatement.emptyscreen();

        } else {
            Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
            Activities.ServiceRequest.RequestAccountStatement.Accountlist();
            Activities.ServiceRequest.RequestAccountStatement.fromdate();
            Dates dates = new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestAccountStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestAccountStatement.correspondenceaddress();
            Activities.ServiceRequest.RequestAccountStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.ServiceRequests_labels();
            CurrentDateTime currentDateTime =new CurrentDateTime();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            OpenNotification.opennotifications("NTB Mobile banking","Dear Customer,Your request for a new Account statement on "+currentDateTime.gettimeanddate()+" SLST for account");


        }
    }

    @Test(priority = 8, description = "Request Account Statement -  Branch ")
    public static void RequestAccountStatementusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true){
            Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_title();
            Activities.ServiceRequest.RequestAccountStatement.emptyscreen();

        }else {
            Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
            Activities.ServiceRequest.RequestAccountStatement.Accountlist();
            Activities.ServiceRequest.RequestAccountStatement.fromdate();
            Dates dates = new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestAccountStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestAccountStatement.selectbranch();
            Activities.ServiceRequest.RequestAccountStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.ServiceRequests_labels();
            CurrentDateTime currentDateTime =new CurrentDateTime();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            OpenNotification.opennotifications("NTB Mobile banking","Dear Customer,Your request for a new Account statement on "+currentDateTime.gettimeanddate()+" SLST for account");

        }
    }
    @Test(priority = 9, description = "Request Card Statement - Email")
    public static void RequestCardStatementusingemail() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true){
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        }else {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.email();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.ServiceRequests_labels();
            CurrentDateTime currentDateTime =new CurrentDateTime();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            OpenNotification.opennotifications("NTB Mobile banking","Dear Customer,Your request for a new Card statement on "+currentDateTime.gettimeanddate()+" SLST for card");

        }
    }

    @Test(priority = 10, description = "Request Card Statement - Address  ")
    public static void RequestCardStatementusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true) {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        }else{
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.correspondenceaddress();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.ServiceRequests_labels();
            CurrentDateTime currentDateTime =new CurrentDateTime();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            OpenNotification.opennotifications("NTB Mobile banking","Dear Customer,Your request for a new Card statement on "+currentDateTime.gettimeanddate()+" SLST for card");


        }
    }

    @Test(priority = 11, description = "Request Card Statement -  Branch ")
    public static void RequestCardStatementusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nocards == true) {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        } else {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.selectbranch();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.ServiceRequests_labels();
            CurrentDateTime currentDateTime =new CurrentDateTime();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            OpenNotification.opennotifications("NTB Mobile banking","Dear Customer,Your request for a new Card statement on "+currentDateTime.gettimeanddate()+" SLST for card");


        }


    }

    @Test(priority = 12, description = "Cheque Book Request using address")
    public static void ChequeBookRequestusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.ChequeBookRequest();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/noCAsLayout").size() != 0;

        if(nocurrentaccount == true){
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.emptyscreen();

        }else{
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_labels();
            Activities.ServiceRequest.ChequeBookRequest.Accountlist();
            Activities.ServiceRequest.ChequeBookRequest.Chequenumberist();
            Activities.ServiceRequest.ChequeBookRequest.correspondenceaddress();
            Activities.ServiceRequest.ChequeBookRequest.submit();
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your request has been raised and is pending for bank’s approval");
            PopUpMessages.popupsuccessmessage_okbuttons("OK");
            CurrentDateTime currentDateTime =new CurrentDateTime();
            OpenNotification.opennotifications("NTB Mobile banking","Dear Customer,Your request for a new chequebook on "+currentDateTime.gettimeanddate()+" SLST for account");

        }

    }

    @Test(priority = 13, description = "Cheque Book Request using branch")
    public static void ChequeBookRequestusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.ChequeBookRequest();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/noCAsLayout").size() != 0;

        if(nocurrentaccount == true){
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.emptyscreen();

        }else{
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_labels();
            Activities.ServiceRequest.ChequeBookRequest.Accountlist();
            Activities.ServiceRequest.ChequeBookRequest.Chequenumberist();
            Activities.ServiceRequest.ChequeBookRequest.selectbranch();
            Activities.ServiceRequest.ChequeBookRequest.submit();
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your request has been raised and is pending for bank’s approval");
            PopUpMessages.popupsuccessmessage_okbuttons("OK");
            CurrentDateTime currentDateTime =new CurrentDateTime();
            OpenNotification.opennotifications("NTB Mobile banking","Dear Customer,Your request for a new chequebook on "+currentDateTime.gettimeanddate()+" SLST for account");


        }

    }

//    @Test(priority = 14, description = "Register for account e-Statement")
//    public static void registerforaccountestatement() throws Exception {
//        HomeScreen.navigationdrawericon();
//        Navigationdrawer.servicerequest();
//        ServiceRequests.RegisterforAccounteStatements();
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;
//
//        if(nocurrentaccount == true){
//            Activities.ServiceRequest.RegisterforAccounteStatements.RequestAccountEStatement_title();
//            Activities.ServiceRequest.RegisterforAccounteStatements.emptyscreen();
//
//        }else{
//            Activities.ServiceRequest.RegisterforAccounteStatements.RequestAccountEStatement_title();
//            Activities.ServiceRequest.RegisterforAccounteStatements.RequestAccountEStatement_labels();
//
//
//            Activities.ServiceRequest.RegisterforAccounteStatements.submit();
//            PopUpMessages.popupmessage_header("Request Submitted");
//            PopUpMessages.popupmessage_content("Your request has been raised and is pending for bank’s approval");
//            PopUpMessages.popupsuccessmessage_okbuttons("OK");
//
//        }
//
//    }

//        @Test(priority = 15, description = "Register for card e-Statement")
//    public static void registerforcardestatement() throws Exception {
//        HomeScreen.navigationdrawericon();
//        Navigationdrawer.servicerequest();
//        ServiceRequests.RegisterforCardeStatements();
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;
//
//        if(nocurrentaccount == true){
//            Activities.ServiceRequest.RegisterforCardeStatements.RegisterforCardeStatements_title();
//            Activities.ServiceRequest.RegisterforCardeStatements.emptyscreen();
//
//        }else{
//            Activities.ServiceRequest.RegisterforCardeStatements.RegisterforCardeStatements_title();
//            Activities.ServiceRequest.RegisterforCardeStatements.RegisterforCardeStatements_labels();
//
//
//            Activities.ServiceRequest.RegisterforCardeStatements.submit();
//            PopUpMessages.popupmessage_header("Request Submitted");
//            PopUpMessages.popupmessage_content("Your request has been raised and is pending for bank’s approval");
//            PopUpMessages.popupsuccessmessage_okbuttons("OK");
//
//        }
//
//    }


    @Test(priority = 16, description = "Stop a Cheque - Negative flow 01")
    public static void negativeflow01() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if (nocurrentaccount == true) {
            StopaCheque.emptyscreen();
            StopaCheque.back();
            ServiceRequests.ServiceRequests_title();

        } else {
            StopaCheque.back();
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.StopaCheque();
            StopaCheque.Accountlist();
            StopaCheque.back();
            PopUpMessages.popupmessage_header("Discard Request");
            PopUpMessages.popupmessage_content("Are you sure you want to discard this request?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            StopaCheque.back();
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            ServiceRequests.ServiceRequests_title();

        }
    }


    @Test(priority = 17, description = "Request Account Statement - Negative flow 02")
    public static void negativeflow02() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true){
            Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
            Activities.ServiceRequest.RequestAccountStatement.emptyscreen();
            RequestAccountStatement.back();
            ServiceRequests.ServiceRequests_title();

        }else {
            RequestAccountStatement.back();
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.RequestAccountStatement();
            RequestAccountStatement.Accountlist();
            RequestAccountStatement.back();
            PopUpMessages.popupmessage_header("Discard Request");
            PopUpMessages.popupmessage_content("Are you sure you want to discard this request?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            RequestAccountStatement.back();
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            ServiceRequests.ServiceRequests_title();

        }
    }

    @Test(priority = 18, description = "Request Card Statement -  Negative Flow 03 ")
    public static void negativeflow03() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nocards == true) {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();
            RequestCardStatement.back();
            ServiceRequests.ServiceRequests_title();

        } else {
            RequestCardStatement.back();
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.RequestCardStatement();
            RequestCardStatement.Cardlist();
            RequestCardStatement.back();
            PopUpMessages.popupmessage_header("Discard Request");
            PopUpMessages.popupmessage_content("Are you sure you want to discard this request?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            RequestCardStatement.back();
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            ServiceRequests.ServiceRequests_title();

        }
    }

    @Test(priority = 19, description = "Cheque Book Request - Negative Flow 04")
    public static void negativeflow04() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.ChequeBookRequest();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/noCAsLayout").size() != 0;

        if(nocurrentaccount == true){
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.emptyscreen();
            ChequeBookRequest.back();
            ServiceRequests.ServiceRequests_title();

        }else{
            ChequeBookRequest.back();
            ServiceRequests.ServiceRequests_title();
            ServiceRequests.ChequeBookRequest();
            ChequeBookRequest.Accountlist();
            ChequeBookRequest.back();
            PopUpMessages.popupmessage_header("Discard Request");
            PopUpMessages.popupmessage_content("Are you sure you want to discard this request?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            ChequeBookRequest.back();
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            ServiceRequests.ServiceRequests_title();

        }

    }

    @Test(priority = 20, description = "History - Negative Flow 05")
    public static void negativeflow05() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.History();
        History.History_title();
        History.back();
        ServiceRequests.ServiceRequests_title();
        }
    @Test(priority = 21, description = "Service Request - Negative Flow 06")
    public static void negativeflow06() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.back();
        HomeScreen.HomeScreen_labels();
    }

    @Test(priority = 22, description = "Register Account e-Statement  ")
    public static void RegisteraccounteStatement() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RegisterforAccounteStatements();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true) {
            Activities.ServiceRequest.RegisterforAccounteStatements.RequestAccountEStatement_title();
            Activities.ServiceRequest.RegisterforAccounteStatements.emptyscreen();

        }else{
//            Activities.ServiceRequest.RegisterforAccounteStatements.RequestAccountEStatement_title();
//            Activities.ServiceRequest.RegisterforAccounteStatements.RequestAccountEStatement_labels();
//            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            RegisterforAccounteStatements.selectaccount();
            RegisterforAccounteStatements.submit();

        }
    }
}





