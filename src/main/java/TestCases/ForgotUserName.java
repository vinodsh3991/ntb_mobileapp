package TestCases;

import Activities.*;
import Activities.ForgotPassword.ForgotPasswordListScreen;
import Activities.ForgotUsername.AccountsForgotUsernameScreen;
import Activities.ForgotUsername.ForgotUsernameListScreen;
import Activities.LoginScreens.FristTimeLoginScreen;
import Activities.LoginScreens.NormalLoginScreen;
import Activities.OTPScreens.BillPaymentOTPSCreen;
import Activities.OTPScreens.CommonOTPScreen;
import Activities.OTPScreens.ForgotUsernameOTPScreen;
import Activities.PopUpMessages.PopUpMessages;
import TestBase.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ForgotUserName extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        FristTimeLoginScreen.forgotusername();

    }

    @Test(priority = 1, description = "My accounts - forgout username")
    public static void myaccountforgotusername() throws Exception {
        ForgotUsernameListScreen.ForgotUsernameListScreen_title();
        ForgotUsernameListScreen.myAccounts();
        AccountsForgotUsernameScreen.AccountsForgotUsernameScreen_title();
        AccountsForgotUsernameScreen.AccountsForgotUsernameScreen_labels();
        AccountsForgotUsernameScreen.enteraccountnumber("029215035020");
        AccountsForgotUsernameScreen.enternicnumber("950431016V");
        AccountsForgotUsernameScreen.enterDOB("12 February 1995");
        AccountsForgotUsernameScreen.next();
        CommonOTPScreen.CommonOTPScreen_title();
        CommonOTPScreen.CommonOTPScreen_labels();
        CommonOTPScreen.enetrOTP("123698745");
        NormalLoginScreen.NormalLoginScreen_labels();

    }

    @Test(priority = 2, description = "My accounts - Forgot username - Negative flow 01")
    public static void negativeflow01() throws Exception {
        ForgotUsernameListScreen.ForgotUsernameListScreen_title();
        ForgotUsernameListScreen.myAccounts();
        AccountsForgotUsernameScreen.AccountsForgotUsernameScreen_title();
        AccountsForgotUsernameScreen.AccountsForgotUsernameScreen_labels();
        AccountsForgotUsernameScreen.enteraccountnumber("029215035020");
        AccountsForgotUsernameScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AccountsForgotUsernameScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FristTimeLoginScreen.FristTimeLoginScreen_labels();

    }

    @Test(priority = 3, description = "My accounts - Forgot username - Negative flow 02")
    public static void negativeflow02() throws Exception {
        ForgotUsernameListScreen.ForgotUsernameListScreen_title();
        ForgotUsernameListScreen.myAccounts();
        AccountsForgotUsernameScreen.AccountsForgotUsernameScreen_title();
        AccountsForgotUsernameScreen.AccountsForgotUsernameScreen_labels();
        AccountsForgotUsernameScreen.enteraccountnumber("029215035020");
        AccountsForgotUsernameScreen.enternicnumber("950431016V");
        AccountsForgotUsernameScreen.enterDOB("12 February 1995");
        AccountsForgotUsernameScreen.next();
        CommonOTPScreen.CommonOTPScreen_title();
        CommonOTPScreen.CommonOTPScreen_labels();
        CommonOTPScreen.backbutton();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AccountsForgotUsernameScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FristTimeLoginScreen.FristTimeLoginScreen_labels();

    }


    @Test(priority = 4, description = "My accounts - Forgot username - Negative flow 03")
    public static void negativeflow03() throws Exception {
        ForgotUsernameListScreen.ForgotUsernameListScreen_title();
        ForgotUsernameListScreen.myAccounts();
        AccountsForgotUsernameScreen.AccountsForgotUsernameScreen_title();
        AccountsForgotUsernameScreen.AccountsForgotUsernameScreen_labels();
        AccountsForgotUsernameScreen.back();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();

    }
    @Test(priority = 5, description = "My accounts - Forgot username - Negative flow 05")
    public static void negativeflow04() throws Exception {
        ForgotUsernameListScreen.ForgotUsernameListScreen_title();
        ForgotUsernameListScreen.back();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();

    }




}