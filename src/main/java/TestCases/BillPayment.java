package TestCases;

import Activities.BillerMaintenance.BillerMaintenance;
import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.AddBillerPayee;
import TestData.LoginDetails;
import Activities.*;
import Activities.BillPayment.*;
import Activities.BillPayment.SelectAccountScreen;
import Activities.BillerMaintenance.BillerDetailsScreen;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PostQuickAccessMenuList.PostQuickAccessMenuList;
import Activities.OTPScreens.BillPaymentOTPSCreen;
import Activities.OTPScreens.LoginOTPScreen;
import io.qameta.allure.Step;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class BillPayment extends Configuration {

    @BeforeMethod( description = "Login to the app")
    @Step("Login with username::{0} and password: {1}")
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }

    @Test(priority = 1, description = "Bill Payment to own cards-  Below OTP")
    public static void billpaymenttocard() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();
        BillPaymentFillingScreen.BillPaymentFillingScreen_labels();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.BillPaymentBillerList_labels();
        BillPaymentBillerList.TaponMycards();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("0.26");
        BillPaymentFillingScreen.remarks("below OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_title();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_labels();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentSuccessScreen.BillPaymentSuccessScreen_title();
        BillPaymentSuccessScreen.BillPaymentSuccessScreen_labels();
        BillPaymentSuccessScreen.homebutton();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You made a payment from Savings - Mega Saver 2009******** to ");


    }

    @Test(priority = 2, description = "Bill Payment to normal biller -  Below OTP")
        public static void billpaymenttobillers() throws Exception {
            HomeScreen.quickaccessmenuicon();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PostQuickAccessMenuList.billpayment();
            BillPaymentFillingScreen.BillPaymentFillingScreen_title();
            BillPaymentFillingScreen.BillPaymentFillingScreen_labels();
            BillPaymentFillingScreen.payto();
            BillPaymentBillerList.BillPaymentBillerList_labels();
            BillPaymentBillerList.selectotherbiller();
            BillPaymentFillingScreen.payfrom();
            SelectAccountScreen.selectaccount();
            BillPaymentFillingScreen.amount("0.26");
            BillPaymentFillingScreen.remarks("below OTP");
            BillPaymentFillingScreen.paybutton();
            BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_title();
            BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_labels();
            BillPaymentVerifyInfoScreen.confirmbutton();
            BillPaymentSuccessScreen.BillPaymentSuccessScreen_title();
            BillPaymentSuccessScreen.BillPaymentSuccessScreen_labels();
            BillPaymentSuccessScreen.homebutton();
            CurrentDateTime currentDateTime =new CurrentDateTime();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            OpenNotification.opennotifications("NTB Mobile banking","You made a payment from Savings - Mega Saver 2009******** to ");

    }

    @Test(priority = 3, description = "Bill Payment to normal biller -  Above OTP")
    public static void billpaymenttobillersaboveotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();
        BillPaymentFillingScreen.BillPaymentFillingScreen_labels();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.BillPaymentBillerList_labels();
        BillPaymentBillerList.selectotherbiller();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("6.69");
        BillPaymentFillingScreen.remarks("Above OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_title();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_labels();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentOTPSCreen.BillPaymentOTPSCreen_labels();
        BillPaymentOTPSCreen.enetrOTP("123698745");
        BillPaymentSuccessScreen.BillPaymentSuccessScreen_title();
        BillPaymentSuccessScreen.BillPaymentSuccessScreen_labels();
        BillPaymentSuccessScreen.homebutton();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You made a payment from Savings - Mega Saver 2009******** to ");

    }
    @Test(priority = 4, description = "Bill Payment to own cards-  Below OTP")
    public static void billpaymenttocardAboveotp() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();
        BillPaymentFillingScreen.BillPaymentFillingScreen_labels();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.BillPaymentBillerList_labels();
        BillPaymentBillerList.TaponMycards();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("6.48");
        BillPaymentFillingScreen.remarks("Above OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_title();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_labels();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentOTPSCreen.BillPaymentOTPSCreen_labels();
        BillPaymentOTPSCreen.enetrOTP("123698745");
        BillPaymentSuccessScreen.BillPaymentSuccessScreen_title();
        BillPaymentSuccessScreen.BillPaymentSuccessScreen_labels();
        BillPaymentSuccessScreen.homebutton();

    }

    @Test(priority = 5, description = "One Time Bill Payment -  Above OTP")
    public static void onetimebillpaymentAboveOTP() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();
        BillPaymentFillingScreen.BillPaymentFillingScreen_labels();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.BillPaymentBillerList_labels();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.OneTimePaymentScreen_lables();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_title();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_labels();
        BillaPaymentConfirmBillerDetailsScreen.confirmbutton();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("6.48");
        BillPaymentFillingScreen.remarks("Above OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_title();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_labels();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentOTPSCreen.BillPaymentOTPSCreen_labels();
        BillPaymentOTPSCreen.enetrOTP("123698745");
        OneTimeBillPaymentSuccessScreen.OneTimeBillPaymentSuccessScreen_title();
        OneTimeBillPaymentSuccessScreen.OneTimeBillPaymentSuccessScreen_labels();
        OneTimeBillPaymentSuccessScreen.homebutton();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You made a payment from Savings - Mega Saver 2009******** to ");


    }

    @Test(priority = 6, description = "One Time Bill Payment -  Below OTP")
    public static void onetimebillpaymentBelowOTP() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();
        BillPaymentFillingScreen.BillPaymentFillingScreen_labels();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.OneTimePaymentScreen_lables();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_title();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_labels();
        BillaPaymentConfirmBillerDetailsScreen.confirmbutton();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("0.35");
        BillPaymentFillingScreen.remarks("Below OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_title();
        BillPaymentVerifyInfoScreen.BillPaymentVerifyInfoScreen_labels();
        BillPaymentVerifyInfoScreen.confirmbutton();
        OneTimeBillPaymentSuccessScreen.OneTimeBillPaymentSuccessScreen_title();
        OneTimeBillPaymentSuccessScreen.OneTimeBillPaymentSuccessScreen_labels();
        OneTimeBillPaymentSuccessScreen.savebiller();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","You made a payment from Savings - Mega Saver 2009******** to ");
        SaveBillerScreen.SaveBillerScreen_lables();
        AddBillerPayee addBillerPayee1 = new AddBillerPayee();
        SaveBillerScreen.enternickname(addBillerPayee1.getOnetimebillername());
        SaveBillerScreen.save();
        SaveBillerConfirmBillerDetailsScreen.SaveBillerConfirmBillerDetailsScreen_title();
        SaveBillerConfirmBillerDetailsScreen.SaveBillerConfirmBillerDetailsScreen_labels();
        SaveBillerConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Biller Saved Successfully.");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        BillerDetailsScreen.BillerDetailsScreen_title();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("New Biller Added","You have successfully added a new biller "+addBillerPayee1.getOnetimebillername()+" Ceb "+currentDateTime.gettimeanddate()+" SLST.");


    }

    @Test(priority = 7, description = "Bill Payment Screen - Negative Flow 01")
    public static void negativeflow01() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.selectotherbiller();
        BillPaymentFillingScreen.backbutton();
        PopUpMessages.popupmessage_header("Cancel Bill Payment");
        PopUpMessages.popupmessage_content("Do you want to cancel the transaction?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        BillPaymentFillingScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();

    }
    @Test(priority = 8, description = "Bill Payment Screen - Negative Flow 02")
    public static void negativeflow02() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.backbutton();
        HomeScreen.HomeScreen_labels();

    }
    @Test(priority = 9, description = "Bill Payment Screen - Negative Flow 03")
    public static void negativeflow03() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.PostQuickAccessMenuList_labels();
        PostQuickAccessMenuList.cross();
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();

    }
    @Test(priority = 10, description = "Bill Payment -Verify Information Screen -  Negative Flow 04")
    public static void negativeflow04() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.selectotherbiller();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("0.26");
        BillPaymentFillingScreen.remarks("below OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.backbutton();
        PopUpMessages.popupmessage_header("Cancel Bill Payment");
        PopUpMessages.popupmessage_content("Do you want to cancel the transaction?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        BillPaymentVerifyInfoScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();

    }

    @Test(priority = 11, description = "Bill Payment -Verify Information Screen -  Negative Flow 05")
    public static void negativeflow05() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.selectotherbiller();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("0.26");
        BillPaymentFillingScreen.remarks("below OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.cancel();
        PopUpMessages.popupmessage_header("Cancel Bill Payment");
        PopUpMessages.popupmessage_content("Do you want to cancel the transaction?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        BillPaymentVerifyInfoScreen.cancel();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();
    }
    @Test(priority = 12, description = "Bill Payment - OTP screen -  Negative Flow 06")
    public static void negativeflow06() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.selectotherbiller();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("6.69");
        BillPaymentFillingScreen.remarks("Above OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.confirmbutton();
        driver.hideKeyboard();
        BillPaymentOTPSCreen.backbutton();
        PopUpMessages.popupmessage_header("Cancel Bill Payment");
        PopUpMessages.popupmessage_content("Do you want to cancel the transaction?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        BillPaymentOTPSCreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();
    }

    @Test(priority = 13, description = "Bill Payment -Verify Information Screen -  Negative Flow 07")
    public static void negativeflow07() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.selectotherbiller();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("0.26");
        BillPaymentFillingScreen.remarks("below OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentSuccessScreen.returntopayment();
        BillPaymentFillingScreen.BillPaymentFillingScreen_title();
    }

    @Test(priority = 14, description = "One Time Bill Payment Screen -  Negative flow 08")
    public static void negativeflow08() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.BillPaymentBillerList_labels();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.backbutton();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this Biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        OneTimePaymentScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillPaymentBillerList.BillPaymentBillerList_labels();

    }

    @Test(priority = 15, description = "One Time Bill Payment Screen -  Negative flow 09")
    public static void negativeflow09() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.BillPaymentBillerList_labels();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.backbutton();
        BillPaymentBillerList.BillPaymentBillerList_labels();

    }

    @Test(priority = 16, description = "One Time Bill Payment Screen-  Negative flow 10")
    public static void negativeflow10() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.BillPaymentBillerList_labels();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.cancel();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this Biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        OneTimePaymentScreen.cancel();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillPaymentBillerList.BillPaymentBillerList_labels();

    }
    @Test(priority = 17, description = "One Time Bill Payment - Confirm Biller Details  -  Negative flow 11")
    public static void negativeflow11() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        BillPaymentBillerList.BillPaymentBillerList_labels();

    }

    @Test(priority = 18, description = "One Time Bill Payment - Confirm Biller Details  -  Negative flow 12")
    public static void negativeflow12() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.cancelbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        BillPaymentBillerList.BillPaymentBillerList_labels();

    }

    @Test(priority = 19, description = "One Time Bill Payment - Save Biller  -  Negative flow 13")
    public static void negativeflow13() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_title();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_labels();
        BillaPaymentConfirmBillerDetailsScreen.confirmbutton();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("6.48");
        BillPaymentFillingScreen.remarks("Above OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentOTPSCreen.enetrOTP("123698745");
        OneTimeBillPaymentSuccessScreen.savebiller();
        SaveBillerScreen.backbutton();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this Biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SaveBillerScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 19, description = "One Time Bill Payment - Save Biller  -  Negative flow 14")
    public static void negativeflow14() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_title();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_labels();
        BillaPaymentConfirmBillerDetailsScreen.confirmbutton();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("6.48");
        BillPaymentFillingScreen.remarks("Above OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentOTPSCreen.enetrOTP("123698745");
        OneTimeBillPaymentSuccessScreen.savebiller();
        SaveBillerScreen.cancel();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this Biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SaveBillerScreen.cancel();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillerMaintenance.BillerMaintenance_title();

    }
    @Test(priority = 20, description = "One Time Bill Payment - Save Biller  -  Negative flow 15")
    public static void negativeflow15() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_title();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_labels();
        BillaPaymentConfirmBillerDetailsScreen.confirmbutton();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("6.48");
        BillPaymentFillingScreen.remarks("Above OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentOTPSCreen.enetrOTP("123698745");
        OneTimeBillPaymentSuccessScreen.savebiller();
        SaveBillerScreen.save();
        SaveBillerConfirmBillerDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        SaveBillerScreen.save();
        SaveBillerConfirmBillerDetailsScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 21, description = "One Time Bill Payment - Save Biller  -  Negative flow 16")
    public static void negativeflow16() throws Exception {
        HomeScreen.quickaccessmenuicon();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PostQuickAccessMenuList.billpayment();
        BillPaymentFillingScreen.payto();
        BillPaymentBillerList.onetimebillpayment();
        OneTimePaymentScreen.selectcategory();
        OneTimePaymentScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        OneTimePaymentScreen.eneteraccnumber(addBillerPayee.getOnetimebillernameaccnumber());
        OneTimePaymentScreen.confirm();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_title();
        BillaPaymentConfirmBillerDetailsScreen.BillaPaymentConfirmBillerDetailsScreen_labels();
        BillaPaymentConfirmBillerDetailsScreen.confirmbutton();
        BillPaymentFillingScreen.payfrom();
        SelectAccountScreen.selectaccount();
        BillPaymentFillingScreen.amount("6.48");
        BillPaymentFillingScreen.remarks("Above OTP");
        BillPaymentFillingScreen.paybutton();
        BillPaymentVerifyInfoScreen.confirmbutton();
        BillPaymentOTPSCreen.enetrOTP("123698745");
        OneTimeBillPaymentSuccessScreen.savebiller();
        SaveBillerScreen.save();
        SaveBillerConfirmBillerDetailsScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        SaveBillerScreen.save();
        SaveBillerConfirmBillerDetailsScreen.cancelbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        BillerMaintenance.BillerMaintenance_title();

    }



}
