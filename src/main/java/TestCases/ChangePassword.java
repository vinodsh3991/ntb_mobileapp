package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.ChangePassworDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.Setting.ChangePasswordScreen;
import Activities.Setting.SettingMainScreen;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ChangePassword extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        ChangePassworDetails changePassworDetails = new ChangePassworDetails();
        LoginUsername.valid_username(changePassworDetails.getUsername());
        LoginPassword.valid_password(changePassworDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "Change Password")
    public static void changepassword() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.ChangePassword();
        ChangePasswordScreen.ChangePasswordScreen_title();
        ChangePasswordScreen.ChangePasswordScreen_labels();
        ChangePassworDetails changePassworDetails = new ChangePassworDetails();
        ChangePasswordScreen.entercurrentpassword(changePassworDetails.getPassword());
        ChangePasswordScreen.enternewpassword(changePassworDetails.getNewpassword());
        ChangePasswordScreen.enterconfirmpassword(changePassworDetails.getConfirmnewpassword());
        ChangePasswordScreen.changebutton();
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Password change successful. Tap OK to login.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        OpenNotification.opennotifications("NTB Mobile banking","You have successfully changed your Nations Mobile Banking Password on");


    }
    @Test(priority = 2, description = "Change Password - Negative Flow 01")
    public static void changepasswordnegativeflow01() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.ChangePassword();
        ChangePassworDetails changePassworDetails = new ChangePassworDetails();
        ChangePasswordScreen.entercurrentpassword(changePassworDetails.getPassword());
        ChangePasswordScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        ChangePasswordScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SettingMainScreen.SettingMainScreen_title();
    }

}
