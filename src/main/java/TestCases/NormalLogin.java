package TestCases;

import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.NavigationDrawer.Navigationdrawer;
import io.qameta.allure.Step;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class NormalLogin extends Configuration {

    @Test(priority = 1, dataProvider =  "validpassword", description = "Verify user can login using valid password")
    @Step("Login with password: {1}")
    public static void normallogin() throws Exception {
        LoginDetails loginDetails = new LoginDetails();
        Login.login_validUsername_ValidPassword();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        HomeScreen.navigationdrawericon();
        Navigationdrawer.logout();
        //NormalLoginScreen.NormalLoginScreen_labels();
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        NormalLoginScreen.loginbutton();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }

    @DataProvider
    public Object[][] validpassword() {
        return new Object[][]{
                new Object[]{"CARDS20", "Pas@1"},



        };
    }
}
