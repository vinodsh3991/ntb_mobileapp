package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PayeeMaintenance.DeletePayeeScreen;
import Activities.PayeeMaintenance.PayeeMaintenance;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class DeletePayee extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "Delete Ceft Payee")
    public static void deleteceftpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.Cefttoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();


    }
    @Test(priority = 2, description = "Delete Slip Payee")
    public static void deleteslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.Sliptoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();


    }
    @Test(priority = 3, description = "Delete third party NTB  Payee")
    public static void thirdpartyntbslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.ntbtoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();


    }
}

