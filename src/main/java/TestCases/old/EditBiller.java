package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.BillerMaintenance.BillerDetailsScreen;
import Activities.BillerMaintenance.BillerMaintenance;
import Activities.BillerMaintenance.ConfirmBillerDetailsScreen;
import Activities.BillerMaintenance.EditBillerScreen;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.EditPayeeBiller;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EditBiller extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }

    @Test(priority = 1, description = "Edit Biller")
    public static void editbiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditBillerScreen.EditBillerScreen_title();
        EditBillerScreen.EditBilerScreen_labels();
        EditPayeeBiller editPayeeBiller = new EditPayeeBiller();
        EditBillerScreen.editbillernickname(editPayeeBiller.getEditbillername());
        EditBillerScreen.editaccountnumber(editPayeeBiller.getEditbilleraccnumber());
        EditBillerScreen.save();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Changes to Biller saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();

    }
}
