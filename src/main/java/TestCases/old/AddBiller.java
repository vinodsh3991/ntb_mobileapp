package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.BillerMaintenance.*;
import Activities.BillerMaintenance.BillerMaintenance;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.AddBillerPayee;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class AddBiller extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "Add Biller")
    public static void addbiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.AddBillerScreen_title();
        AddBillerScreen.AddBillerScreen_title();
        AddBillerScreen.selectcategory();
        AddBillerScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerScreen.addnickname(addBillerPayee.getBillername());
        AddBillerScreen.addaccountnumber(addBillerPayee.getBilleraccnumber());
        AddBillerScreen.Add();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Biller Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 2, description = "Add Biller Account")
    public static void addbilleraccount() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.plusicon();
        AddBillerAccountScreen.AddBillerAccountScreen_title();
        AddBillerAccountScreen.AddBillerAccountScreen_labels();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerAccountScreen.addnickname(addBillerPayee.getBilleraccountname());
        AddBillerAccountScreen.addaccountnumber(addBillerPayee.getBilleraccaccnumber());
        AddBillerAccountScreen.Add();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Biller Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();

    }
}