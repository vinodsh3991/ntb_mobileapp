package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PostQuickAccessMenuList.PostFAQScreen;
import Activities.PostQuickAccessMenuList.PostQuickAccessMenuList;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class FAQ extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "FAQ - Navigation Drawer")
    public static void FAQ() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.faq();
        PostFAQScreen.PostFAQScreen_labels();
        PostFAQScreen.more();
        PostFAQScreen.scrolltohide();
        PostFAQScreen.quickaccessmenu();
        PostQuickAccessMenuList.PostQuickAccessMenuList_labels();
        PostQuickAccessMenuList.cross();
        PostFAQScreen.back();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }
}
