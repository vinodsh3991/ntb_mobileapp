package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.ServiceRequest.ServiceRequests;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ChequeBookRequest extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }
    @Test(priority = 1, description = "Cheque Book Request using address")
    public static void ChequeBookRequestusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.ChequeBookRequest();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/noCAsLayout").size() != 0;

        if(nocurrentaccount == true){
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.emptyscreen();

        }else{
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_labels();
            Activities.ServiceRequest.ChequeBookRequest.Accountlist();
            Activities.ServiceRequest.ChequeBookRequest.Chequenumberist();
            Activities.ServiceRequest.ChequeBookRequest.correspondenceaddress();
            Activities.ServiceRequest.ChequeBookRequest.submit();
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your request has been raised and is pending for bank’s approval");
            PopUpMessages.popupsuccessmessage_okbuttons("OK");

        }

    }

    @Test(priority = 2, description = "Cheque Book Request using branch")
    public static void ChequeBookRequestusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.ChequeBookRequest();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/noCAsLayout").size() != 0;

        if(nocurrentaccount == true){
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.emptyscreen();

        }else{
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_labels();
            Activities.ServiceRequest.ChequeBookRequest.Accountlist();
            Activities.ServiceRequest.ChequeBookRequest.Chequenumberist();
            Activities.ServiceRequest.ChequeBookRequest.selectbranch();
            Activities.ServiceRequest.ChequeBookRequest.submit();
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your request has been raised and is pending for bank’s approval");
            PopUpMessages.popupsuccessmessage_okbuttons("OK");

        }

    }

}
