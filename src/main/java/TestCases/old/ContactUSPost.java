package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PostQuickAccessMenuList.ContactUsScreen;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ContactUSPost extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "Contact Us nation email - Navigation Drawer")
    public static void contactusnationemail() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.nationemail();
    }
    @Test(priority = 2, description = "Contact Us american email - Navigation Drawer")
    public static void contactusamericanemail() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.americanemail();
    }

    @Test(priority = 3, description = "Contact Us master email - Navigation Drawer")
    public static void contactusmasteremail() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.masteremail();
    }
    @Test(priority = 4, description = "Contact Us nation url - Navigation Drawer")
    public static void contactusnationurl() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.nationurl();
    }
    @Test(priority = 5, description = "Contact Us american url - Navigation Drawer")
    public static void contactusamericanurl() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.americanurl();
    }



}
