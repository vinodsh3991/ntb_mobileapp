package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PayeeMaintenance.ConfirmPayeeDetailsScreen;
import Activities.PayeeMaintenance.EditPayeeScreen;
import Activities.PayeeMaintenance.PayeeDetailsScreen;
import Activities.PayeeMaintenance.PayeeMaintenance;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.EditPayeeBiller;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EditPayee extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }

    @Test(priority = 1, description = "Edit Ceft Payee")
    public static void editceftpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.Cefttoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getCefteditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getCefteditpayeename());
        EditPayeeScreen.selectslipbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getCefteditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsslip_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }
    @Test(priority = 2, description = "Edit Slip Payee")
    public static void editslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.Sliptoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getSlipeditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getSlipeditpayeename());
        EditPayeeScreen.selectceftbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getSlipeditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 3, description = "Edit NTB Payee")
    public static void editntbpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.ntbtoscroll();
        PayeeDetailsScreen.thirdpartyntbPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getNtbeditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getNtbeditpayeename());
        EditPayeeScreen.selectceftbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getNtbeditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }
}

