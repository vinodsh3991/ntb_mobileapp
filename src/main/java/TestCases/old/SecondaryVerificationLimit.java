package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.Setting.SecondaryVerificationScreen;
import Activities.Setting.SettingMainScreen;
import Activities.OTPScreens.LoginOTPScreen;
import Activities.OTPScreens.SecondaryVerificationLimitOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SecondaryVerificationLimit extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }

    @Test(priority = 1, description = "Change Secondary Verification Limit")
    public static void changesecondarylimit() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SecondaryVerifiation();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SecondaryVerificationScreen.SecondaryVerification_title();
        SecondaryVerificationScreen.SecondaryVerification_labels();
        SecondaryVerificationScreen.amount("20");
        SecondaryVerificationScreen.save();
        SecondaryVerificationLimitOTPScreen.enetrOTP("12395845");
        PopUpMessages.popupmessage_header("Limit Change Success");
        PopUpMessages.popupmessage_content("Secondary Verification Limit change request was successful");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        SecondaryVerificationScreen.SecondaryVerification_title();


    }


}