package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PostQuickAccessMenuList.PostQuickAccessMenuList;
import Activities.Setting.EditQuickAccessMenuScreen;
import Activities.Setting.SettingMainScreen;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EditQuickAccessMenu extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }

    @Test(priority = 1, description = "select less than 6 items")
    public static void lessthan6items() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.selectlessthan6items();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Select At Least 6 Items");
        PopUpMessages.popupmessage_content("Please select a total of 6 items");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }

    @Test(priority = 2, description = "select greater than 6 items")
    public static void greaterthan6items() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.selectgreaterthan6items();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Maximum Count Exceeded");
        PopUpMessages.popupmessage_content("A maximum of 6 items can be added to the quick access menu.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }
    @Test(priority = 3, description = "Edit Quick Access Menu List")
    public static void Editmenulist() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.specialoffers();
        EditQuickAccessMenuScreen.ServiceRequests();
        EditQuickAccessMenuScreen.BillerMaintenance();
        EditQuickAccessMenuScreen.PayeeMaintenance();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Your menu items have been changed successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.payeeMaintenance();
        PostQuickAccessMenuList.billerMaintenance();

    }
    @Test(priority = 4, description = "Default")
    public static void Default() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.ServiceRequests();
        EditQuickAccessMenuScreen.BillerMaintenance();
        EditQuickAccessMenuScreen.checkdefault();

    }


}
