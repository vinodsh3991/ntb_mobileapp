package TestCases.old;

import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.LoginScreens.FristTimeLoginScreen;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PostQuickAccessMenuList.PostCalculatorScreen;
import Activities.PostQuickAccessMenuList.PostLeasingCalculatorScreen;
import Activities.PostQuickAccessMenuList.PostQuickAccessMenuList;
import TestBase.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PostCalculator extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginUsername.valid_username("BothTEN");
        LoginPassword.valid_password("Pasword@2");
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        HomeScreen.quickaccessmenuicon();
    }

    @Test(priority = 1, description = "Leasing calculator")
    public static void Leasing_calculator() throws Exception {
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.leasingcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.PostLeasingCalculatorScreen_title();
        PostLeasingCalculatorScreen.PostLeasingCalculatorScreen_labels();
        PostLeasingCalculatorScreen.BrandNew();
        PostLeasingCalculatorScreen.CarSUV();
        PostLeasingCalculatorScreen.enteradvance("5000");
        PostLeasingCalculatorScreen.enterleaseamount("6000");
        PostLeasingCalculatorScreen.selectduration();
        PostLeasingCalculatorScreen.calculate();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.MonthlyInstallment_label();
        PostLeasingCalculatorScreen.arrow();
        PostLeasingCalculatorScreen.yes();
        PopUpMessages.popupmessage_header("Details Submitted");
        PopUpMessages.popupmessage_content("Your leasing quotation request has been sent to the bank successfully, we will contact you for further details.");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        PostCalculatorScreen.PostCalculatorScreen_title();



    }
}
