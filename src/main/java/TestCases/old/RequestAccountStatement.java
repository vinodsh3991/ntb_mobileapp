package TestCases.old;

import Activities.Calender.ServiceRequestCalender;
import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.ServiceRequest.ServiceRequests;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.Dates;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import Listeners.AllureReportListener;
import java.util.concurrent.TimeUnit;
@Listeners({AllureReportListener.class})

public class RequestAccountStatement extends Configuration {
    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }
    @Test(priority = 1, description = "Request Account Statement - Email")
    public static void RequestAccountStatementusingemail() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
        Activities.ServiceRequest.RequestAccountStatement.Accountlist();
        Activities.ServiceRequest.RequestAccountStatement.fromdate();
        Dates dates =new Dates();
        ServiceRequestCalender.selectYear(dates.getYear());
        ServiceRequestCalender.selectmonth(dates.getFromdate());
        Activities.ServiceRequest.RequestAccountStatement.todate();
        ServiceRequestCalender.selectmonth(dates.getTodate());
        Activities.ServiceRequest.RequestAccountStatement.email();
        Activities.ServiceRequest.RequestAccountStatement.submit();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Request Submitted");
        PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }

    @Test(priority = 2, description = "Request Account Statement - Address  ")
    public static void RequestAccountStatementusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
        Activities.ServiceRequest.RequestAccountStatement.Accountlist();
        Activities.ServiceRequest.RequestAccountStatement.fromdate();
        Dates dates =new Dates();
        ServiceRequestCalender.selectYear(dates.getYear());
        ServiceRequestCalender.selectmonth(dates.getFromdate());
        Activities.ServiceRequest.RequestAccountStatement.todate();
        ServiceRequestCalender.selectmonth(dates.getTodate());
        Activities.ServiceRequest.RequestAccountStatement.correspondenceaddress();
        Activities.ServiceRequest.RequestAccountStatement.submit();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Request Submitted");
        PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }

    @Test(priority = 3, description = "Request Account Statement -  Branch ")
    public static void RequestAccountStatementusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
        Activities.ServiceRequest.RequestAccountStatement.Accountlist();
        Activities.ServiceRequest.RequestAccountStatement.fromdate();
        Dates dates =new Dates();
        ServiceRequestCalender.selectYear(dates.getYear());
        ServiceRequestCalender.selectmonth(dates.getFromdate());
        Activities.ServiceRequest.RequestAccountStatement.todate();
        ServiceRequestCalender.selectmonth(dates.getTodate());
        Activities.ServiceRequest.RequestAccountStatement.selectbranch();
        Activities.ServiceRequest.RequestAccountStatement.submit();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Request Submitted");
        PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }


}
