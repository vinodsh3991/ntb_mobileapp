package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.Setting.SettingMainScreen;
import Activities.Setting.TransactionLimitScreen;
import Activities.OTPScreens.LoginOTPScreen;
import Activities.OTPScreens.TransactionLimitOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TransactionLimit extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }

    @Test(priority = 1, description = "Change Transaction Limit")
    public static void changetransactionlimit() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.TransactionLimits();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        TransactionLimitScreen.TransactionLimit_title();
        TransactionLimitScreen.amount("7");
        TransactionLimitScreen.save();
        TransactionLimitOTPScreen.enetrOTP("12395845");
        PopUpMessages.popupmessage_header("Limit Change Success");
        PopUpMessages.popupmessage_content("Transaction Limit change request was successful");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        TransactionLimitScreen.TransactionLimit_title();


    }


}