package TestCases.old;

import Activities.Calender.ServiceRequestCalender;
import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.ServiceRequest.ServiceRequests;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.Dates;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import Listeners.AllureReportListener;

import java.util.concurrent.TimeUnit;
@Listeners({AllureReportListener.class})


public class RequestCardStatement extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }



    }
    @Test(priority = 1, description = "Request Card Statement - Email")
    public static void RequestCardStatementusingemail() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true){
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        }else {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.email();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
        }
    }

    @Test(priority = 2, description = "Request Card Statement - Address  ")
    public static void RequestCardStatementusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true) {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        }else{
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.correspondenceaddress();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");

        }
    }

    @Test(priority = 3, description = "Request Card Statement -  Branch ")
    public static void RequestCardStatementusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nocards == true) {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        } else {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.selectbranch();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");

        }


    }

}
