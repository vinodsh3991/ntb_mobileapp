package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.BillerMaintenance.BillerDetailsScreen;
import Activities.BillerMaintenance.BillerMaintenance;
import Activities.BillerMaintenance.DeleteBillerScreen;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class DeleteBiller extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "Delete biller account")
    public static void deletebilleraccount() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        DeleteBillerScreen.scrolltobilleraccount();
        DeleteBillerScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Accounts?");
        PopUpMessages.popupmessage_content("Are you sure you want to Delete?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Biller(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerDetailsScreen.BillerDetailsScreen_title();


    }

    @Test(priority = 1, description = "Delete biller ")
    public static void deletebiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeleteBillerScreen.scrolltobiller();
        DeleteBillerScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Billers?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Biller(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Biller(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();


    }


}
