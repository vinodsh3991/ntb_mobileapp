package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.ServiceRequest.ServiceRequests;
import Activities.ServiceRequest.StopChequeSuccessScreen;
import Activities.ServiceRequest.StopaCheque;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class StopCheque extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }
    @Test(priority = 1, description = "Stop a Cheque - success path(anothercheque)")
    public static void StopaCheque_success1() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if (nocurrentaccount == true) {
            StopaCheque.emptyscreen();

        } else {
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            StopChequeSuccessScreen.StopChequeSuccessScreen_labels();
            StopChequeSuccessScreen.anothercheque();
            StopaCheque.StopaCheque_title();
            StopaCheque.StopaCheque_title();

        }
    }

    @Test(priority = 2, description = "Stop a Cheque - success path(Home)")
    public static void StopaCheque_success2() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if (nocurrentaccount == true) {
            StopaCheque.emptyscreen();

        } else {
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            StopChequeSuccessScreen.StopChequeSuccessScreen_labels();
            StopChequeSuccessScreen.home();
            HomeScreen.HomeScreen_labels();

        }
    }



    @Test(priority = 3, description = "Stop a Cheque - already used cheque")
        public static void StopaCheque_already() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

            if(nocurrentaccount == true){
                StopaCheque.emptyscreen();

            }else{
                StopaCheque.Accountlist();
                StopaCheque.enterchequenbr("45565");
                StopaCheque.ReasonList();
                StopaCheque.StopaCheque();
                PopUpMessages.popupmessage_header("Stop This Cheque?");
                PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
                PopUpMessages.popupsuccessmessage_negativebutton("Yes");
                PopUpMessages.popupmessage_header("Cheque already presented");
                PopUpMessages.popupmessage_content("The Cheque number you entered has already been presented. Please enter a valid cheque number");
                PopUpMessages.popupsuccessmessage_buttons("OK");
                StopaCheque.StopaCheque_title();
            }

        }

    @Test(priority = 4, description = "Stop a Cheque - invalid cheque")
    public static void StopaCheque_invalid() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if(nocurrentaccount == true){
            StopaCheque.emptyscreen();

        }else{
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_negativebutton("Yes");
            PopUpMessages.popupmessage_header("Invalid Cheque Book");
            PopUpMessages.popupmessage_content("You have entered an incorrect cheque number. Please enter the cheque number correctly.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            StopaCheque.StopaCheque_title();
        }

    }

    @Test(priority = 5, description = "Stop a Cheque - no need")
    public static void StopaCheque_noneed() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.StopaCheque();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/emptyChequeBook").size() != 0;

        if(nocurrentaccount == true){
            StopaCheque.emptyscreen();

        }else{
            StopaCheque.Accountlist();
            StopaCheque.enterchequenbr("45565");
            StopaCheque.ReasonList();
            StopaCheque.StopaCheque();
            PopUpMessages.popupmessage_header("Stop This Cheque?");
            PopUpMessages.popupmessage_content("Are you sure you want to stop this cheque?");
            PopUpMessages.popupsuccessmessage_positivebutton("No");
            StopaCheque.StopaCheque_title();
        }
        }

    }





