package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.Setting.PostLanguageSelectionScreen;
import Activities.Setting.SettingMainScreen;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LanguageSettings extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


    }

    @Test(priority = 1, description = "Change Language")
    public static void changelanguages() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.Languagesetting();
        PostLanguageSelectionScreen.LanguageSettings_title();
        PostLanguageSelectionScreen.LanguageSettings_labels();
        PostLanguageSelectionScreen.sinhalabutton();
        PostLanguageSelectionScreen.sinhalatitle();
        PostLanguageSelectionScreen.tamilbutton();
        PostLanguageSelectionScreen.tamiltitle();
        PostLanguageSelectionScreen.englishbutton();
        PostLanguageSelectionScreen.englishtitle();
    }


}
