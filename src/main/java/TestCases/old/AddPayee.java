package TestCases.old;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PayeeMaintenance.AddPayeeScreen;
import Activities.PayeeMaintenance.ConfirmPayeeDetailsScreen;
import Activities.PayeeMaintenance.PayeeMaintenance;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.AddBillerPayee;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class AddPayee extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }


    @Test(priority = 1, description = "Add Ceft Payee")
        public static void addceftpayee() throws Exception {
             HomeScreen.navigationdrawericon();
             Navigationdrawer.payeemaintenance();
             driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
             PayeeMaintenance.plusicon();
            AddPayeeScreen.AddPayeeScreen_title();
            AddPayeeScreen.AddPayeeScreen_labels();
             AddBillerPayee addBillerPayee = new AddBillerPayee();
            AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
            AddPayeeScreen.addpayeename(addBillerPayee.getCeftpayeename());
            AddPayeeScreen.selectceftbrank();
            AddPayeeScreen.addaccountnumber(addBillerPayee.getCeftpayeeaccountnmber());
            AddPayeeScreen.addbutton();
            ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
            ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
            ConfirmPayeeDetailsScreen.confirmbutton();
            PopUpMessages.popupmessage_header("Payee Saved");
            PopUpMessages.popupmessage_content("Payee Saved Successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
            PayeeMaintenance.PayeeMaintenance_title();

    }
    @Test(priority = 2, description = "Add Slip Payee")
    public static void addslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.plusicon();
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.AddPayeeScreen_labels();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getSlippayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getSlippayeename());
        AddPayeeScreen.selectslipbank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getSlippayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsslip_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 3, description = "Add NTB Payee")
    public static void addntbpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.plusicon();
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.AddPayeeScreen_labels();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getNtbpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getNtbpayeename());
        AddPayeeScreen.selectntbbank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getNtbpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();
    }
}

