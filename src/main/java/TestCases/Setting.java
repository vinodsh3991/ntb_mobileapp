package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PostQuickAccessMenuList.PostQuickAccessMenuList;
import Activities.Setting.*;
import Activities.OTPScreens.LoginOTPScreen;
import Activities.OTPScreens.SecondaryVerificationLimitOTPScreen;
import Activities.OTPScreens.TransactionLimitOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Setting extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "Change Transaction Limit")
    public static void changetransactionlimit() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.TransactionLimits();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        TransactionLimitScreen.TransactionLimit_title();
        TransactionLimitScreen.amount("25");
        TransactionLimitScreen.save();
        TransactionLimitOTPScreen.enetrOTP("12395845");
        PopUpMessages.popupmessage_header("Limit Change Success");
        PopUpMessages.popupmessage_content("Transaction Limit change request was successful");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        TransactionLimitScreen.TransactionLimit_title();
        OpenNotification.opennotifications("NTB Mobile banking","You have successfully changed your Nations Mobile Banking Transfers to other banks Limit from");
    }
    @Test(priority = 2, description = "Change Secondary Verification Limit")
    public static void changesecondarylimit() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SecondaryVerifiation();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SecondaryVerificationScreen.SecondaryVerification_title();
        SecondaryVerificationScreen.SecondaryVerification_labels();
        SecondaryVerificationScreen.amount("51");
        SecondaryVerificationScreen.save();
        SecondaryVerificationLimitOTPScreen.enetrOTP("12395845");
        PopUpMessages.popupmessage_header("Limit Change Success");
        PopUpMessages.popupmessage_content("Secondary Verification Limit change request was successful");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        SecondaryVerificationScreen.SecondaryVerification_title();
        OpenNotification.opennotifications("NTB Mobile banking","You have successfully changed your Nations Mobile Banking Secondary Verification Limit from");

    }

    @Test(priority = 3, description = "Change Language")
    public static void changelanguages() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.Languagesetting();
        PostLanguageSelectionScreen.LanguageSettings_title();
        PostLanguageSelectionScreen.LanguageSettings_labels();
        PostLanguageSelectionScreen.sinhalabutton();
        PostLanguageSelectionScreen.sinhalatitle();
        PostLanguageSelectionScreen.tamilbutton();
        PostLanguageSelectionScreen.tamiltitle();
        PostLanguageSelectionScreen.englishbutton();
        PostLanguageSelectionScreen.englishtitle();
    }

    @Test(priority = 4, description = "select less than 6 items")
    public static void lessthan6items() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.selectlessthan6items();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Select At Least 6 Items");
        PopUpMessages.popupmessage_content("Please select a total of 6 items");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }

    @Test(priority = 5, description = "select greater than 6 items")
    public static void greaterthan6items() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.selectgreaterthan6items();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Maximum Count Exceeded");
        PopUpMessages.popupmessage_content("A maximum of 6 items can be added to the quick access menu.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }
    @Test(priority = 6, description = "Edit Quick Access Menu List")
    public static void Editmenulist() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.specialoffers();
        EditQuickAccessMenuScreen.ServiceRequests();
        EditQuickAccessMenuScreen.BillerMaintenance();
        EditQuickAccessMenuScreen.PayeeMaintenance();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Your menu items have been changed successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.payeeMaintenance();
        PostQuickAccessMenuList.billerMaintenance();

    }
    @Test(priority = 7, description = "Default")
    public static void Default() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.ServiceRequests();
        EditQuickAccessMenuScreen.BillerMaintenance();
        EditQuickAccessMenuScreen.checkdefault();

    }

    @Test(priority = 9, description = "Change Language - Negative Flow")
    public static void changelanguagesnegativeflow1() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.Languagesetting();
        PostLanguageSelectionScreen.LanguageSettings_title();
        PostLanguageSelectionScreen.LanguageSettings_labels();
        PostLanguageSelectionScreen.back();
        SettingMainScreen.SettingMainScreen_title();
    }

    @Test(priority = 10, description = "Biometric Options ")
    public static void Biometric() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.BiometricOptions();
        ConfigureBiometricScreen.arrow();
        FingerprintScreen.FingerprintScreen_title();
        FingerprintScreen.FingerprintScreen_labels();
        FingerprintScreen.ontoggle();
        FingerprintScreen.arrow();
        FingerprintSetUpScreen.FingerprintSetUpScreen_title();
        FingerprintSetUpScreen.FingerprintSetUpScreen_labels();
        Runtime.getRuntime().exec("adb -e emu finger touch 1");
    }

    @Test(priority = 11, description = "Profile setting - update profile picture")
    public static void updateprofilepic() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.Profilesetting();
        ProfileSettingScreen.ProfileSettingScreen_title();
        ProfileSettingScreen.ProfileSettingScreen_labels();
        ProfileSettingScreen.edit();
        ProfileSettingScreen.takepicture();
        ProfileSettingScreen.ProfileSettingScreen_title();
    }
    @Test(priority = 12, description = "Profile setting - Negative Flow 01")
    public static void profilesettingnegativeflow01() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.Profilesetting();
        ProfileSettingScreen.back();
        SettingMainScreen.SettingMainScreen_title();
    }
    @Test(priority = 13, description = "Change Transaction Limit - Negative Flow 01")
    public static void transactionlimitnegaiveflow01() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.TransactionLimits();
        TransactionLimitScreen.TransactionLimit_title();
        TransactionLimitScreen.amount("70");
        TransactionLimitScreen.back();
        SettingMainScreen.SettingMainScreen_title();
    }

    @Test(priority = 14, description = "Change Transaction Limit - Negative Flow 02")
    public static void transactionlimitnegaiveflow02() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.TransactionLimits();
        TransactionLimitScreen.TransactionLimit_title();
        TransactionLimitScreen.amount("70");
        TransactionLimitScreen.cancel();

    }

    @Test(priority = 15, description = "Change Secondary Verification Limit - Negative Flow 01")
    public static void secondarylimitnegaiveflow01() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SecondaryVerifiation();
        SecondaryVerificationScreen.SecondaryVerification_title();
        SecondaryVerificationScreen.amount("70");
        SecondaryVerificationScreen.back();
        SettingMainScreen.SettingMainScreen_title();
    }

    @Test(priority = 16, description = "Change Secondary Verification Limit - Negative Flow 02")
    public static void secondarylimitnegaiveflow02() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SecondaryVerifiation();
        SecondaryVerificationScreen.SecondaryVerification_title();
        SecondaryVerificationScreen.amount("70");
        SecondaryVerificationScreen.cancel();


    }

}
