package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.FDAccountOpening.FDOpenSuccessScreen;
import Activities.FDAccountOpening.FDTandCScreen;
import Activities.FDAccountOpening.FixedDepositAccountOpeningScreen;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.SavingAccountOpening.OpenAccountsScreen;
import Activities.OTPScreens.FDOTPScreen;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class FDAccountOpening extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

        }
    }

    @Test(priority = 1, description = "Fixed Deposit Calculator(Matutity)")
    public static void FDOpenMaturity() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.FixedDepositAccountOpeningScreen_title();
        FixedDepositAccountOpeningScreen.FixedDepositAccountOpeningScreen_labels();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("5000000");
        FixedDepositAccountOpeningScreen.selectmaturity();
        FixedDepositAccountOpeningScreen.selectmonthordays();
        FixedDepositAccountOpeningScreen.continuebtn();
        FixedDepositAccountOpeningScreen.aftercalculate_labels();
        FixedDepositAccountOpeningScreen.NIR();
        FixedDepositAccountOpeningScreen.ATR();
        FixedDepositAccountOpeningScreen.enterbranch();
        FixedDepositAccountOpeningScreen.confirmbtn2();
        FDTandCScreen.FDTandCScreen_title();
        FDTandCScreen.FDTandCScreen_labels();
        FDTandCScreen.agree();
        FDOTPScreen.enetrOTP("1232556984");
        FDOpenSuccessScreen.FDOpenSuccessScreen_title();
        FDOpenSuccessScreen.FDOpenSuccessScreen_labels();
        FDOpenSuccessScreen.home();
        HomeScreen.HomeScreen_labels();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","Dear MR.MAHINDA AKMEEMANA, You have opened a Fixed Deposit Maturity account");

    }

    @Test(priority = 2, description = "Fixed Deposit Calculator(Flexi Fixed Deposit)")
    public static void FDOpenFlexi() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.FixedDepositAccountOpeningScreen_title();
        FixedDepositAccountOpeningScreen.FixedDepositAccountOpeningScreen_labels();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("5000000");
        FixedDepositAccountOpeningScreen.selectflexi();
        FixedDepositAccountOpeningScreen.selectmonthordays();
        FixedDepositAccountOpeningScreen.continuebtn();
        FixedDepositAccountOpeningScreen.aftercalculate_labels();
        FixedDepositAccountOpeningScreen.NIR();
        FixedDepositAccountOpeningScreen.ATR();
        FixedDepositAccountOpeningScreen.enterbranch();
        FixedDepositAccountOpeningScreen.confirmbtn2();
        FDTandCScreen.FDTandCScreen_title();
        FDTandCScreen.FDTandCScreen_labels();
        FDTandCScreen.agree();
        FDOTPScreen.enetrOTP("1232556984");
        FDOpenSuccessScreen.FDOpenSuccessScreen_title();
        FDOpenSuccessScreen.FDOpenSuccessScreen_labels();
        FDOpenSuccessScreen.home();
        HomeScreen.HomeScreen_labels();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("NTB Mobile banking","Dear MR.MAHINDA AKMEEMANA, You have opened a Flexi Fixed Deposit account");

    }
    @Test(priority = 3, description = "Fixed Deposit Calculator(rates Link )")
    public static void FDopenratelink() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.interatestrateslink();

    }

    @Test(priority = 4, description = "Fixed Deposit Calculator(Checking Deposit Amount and back button message) - Pre Login")
    public static void FDopendepositamountvalidation() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("500");
        FixedDepositAccountOpeningScreen.continuebtn();
        PopUpMessages.popupmessage_header("Invalid Deposit Amount");
        PopUpMessages.popupmessage_content("Deposit amount should be higher than LKR 50,000.00.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        FixedDepositAccountOpeningScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FixedDepositAccountOpeningScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 5, description = "Fixed Deposit Calculator(Negative Flow 1)")
    public static void FDOpennegativeflow1() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("5000000");
        FixedDepositAccountOpeningScreen.selectflexi();
        FixedDepositAccountOpeningScreen.selectmonthordays();
        FixedDepositAccountOpeningScreen.continuebtn();
        FixedDepositAccountOpeningScreen.aftercalculate_labels();
        FixedDepositAccountOpeningScreen.enterbranch();
        FixedDepositAccountOpeningScreen.confirmbtn2();
        FDTandCScreen.FDTandCScreen_title();
        FDTandCScreen.FDTandCScreen_labels();
        FDTandCScreen.agree();
        driver.hideKeyboard();
        FDOTPScreen.backbutton();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FDOTPScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FDTandCScreen.decline();
        HomeScreen.HomeScreen_labels();


    }

    @Test(priority = 6, description = "Fixed Deposit Calculator(Negative Flow 2)")
    public static void FDOpennegativeflow2() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("5000000");
        FixedDepositAccountOpeningScreen.selectflexi();
        FixedDepositAccountOpeningScreen.selectmonthordays();
        FixedDepositAccountOpeningScreen.continuebtn();
        FixedDepositAccountOpeningScreen.aftercalculate_labels();
        FixedDepositAccountOpeningScreen.enterbranch();
        FixedDepositAccountOpeningScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FixedDepositAccountOpeningScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();


    }

    }