package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.SavingAccountOpening.OpenAccountsScreen;
import Activities.SavingAccountOpening.SVTandCScreen;
import Activities.SavingAccountOpening.SavingAccountOpeningScreen;
import Activities.OTPScreens.LoginOTPScreen;
import Activities.OTPScreens.SavingOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SavingAccountOpening extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();


        try{
        if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
            LoginOTPScreen.enetrOTP("456325896");
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);


        } }catch (NoSuchElementException e){
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
        }

    @Test(priority = 1, description = "Saving Account Opening - Nation Saver")
    public static void nationsaveraccountopen() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationSaver();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200000");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        SavingOTPScreen.enetrOTP("15565477");
        PopUpMessages.popupmessage_header("Account Opened");
        PopUpMessages.popupmessage_content("You have successfully opened an account.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        HomeScreen.HomeScreen_labels();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("Nations Mobile Banking","Dear MR.MAHINDA AKMEEMANA, You have opened a Nations Saver account 2006******** using Nations Mobile Banking App with LKR 2,000.00 from Savings - Mega Saver account 2009******** on "+currentDateTime.gettimeanddate()+" SLST.");

    }

    @Test(priority = 2, description = "Saving Account Opening - Nation Max Bonus")
    public static void nationmaxbonusaccountopen() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationsMaxBonus();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        SavingOTPScreen.enetrOTP("15565477");
        PopUpMessages.popupmessage_header("Account Opened");
        PopUpMessages.popupmessage_content("You have successfully opened an account.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 3, description = "Saving Account Opening - Nation Mega Saver")
    public static void nationmegasaveraccountopen() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationsMegaSaver();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        SavingOTPScreen.enetrOTP("15565477");
        PopUpMessages.popupmessage_header("Account Opened");
        PopUpMessages.popupmessage_content("You have successfully opened an account.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 4, description = "Saving Account Oepning - Nation Tax Planner")
    public static void nationtaxplanneraccountopen() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationsTaxPlanner();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        SavingOTPScreen.enetrOTP("15565477");
        PopUpMessages.popupmessage_header("Account Opened");
        PopUpMessages.popupmessage_content("You have successfully opened an account.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 5, description = "Saving Account Opening - Negative flow 01")
    public static void SAnegativeflow1() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationSaver();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        driver.hideKeyboard();
        SavingOTPScreen.backbutton();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SavingOTPScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SVTandCScreen.decline();
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 6, description = "Saving Account Opening - Negative flow 02")
    public static void SAnegativeflow2() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.back();
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 7, description = "Saving Account Opening - Negative flow 03")
    public static void SAnegativeflow3() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationSaver();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.cancel();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SavingAccountOpeningScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();
    }



}
