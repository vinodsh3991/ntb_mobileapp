package TestCases;

import Activities.Calender.ServiceRequestCalender;
import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.*;
import Activities.*;
import Activities.BillerMaintenance.*;
import Activities.FDAccountOpening.FDOpenSuccessScreen;
import Activities.FDAccountOpening.FDTandCScreen;
import Activities.FDAccountOpening.FixedDepositAccountOpeningScreen;
import Activities.Inbox.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PayeeMaintenance.*;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PostQuickAccessMenuList.*;
import Activities.SavingAccountOpening.OpenAccountsScreen;
import Activities.SavingAccountOpening.SVTandCScreen;
import Activities.SavingAccountOpening.SavingAccountOpeningScreen;
import Activities.ServiceRequest.ServiceRequests;
import Activities.Setting.*;
import Activities.OTPScreens.*;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CompleteRUN01 extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }

    @Test(priority = 1, description = "Saving Account Opening - Nation Saver")
    public static void nationsaveraccountopen() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationSaver();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        SavingOTPScreen.enetrOTP("15565477");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Account Opened");
        PopUpMessages.popupmessage_content("You have successfully opened an account.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        HomeScreen.HomeScreen_labels();
    }

    @Test(priority = 2, description = "Saving Account Opening - Nation Max Bonus")
    public static void nationmaxbonusaccountopen() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationsMaxBonus();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        SavingOTPScreen.enetrOTP("15565477");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Account Opened");
        PopUpMessages.popupmessage_content("You have successfully opened an account.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 3, description = "Saving Account Opening - Nation Mega Saver")
    public static void nationmegasaveraccountopen() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationsMegaSaver();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        SavingOTPScreen.enetrOTP("15565477");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Account Opened");
        PopUpMessages.popupmessage_content("You have successfully opened an account.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 4, description = "Saving Account Oepning - Nation Tax Planner")
    public static void nationtaxplanneraccountopen() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationsTaxPlanner();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        SavingOTPScreen.enetrOTP("15565477");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Account Opened");
        PopUpMessages.popupmessage_content("You have successfully opened an account.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 5, description = "Saving Account Opening - Negative flow 01")
    public static void SAnegativeflow1() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationSaver();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.confirm();
        SVTandCScreen.SVTandCScreen_title();
        SVTandCScreen.SVTandCScreen_labels();
        SVTandCScreen.agree();
        driver.hideKeyboard();
        SavingOTPScreen.backbutton();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SavingOTPScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        SVTandCScreen.decline();
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 6, description = "Saving Account Opening - Negative flow 02")
    public static void SAnegativeflow2() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.back();
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 7, description = "Saving Account Opening - Negative flow 03")
    public static void SAnegativeflow3() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.savingaccopen();
        SavingAccountOpeningScreen.SavingAccountOpeningScreen_title();
        SavingAccountOpeningScreen.enteracctype();
        SavingAccountOpeningScreen.selectNationSaver();
        SavingAccountOpeningScreen.enterfundingaccount();
        SavingAccountOpeningScreen.enterbranch();
        SavingAccountOpeningScreen.enteramount("200080");
        SavingAccountOpeningScreen.enterpurpose();
        SavingAccountOpeningScreen.cancel();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        SavingAccountOpeningScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 8, description = "Fixed Deposit Calculator(Matutity)")
    public static void FDOpenMaturity() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.FixedDepositAccountOpeningScreen_title();
        FixedDepositAccountOpeningScreen.FixedDepositAccountOpeningScreen_labels();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("5000000");
        FixedDepositAccountOpeningScreen.selectmaturity();
        FixedDepositAccountOpeningScreen.selectmonthordays();
        FixedDepositAccountOpeningScreen.continuebtn();
        FixedDepositAccountOpeningScreen.aftercalculate_labels();
        FixedDepositAccountOpeningScreen.NIR();
        FixedDepositAccountOpeningScreen.ATR();
        FixedDepositAccountOpeningScreen.enterbranch();
        FixedDepositAccountOpeningScreen.confirmbtn2();
        FDTandCScreen.FDTandCScreen_title();
        FDTandCScreen.FDTandCScreen_labels();
        FDTandCScreen.agree();
        FDOTPScreen.enetrOTP("1232556984");
        FDOpenSuccessScreen.FDOpenSuccessScreen_title();
        FDOpenSuccessScreen.FDOpenSuccessScreen_labels();
        FDOpenSuccessScreen.home();
        HomeScreen.HomeScreen_labels();
    }

    @Test(priority = 9, description = "Fixed Deposit Calculator(Flexi Fixed Deposit)")
    public static void FDOpenFlexi() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.OpenAccountsScreen_title();
        OpenAccountsScreen.OpenAccountsScreen_labels();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.FixedDepositAccountOpeningScreen_title();
        FixedDepositAccountOpeningScreen.FixedDepositAccountOpeningScreen_labels();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("5000000");
        FixedDepositAccountOpeningScreen.selectflexi();
        FixedDepositAccountOpeningScreen.selectmonthordays();
        FixedDepositAccountOpeningScreen.continuebtn();
        FixedDepositAccountOpeningScreen.aftercalculate_labels();
        FixedDepositAccountOpeningScreen.NIR();
        FixedDepositAccountOpeningScreen.ATR();
        FixedDepositAccountOpeningScreen.enterbranch();
        FixedDepositAccountOpeningScreen.confirmbtn2();
        FDTandCScreen.FDTandCScreen_title();
        FDTandCScreen.FDTandCScreen_labels();
        FDTandCScreen.agree();
        FDOTPScreen.enetrOTP("1232556984");
        FDOpenSuccessScreen.FDOpenSuccessScreen_title();
        FDOpenSuccessScreen.FDOpenSuccessScreen_labels();
        FDOpenSuccessScreen.home();
        HomeScreen.HomeScreen_labels();
    }
    @Test(priority = 10, description = "Fixed Deposit Calculator(rates Link )")
    public static void FDopenratelink() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.interatestrateslink();

    }

    @Test(priority = 11, description = "Fixed Deposit Calculator(Checking Deposit Amount and back button message) - Pre Login")
    public static void FDopendepositamountvalidation() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("500");
        FixedDepositAccountOpeningScreen.continuebtn();
        PopUpMessages.popupmessage_header("Invalid Deposit Amount");
        PopUpMessages.popupmessage_content("Deposit amount should be higher than LKR 50,000.00.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        FixedDepositAccountOpeningScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FixedDepositAccountOpeningScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 12, description = "Fixed Deposit Calculator(Negative Flow 1)")
    public static void FDOpennegativeflow1() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("5000000");
        FixedDepositAccountOpeningScreen.selectflexi();
        FixedDepositAccountOpeningScreen.selectmonthordays();
        FixedDepositAccountOpeningScreen.continuebtn();
        FixedDepositAccountOpeningScreen.aftercalculate_labels();
        FixedDepositAccountOpeningScreen.enterbranch();
        FixedDepositAccountOpeningScreen.confirmbtn2();
        FDTandCScreen.FDTandCScreen_title();
        FDTandCScreen.FDTandCScreen_labels();
        FDTandCScreen.agree();
        driver.hideKeyboard();
        FDOTPScreen.backbutton();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FDOTPScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        FDTandCScreen.decline();
        HomeScreen.HomeScreen_labels();


    }

    @Test(priority = 13, description = "Fixed Deposit Calculator(Negative Flow 2)")
    public static void FDOpennegativeflow2() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.openaccount();
        OpenAccountsScreen.fdopen();
        FixedDepositAccountOpeningScreen.selectfundingaccount();
        FixedDepositAccountOpeningScreen.enterdepositeamount("5000000");
        FixedDepositAccountOpeningScreen.selectflexi();
        FixedDepositAccountOpeningScreen.selectmonthordays();
        FixedDepositAccountOpeningScreen.continuebtn();
        FixedDepositAccountOpeningScreen.aftercalculate_labels();
        FixedDepositAccountOpeningScreen.enterbranch();
        FixedDepositAccountOpeningScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FixedDepositAccountOpeningScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 14, description = "Add Ceft Payee")
    public static void addceftpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.plusicon();
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.AddPayeeScreen_labels();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getCeftpayeename());
        AddPayeeScreen.selectceftbrank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getCeftpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }
    @Test(priority = 15, description = "Add Slip Payee")
    public static void addslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.plusicon();
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.AddPayeeScreen_labels();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getSlippayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getSlippayeename());
        AddPayeeScreen.selectslipbank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getSlippayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsslip_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 16, description = "Add NTB Payee")
    public static void addntbpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.plusicon();
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.AddPayeeScreen_labels();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getNtbpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getNtbpayeename());
        AddPayeeScreen.selectntbbank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getNtbpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();
    }

    @Test(priority = 17, description = "Edit Ceft Payee")
    public static void editceftpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.Cefttoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getCefteditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getCefteditpayeename());
        EditPayeeScreen.selectslipbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getCefteditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsslip_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }
    @Test(priority = 18, description = "Edit Slip Payee")
    public static void editslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.Sliptoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getSlipeditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getSlipeditpayeename());
        EditPayeeScreen.selectceftbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getSlipeditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 19, description = "Edit NTB Payee")
    public static void editntbpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.ntbtoscroll();
        PayeeDetailsScreen.thirdpartyntbPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getNtbeditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getNtbeditpayeename());
        EditPayeeScreen.selectceftbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getNtbeditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 20, description = "Delete Ceft Payee")
    public static void deleteceftpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.Cefttoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();


    }
    @Test(priority = 21, description = "Delete Slip Payee")
    public static void deleteslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.Sliptoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();


    }

    @Test(priority = 22, description = "Delete third party NTB  Payee")
    public static void thirdpartyntbslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.ntbtoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 23, description = "Add Biller")
    public static void addbiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.AddBillerScreen_title();
        AddBillerScreen.AddBillerScreen_title();
        AddBillerScreen.selectcategory();
        AddBillerScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerScreen.addnickname(addBillerPayee.getBillername());
        AddBillerScreen.addaccountnumber(addBillerPayee.getBilleraccnumber());
        AddBillerScreen.Add();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Biller Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 24, description = "Add Biller Account")
    public static void addbilleraccount() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.plusicon();
        AddBillerAccountScreen.AddBillerAccountScreen_title();
        AddBillerAccountScreen.AddBillerAccountScreen_labels();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerAccountScreen.addnickname(addBillerPayee.getBilleraccountname());
        AddBillerAccountScreen.addaccountnumber(addBillerPayee.getBilleraccaccnumber());
        AddBillerAccountScreen.Add();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Biller Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();

    }
    @Test(priority = 25, description = "Edit Biller")
    public static void editbiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditBillerScreen.EditBillerScreen_title();
        EditBillerScreen.EditBilerScreen_labels();
        EditPayeeBiller editPayeeBiller = new EditPayeeBiller();
        EditBillerScreen.editbillernickname(editPayeeBiller.getEditbillername());
        EditBillerScreen.editaccountnumber(editPayeeBiller.getEditbilleraccnumber());
        EditBillerScreen.save();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Changes to Biller saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 26, description = "Delete biller account")
    public static void deletebilleraccount() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        DeleteBillerScreen.scrolltobilleraccount();
        DeleteBillerScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Accounts?");
        PopUpMessages.popupmessage_content("Are you sure you want to Delete?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Biller(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerDetailsScreen.BillerDetailsScreen_title();


    }

    @Test(priority = 27, description = "Delete biller ")
    public static void deletebiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeleteBillerScreen.scrolltobiller();
        DeleteBillerScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Billers?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Biller(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Biller(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();


    }
    @Test(priority = 28, description = "Change Transaction Limit")
    public static void changetransactionlimit() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.TransactionLimits();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        TransactionLimitScreen.TransactionLimit_title();
        TransactionLimitScreen.amount("7");
        TransactionLimitScreen.save();
        TransactionLimitOTPScreen.enetrOTP("12395845");
        PopUpMessages.popupmessage_header("Limit Change Success");
        PopUpMessages.popupmessage_content("Transaction Limit change request was successful");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        TransactionLimitScreen.TransactionLimit_title();

    }
    @Test(priority = 29, description = "Change Secondary Verification Limit")
    public static void changesecondarylimit() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SecondaryVerifiation();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SecondaryVerificationScreen.SecondaryVerification_title();
        SecondaryVerificationScreen.SecondaryVerification_labels();
        SecondaryVerificationScreen.amount("20");
        SecondaryVerificationScreen.save();
        SecondaryVerificationLimitOTPScreen.enetrOTP("12395845");
        PopUpMessages.popupmessage_header("Limit Change Success");
        PopUpMessages.popupmessage_content("Secondary Verification Limit change request was successful");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        SecondaryVerificationScreen.SecondaryVerification_title();

    }

    @Test(priority = 30, description = "Change Language")
    public static void changelanguages() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        SettingMainScreen.Languagesetting();
        PostLanguageSelectionScreen.LanguageSettings_title();
        PostLanguageSelectionScreen.LanguageSettings_labels();
        PostLanguageSelectionScreen.sinhalabutton();
        PostLanguageSelectionScreen.sinhalatitle();
        PostLanguageSelectionScreen.tamilbutton();
        PostLanguageSelectionScreen.tamiltitle();
        PostLanguageSelectionScreen.englishbutton();
        PostLanguageSelectionScreen.englishtitle();
    }
    @Test(priority = 31, description = "Terms and Conditions - Navigation Drawer")
    public static void tandc() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.tandc();
        NormalTandCScreen.TandCscreen_labels();
        NormalTandCScreen.quickaccessmenu();
        PostQuickAccessMenuList.PostQuickAccessMenuList_labels();
        PostQuickAccessMenuList.cross();
        NormalTandCScreen.backbutton();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }
    @Test(priority = 32, description = "FAQ - Navigation Drawer")
    public static void FAQ() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.faq();
        PostFAQScreen.PostFAQScreen_labels();
        PostFAQScreen.more();
        PostFAQScreen.scrolltohide();
        PostFAQScreen.quickaccessmenu();
        PostQuickAccessMenuList.PostQuickAccessMenuList_labels();
        PostQuickAccessMenuList.cross();
        PostFAQScreen.back();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }

    @Test(priority = 33, description = "Contact Us nation email - Navigation Drawer")
    public static void contactusnationemail() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.nationemail();
    }
    @Test(priority = 34, description = "Contact Us american email - Navigation Drawer")
    public static void contactusamericanemail() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.americanemail();
    }

    @Test(priority = 35, description = "Contact Us master email - Navigation Drawer")
    public static void contactusmasteremail() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.masteremail();
    }
    @Test(priority = 36, description = "Contact Us nation url - Navigation Drawer")
    public static void contactusnationurl() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.nationurl();
    }
    @Test(priority = 37, description = "Contact Us american url - Navigation Drawer")
    public static void contactusamericanurl() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.contactus();
        ContactUsScreen.ContatUs_title();
        ContactUsScreen.ContactUsScreen_labels();
        ContactUsScreen.americanurl();
    }

    @Test(priority = 38, description = "Compose a mail - inbox screen")
    public static void composemailinbox() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        ComposeScreen.messagebox("First message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 39, description = "Compose a mail with attachment - inbox screen")
    public static void composemailattachmentinbox() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        Get_attachment.attachimage();
        ComposeScreen.messagebox("First message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 40, description = "Reply for a mail - inbox screen")
    public static void replyforemailinbox() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.MailThreadScreen_title();
        InboxMailThreadScreen.MailThreadScreen_labels();
        InboxMailThreadScreen.taponmessage();
        InboxMessageDetailsScreen.MessageDetailsScreen_title();
        InboxMessageDetailsScreen.MessageDetailsScreen_labels();
        InboxMessageDetailsScreen.replybutton();
        ReplyMailScreen.ReplyMailScreen_title();
        ReplyMailScreen.ReplyMailScreen_labels();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.morebutton();
        ReplyMailScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 41, description = "Reply for a mail with attchment - inbox screen")
    public static void replyforemailinboxattachment() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.MailThreadScreen_title();
        InboxMailThreadScreen.MailThreadScreen_labels();
        InboxMailThreadScreen.taponmessage();
        InboxMessageDetailsScreen.MessageDetailsScreen_title();
        InboxMessageDetailsScreen.MessageDetailsScreen_labels();
        InboxMessageDetailsScreen.replybutton();
        ReplyMailScreen.ReplyMailScreen_title();
        ReplyMailScreen.ReplyMailScreen_labels();
        Get_attachment.attachimage();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.morebutton();
        ReplyMailScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }

    @Test(priority = 42, description = "Delete a mail thread - inbox screen")
    public static void inboxdeletemailthread() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.selectmail();
        InboxMainScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }

    @Test(priority = 43, description = "Delete a message - inbox screen")
    public static void inboxdeletemessage() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.MailThreadScreen_title();
        InboxMailThreadScreen.MailThreadScreen_labels();
        InboxMailThreadScreen.taponmessage();
        InboxMessageDetailsScreen.MessageDetailsScreen_title();
        InboxMessageDetailsScreen.MessageDetailsScreen_labels();
        InboxMessageDetailsScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 44, description = "Compose a mail - Sent screen")
    public static void composemailsent() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        ComposeScreen.messagebox("First message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 45, description = "Compose a mail with attachment - Sent screen")
    public static void composemailsentattachment() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        Get_attachment.attachimage();
        ComposeScreen.messagebox("First message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 46, description = "Reply for a mail - sent screen")
    public static void replyforemailsent() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.taponmail();
        SentMailThreadScreen.SentMailThreadScreen_title();
        SentMailThreadScreen.SentMailThreadScreen_labels();
        SentMailThreadScreen.taponmessage();
        SentMessageDetailsScreen.SentMessageDetailsScreen_title();
        SentMessageDetailsScreen.SentMessageDetailsScreen_labels();
        SentMessageDetailsScreen.replybutton();
        ReplyMailScreen.ReplyMailScreen_title();
        ReplyMailScreen.ReplyMailScreen_labels();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.morebutton();
        ReplyMailScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 47, description = "Reply for a mail with attchment - sent screen")
    public static void replyforemailsentattachment() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.taponmail();
        SentMailThreadScreen.SentMailThreadScreen_title();
        SentMailThreadScreen.SentMailThreadScreen_labels();
        SentMailThreadScreen.taponmessage();
        SentMessageDetailsScreen.SentMessageDetailsScreen_title();
        SentMessageDetailsScreen.SentMessageDetailsScreen_labels();
        SentMessageDetailsScreen.replybutton();
        ReplyMailScreen.ReplyMailScreen_title();
        ReplyMailScreen.ReplyMailScreen_labels();
        Get_attachment.attachimage();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.morebutton();
        ReplyMailScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 48, description = "Delete a mail thread - Sent Screen")
    public static void sentdeletemailthread() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.selectmail();
        SentMainScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }

    @Test(priority = 49, description = "Delete a message - Sent Screen")
    public static void sentdeletemessage() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.taponmail();
        SentMailThreadScreen.SentMailThreadScreen_title();
        SentMailThreadScreen.SentMailThreadScreen_labels();
        SentMailThreadScreen.taponmessage();
        SentMessageDetailsScreen.SentMessageDetailsScreen_title();
        SentMessageDetailsScreen.SentMessageDetailsScreen_labels();
        SentMessageDetailsScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }

    @Test(priority = 50, description = "Request Account Statement - Email")
    public static void RequestAccountStatementusingemail() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
        Activities.ServiceRequest.RequestAccountStatement.Accountlist();
        Activities.ServiceRequest.RequestAccountStatement.fromdate();
        Dates dates =new Dates();
        ServiceRequestCalender.selectYear(dates.getYear());
        ServiceRequestCalender.selectmonth(dates.getFromdate());
        Activities.ServiceRequest.RequestAccountStatement.todate();
        ServiceRequestCalender.selectmonth(dates.getTodate());
        Activities.ServiceRequest.RequestAccountStatement.email();
        Activities.ServiceRequest.RequestAccountStatement.submit();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Request Submitted");
        PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }

    @Test(priority = 51, description = "Request Account Statement - Address  ")
    public static void RequestAccountStatementusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
        Activities.ServiceRequest.RequestAccountStatement.Accountlist();
        Activities.ServiceRequest.RequestAccountStatement.fromdate();
        Dates dates =new Dates();
        ServiceRequestCalender.selectYear(dates.getYear());
        ServiceRequestCalender.selectmonth(dates.getFromdate());
        Activities.ServiceRequest.RequestAccountStatement.todate();
        ServiceRequestCalender.selectmonth(dates.getTodate());
        Activities.ServiceRequest.RequestAccountStatement.correspondenceaddress();
        Activities.ServiceRequest.RequestAccountStatement.submit();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Request Submitted");
        PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }

    @Test(priority = 52, description = "Request Account Statement -  Branch ")
    public static void RequestAccountStatementusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestAccountStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Activities.ServiceRequest.RequestAccountStatement.RequestAccountStatement_labels();
        Activities.ServiceRequest.RequestAccountStatement.Accountlist();
        Activities.ServiceRequest.RequestAccountStatement.fromdate();
        Dates dates =new Dates();
        ServiceRequestCalender.selectYear(dates.getYear());
        ServiceRequestCalender.selectmonth(dates.getFromdate());
        Activities.ServiceRequest.RequestAccountStatement.todate();
        ServiceRequestCalender.selectmonth(dates.getTodate());
        Activities.ServiceRequest.RequestAccountStatement.selectbranch();
        Activities.ServiceRequest.RequestAccountStatement.submit();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Request Submitted");
        PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }
    @Test(priority = 53, description = "Request Card Statement - Email")
    public static void RequestCardStatementusingemail() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true){
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        }else {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.email();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");
        }
    }

    @Test(priority = 54, description = "Request Card Statement - Address  ")
    public static void RequestCardStatementusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if(nocards == true) {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        }else{
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.correspondenceaddress();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");

        }
    }

    @Test(priority = 55, description = "Request Card Statement -  Branch ")
    public static void RequestCardStatementusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.RequestCardStatement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Boolean nocards = driver.findElementsById("com.nationstrust.mobilebanking:id/empty_list").size() != 0;

        if (nocards == true) {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.emptyscreen();

        } else {
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_title();
            Activities.ServiceRequest.RequestCardStatement.RequesCardStatement_labels();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            Activities.ServiceRequest.RequestCardStatement.Cardlist();
            Activities.ServiceRequest.RequestCardStatement.fromdate();
            Dates dates =new Dates();
            ServiceRequestCalender.selectYear(dates.getYear());
            ServiceRequestCalender.selectmonth(dates.getFromdate());
            Activities.ServiceRequest.RequestCardStatement.todate();
            ServiceRequestCalender.selectmonth(dates.getTodate());
            Activities.ServiceRequest.RequestCardStatement.selectbranch();
            Activities.ServiceRequest.RequestCardStatement.submit();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your statement request has been placed successfully.");
            PopUpMessages.popupsuccessmessage_buttons("OK");

        }


    }

    @Test(priority = 56, description = "Cheque Book Request using address")
    public static void ChequeBookRequestusingaddress() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.ChequeBookRequest();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/noCAsLayout").size() != 0;

        if(nocurrentaccount == true){
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.emptyscreen();

        }else{
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_labels();
            Activities.ServiceRequest.ChequeBookRequest.Accountlist();
            Activities.ServiceRequest.ChequeBookRequest.Chequenumberist();
            Activities.ServiceRequest.ChequeBookRequest.correspondenceaddress();
            Activities.ServiceRequest.ChequeBookRequest.submit();
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your request has been raised and is pending for bank’s approval");
            PopUpMessages.popupsuccessmessage_okbuttons("OK");

        }

    }

    @Test(priority = 57, description = "Cheque Book Request using branch")
    public static void ChequeBookRequestusingbranch() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.servicerequest();
        ServiceRequests.ChequeBookRequest();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Boolean nocurrentaccount = driver.findElementsById("com.nationstrust.mobilebanking:id/noCAsLayout").size() != 0;

        if(nocurrentaccount == true){
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.emptyscreen();

        }else{
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_title();
            Activities.ServiceRequest.ChequeBookRequest.ChequeBookRequest_labels();
            Activities.ServiceRequest.ChequeBookRequest.Accountlist();
            Activities.ServiceRequest.ChequeBookRequest.Chequenumberist();
            Activities.ServiceRequest.ChequeBookRequest.selectbranch();
            Activities.ServiceRequest.ChequeBookRequest.submit();
            PopUpMessages.popupmessage_header("Request Submitted");
            PopUpMessages.popupmessage_content("Your request has been raised and is pending for bank’s approval");
            PopUpMessages.popupsuccessmessage_okbuttons("OK");

        }

    }


    @Test(priority = 58, description = "Leasing calculator")
    public static void Leasing_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.leasingcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.PostLeasingCalculatorScreen_title();
        PostLeasingCalculatorScreen.PostLeasingCalculatorScreen_labels();
        PostLeasingCalculatorScreen.BrandNew();
        PostLeasingCalculatorScreen.CarSUV();
        PostLeasingCalculatorScreen.enteradvance("5000");
        PostLeasingCalculatorScreen.enterleaseamount("6000");
        PostLeasingCalculatorScreen.selectduration();
        PostLeasingCalculatorScreen.calculate();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.MonthlyInstallment_label();
        PostLeasingCalculatorScreen.arrow();
        PostLeasingCalculatorScreen.yes();
        PopUpMessages.popupmessage_header("Details Submitted");
        PopUpMessages.popupmessage_content("Your leasing quotation request has been sent to the bank successfully, we will contact you for further details.");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
        PostCalculatorScreen.PostCalculatorScreen_title();

    }

    @Test(priority = 59, description = "Fixed Deposit calculator - Maturity")
    public static void FDMaturity_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectmaturity();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.selectfundingaccount();
        PostFDFundingScreen.enterbranch();
        PostFDFundingScreen.confirm();
        PostFDTandCScreen.FDTandCScreen_title();
        PostFDTandCScreen.FDTandCScreen_labels();
        PostFDTandCScreen.agree();
        FDOTPScreen.enetrOTP("1232556984");
        PostFDOpenSuccessScreen.FDOpenSuccessScreen_title();
        PostFDOpenSuccessScreen.FDOpenSuccessScreen_labels();
        PostFDOpenSuccessScreen.home();
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 60, description = "Fixed Deposit calculator - Flexi Fixed Deposit")
    public static void FDFlexi_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.selectfundingaccount();
        PostFDFundingScreen.enterbranch();
        PostFDFundingScreen.confirm();
        PostFDTandCScreen.FDTandCScreen_title();
        PostFDTandCScreen.FDTandCScreen_labels();
        PostFDTandCScreen.agree();
        FDOTPScreen.enetrOTP("1232556984");
        PostFDOpenSuccessScreen.FDOpenSuccessScreen_title();
        PostFDOpenSuccessScreen.FDOpenSuccessScreen_labels();
        PostFDOpenSuccessScreen.home();
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 61, description = "Fixed Deposit calculator - Negative Flow 01")
    public static void FDNegativeflow01_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        PostFDCalculatorScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();

    }

    @Test(priority = 62, description = "Fixed Deposit calculator - Negative Flow 02")
    public static void FDNegativeFlow02_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.no();
        PostCalculatorScreen.PostCalculatorScreen_title();

    }

    @Test(priority = 63, description = "Fixed Deposit calculator - Negative Flow 03")
    public static void FDNegative03_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.cancel();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        PostFDFundingScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        HomeScreen.HomeScreen_labels();

    }
    @Test(priority = 64, description = "Fixed Deposit calculator - Negative Flow 04")
    public static void FDNegativeFlow4_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.selectfundingaccount();
        PostFDFundingScreen.enterbranch();
        PostFDFundingScreen.confirm();
        PostFDTandCScreen.FDTandCScreen_title();
        PostFDTandCScreen.FDTandCScreen_labels();
        PostFDTandCScreen.decline();
        HomeScreen.HomeScreen_labels();

    }
    @Test(priority = 65, description = "Fixed Deposit calculator - Negative flow 05")
    public static void FDNegativeFlow5_calculator() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.calculator();
        PostCalculatorScreen.PostCalculatorScreen_title();
        PostCalculatorScreen.PostCalculatorScreen_labels();
        PostCalculatorScreen.fdcalculator();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostFDCalculatorScreen.PostFDCalculatorScreen_title();
        PostFDCalculatorScreen.PostFDCalculatorScreen_labels();
        PostFDCalculatorScreen.enterdepositeamount("5000000");
        PostFDCalculatorScreen.selectflexi();
        PostFDCalculatorScreen.selectmonthordays();
        PostFDCalculatorScreen.calculate();
        PostFDCalculatorScreen.aftercalculate_labels();
        PostFDCalculatorScreen.NIR();
        PostFDCalculatorScreen.ATR();
        PostFDCalculatorScreen.yes();
        PostFDFundingScreen.PostFDFundingScreen_title();
        PostFDFundingScreen.PostFDFundingScreen_labels();
        PostFDFundingScreen.NIR();
        PostFDFundingScreen.ATR();
        PostFDFundingScreen.selectfundingaccount();
        PostFDFundingScreen.enterbranch();
        PostFDFundingScreen.confirm();
        PostFDTandCScreen.FDTandCScreen_title();
        PostFDTandCScreen.FDTandCScreen_labels();
        PostFDTandCScreen.agree();
        driver.hideKeyboard();
        FDOTPScreen.backbutton();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        FDOTPScreen.backbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PostFDTandCScreen.FDTandCScreen_title();

    }
    @Test(priority = 66, description = "select less than 6 items")
    public static void lessthan6items() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.selectlessthan6items();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Select At Least 6 Items");
        PopUpMessages.popupmessage_content("Please select a total of 6 items");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }

    @Test(priority = 67, description = "select greater than 6 items")
    public static void greaterthan6items() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.selectgreaterthan6items();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Maximum Count Exceeded");
        PopUpMessages.popupmessage_content("A maximum of 6 items can be added to the quick access menu.");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }
    @Test(priority = 68, description = "Edit Quick Access Menu List")
    public static void Editmenulist() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.specialoffers();
        EditQuickAccessMenuScreen.ServiceRequests();
        EditQuickAccessMenuScreen.BillerMaintenance();
        EditQuickAccessMenuScreen.PayeeMaintenance();
        EditQuickAccessMenuScreen.save();
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Your menu items have been changed successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.payeeMaintenance();
        PostQuickAccessMenuList.billerMaintenance();

    }
    @Test(priority = 69, description = "Default")
    public static void Default() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.setting();
        SettingMainScreen.SettingMainScreen_title();
        SettingMainScreen.SettingMainScreen_labels();
        SettingMainScreen.Editquickaccessmenu();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditQuickAccessMenuScreen.EditQuickAccessMenuScreen_labels();
        EditQuickAccessMenuScreen.ServiceRequests();
        EditQuickAccessMenuScreen.BillerMaintenance();
        EditQuickAccessMenuScreen.checkdefault();

    }


}

