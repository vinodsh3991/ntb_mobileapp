package TestCases;

import Activities.*;
import Activities.LoginScreens.FristTimeLoginScreen;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PostQuickAccessMenuList.PostLeasingCalculatorScreen;
import Activities.PreQuickAccessMenuListScreens.*;
import TestBase.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PreQuickAccessMenu extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        FristTimeLoginScreen.quickaccessmenu();
        PreQuickAccessMenuList.PreQuickAccessMenuList_labels();
    }

    @Test(priority = 1, description = "Language")
    public static void changelanguage() throws Exception {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PreQuickAccessMenuList.language();
        PreLanguageSelectionScreen.sinhalabutton();
        PreLanguageSelectionScreen.sinhalatitle();
        PreLanguageSelectionScreen.tamilbutton();
        PreLanguageSelectionScreen.tamiltitle();
        PreLanguageSelectionScreen.englishbutton();
        PreLanguageSelectionScreen.englishtitle();

    }

    @Test(priority = 2, description = "FAQ")
    public static void FAQ() throws Exception {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PreQuickAccessMenuList.FAQ();
        PreFAQScreen.FAQscreen_title();
        PreFAQScreen.FAQscreen_title_labels();
        PreFAQScreen.morequestions();
        PreFAQScreen.taponaquestion();
        PreFAQScreen.closeanswer();
        PreFAQScreen.hidequestions();
        PreFAQScreen.quickaccessmenu();
        PreQuickAccessMenuList.PreQuickAccessMenuList_labels();
        PreQuickAccessMenuList.cross();

    }

    @Test(priority =3, description = "Contact Us nation email - Pre Login")
    public static void contactusnationemail() throws Exception {
        PreQuickAccessMenuList.contactus();
        PreContactUsScreen.ContatUs_title();
        PreContactUsScreen.ContactUsScreen_labels();
        PreContactUsScreen.nationemail();
    }
    @Test(priority = 4, description = "Contact Us american email - Pre Login")
    public static void contactusamericanemail() throws Exception {
        PreQuickAccessMenuList.contactus();
        PreContactUsScreen.ContatUs_title();
        PreContactUsScreen.ContactUsScreen_labels();
        PreContactUsScreen.americanemail();
    }

    @Test(priority = 5, description = "Contact Us master email - Pre Login")
    public static void contactusmasteremail() throws Exception {
        PreQuickAccessMenuList.contactus();
        PreContactUsScreen.ContatUs_title();
        PreContactUsScreen.ContactUsScreen_labels();
        PreContactUsScreen.masteremail();
    }
    @Test(priority = 6, description = "Contact Us nation url - Pre Login")
    public static void contactusnationurl() throws Exception {
        PreQuickAccessMenuList.contactus();
        PreContactUsScreen.ContatUs_title();
        PreContactUsScreen.ContactUsScreen_labels();
        PreContactUsScreen.nationurl();
    }
    @Test(priority = 7, description = "Contact Us american url - Pre Login")
    public static void contactusamericanurl() throws Exception {
        PreQuickAccessMenuList.contactus();
        PreContactUsScreen.ContatUs_title();
        PreContactUsScreen.ContactUsScreen_labels();
        PreContactUsScreen.americanurl();
    }
    @Test(priority = 8, description = "Special Offers call - Pre Login")
    public static void Specialofferscall() throws Exception {
        PreQuickAccessMenuList.offers();
        PreSpecialOffersScreen.PreSpecialOffers_title();
        PreSpecialOffersScreen.PreSpecialOffers_labels();
        PreSpecialOffersScreen.quickaccessmenu();
        PreQuickAccessMenuList.PreQuickAccessMenuList_labels();
        PreQuickAccessMenuList.cross();
        PreSpecialOffersScreen.taponoffer();
        PreSpecialDetailsScreen.PreSpecialDetails_labels();
        PreSpecialDetailsScreen.call();

    }

    @Test(priority = 9, description = "Special Offers more info- Pre Login")
    public static void Specialoffersmoreinfor() throws Exception {
        PreQuickAccessMenuList.offers();
        PreSpecialOffersScreen.PreSpecialOffers_title();
        PreSpecialOffersScreen.PreSpecialOffers_labels();
        PreSpecialOffersScreen.quickaccessmenu();
        PreQuickAccessMenuList.PreQuickAccessMenuList_labels();
        PreQuickAccessMenuList.cross();
        PreSpecialOffersScreen.taponoffer();
        PreSpecialDetailsScreen.PreSpecialDetails_labels();
        PreSpecialDetailsScreen.moreinfo();

    }

    @Test(priority = 10, description = "Bank Products - share  - Pre Login")
    public static void bankproductsshare() throws Exception {
        PreQuickAccessMenuList.bankproducts();
        PreBankProductsScreen.PreBankProductsScreen_title();
        PreBankProductsScreen.PreBankProductsScreen_labels();
        PreBankProductsScreen.personalarrow();
        PreBankProductsScreen.peraonalshare();

    }

    @Test(priority = 11, description = "Bank Products - More - Pre Login")
    public static void bankproductsmore() throws Exception {
        PreQuickAccessMenuList.bankproducts();
        PreBankProductsScreen.PreBankProductsScreen_title();
        PreBankProductsScreen.PreBankProductsScreen_labels();
        PreBankProductsScreen.personalarrow();
        PreBankProductsScreen.personalmore();
    }

    @Test(priority = 12, description = "Bank Products - arrows behaviour - Pre Login")
    public static void bankproductsarrows() throws Exception {
        PreQuickAccessMenuList.bankproducts();
        PreBankProductsScreen.PreBankProductsScreen_title();
        PreBankProductsScreen.PreBankProductsScreen_labels();
        PreBankProductsScreen.personalarrow();
        PreBankProductsScreen.personalexpanded();
        PreBankProductsScreen.personalarrow();
        PreBankProductsScreen.businessarrow();
        PreBankProductsScreen.businessexpanded();
        PreBankProductsScreen.businessarrow();
        PreBankProductsScreen.corporatearrow();
        PreBankProductsScreen.corporateexpanded();
        PreBankProductsScreen.corporatearrow();
        PreBankProductsScreen.quicaccessmenu();
        PreQuickAccessMenuList.PreQuickAccessMenuList_labels();
        PreQuickAccessMenuList.cross();

    }

    @Test(priority = 13, description = "Rates - Pre Login")
    public static void rates() throws Exception {
        PreQuickAccessMenuList.rates();
        PreRatesScreen.PreRatesScreen_title();
        PreRatesScreen.PreRatesScreen_labels();
        PreRatesScreen.quicaccessmenu();
        PreQuickAccessMenuList.cross();
    }
    @Test(priority = 14, description = "Fixed Deposit Calculator(Matutity) - Pre Login")
    public static void FDcalculatorMaturity() throws Exception {
        PreQuickAccessMenuList.calculator();
        PreMainCalculatorScreen.PreMainCalculatorScreen_title();
        PreMainCalculatorScreen.PreMainCalculatorScreen_labels();
        PreMainCalculatorScreen.fdcalculator();
        PreFDcalculatorScreen.PreFDcalculatorScreen_title();
        PreFDcalculatorScreen.PreFDcalculatorScreen_labels();
        PreFDcalculatorScreen.enterdepositeamount("6000000");
        PreFDcalculatorScreen.selectmaturity();
        PreFDcalculatorScreen.selectmonthordays();
        PreFDcalculatorScreen.calculate();
        PreFDcalculatorScreen.aftercalculate_labels();
        PreFDcalculatorScreen.NIR();
        PreFDcalculatorScreen.ATR();
    }

    @Test(priority = 15, description = "Fixed Deposit Calculator(Flexi FD ) - Pre Login")
    public static void FDcalculatorFlexi() throws Exception {
        PreQuickAccessMenuList.calculator();
        PreMainCalculatorScreen.PreMainCalculatorScreen_title();
        PreMainCalculatorScreen.PreMainCalculatorScreen_labels();
        PreMainCalculatorScreen.fdcalculator();
        PreFDcalculatorScreen.PreFDcalculatorScreen_title();
        PreFDcalculatorScreen.PreFDcalculatorScreen_labels();
        PreFDcalculatorScreen.enterdepositeamount("6000000");
        PreFDcalculatorScreen.selectflexi();
        PreFDcalculatorScreen.selectmonthordays();
        PreFDcalculatorScreen.calculate();
        PreFDcalculatorScreen.aftercalculate_labels();
        PreFDcalculatorScreen.NIR();
        PreFDcalculatorScreen.ATR();
    }
    @Test(priority = 16, description = "Fixed Deposit Calculator(rates Link ) - Pre Login")
    public static void FDcalculatorratelink() throws Exception {
        PreQuickAccessMenuList.calculator();
        PreMainCalculatorScreen.fdcalculator();
        PreFDcalculatorScreen.interatestrateslink();

    }

    @Test(priority = 17, description = "Fixed Deposit Calculator(Checking Deposit Amount and back button message) - Pre Login")
    public static void FDcalculatordepositamountvalidation() throws Exception {
        PreQuickAccessMenuList.calculator();
        PreMainCalculatorScreen.fdcalculator();
        PreFDcalculatorScreen.enterdepositeamount("50");
        PreFDcalculatorScreen.calculate();
        PopUpMessages.popupmessage_header("Invalid Deposit Amount");
        PopUpMessages.popupmessage_content("Deposit amount should be higher than LKR 50,000.00.");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
        PreFDcalculatorScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        PreFDcalculatorScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PreMainCalculatorScreen.PreMainCalculatorScreen_title();


    }
    @Test(priority = 18, description = "Leasing Calculator - Pre Login")
    public static void leasingcalculator() throws Exception {
        PreQuickAccessMenuList.calculator();
        PreMainCalculatorScreen.leasingcalculator();
        PreLeasingCalculatorScreen.PreLeasingCalculatorScreen_title();
        PreLeasingCalculatorScreen.PreLeasingCalculatorScreen_labels();
        PreLeasingCalculatorScreen.BrandNew();
        PreLeasingCalculatorScreen.CarSUV();
        PreLeasingCalculatorScreen.enteradvance("200");
        PreLeasingCalculatorScreen.enterleaseamount("6000");
        PreLeasingCalculatorScreen.selectduration();
        PreLeasingCalculatorScreen.calculate();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.MonthlyInstallment_label();
        PostLeasingCalculatorScreen.arrow();
        PostLeasingCalculatorScreen.yes();
        PreSubmitDetailsScreen.PreSubmitDetailsScreen_title();
        PreSubmitDetailsScreen.PreSubmitDetailsScreen_labels();
        PreSubmitDetailsScreen.entername("fjhff");
        PreSubmitDetailsScreen.enteremail("gdggd@gmail.com");
        PreSubmitDetailsScreen.enterphonenumber("0715544663");
        PreSubmitDetailsScreen.submit();
        PopUpMessages.popupmessage_header("Details Submitted");
        PopUpMessages.popupmessage_content("Your leasing quotation request has been sent to the bank successfully, we will contact you for further details.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PreMainCalculatorScreen.PreMainCalculatorScreen_title();
    }


    @Test(priority = 19, description = "Leasing Calculator(validaion and negative scenarios ) - Pre Login")
    public static void leasingcalculatorvalidation() throws Exception {
        PreQuickAccessMenuList.calculator();
        PreMainCalculatorScreen.leasingcalculator();
        PreLeasingCalculatorScreen.PreLeasingCalculatorScreen_title();
        PreLeasingCalculatorScreen.PreLeasingCalculatorScreen_labels();
        PreLeasingCalculatorScreen.BrandNew();
        PreLeasingCalculatorScreen.CarSUV();
        PreLeasingCalculatorScreen.enteradvance("200");
        PreLeasingCalculatorScreen.enterleaseamount("6000");
        PreLeasingCalculatorScreen.selectduration();
        PreLeasingCalculatorScreen.calculate();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.MonthlyInstallment_label();
        PostLeasingCalculatorScreen.arrow();
        PostLeasingCalculatorScreen.yes();
        PreSubmitDetailsScreen.PreSubmitDetailsScreen_title();
        PreSubmitDetailsScreen.PreSubmitDetailsScreen_labels();
        PreSubmitDetailsScreen.entername("fjhff");
        PreSubmitDetailsScreen.enteremail("gdggd");
        PreSubmitDetailsScreen.enterphonenumber("0715544663");
        PreSubmitDetailsScreen.submit();
        PopUpMessages.popupmessage_header("Invalid Email");
        PopUpMessages.popupmessage_content("Invalid email");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PreSubmitDetailsScreen.clearemail();
        PreSubmitDetailsScreen.enteremail("hahha@gmail.com");
        PreSubmitDetailsScreen.clearphonenumber();
        PreSubmitDetailsScreen.enterphonenumber("3132");
        PreSubmitDetailsScreen.submit();
        PopUpMessages.popupmessage_header("Invalid Phone Number");
        PopUpMessages.popupmessage_content("Phone number should be more than 10 digits");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        PreSubmitDetailsScreen.back();
        PreLeasingCalculatorScreen.back();
        PopUpMessages.popupmessage_header("Confirm");
        PopUpMessages.popupmessage_content("Are you sure you want to leave the page?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        PreLeasingCalculatorScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PreMainCalculatorScreen.PreMainCalculatorScreen_title();


    }

    @Test(priority = 20, description = "Leasing Calculator(No more ) - Pre Login")
    public static void leasingcalculatornomore() throws Exception {
        PreQuickAccessMenuList.calculator();
        PreMainCalculatorScreen.leasingcalculator();
        PreLeasingCalculatorScreen.PreLeasingCalculatorScreen_title();
        PreLeasingCalculatorScreen.PreLeasingCalculatorScreen_labels();
        PreLeasingCalculatorScreen.BrandNew();
        PreLeasingCalculatorScreen.CarSUV();
        PreLeasingCalculatorScreen.enteradvance("200");
        PreLeasingCalculatorScreen.enterleaseamount("6000");
        PreLeasingCalculatorScreen.selectduration();
        PreLeasingCalculatorScreen.calculate();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PostLeasingCalculatorScreen.MonthlyInstallment_label();
        PostLeasingCalculatorScreen.arrow();
        PostLeasingCalculatorScreen.no();
        PreMainCalculatorScreen.PreMainCalculatorScreen_title();

    }
    @Test(priority = 21, description = "Terms and Conditions - Pre Login")
    public static void Termsandconditions() throws Exception {
        PreQuickAccessMenuList.tandc();
        PreTandCSCreen.TandCscreen_labels_Labels();
        PreTandCSCreen.quickaccessmenu();
        PreQuickAccessMenuList.PreQuickAccessMenuList_labels();
        PreQuickAccessMenuList.cross();
        PreTandCSCreen.backbutton();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
    }

    @Test(priority = 22, description = "Location View  -  Branch")
    public static void locationViewBranch() throws Exception {
        PreQuickAccessMenuList.locationview();
        PreLocationViewScreen.PreLocationViewScreen_title();
        PreLocationViewScreen.PreLocationViewScreen_labels();
        PreLocationViewScreen.searchbranch("Akuressa");
    }

    @Test(priority = 23, description = "Location View  - NoResults")
    public static void locationViewNoResults() throws Exception {
        PreQuickAccessMenuList.locationview();
        PreLocationViewScreen.PreLocationViewScreen_title();
        PreLocationViewScreen.PreLocationViewScreen_labels();
        PreLocationViewScreen.searchbranch("hdsahdashds");
        PopUpMessages.popupmessage_header("No Results");
        PopUpMessages.popupmessage_content("No search results found");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");
    }

    @Test(priority = 24, description = "Location View  - ATM")
    public static void locationViewATM() throws Exception {
        PreQuickAccessMenuList.locationview();
        PreLocationViewScreen.PreLocationViewScreen_title();
        PreLocationViewScreen.PreLocationViewScreen_labels();
        PreLocationViewScreen.selfservice();
        PreLocationViewScreen.searchATM("Akuressa");
    }


    @Test(priority = 25, description = "Location View  - Branch List ")
    public static void locationBranchList() throws Exception {
        PreQuickAccessMenuList.locationview();
        PreLocationViewScreen.PreLocationViewScreen_title();
        PreLocationViewScreen.PreLocationViewScreen_labels();
        PreLocationViewScreen.menuicon();
        PreLocationViewScreen.branchlist();
        PreBranchLocationViewDetailsScreen.PreBranchLocationViewDetailsScreen_title();
        PreBranchLocationViewDetailsScreen.PreBranchLocationViewDetailsScreen_labels();
        PreBranchLocationViewDetailsScreen.btnGo();
    }


    @Test(priority = 26, description = "Location View  - ATM List ")
    public static void locationATMList() throws Exception {
        PreQuickAccessMenuList.locationview();
        PreLocationViewScreen.PreLocationViewScreen_title();
        PreLocationViewScreen.PreLocationViewScreen_labels();
        PreLocationViewScreen.menuicon();
        PreLocationViewScreen.selfservice();
        PreLocationViewScreen.branchlist();
        PreAtmLocationViewDetailsScreen.PreAtmLocationViewDetailsScreen_title();
        PreAtmLocationViewDetailsScreen.PreAtmLocationViewDetailsScreen_labels();
        PreAtmLocationViewDetailsScreen.btnGo();
    }


    @Test(priority = 27, description = "Location View  - Loaction details back button ")
    public static void locationDetailsbackbutton() throws Exception {
        PreQuickAccessMenuList.locationview();
        PreLocationViewScreen.menuicon();
        PreLocationViewScreen.branchlist();
        PreBranchLocationViewDetailsScreen.back();
        PreLocationViewScreen.PreLocationViewScreen_title();
        PreLocationViewScreen.selfservice();
        PreLocationViewScreen.branchlist();
        PreAtmLocationViewDetailsScreen.back();
        PreLocationViewScreen.PreLocationViewScreen_title();

    }




}
