package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.BillerMaintenance.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.AddBillerPayee;
import TestData.EditPayeeBiller;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class BillerMaintenanceTest extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "Add Biller")
    public static void addbiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.AddBillerScreen_title();
        AddBillerScreen.AddBillerScreen_title();
        AddBillerScreen.selectcategory();
        AddBillerScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerScreen.addnickname(addBillerPayee.getBillername());
        AddBillerScreen.addaccountnumber(addBillerPayee.getBilleraccnumber());
        AddBillerScreen.Add();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Biller Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("New Biller Added","You have successfully added a new biller "+addBillerPayee.getBillername()+" Ceb "+currentDateTime.gettimeanddate()+" SLST.");


    }

    @Test(priority = 2, description = "Add Biller Account")
    public static void addbilleraccount() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.plusicon();
        AddBillerAccountScreen.AddBillerAccountScreen_title();
        AddBillerAccountScreen.AddBillerAccountScreen_labels();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerAccountScreen.addnickname(addBillerPayee.getBilleraccountname());
        AddBillerAccountScreen.addaccountnumber(addBillerPayee.getBilleraccaccnumber());
        AddBillerAccountScreen.Add();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Biller Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("New Biller Added","You have successfully added a new biller "+addBillerPayee.getBilleraccountname()+" Ceb "+currentDateTime.gettimeanddate()+" SLST.");


    }
    @Test(priority = 3, description = "Edit Biller")
    public static void editbiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditBillerScreen.EditBillerScreen_title();
        EditBillerScreen.EditBilerScreen_labels();
        EditPayeeBiller editPayeeBiller = new EditPayeeBiller();
        EditBillerScreen.editbillernickname(editPayeeBiller.getEditbillername());
        EditBillerScreen.editaccountnumber(editPayeeBiller.getEditbilleraccnumber());
        EditBillerScreen.save();
        ConfirmBillerDetailsScreen.ConfirmBillerDetails_labels();
        ConfirmBillerDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Biller Saved");
        PopUpMessages.popupmessage_content("Changes to Biller saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 4, description = "Delete biller account")
    public static void deletebilleraccount() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        DeleteBillerScreen.selectbilleraccount();
        DeleteBillerScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Accounts?");
        PopUpMessages.popupmessage_content("Are you sure you want to Delete?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Biller(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");

    }

    @Test(priority = 5, description = "Delete biller ")
    public static void deletebiller() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeleteBillerScreen.selectbiller();
        DeleteBillerScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Billers?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Biller(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Biller(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        BillerMaintenance.BillerMaintenance_title();
    }

    @Test(priority = 6, description = "Add biller - Negative flow 01 ")
    public static void addbillernegativeflow01() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.selectcategory();
        AddBillerScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerScreen.addnickname(addBillerPayee.getBillername());
        AddBillerScreen.back();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this Biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AddBillerScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this Biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AddBillerScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 7, description = "Add biller - Negative flow 02 ")
    public static void addbillernegativeflow02() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.selectcategory();
        AddBillerScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerScreen.addnickname(addBillerPayee.getBillername());
        AddBillerScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this Biller?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillerMaintenance.BillerMaintenance_title();
    }

    @Test(priority = 8, description = "Add biller - Negative flow 03 ")
    public static void addbillernegativeflow03() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.cancelbutton();
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.back();
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 10, description = "Add biller - Negative flow 04 ")
    public static void addbillernegativeflow04() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.selectcategory();
        AddBillerScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerScreen.addnickname(addBillerPayee.getBillername());
        AddBillerScreen.addaccountnumber(addBillerPayee.getBilleraccnumber());
        AddBillerScreen.Add();
        ConfirmBillerDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        AddBillerScreen.AddBillerScreen_title();
        AddBillerScreen.Add();
        ConfirmBillerDetailsScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        AddBillerScreen.AddBillerScreen_title();

    }

    @Test(priority = 11, description = "Add biller - Negative flow 05 ")
    public static void addbillernegativeflow05() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.selectcategory();
        AddBillerScreen.selectbiller();
        AddBillerPayee addBillerPayee =new AddBillerPayee();
        AddBillerScreen.addnickname(addBillerPayee.getBillername());
        AddBillerScreen.addaccountnumber(addBillerPayee.getBilleraccnumber());
        AddBillerScreen.Add();
        ConfirmBillerDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this biller?");
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        BillerMaintenance.BillerMaintenance_title();
        BillerMaintenance.plusicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddBillerScreen.selectcategory();
        AddBillerScreen.selectbiller();
        AddBillerScreen.addnickname(addBillerPayee.getBillername());
        AddBillerScreen.addaccountnumber(addBillerPayee.getBilleraccnumber());
        AddBillerScreen.Add();
        ConfirmBillerDetailsScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Biller");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this biller?");
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        BillerMaintenance.BillerMaintenance_title();
    }

    @Test(priority = 12, description = "Edit Biller - Negative flow 01")
    public static void editbillernegativeflow1() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditPayeeBiller editPayeeBiller = new EditPayeeBiller();
        EditBillerScreen.editbillernickname(editPayeeBiller.getEditbillername());
        EditBillerScreen.back();
        PopUpMessages.popupmessage_header("Discard Changes");
        PopUpMessages.popupmessage_content("Are you sure you want to discard the changes made to the biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        EditBillerScreen.cancel();
        PopUpMessages.popupmessage_header("Discard Changes");
        PopUpMessages.popupmessage_content("Are you sure you want to discard the changes made to the biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        EditBillerScreen.cancel();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillerDetailsScreen.BillerDetailsScreen_title();
        BillerDetailsScreen.editicon();
        EditBillerScreen.editbillernickname(editPayeeBiller.getEditbillername());
        EditBillerScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        BillerDetailsScreen.BillerDetailsScreen_title();

    }

    @Test(priority = 13, description = "Edit Biller - Negative flow 02")
    public static void editbillernegativeflow2() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditBillerScreen.back();
        BillerDetailsScreen.BillerDetailsScreen_title();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditBillerScreen.cancel();
        BillerDetailsScreen.BillerDetailsScreen_title();
        BillerDetailsScreen.back();
        BillerMaintenance.BillerMaintenance_title();

    }

    @Test(priority = 14, description = "Edit Biller - Negative flow 03")
    public static void editbillernegativeflow3() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditPayeeBiller editPayeeBiller =new EditPayeeBiller();
        EditBillerScreen.editbillernickname(editPayeeBiller.getEditbillername());
        EditBillerScreen.editaccountnumber(editPayeeBiller.getEditbilleraccnumber());
        EditBillerScreen.save();
        ConfirmBillerDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Changes");
        PopUpMessages.popupmessage_content("Are you sure you want to discard the changes made to the biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        ConfirmBillerDetailsScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Changes");
        PopUpMessages.popupmessage_content("Are you sure you want to discard the changes made to the biller?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        ConfirmBillerDetailsScreen.cancelbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        EditBillerScreen.EditBillerScreen_title();
        EditBillerScreen.save();
        ConfirmBillerDetailsScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        EditBillerScreen.EditBillerScreen_title();

    }

    @Test(priority = 15, description = "Edit Biller - Negative flow 04")
    public static void editbillernegativeflow4() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditBillerScreen.back();
        BillerDetailsScreen.BillerDetailsScreen_title();
        BillerDetailsScreen.editicon();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        EditBillerScreen.cancel();
        BillerDetailsScreen.BillerDetailsScreen_title();


    }

    @Test(priority = 16, description = "Biller Maintenance - Negative flow 05")
    public static void billermaintenancenegativeflow4() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        BillerMaintenance.back();
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 17, description = "Delete biller account - Negative Flow 06")
    public static void deletebilleraccountnegativeflow06() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        BillerMaintenance.scrolltobiller();
        DeleteBillerScreen.selectbilleraccount();
        DeleteBillerScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Accounts?");
        PopUpMessages.popupmessage_content("Are you sure you want to Delete?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");


    }

    @Test(priority = 18, description = "Delete biller  - Negative Flow 07")
    public static void deletebillernegativeflow07() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.billermaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeleteBillerScreen.selectbiller();
        DeleteBillerScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Billers?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Biller(s)?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");

    }
}