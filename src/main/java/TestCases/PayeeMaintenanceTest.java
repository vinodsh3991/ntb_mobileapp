package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.NavigationDrawer.Navigationdrawer;
import Activities.PayeeMaintenance.*;
import Activities.PopUpMessages.PopUpMessages;
import Activities.OTPScreens.LoginOTPScreen;
import TestData.AddBillerPayee;
import TestData.EditPayeeBiller;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PayeeMaintenanceTest extends Configuration {

    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
       // driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);



            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }


    @Test(priority = 1, description = "Add Ceft Payee")
    public static void addceftpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.plusicon();
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.AddPayeeScreen_labels();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getCeftpayeename());
        AddPayeeScreen.selectceftbrank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getCeftpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("New Payee Added","You have successfully added a new payee "+addBillerPayee.getCeftpayeenickname()+ " AMANA BANK LTD "+currentDateTime.gettimeanddate()+" SLST.");


    }
    @Test(priority = 2, description = "Add Slip Payee")
    public static void addslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.plusicon();
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.AddPayeeScreen_labels();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getSlippayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getSlippayeename());
        AddPayeeScreen.selectslipbank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getSlippayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsslip_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("New Payee Added","You have successfully added a new payee "+addBillerPayee.getSlippayeenickname()+" ALLIANCE FINANCE COMPANY PLC "+currentDateTime.gettimeanddate()+" SLST.");


    }

    @Test(priority = 3, description = "Add NTB Payee")
    public static void addntbpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.plusicon();
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.AddPayeeScreen_labels();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getNtbpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getNtbpayeename());
        AddPayeeScreen.selectntbbank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getNtbpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Payee Saved Successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();
        CurrentDateTime currentDateTime =new CurrentDateTime();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        OpenNotification.opennotifications("New Payee Added","You have successfully added a new payee "+addBillerPayee.getNtbpayeenickname()+" Nations Trust Bank "+currentDateTime.gettimeanddate()+" SLST.");

    }

    @Test(priority = 4, description = "Edit Ceft Payee")
    public static void editceftpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.Cefttoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getCefteditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getCefteditpayeename());
        EditPayeeScreen.selectslipbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getCefteditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsslip_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();

    }
    @Test(priority = 5, description = "Edit Slip Payee")
    public static void editslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.Sliptoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getSlipeditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getSlipeditpayeename());
        EditPayeeScreen.selectceftbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getSlipeditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 6, description = "Edit NTB Payee")
    public static void editntbpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.ntbtoscroll();
        PayeeDetailsScreen.thirdpartyntbPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeBiller editPayeeBiller=new EditPayeeBiller();
        EditPayeeScreen.editpayeenickname(editPayeeBiller.getNtbeditpayeenickname());
        EditPayeeScreen.editpayeename(editPayeeBiller.getNtbeditpayeename());
        EditPayeeScreen.selectceftbank();
        EditPayeeScreen.editaccountnumber(editPayeeBiller.getNtbeditpayeeaccountnmber());
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.confirmbutton();
        PopUpMessages.popupmessage_header("Payee Saved");
        PopUpMessages.popupmessage_content("Changes to payee saved successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();

    }
    @Test(priority = 7, description = "Delete Ceft Payee")
    public static void deleteceftpayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.Cefttoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();


    }
    @Test(priority = 8, description = "Delete Slip Payee")
    public static void deleteslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.Sliptoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();


    }
    @Test(priority = 9, description = "Delete third party NTB  Payee")
    public static void thirdpartyntbslippayee() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DeletePayeeScreen.ntbtoscroll();
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Payees?");
        PopUpMessages.popupmessage_content("Are you sure you want to delete selected Payee(s)?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        DeletePayeeScreen.deleteicon();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Selected Payee(s) were deleted successfully");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        Activities.PayeeMaintenance.PayeeMaintenance.PayeeMaintenance_title();
    }

    @Test(priority = 10, description = "Add Payee screen - negative flow 01")
    public static void addpayeenegativeflow1() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.plusicon();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getSlippayeenickname());
        AddPayeeScreen.back();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AddPayeeScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PayeeMaintenance.PayeeMaintenance_title();
        PayeeMaintenance.plusicon();
        AddPayeeScreen.addnickname(addBillerPayee.getSlippayeenickname());
        AddPayeeScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        AddPayeeScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PayeeMaintenance.PayeeMaintenance_title();

    }


    @Test(priority = 11, description = "Add Payee screen - negative flow 03")
    public static void addpayeenegativeflow2() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.plusicon();
        AddPayeeScreen.cancelbutton();
        PayeeMaintenance.PayeeMaintenance_title();
        PayeeMaintenance.plusicon();
        AddPayeeScreen.back();
        PayeeMaintenance.PayeeMaintenance_title();
    }


    @Test(priority = 12, description = "Confirm Payee Screen - Negative flow 04")
    public static void addpayeenegativeflow3() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.plusicon();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getCeftpayeename());
        AddPayeeScreen.selectceftbrank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getCeftpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("Edit");
        AddPayeeScreen.AddPayeeScreen_title();
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.cancelbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }


    @Test(priority = 13, description = "Confirm Payee Screen - Negative flow 05")
    public static void addpayeenegativeflow4() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.plusicon();
        AddBillerPayee addBillerPayee = new AddBillerPayee();
        AddPayeeScreen.addnickname(addBillerPayee.getCeftpayeenickname());
        AddPayeeScreen.addpayeename(addBillerPayee.getCeftpayeename());
        AddPayeeScreen.selectceftbrank();
        AddPayeeScreen.addaccountnumber(addBillerPayee.getCeftpayeeaccountnmber());
        AddPayeeScreen.addbutton();
        ConfirmPayeeDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Add Payee");
        PopUpMessages.popupmessage_content("Are you sure you want to discard this payee?");
        PopUpMessages.popupsuccessmessage_negativebutton("OK");
        PayeeMaintenance.PayeeMaintenance_title();

    }

    @Test(priority = 14, description = "Payee Maintenance - Negative flow 05")
    public static void payeemaintenancenegativeflow4() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PayeeMaintenance.back();
        HomeScreen.HomeScreen_labels();

    }

    @Test(priority = 15, description = "Edit Slip Payee -  Negative flow 06")
    public static void editslippayeenegativeflow06() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.Sliptoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeScreen.selectceftbank();
        EditPayeeScreen.cancel();
        PopUpMessages.popupmessage_header("Discard Changes");
        PopUpMessages.popupmessage_content("Are you sure you want to discard the changes to the payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        EditPayeeScreen.cancel();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        PayeeDetailsScreen.PayeeDetailsScreen_title();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();

    }

    @Test(priority = 16, description = "Edit Slip Payee -  Negative flow 07")
    public static void editslippayeenegativeflow07() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.Sliptoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeScreen.selectceftbank();
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetails_title();
        ConfirmPayeeDetailsScreen.ConfirmPayeeDetailsceft_labels();
        ConfirmPayeeDetailsScreen.cancelbutton();
        PopUpMessages.popupmessage_header("Discard Changes");
        PopUpMessages.popupmessage_content("Are you sure you want to discard the changes to the payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        ConfirmPayeeDetailsScreen.cancelbutton();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        EditPayeeScreen.ceftEditPayeeScreen_labels();
        EditPayeeScreen.save();
        ConfirmPayeeDetailsScreen.back();
        PopUpMessages.popupmessage_header("Discard Changes");
        PopUpMessages.popupmessage_content("Are you sure you want to discard the changes to the payee?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        ConfirmPayeeDetailsScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        EditPayeeScreen.ceftEditPayeeScreen_labels();

    }

    @Test(priority = 17, description = "Edit Slip Payee -  Negative flow 08")
    public static void editslippayeenegativeflow08() throws Exception {
        HomeScreen.navigationdrawericon();
        Navigationdrawer.payeemaintenance();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Activities.PayeeMaintenance.PayeeMaintenance.Sliptoscroll();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeScreen.slipEditPayeeScreen_labels();
        EditPayeeScreen.cancel();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();
        PayeeDetailsScreen.editicon();
        EditPayeeScreen.slipEditPayeeScreen_labels();
        EditPayeeScreen.back();
        PayeeDetailsScreen.SlipceftPayeeSetailsScreen_labels();

    }

}


