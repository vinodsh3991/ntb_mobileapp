package TestCases;

import Activities.LoginScreens.FristTimeLoginScreen;
import TestBase.Configuration;
import TestData.LoginDetails;
import Activities.*;
import Activities.Inbox.*;
import Activities.LoginDetails.LoginPassword;
import Activities.LoginDetails.LoginUsername;
import Activities.PopUpMessages.PopUpMessages;
import Activities.PostQuickAccessMenuList.PostQuickAccessMenuList;
import Activities.OTPScreens.LoginOTPScreen;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Inbox extends Configuration {
    @BeforeMethod
    public static void login() throws Exception {
        Common.clearData();
        LanguageSelection.english_button();
        FristTimeLoginScreen.devicelocationpopup();
        FristTimeLoginScreen.FristTimeLoginScreen_labels();
        LoginDetails loginDetails = new LoginDetails();
        LoginUsername.valid_username(loginDetails.getUsername());
        LoginPassword.valid_password(loginDetails.getPassword());
        driver.hideKeyboard();
        FristTimeLoginScreen.loginbutton();
        try{
            if(driver.findElementById("com.nationstrust.mobilebanking:id/otpText").isDisplayed()){
                LoginOTPScreen.enetrOTP("456325896");
                driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

            } }catch (NoSuchElementException e){

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

    }

    @Test(priority = 1, description = "Compose a mail - inbox screen")
    public static void composemailinbox() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        ComposeScreen.messagebox("First message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 2, description = "Compose a mail with attachment - inbox screen")
    public static void composemailattachmentinbox() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        Get_attachment.attachimage();
        ComposeScreen.messagebox("First message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 3, description = "Reply for a mail - inbox screen")
    public static void replyforemailinbox() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.MailThreadScreen_title();
        InboxMailThreadScreen.MailThreadScreen_labels();
        InboxMailThreadScreen.taponmessage();
        InboxMessageDetailsScreen.MessageDetailsScreen_title();
        InboxMessageDetailsScreen.MessageDetailsScreen_labels();
        InboxMessageDetailsScreen.replybutton();
        ReplyMailScreen.ReplyMailScreen_title();
        ReplyMailScreen.ReplyMailScreen_labels();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.morebutton();
        ReplyMailScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 4, description = "Reply for a mail with attchment - inbox screen")
    public static void replyforemailinboxattachment() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.MailThreadScreen_title();
        InboxMailThreadScreen.MailThreadScreen_labels();
        InboxMailThreadScreen.taponmessage();
        InboxMessageDetailsScreen.MessageDetailsScreen_title();
        InboxMessageDetailsScreen.MessageDetailsScreen_labels();
        InboxMessageDetailsScreen.replybutton();
        ReplyMailScreen.ReplyMailScreen_title();
        ReplyMailScreen.ReplyMailScreen_labels();
        Get_attachment.attachimage();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.morebutton();
        ReplyMailScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }

    @Test(priority = 5, description = "Delete a mail thread - inbox screen")
    public static void inboxdeletemailthread() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.selectmail();
        InboxMainScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }

    @Test(priority = 6, description = "Delete a message - inbox screen")
    public static void inboxdeletemessage() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.MailThreadScreen_title();
        InboxMailThreadScreen.MailThreadScreen_labels();
        InboxMailThreadScreen.taponmessage();
        InboxMessageDetailsScreen.MessageDetailsScreen_title();
        InboxMessageDetailsScreen.MessageDetailsScreen_labels();
        InboxMessageDetailsScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 7, description = "Compose a mail - Sent screen")
    public static void composemailsent() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        ComposeScreen.messagebox("First message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 8, description = "Compose a mail with attachment - Sent screen")
    public static void composemailsentattachment() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        Get_attachment.attachimage();
        ComposeScreen.messagebox("First message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 9, description = "Reply for a mail - sent screen")
    public static void replyforemailsent() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.taponmail();
        SentMailThreadScreen.SentMailThreadScreen_title();
        SentMailThreadScreen.SentMailThreadScreen_labels();
        SentMailThreadScreen.taponmessage();
        SentMessageDetailsScreen.SentMessageDetailsScreen_title();
        SentMessageDetailsScreen.SentMessageDetailsScreen_labels();
        SentMessageDetailsScreen.replybutton();
        ReplyMailScreen.ReplyMailScreen_title();
        ReplyMailScreen.ReplyMailScreen_labels();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.morebutton();
        ReplyMailScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 10, description = "Reply for a mail with attchment - sent screen")
    public static void replyforemailsentattachment() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.taponmail();
        SentMailThreadScreen.SentMailThreadScreen_title();
        SentMailThreadScreen.SentMailThreadScreen_labels();
        SentMailThreadScreen.taponmessage();
        SentMessageDetailsScreen.SentMessageDetailsScreen_title();
        SentMessageDetailsScreen.SentMessageDetailsScreen_labels();
        SentMessageDetailsScreen.replybutton();
        ReplyMailScreen.ReplyMailScreen_title();
        ReplyMailScreen.ReplyMailScreen_labels();
        Get_attachment.attachimage();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.morebutton();
        ReplyMailScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Success");
        PopUpMessages.popupmessage_content("Email has been sent successfully.");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }
    @Test(priority = 11, description = "Delete a mail thread - Sent Screen")
    public static void sentdeletemailthread() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.selectmail();
        SentMainScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }

    @Test(priority = 12, description = "Delete a message - Sent Screen")
    public static void sentdeletemessage() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.senttab();
        SentMainScreen.taponmail();
        SentMailThreadScreen.SentMailThreadScreen_title();
        SentMailThreadScreen.SentMailThreadScreen_labels();
        SentMailThreadScreen.taponmessage();
        SentMessageDetailsScreen.SentMessageDetailsScreen_title();
        SentMessageDetailsScreen.SentMessageDetailsScreen_labels();
        SentMessageDetailsScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
    }


    @Test(priority = 13, description = "Negative Flow 01")
    public static void negativeflow1() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.composebutton();
        ComposeScreen.selectsubject();
        ComposeScreen.back();
        PopUpMessages.popupmessage_header("Cancel Message");
        PopUpMessages.popupmessage_content("Are you sure you want to cancel the message?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        ComposeScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
    }

    @Test(priority = 14, description = "Negative Flow 02")
    public static void negativeflow2() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.composebutton();
        ComposeScreen.selectsubject();
        Get_attachment.attachimage();
        Get_attachment.attachimage();
        Get_attachment.attachimage();
        Get_attachment.attachimage();
        Get_attachment.attachimage();
        Get_attachment.attachimage();
        PopUpMessages.popupmessage_header("Attachment Limit");
        PopUpMessages.popupmessage_content("Maximum allowed attachment count reached");
        PopUpMessages.popupsuccessmessage_okbuttons("OK");
    }


    @Test(priority = 15, description = "Negative Flow 03")
    public static void negativeflow3() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.searchbar("gasjhafags");
        PopUpMessages.popupmessage_header("No Search Results");
        PopUpMessages.popupmessage_content("No results found");
        PopUpMessages.popupsuccessmessage_positivebutton("OK");

    }

    @Test(priority = 16, description = "Negative Flow 04")
    public static void negativeflow4() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.selectmail();
        InboxMainScreen.selectallicon();
        InboxMainScreen.deleteicon();
        PopUpMessages.popupmessage_header("Delete Selected Message(s)?");
        PopUpMessages.popupmessage_content("Are you sure you want to permanently delete the email(s)?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        InboxMainScreen.deleteicon();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PopUpMessages.popupmessage_header("Messages Deleted");
        PopUpMessages.popupmessage_content("Selected messages have been deleted");
        PopUpMessages.popupsuccessmessage_buttons("OK");
        InboxMainScreen.inboxemptyscreen();
    }



    @Test(priority = 17, description = "Compose a mail - inbox screen")
    public static void searchmail() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.InboxMainScreen_title();
        InboxMainScreen.InboxMainScreen_labels();
        InboxMainScreen.composebutton();
        ComposeScreen.ComposeScreen_title();
        ComposeScreen.ComposeScreen_labels();
        ComposeScreen.selectsubject();
        ComposeScreen.messagebox("Search message");
        ComposeScreen.sendbutton();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PopUpMessages.popupsuccessmessage_buttons("OK");
        InboxMainScreen.searchbar("Fund transfer");
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.MailThreadScreen_title();
        InboxMailThreadScreen.MailThreadScreen_labels();

    }

    @Test(priority = 18, description = "Negative Flow 05")
    public static void negativeflow5() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.taponmessage();
        InboxMessageDetailsScreen.replybutton();
        ReplyMailScreen.messagebox("Reply for a email");
        ReplyMailScreen.back();
        PopUpMessages.popupmessage_header("Cancel Message");
        PopUpMessages.popupmessage_content("Are you sure you want to cancel the message?");
        PopUpMessages.popupsuccessmessage_positivebutton("No");
        ReplyMailScreen.back();
        PopUpMessages.popupsuccessmessage_negativebutton("Yes");
        InboxMessageDetailsScreen.MessageDetailsScreen_title();
        InboxMessageDetailsScreen.MessageDetailsScreen_labels();
    }

    @Test(priority = 19, description = "Negative Flow 06")
    public static void negativeflow6() throws Exception {
        HomeScreen.quickaccessmenuicon();
        PostQuickAccessMenuList.inbox();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        InboxMainScreen.taponmail();
        InboxMailThreadScreen.taponmessage();
        InboxMessageDetailsScreen.back();
        InboxMailThreadScreen.back();
        InboxMainScreen.back();
        HomeScreen.HomeScreen_labels();
    }


}
